import { Injectable } from '@angular/core';
import { ToastrManagerService } from './toastr.manager.service';
import { EnToastrTitle } from '../enums/toastr.enum';
import { IToastrConfig } from '../interfaces/IToastrConfig';

@Injectable({
  providedIn: 'root',
})
export class ToastrService {
  constructor(private toastrManager: ToastrManagerService) {}

  public success(
    message: string,
    title: string = EnToastrTitle.SUCCESS,
    customConfig?: IToastrConfig,
  ): void {
    this.toastrManager.insertToastr(message, title, 'success', customConfig);
  }

  public info(
    message: string,
    title: string = EnToastrTitle.INFO,
    customConfig?: IToastrConfig,
  ): void {
    this.toastrManager.insertToastr(message, title, 'info', customConfig);
  }

  public warning(
    message: string,
    title: string = EnToastrTitle.WARNING,
    customConfig?: IToastrConfig,
  ): void {
    this.toastrManager.insertToastr(message, title, 'warning', customConfig);
  }

  public danger(
    message: string,
    title: string = EnToastrTitle.DANGER,
    customConfig?: IToastrConfig,
  ): void {
    this.toastrManager.insertToastr(message, title, 'danger', customConfig);
  }
}
