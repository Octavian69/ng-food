import { Inject, Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { isEqual } from '@handlers/conditions.handlers';
import { assign, removeItem } from '@handlers/utils.handlers';
import { BehaviorSubject, Observable } from 'rxjs';
import { Toastr } from '../classes/Toastr';
import { ToastrDB } from '../db/toastr.db';
import { TOASTR_ROOT_CONFIG } from '../injection/toastr.tokens';
import { IToastrConfig } from '../interfaces/IToastrConfig';
import { ToastrRootMapConfig, ToastrStatus } from '../types/toastr.types';

@Injectable({
  providedIn: 'root',
})
export class ToastrManagerService extends ExtendsFactory(
  State({ db: ToastrDB }),
) {
  constructor(
    @Inject(TOASTR_ROOT_CONFIG) private rootConfig: ToastrRootMapConfig,
  ) {
    super();
  }

  private toastrs$: BehaviorSubject<Toastr[]> = new BehaviorSubject<Toastr[]>(
    [],
  );

  public insertToastr(
    message: string,
    title: string,
    status: ToastrStatus,
    customConfig: IToastrConfig,
  ): void {
    const rootConfig: IToastrConfig = this.rootConfig.get(status);
    const callConfig: IToastrConfig = assign(rootConfig, customConfig);
    const toastr: Toastr = new Toastr(status, title, message, callConfig);
    const { insertTo } = toastr.config;

    const toastrs: Toastr[] = this.get();
    const insertMethod = isEqual(insertTo, 'begin') ? 'unshift' : 'push';

    toastrs[insertMethod](toastr);

    this.next(toastrs);
  }

  public removeToastr(toastr: Toastr): void {
    const toastrs: Toastr[] = removeItem(this.get(), toastr, '_id');

    this.next(toastrs);
  }

  public get(): Toastr[] {
    return this.toastrs$.getValue();
  }

  public next(toastrs: Toastr[]): void {
    this.toastrs$.next(toastrs);
  }

  public clear(): void {
    this.next([]);
  }

  public listen(): Observable<Toastr[]> {
    return this.toastrs$.asObservable();
  }
}
