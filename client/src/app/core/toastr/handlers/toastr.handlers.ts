import { getDataFromDB } from '@handlers/structutral.handlers';
import { assign } from '@handlers/utils.handlers';
import { ToastrDB } from '../db/toastr.db';
import { IToastrConfig } from '../interfaces/IToastrConfig';
import {
  ToastrRootConfig,
  ToastrRootMapConfig,
  ToastrStatus,
} from '../types/toastr.types';

export function buildToastrRootConfiguration(
  customConfiguration: ToastrRootConfig,
): ToastrRootMapConfig {
  const defaultConfiguration: ToastrRootConfig = getDataFromDB(
    ['configurations'],
    ToastrDB,
  );

  const rootConfiguration: ToastrRootMapConfig = Object.keys(
    defaultConfiguration,
  ).reduce((accum: ToastrRootMapConfig, statusKey: ToastrStatus) => {
    const defaultConfig: IToastrConfig = defaultConfiguration[statusKey];
    const customConfig: IToastrConfig = customConfiguration[statusKey];
    const statusConfig: IToastrConfig = assign(defaultConfig, customConfig);

    accum.set(statusKey, statusConfig);

    return accum;
  }, new Map());

  return rootConfiguration;
}
