import { IToastrConfig } from '../interfaces/IToastrConfig';

export const ToastrStatuses = ['success', 'info', 'warning', 'danger'] as const;
export type ToastrStatus = typeof ToastrStatuses[number];
export type ToastrRootConfig = {
  [K in typeof ToastrStatuses[number]]?: IToastrConfig;
};
export type ToastrRootMapConfig = Map<ToastrStatus, IToastrConfig>;
