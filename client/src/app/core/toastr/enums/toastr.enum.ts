export enum EnToastrConfig {
  DURATION = 3000,
}

export enum EnToastrTitle {
  DANGER = 'Внимание!',
  WARNING = 'Внимание!',
  INFO = 'Информация',
  SUCCESS = 'Успешно',
}
