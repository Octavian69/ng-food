export const ToastrDB = {
  configurations: {
    success: {
      background: 'linear-gradient(to right, rgb(82, 194, 52), rgb(6, 23, 0))',
      icon: 'check_circle',
      destroyByClick: true,
      undestroyByMouseEnter: true,
    },
    info: {
      background: 'linear-gradient(to top, #00c6fb 0%, #005bea 100%)',
      icon: 'info',
      destroyByClick: true,
      undestroyByMouseEnter: true,
    },
    warning: {
      background:
        'radial-gradient(circle at 65% 15%,rgb(247, 151, 30),rgb(255, 210, 0))',
      icon: 'warning',
      destroyByClick: true,
      undestroyByMouseEnter: true,
    },
    danger: {
      background:
        'linear-gradient(to right, rgb(237, 33, 58), rgb(147, 41, 30))',
      icon: 'error_outline',
      destroyByClick: true,
      undestroyByMouseEnter: true,
    },
  },
} as const;
