import { randomid } from '@handlers/string.handlers';
import { assign } from '@handlers/utils.handlers';
import { IToastrConfig } from '../interfaces/IToastrConfig';
import { ToastrStatus } from '../types/toastr.types';

export class Toastr {
  public _id: string = randomid();
  public config: IToastrConfig = {
    destroyByClick: true,
    duration: 3000,
    insertTo: 'end',
  };

  constructor(
    public status: ToastrStatus,
    public title: string,
    public message: string,
    config: IToastrConfig = {},
  ) {
    this.config = assign(this.config, config);
  }
}
