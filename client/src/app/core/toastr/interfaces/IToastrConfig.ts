import { TInsert } from '@core/types/state.types';

export interface IToastrConfig {
  background?: string;
  icon?: string;
  destroyByClick?: boolean;
  undestroyByMouseEnter?: boolean;
  duration?: number;
  insertTo?: TInsert;
}
