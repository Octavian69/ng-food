import { InjectionToken } from '@angular/core';
import { ToastrRootMapConfig } from '../types/toastr.types';

export const TOASTR_ROOT_CONFIG = new InjectionToken<ToastrRootMapConfig>(
  'toastr config',
);
