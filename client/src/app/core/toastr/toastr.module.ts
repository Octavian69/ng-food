import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { DirectivesModule } from '@core/modules/directives.module';
import { ToastrListComponent } from './components/toastr-list/toastr-list.component';
import { ToastrComponent } from './components/toastr/toastr.component';
import { buildToastrRootConfiguration } from './handlers/toastr.handlers';
import { TOASTR_ROOT_CONFIG } from './injection/toastr.tokens';
import { ToastrRootConfig, ToastrRootMapConfig } from './types/toastr.types';

@NgModule({
  declarations: [ToastrComponent, ToastrListComponent],
  imports: [CommonModule, DirectivesModule],
  exports: [ToastrListComponent],
})
export class ToastrModule {
  static forRoot(
    customConfigurations: ToastrRootConfig = {},
  ): ModuleWithProviders<ToastrModule> {
    const rootConfigurationMap: ToastrRootMapConfig = buildToastrRootConfiguration(
      customConfigurations,
    );

    return {
      ngModule: ToastrModule,
      providers: [
        {
          provide: TOASTR_ROOT_CONFIG,
          useValue: rootConfigurationMap,
        },
      ],
    };
  }
}
