import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ANShowTranslate } from '@core/animations/animations';
import { Toastr } from '@core/toastr/classes/Toastr';
import { ToastrManagerService } from '@core/toastr/services/toastr.manager.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'ng-toastr-list',
  templateUrl: './toastr-list.component.html',
  styleUrls: ['./toastr-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowTranslate()],
})
export class ToastrListComponent {
  public toastrs$: Observable<Toastr[]> = this.toastrManagerService.listen();

  constructor(private toastrManagerService: ToastrManagerService) {}

  public close(toastr: Toastr): void {
    this.toastrManagerService.removeToastr(toastr);
  }
}
