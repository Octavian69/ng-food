import { ISimple } from '@core/interfaces/shared/ISimple';
import { Toastr } from '@core/toastr/classes/Toastr';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-toastr',
  templateUrl: './toastr.component.html',
  styleUrls: ['./toastr.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToastrComponent implements OnInit {
  private timeout$: any;

  @Input() public toastr: Toastr;
  @Output('close') _close = new EventEmitter<Toastr>();

  ngOnInit() {
    this.destroyTimeout();
  }

  private destroyTimeout(): void {
    this.timeout$ = setTimeout(() => {
      this.close();
    }, this.toastr.config.duration);
  }

  public getStatusClass(): ISimple<boolean> {
    const { status } = this.toastr;

    return { [status]: true };
  }

  public getCss(): ISimple<string> {
    const { background } = this.toastr.config;

    return { background };
  }

  public undestroyByMouseEnter(type: 'enter' | 'leave'): void {
    const {
      config: { undestroyByMouseEnter },
    } = this.toastr;

    if (undestroyByMouseEnter) {
      switch (type) {
        case 'enter': {
          this.reset();
          break;
        }
        case 'leave': {
          this.destroyTimeout();
        }
      }
    }
  }

  public destroyByClick(): void {
    if (this.toastr.config.destroyByClick) {
      this.close();
    }
  }

  public close(): void {
    this._close.emit(this.toastr);
    this.reset();
  }

  private reset(): void {
    clearTimeout(this.timeout$);
    this.timeout$ = null;
  }
}
