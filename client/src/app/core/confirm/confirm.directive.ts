import {
  Directive,
  ViewContainerRef,
  AfterViewInit,
  ComponentFactoryResolver,
  ComponentFactory,
  ComponentRef,
  Injector,
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { isEqual } from '@handlers/conditions.handlers';
import { TViewState } from '@core/types/view.types';
import { ConfirmService } from './confirm.service';
import { ConfirmComponent } from './components/confirm/confirm.component';

@UntilDestroy()
@Directive({
  selector: '[ngConfirm]',
})
export class ConfirmDirective implements AfterViewInit {
  private unsubscriber$: Subject<boolean> = new Subject();

  constructor(
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private viewContainer: ViewContainerRef,
    private confirmService: ConfirmService,
  ) {}

  ngAfterViewInit() {
    this.init();
  }

  private init(): void {
    this.confirmService
      .getStream('message$')
      .pipe(untilDestroyed(this))
      .subscribe((message: string) => {
        message ? this.show(message) : this.hide();
      });
  }

  private show(message: string): void {
    const factory: ComponentFactory<ConfirmComponent> = this.resolver.resolveComponentFactory(
      ConfirmComponent,
    );
    const component$: ComponentRef<ConfirmComponent> = this.viewContainer.createComponent(
      factory,
      0,
      this.injector,
    );

    this.initComponent(component$, message);
  }

  private initComponent(
    component$: ComponentRef<ConfirmComponent>,
    message: string,
  ): void {
    const { instance } = component$;

    Object.assign(instance, { message });

    instance._reply
      .pipe(takeUntil(this.unsubscriber$), untilDestroyed(this))
      .subscribe((value: boolean) => {
        this.confirmService.reply(value);
      });

    this.confirmService
      .getStream('loading$')
      .pipe(takeUntil(this.unsubscriber$), untilDestroyed(this))
      .subscribe((status: TViewState) => {
        const loading: boolean = isEqual(status, 'show');
        Object.assign(instance, { loading, disabled: loading });

        if (isEqual(status, 'hide')) this.hide();
      });
  }

  private hide(): void {
    this.unsubscriber$.next(true);
    this.viewContainer.clear();
  }
}
