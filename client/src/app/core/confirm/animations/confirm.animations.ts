import {
  query,
  transition,
  trigger,
  useAnimation
  } from '@angular/animations';
import { ANTransform } from '@core/animations/animations';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import { EnShadow } from '@core/components/shadow/enums/shadow.enum';

export const ANShowConfirm = trigger('ANShowConfirm', [
  transition(':enter', [
    query(
      '.content',
      [
        useAnimation(ANTransform, {
          params: {
            fromState: 'translate(-50%, 100%)',
            toState: 'translate(-50%, 0%)',
            timing: EnShadow.ANIMATION_TIMING,
          },
        }),
      ],
      queryOptions(),
    ),
  ]),
  transition(':leave', [
    query(
      '.content',
      [
        useAnimation(ANTransform, {
          params: {
            fromState: 'translate(-50%,0%)',
            toState: 'translate(-50%, 100%)',
            timing: EnShadow.ANIMATION_TIMING,
          },
        }),
      ],
      queryOptions(),
    ),
  ]),
]);
