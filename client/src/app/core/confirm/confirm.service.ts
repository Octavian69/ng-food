import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StreamManager } from '@core/managers/Stream.manager';
import { TViewState } from '@core/types/view.types';
import { Subject } from 'rxjs';
import { first, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ConfirmService extends ExtendsFactory(StreamManager()) {
  private message$: Subject<string> = new Subject();
  private confirm$: Subject<boolean> = new Subject();
  private loading$: Subject<TViewState> = new Subject();

  public reply(value: boolean): void {
    this.emitToStream('confirm$', value);
  }

  public confirm(message: string, loading: boolean = false): Promise<boolean> {
    this.emitToStream('message$', message);

    return this.getStream<boolean>('confirm$')
      .pipe(
        first(),
        tap((value: boolean) => {
          if (!value || (value && !loading)) this.hide();
          else if (loading) this.loading('show');
        }),
      )
      .toPromise();
  }

  public hide(): void {
    this.emitToStream('message$', null);
  }

  public loading(status: TViewState): void {
    this.emitToStream('loading$', status);
  }
}
