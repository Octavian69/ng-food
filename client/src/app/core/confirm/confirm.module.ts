import { NgModule } from '@angular/core';
import { CoreModule } from '@core/modules/core.module';
import { ComponentsModule } from '@core/modules/components.module';
import { ConfirmDirective } from './confirm.directive';
import { ConfirmComponent } from './components/confirm/confirm.component';

const declarations = [ConfirmDirective, ConfirmComponent];

@NgModule({
  declarations,
  imports: [CoreModule, ComponentsModule],
  exports: declarations,
})
export class ConfirmModule {}
