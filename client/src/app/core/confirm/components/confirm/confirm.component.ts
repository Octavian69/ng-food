import { ANPropShow } from '@core/animations/animations';
import { ANShowConfirm } from '@core/confirm/animations/confirm.animations';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'translateY(100%)',
      toValue: 'translateY(0%)',
    }),
    ANShowConfirm,
  ],
})
export class ConfirmComponent {
  @Input() disabled: boolean = false;
  @Input() loading: boolean = false;
  @Input() message: string;
  @Output('reply') _reply = new EventEmitter<boolean>();

  public reply(value: boolean): void {
    if (!this.disabled) this._reply.emit(value);
  }
}
