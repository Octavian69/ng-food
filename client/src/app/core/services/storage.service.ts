import { Inject, Injectable } from '@angular/core';
import { ServicesModule } from '@core/modules/services.module';
import { WINDOW } from '@core/utils/injection/app-tokens';

@Injectable({
  providedIn: ServicesModule,
})
export class StorageService {
  constructor(@Inject(WINDOW) private window: Window) {}

  public get<T>(key: string, storage: TStorage = 'sessionStorage'): T {
    return JSON.parse(this.window[storage].getItem(key));
  }

  public remove(key: string, storage: TStorage = 'sessionStorage'): void {
    this.window[storage].removeItem(key);
  }

  public set<T>(
    key: string,
    value: T,
    storage: TStorage = 'sessionStorage',
  ): void {
    this.window[storage].setItem(key, JSON.stringify(value));
  }

  public clear(): void {
    this.window.localStorage.clear();
    this.window.sessionStorage.clear();
  }

  public reset(storage: TStorage = 'sessionStorage'): void {
    this.window[storage].clear();
  }
}

export type TStorage = 'localStorage' | 'sessionStorage';
