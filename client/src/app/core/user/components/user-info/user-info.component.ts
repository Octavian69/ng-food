import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IUserInfo } from '@core/user/interfaces/IUserInfo';
import { UserViewService } from '@core/user/services/user.view.service';
import {
  ANShowFade,
  ANShowStyleStagger,
  ANShowTranslate,
} from '@core/animations/animations';

@Component({
  selector: 'ng-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowStyleStagger(), ANShowTranslate(), ANShowFade()],
})
export class UserInfoComponent {
  @Input('user') user: IUserInfo;

  constructor(public viewService: UserViewService) {}
}
