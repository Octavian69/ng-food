import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { FileUpload } from '@core/file/classes/FileUpload';
import { getRange } from '@core/form/handlers/form.handlers';
import { TControlOption, TRange } from '@core/form/types/form.types';
import { IUser } from '@core/user/interfaces/IUser';
import { IUserInfo } from '@core/user/interfaces/IUserInfo';
import { NSUser } from '@core/user/namespaces/user.namespace';
import { UserViewService } from '@core/user/services/user.view.service';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {
  DisabledFormStatus,
  RequiredRange,
} from '@core/form/decorators/decorators';

@Component({
  selector: 'ng-user-edit-modal',
  templateUrl: './user-edit-modal.component.html',
  styleUrls: ['./user-edit-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserModalComponent implements OnInit {
  public form: FormGroup;
  private file: File;

  public genderOptions: TControlOption<string>[] = this.viewService.getDataFromDB(
    ['user', 'gender', 'options'],
  );

  @DisabledFormStatus() public disabled: boolean;
  @Input()
  user: IUserInfo;
  @Input() loading: boolean;
  @Output('edit') _edit = new EventEmitter<FileRequestBody<IUser>>();

  @Output('hideEditModal')
  _hideEditModal: EventEmitter<void> = new EventEmitter();

  constructor(private fb: FormBuilder, public viewService: UserViewService) {}

  ngOnInit(): void {
    this.init();
  }

  private init(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      Name: [
        null,
        [
          Validators.required,
          Validators.minLength(getRange(NSUser, 'Name', 'min')),
          Validators.maxLength(getRange(NSUser, 'Name', 'max')),
        ],
      ],
      Position: [
        null,
        [
          Validators.minLength(getRange(NSUser, 'Position', 'min')),
          Validators.maxLength(getRange(NSUser, 'Position', 'max')),
        ],
      ],
      Gender: [null],
    });

    this.form.patchValue({ ...this.user });
  }

  @RequiredRange(NSUser)
  public getRange(controlName: string, rangeType: TRange) {}

  public changeFile({ file }: FileUpload<'IMG'>): void {
    this.file = file;
  }

  private isExistFile(): boolean {
    return Boolean(this.file) && this.form.valid;
  }

  public isDisabledForm(): boolean {
    return this.disabled && !this.isExistFile();
  }

  public close(): void {
    this._hideEditModal.emit();
  }

  public reset(): void {
    this.form.markAsUntouched();
    this.file = null;
  }

  public edit(): void {
    const { value } = this.form;
    const json = Object.assign({}, this.user, value);
    const body: FileRequestBody<IUser> = new FileRequestBody(json, this.file);

    this._edit.emit(body);
    this.reset();
  }
}
