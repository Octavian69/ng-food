import { Initialize } from '@core/decorators/decorators';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitDetectionChanges } from '@core/managers/interfaces/hooks/OnInitDetectionChanges';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { IMessage } from '@core/message/IMessage';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { IUser } from '@core/user/interfaces/IUser';
import { IUserInfo } from '@core/user/interfaces/IUserInfo';
import { UserDataService } from '@core/user/services/user.data.service';
import { UserViewService } from '@core/user/services/user.view.service';
import { UserDataFlags } from '@core/user/states/User.data.flags';
import { UserViewFlags } from '@core/user/states/User.view.flags';
import { TUserDataAction } from '@core/user/types/user.types';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { LoginService } from '@src/auth/services/login.service';
import { Observable } from 'rxjs';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import {
  ANShowUserAvatar,
  ANShowUserModal,
} from '@core/user/animations/user.animations';

@UntilDestroy()
@Component({
  selector: 'ng-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.scss'],
  providers: [UserViewService],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowUserAvatar, ANShowUserModal],
})
export class UserContentComponent
  implements
    OnInit,
    OnInitStates,
    OnInitActionListeners,
    OnInitDetectionChanges {
  public user$: Observable<IUserInfo> = this.dataService.getStream('user$');
  public viewFlags: UserViewFlags;
  public dataFlags: UserDataFlags;

  constructor(
    private cdr: ChangeDetectorRef,
    private toastr: ToastrService,
    private loginService: LoginService,
    private dataService: UserDataService,
    private viewService: UserViewService,
  ) {}

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy() {
    this.dataService.destroy();
  }

  @Initialize()
  initStates(): void {
    this.viewFlags = this.viewService.getFullState('flags');
    this.dataFlags = this.dataService.getFullState('flags');
  }

  @Initialize()
  initActionListeners(): void {
    this.dataService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe(({ type, payload }: TUserDataAction) => {
        switch (type) {
          case 'edit': {
            this.afterEdit(payload);
            break;
          }
          case 'remove': {
            this.afterRemove(payload);
            break;
          }
        }

        this.cdr.detectChanges();
      });
  }

  @Initialize()
  initDetectionChanges() {
    this.dataService
      .detectChanges()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.cdr.detectChanges();
      });
  }

  private init(): void {
    this.dataService.initUser();
  }

  public setFlag(key: keyof UserViewFlags, value: boolean): void {
    this.viewService.setState('flags', key, value);
  }

  public edit(body: FileRequestBody<IUser>): void {
    this.dataService.edit(body);
  }

  private afterEdit({ message }: IMessage): void {
    this.viewService.setState('flags', 'isShowEditModal', false);
    this.toastr.success(message);
  }

  private afterRemove({ message }: IMessage): void {
    this.toastr.success(message);
    this.loginService.logout();
  }
}
