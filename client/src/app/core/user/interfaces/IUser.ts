import { TUserGender } from "../types/user.types";

export interface IUser {
  Login: string;
  Password: string;
  Name: string;
  Position: string;
  Avatar: string;
  Created: Date;
  Gender: TUserGender;
  _id?: string;
}