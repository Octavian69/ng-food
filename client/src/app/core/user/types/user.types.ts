import { IStateAction } from '@core/managers/interfaces/IStateAction';
import { IMessage } from '@core/message/IMessage';

export type TUserDataAction = IStateAction<'edit' | 'remove', IMessage>;

export type TUserGender = 'male' | 'female';
