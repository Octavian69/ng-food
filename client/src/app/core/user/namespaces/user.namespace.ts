import { ISimple } from '@core/interfaces/shared/ISimple';

export namespace NSUser {
  export const min: ISimple<number> = {
    Name: 3,
    Position: 3,
  };
  export const max: ISimple<number> = {
    Name: 40,
    Position: 70,
  };
}
