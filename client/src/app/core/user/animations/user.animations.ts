import { ANFade, ANStylesShow, ANTransform } from '@core/animations/animations';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import { EnShadow } from '@core/components/shadow/enums/shadow.enum';
import {
  group,
  query,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';

export const ANShowUserAvatar = ANStylesShow({
  animationName: 'ANShowUserAvatar',
  fromStyle: {
    opacity: '0',
    transform: 'perspective(300px) rotateY(0deg) scale(.3)',
  },
  toStyle: {
    opacity: '*',
    transform: 'perspective(300px) rotateY(360deg) scale(1)',
  },
  timingFrom: '1s',
});

export const ANShowUserModal = trigger('ANShowUserModal', [
  transition(':enter', [
    query(
      '.modal',
      [
        group([
          useAnimation(ANFade, {
            params: { timing: EnShadow.ANIMATION_TIMING },
          }),
          useAnimation(ANTransform, {
            params: {
              fromState: 'translateY(-10%)',
              toState: 'translateY(0%)',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ]),
      ],
      queryOptions(),
    ),
  ]),
  transition(':leave', [
    query(
      '.modal',
      [
        group([
          useAnimation(ANFade, {
            params: {
              fromState: '1',
              toState: '0',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
          useAnimation(ANTransform, {
            params: {
              fromState: 'translateY(0%)',
              toState: 'translateY(-10%)',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ]),
      ],
      queryOptions(),
    ),
  ]),
]);
