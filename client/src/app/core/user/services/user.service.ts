import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMessage } from '@core/message/IMessage';
import { IUserInfo } from '../interfaces/IUserInfo';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  public getById(id: string): Observable<IUserInfo> {
    return this.http.get<IUserInfo>(`@/user/get-by-id/${id}`);
  }

  public getByLogin(login: string): Observable<IUserInfo> {
    const params: HttpParams = new HttpParams({
      fromObject: { login },
    });

    return this.http.get<IUserInfo>(`@/user/get-by-login`, { params });
  }

  public remove(): Observable<IMessage> {
    return this.http.delete<IMessage>('@/user/remove');
  }

  public edit(user: FormData): Observable<IUserInfo> {
    return this.http.patch<IUserInfo>('@/user/edit', user);
  }
}
