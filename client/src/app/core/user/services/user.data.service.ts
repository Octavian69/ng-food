import { Injectable } from '@angular/core';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { convertToFormData } from '@core/file/handlers/file.handlers';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { IMessage } from '@core/message/IMessage';
import { StorageService } from '@core/services/storage.service';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UserService } from './user.service';
import { UserDB } from '../db/users.db';
import { IUser } from '../interfaces/IUser';
import { IUserInfo } from '../interfaces/IUserInfo';
import { UserInfo } from '../models/UserInfo.model';
import { UserDataFlags } from '../states/User.data.flags';
import { TUserDataAction } from '../types/user.types';

@Injectable()
export class UserDataService extends ExtendsFactory(
  State({
    flags: UserDataFlags,
    db: UserDB,
  }),
  StreamManager<TUserDataAction>(),
) {
  public user$: BehaviorSubject<IUserInfo> = new BehaviorSubject(null);

  constructor(
    private storage: StorageService,
    private userService: UserService,
  ) {
    super();
  }

  public id(): string {
    const { _id } = this.getStreamValue<IUserInfo>('user$');

    return _id;
  }

  public initUser(): void {
    const user: IUserInfo = this.storage.get('user');

    this.emitToStream('user$', user);
  }

  public login(loginUser: Partial<IUser>): void {
    const user: IUserInfo = new UserInfo(loginUser);

    this.update(user);
  }

  public update(user: IUserInfo): void {
    this.emitToStream('user$', user);
    this.storage.set('user', user);
  }

  public edit(body: FileRequestBody<IUser>): void {
    this.setState('flags', 'isEditUser', true);

    const editUser = convertToFormData(body, 'avatar', 'user');
    const next = (user: IUserInfo) => {
      const message: string = this.getDataFromDB(['messages', 'edit-success']);
      this.action(new StateAction('edit', { message }));
      this.update(user);
    };

    this.userService
      .edit(editUser)
      .pipe(
        this.untilDestroyed(),
        finalize(() => {
          this.setState('flags', 'isEditUser', false);
          this.detect('edit');
        }),
      )
      .subscribe(next);
  }

  public remove(): void {
    const next = ({ message }: IMessage) => {
      this.action(new StateAction('remove', { message }));
    };

    this.userService.remove().pipe(this.untilDestroyed()).subscribe(next);
  }

  public destroy() {
    this.destroyStreams(['user$']);
    this.destroyStates('flags');
  }
}
