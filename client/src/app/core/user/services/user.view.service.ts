import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { UserDataService } from './user.data.service';
import { UserDB } from '../db/users.db';
import { IUserInfo } from '../interfaces/IUserInfo';
import { UserViewFlags } from '../states/User.view.flags';

@Injectable()
export class UserViewService extends ExtendsFactory(
  State({ flags: UserViewFlags, db: UserDB }),
) {
  constructor(private dataService: UserDataService) {
    super();
  }

  public getEmptyAvatar(): string {
    const user: IUserInfo = this.dataService.getStreamValue('user$');
    const avatarPath: string = `user/${
      user?.Gender === 'female' ? 'woman' : 'man'
    }.jpg`;

    return avatarPath;
  }
}
