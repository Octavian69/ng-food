import { NgModule } from '@angular/core';
import { SharedModule } from '../modules/shared.module';
import { UserService } from './services/user.service';
import { UserDataService } from './services/user.data.service';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { UserModalComponent } from './components/user-edit-modal/user-edit-modal.component';
import { UserContentComponent } from './components/user-content/user-content.component';
import { FileModule } from '@core/file/file.module';

@NgModule({
  declarations: [UserContentComponent, UserInfoComponent, UserModalComponent],
  imports: [SharedModule, FileModule],
  providers: [UserService, UserDataService],
  exports: [UserContentComponent],
})
export class UserModule {}
