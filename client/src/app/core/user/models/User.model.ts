import { IUser } from '../interfaces/IUser';
import { TUserGender } from '../types/user.types';

export class User implements IUser {
  constructor(
    public Login: string,
    public Name: string,
    public Password: string,
    public Created: Date = new Date,
    public Avatar: string = null,
    public Gender: TUserGender = null,
    public Position: string = null,
  ) { }
}