import { selectedProps } from '@handlers/structutral.handlers';
import { IUserInfo } from '../interfaces/IUserInfo';
import { TUserGender } from '../types/user.types';

export class UserInfo implements IUserInfo {
  public Name: string = null;
  public Position: string = null;
  public Avatar: string = null;
  public Created: Date = null;
  public Gender: TUserGender = null;
  public _id: string = null;

  constructor(user: IUserInfo | Partial<IUserInfo>) {
    const props = Object.keys(this) as (keyof IUserInfo)[];
    const selected = selectedProps(user, props);

    Object.assign(this, selected);
  }
}
