export const UserDB = {
  messages: {
    'edit-success': 'Пользователь отредактирован.',
  },
  user: {
    gender: {
      labels: {
        male: 'Мужской',
        female: 'Женский',
      },
      options: [
        { id: 1, label: 'Женский', value: 'female' },
        { id: 2, label: 'Мужской', value: 'male' },
      ],
    },
  },
} as const;

export type TUserDB = typeof UserDB;
