import { Pipe, PipeTransform } from '@angular/core';
import { isNotNullish } from '@handlers/conditions.handlers';

@Pipe({
  name: 'defined',
})
export class DefinedPipe implements PipeTransform {
  transform(value: any): boolean {
    return isNotNullish(value);
  }
}
