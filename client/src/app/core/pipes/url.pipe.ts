import { Pipe, PipeTransform } from '@angular/core';
import { joinUrl } from '@handlers/string.handlers';

@Pipe({
  name: 'url',
})
export class UrlPipe implements PipeTransform {
  transform(value: string[]): string {
    return joinUrl(value);
  }
}
