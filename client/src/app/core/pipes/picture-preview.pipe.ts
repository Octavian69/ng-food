import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { EnApp } from '@core/enums/app.enum';
import { joinUrl } from '@handlers/string.handlers';
import { proxyUrl } from '@handlers/utils.handlers';
@Pipe({
  name: 'picturePreview',
})
export class PicturePreviewPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(
    value: string,
    preview: string,
    defaultImg: string,
  ): string | SafeResourceUrl {
    switch (true) {
      case Boolean(preview):
        return this.sanitizer.bypassSecurityTrustResourceUrl(preview);
      case Boolean(value):
        return value.startsWith(EnApp.SERVER_UPLOAD_ROOT)
          ? proxyUrl(value)
          : value;
      case Boolean(defaultImg):
        return joinUrl([EnApp.ASSETS_IMGS_PATH, defaultImg]);
      default:
        console.error(`FILE: "${value}" NOT FOUND`);
    }
  }
}
