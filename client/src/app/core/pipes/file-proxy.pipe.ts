import { Pipe, PipeTransform } from '@angular/core';
import { proxyUrl } from '@handlers/utils.handlers';
@Pipe({
  name: 'fileproxy',
})
export class FileProxyPipe implements PipeTransform {
  transform(filepath: string): string {
    return proxyUrl(filepath);
  }
}
