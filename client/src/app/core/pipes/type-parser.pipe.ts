import { Pipe, PipeTransform } from '@angular/core';
import { ExtractValues, JSDataType } from '@core/types/libs.types';

type ParseTypes =
  | ExtractValues<JSDataType, 'string' | 'number' | 'boolean'>
  | 'date'
  | 'array';

@Pipe({
  name: 'toType',
})
export class TypeParser implements PipeTransform {
  transform(value: unknown, type: ParseTypes): unknown {
    switch (type) {
      case 'string':
        return String(value);
      case 'number':
        return Number(value);
      case 'boolean':
        return Boolean(value);
      case 'date':
        return new Date(value as string | number);
      case 'array':
        return Array.from(value as Iterable<unknown>);
      default:
        return value;
    }
  }
}
