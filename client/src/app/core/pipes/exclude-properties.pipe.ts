import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'excludeProps',
})
export class ExcludePropertiesPipe implements PipeTransform {
  transform<T extends object>(object: T, props: (keyof T)[] = []) {
    if (!object || !props?.length) return {};
    
    return Object.entries(object).reduce((accum, [key, value]) => {
      const isExclude: boolean = props.includes(key as keyof T);
      if (!isExclude) {
        accum[key] = value;
      }

      return accum;
    }, {});
  }
}
