import { Pipe, PipeTransform } from '@angular/core';
import { EnApp } from '@core/enums/app.enum';
import { joinUrl } from '@handlers/string.handlers';
@Pipe({
  name: 'imgAssetsUrl',
})
export class AssetsImgUrlPipe implements PipeTransform {
  transform(value: string): string {
    return joinUrl([EnApp.ASSETS_IMGS_PATH, value]);
  }
}
