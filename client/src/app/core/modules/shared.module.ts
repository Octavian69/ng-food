import { NgModule } from '@angular/core';
import { TooltipModule } from '@core/tooltip/tooltip.module';
import { ComponentsModule } from './components.module';
import { CoreModule } from './core.module';
import { DirectivesModule } from './directives.module';
import { NebularModule } from './nebular.module';
import { PipesModule } from './pipes.module';
import { FormModule } from '../form/form.module';

const modules = [
  NebularModule,
  CoreModule,
  FormModule,
  ComponentsModule,
  DirectivesModule,
  PipesModule,
  TooltipModule,
];

@NgModule({
  imports: modules,
  exports: modules,
})
export class SharedModule {}
