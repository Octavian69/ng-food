import { NgModule } from '@angular/core';
import { AffixPipe } from '@core/pipes/affix.pipe';
import { AssetsImgUrlPipe } from '@core/pipes/assets-img-url.pipe';
import { BooleanPipe } from '@core/pipes/boolean.pipe';
import { ConditionPipe } from '@core/pipes/condition.pipe';
import { DefinedPipe } from '@core/pipes/defined.pipe';
import { EqualPipe } from '@core/pipes/equal.pipe';
import { ExcludePropertiesPipe } from '@core/pipes/exclude-properties.pipe';
import { FileProxyPipe } from '@core/pipes/file-proxy.pipe';
import { IncludesPipe } from '@core/pipes/includes.pipe';
import { MapPipe } from '@core/pipes/map.pipe';
import { PickPipe } from '@core/pipes/pick.pipe';
import { PicturePreviewPipe } from '@core/pipes/picture-preview.pipe';
import { ReplaceDefaultPipe } from '@core/pipes/replace-default.pipe';
import { SanitizerPipe } from '@core/pipes/sanitizer.pipe';
import { SplitStringPipe } from '@core/pipes/split-str.pipe';
import { SymbolLimit } from '@core/pipes/symbol-limit.pipe';
import { TypeParser } from '@core/pipes/type-parser.pipe';
import { UrlPipe } from '@core/pipes/url.pipe';

const declarations = [
  AssetsImgUrlPipe,
  SplitStringPipe,
  SanitizerPipe,
  FileProxyPipe,
  UrlPipe,
  PicturePreviewPipe,
  ReplaceDefaultPipe,
  BooleanPipe,
  DefinedPipe,
  ConditionPipe,
  EqualPipe,
  IncludesPipe,
  PickPipe,
  SymbolLimit,
  MapPipe,
  TypeParser,
  AffixPipe,
  ExcludePropertiesPipe,
];

@NgModule({
  declarations,
  exports: declarations,
})
export class PipesModule {}
