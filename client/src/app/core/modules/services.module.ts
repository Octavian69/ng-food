import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule({})
export class ServicesModule {
  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: []
    }
  }
}