import { NgModule } from '@angular/core';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { EnTheme } from '../theme/enums/EnTheme.enum';
import {
  NbButtonModule,
  NbCalendarRangeModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbPopoverModule,
  NbStepperModule,
  NbThemeModule,
  NbTooltipModule,
} from '@nebular/theme';

export type ToastrPosition =
  | 'top-right'
  | 'bottom-right'
  | 'top-left'
  | 'bottom-left'
  | 'top-end'
  | 'bottom-end'
  | 'top-start'
  | 'bottom-start';

const modules = [
  NbLayoutModule,
  NbIconModule,
  NbLayoutModule,
  NbButtonModule,
  NbTooltipModule,
  NbStepperModule,
  NbCardModule,
  NbInputModule,
  NbCheckboxModule,
  NbFormFieldModule,
  NbPopoverModule,
  NbEvaIconsModule,
  NbCalendarRangeModule,
  NbDatepickerModule,
  NbThemeModule,
];
@NgModule({
  declarations: [],
  imports: [
    ...modules,
    NbThemeModule.forRoot({ name: EnTheme.DARK }),
    NbDatepickerModule.forRoot(),
  ],
  exports: modules,
})
export class NebularModule {}
