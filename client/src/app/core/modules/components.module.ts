import { NgModule } from '@angular/core';
import { ButtonComponent } from '@core/components/buttons/button/button.component';
import { LoaderButtonComponent } from '@core/components/buttons/loader-button/loader-button.component';
import { CircleLoaderComponent } from '@core/components/circle-loader/circle-loader.component';
import { EmptyMessageComponent } from '@core/components/empty-message/empty-message.component';
import { PictureComponent } from '@core/components/picture/picture.component';
import { PizzaSpinnerComponent } from '@core/components/pizza-spinner/pizza-spinner.component';
import { ShadowComponent } from '@core/components/shadow/shadow.component';
import { SquareLoaderComponent } from '@core/components/square-loader/square-loader.component';
import { ThemePictureComponent } from '@core/components/theme-picture/theme-picture.component';
import { CoreModule } from './core.module';
import { DirectivesModule } from './directives.module';
import { NebularModule } from './nebular.module';
import { PipesModule } from './pipes.module';
import { BadgeComponent } from '../components/badge/badge.component';
import { BorderWrapperComponent } from '../components/border-wrapper/border-wrapper.component';
import { ExpandButtonComponent } from '../components/buttons/expand-button/expand-button.component';
import { IconButtonComponent } from '../components/buttons/icon-button/icon-button.component';

const declarations = [
  ShadowComponent,
  CircleLoaderComponent,
  LoaderButtonComponent,
  PictureComponent,
  ThemePictureComponent,
  ButtonComponent,
  PizzaSpinnerComponent,
  SquareLoaderComponent,
  IconButtonComponent,
  BorderWrapperComponent,
  ExpandButtonComponent,
  BadgeComponent,
  EmptyMessageComponent,
];

@NgModule({
  declarations,
  imports: [CoreModule, NebularModule, DirectivesModule, PipesModule],
  exports: declarations,
})
export class ComponentsModule {}
