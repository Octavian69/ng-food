import { NgModule } from '@angular/core';
import { AllowStatusDirective } from '@core/directives/allow-status.directive';
import { AsyncDirective } from '@core/directives/async.directive';
import { AuthDirective } from '@core/directives/auth.directive';
import { LoaderDirective } from '@core/directives/loader.directive';
import { MutationDirective } from '@core/directives/mutation.directive';
import { PageEndpointDirective } from '@core/directives/page-endpoint.directive';
import { PlacementDirective } from '@core/directives/placement.directive';
import { RevokeUrlDirective } from '@core/directives/revoke-url.directive';
import { SwitchContainCaseDirective } from '@core/directives/switch-contain-case.directive';
import { SwitchContainDefault } from '@core/directives/switch-contain-default.directive';
import { SwitchContainDirective } from '@core/directives/switch-contain.directive';
import { ThemeDirective } from '@core/theme/directives/theme.directive';
import { CoreModule } from './core.module';

const directives = [
  AuthDirective,
  ThemeDirective,
  AllowStatusDirective,
  PlacementDirective,
  MutationDirective,
  RevokeUrlDirective,
  LoaderDirective,
  AsyncDirective,
  PageEndpointDirective,
  SwitchContainDirective,
  SwitchContainCaseDirective,
  SwitchContainDefault,
];

@NgModule({
  declarations: directives,
  imports: [CoreModule],
  exports: directives,
})
export class DirectivesModule {}
