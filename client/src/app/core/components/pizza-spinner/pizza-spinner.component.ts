import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { ISimple } from '../../interfaces/shared/ISimple';
import { NbComponentSize } from '@nebular/theme';

@Component({
  selector: 'ng-pizza-spinner',
  templateUrl: './pizza-spinner.component.html',
  styleUrls: ['./pizza-spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PizzaSpinnerComponent {
  @Input('offset') offset: string = '2rem';
  @Input('size') size: NbComponentSize = 'small';
  @Input('contentCenter') contentCenter: boolean = true;
  @Input('loading') loading: boolean = false;

  @HostBinding('attr.data-size') _size: NbComponentSize = this.size;
}
