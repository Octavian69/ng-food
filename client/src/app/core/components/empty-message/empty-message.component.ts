import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ISimple } from '@core/interfaces/shared/ISimple';

@Component({
  selector: 'ng-empty-message',
  templateUrl: './empty-message.component.html',
  styleUrls: ['./empty-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyMessageComponent {
  @Input() public text: string = 'Отсутствует';
  @Input('font') public fontSize: string = '2rem';
  @Input('align') public textAlign: 'left' | 'center' | 'right' = 'center';

  public getStyles(): ISimple<string> {
    const { fontSize, textAlign } = this;
    return {
      fontSize,
      textAlign,
    };
  }
}
