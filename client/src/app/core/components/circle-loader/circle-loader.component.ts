import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ISimple } from '@core/interfaces/shared/ISimple';

@Component({
  selector: 'ng-circle-loader',
  templateUrl: './circle-loader.component.html',
  styleUrls: ['./circle-loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CircleLoaderComponent {

  @Input() loading: boolean = false;
  @Input() size: string = '1.2rem';
  @Input() strokeWidth: string = '3px'

  public getCssStyles(): ISimple<string> {
    const { size, strokeWidth } = this;

    return {
      width: size,
      height: size,
      borderTopWidth: strokeWidth,
      borderBottomWidth: strokeWidth
    }
  }
}
