import { ThemeService } from '@core/theme/services/theme.service';
import { TAppTheme } from '@core/theme/types/theme.types';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TPictureFit, TPictureShape } from '../picture/utils/picture.types';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@UntilDestroy()
@Component({
  selector: 'ng-theme-picture',
  templateUrl: './theme-picture.component.html',
  styleUrls: ['./theme-picture.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemePictureComponent implements OnInit {
  public defaultUrl: string = this.themeService.getCurrentTheme();

  @Input('shape')
  shape: TPictureShape = 'round';
  @Input('alt') alt: string = 'img';
  @Input('imgUrl') imgUrl: string;
  @Input('preview') preview: string;
  @Input('darkUrl') darkUrl: string;
  @Input('lightUrl') lightUrl: string;
  @Input('objectFit') objectFit: TPictureFit = 'cover';

  constructor(
    private cdr: ChangeDetectorRef,
    private themeService: ThemeService,
  ) {}

  ngOnInit() {
    this.initThemeListener();
  }

  private initThemeListener(): void {
    this.themeService
      .onThemeChange()
      .pipe(untilDestroyed(this))
      .subscribe((theme: TAppTheme) => {
        this.defaultUrl = theme === 'dark_mode' ? this.darkUrl : this.lightUrl;
        this.cdr.detectChanges();
      });
  }
}
