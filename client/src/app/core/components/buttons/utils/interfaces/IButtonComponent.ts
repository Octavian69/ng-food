import { NbButtonAppearance, NbComponentSize } from '@nebular/theme';
import { TButton, TButtonShape } from '../types/buttons.types';

export interface IButtonComponent {
  type: TButton;
  text: string;
  icon: string;
  disabled: boolean;
  appearance: NbButtonAppearance;
  size: NbComponentSize;
  shape: TButtonShape;

  action: () => void;
}
