import { IButtonComponent } from './IButtonComponent';

export interface ILoaderButtonComponent extends IButtonComponent {
  loading: boolean;
}
