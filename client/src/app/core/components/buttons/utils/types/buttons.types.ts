import { NbComponentShape } from '@nebular/theme';

export type TButtonShape = NbComponentShape | 'circle';
export type TButton = 'button' | 'submit';
