import { NbButtonAppearance, NbComponentSize } from '@nebular/theme';
import { ILoaderButtonComponent } from '../utils/interfaces/ILoaderButtonComponent';
import { TButton, TButtonShape } from '../utils/types/buttons.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-loader-button',
  templateUrl: './loader-button.component.html',
  styleUrls: ['./loader-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderButtonComponent implements ILoaderButtonComponent {
  public shape: TButtonShape = 'rectangle';
  public size: NbComponentSize = 'medium';

  @Input() type: TButton = 'button';
  @Input() text: string;
  @Input() icon: string;
  @Input() disabled: boolean = false;
  @Input() loading: boolean = false;
  @Input() appearance: NbButtonAppearance = 'filled';
  @Input('size') set _size(size: NbComponentSize) {
    if (size) {
      this.size = size;
      this.dataSize = size;
    }
  }
  @Input('shape') set _shape(shape: TButtonShape) {
    if (shape) {
      this.shape = shape;
      this.dataShape = shape;
    }
  }

  @Output('action') _action = new EventEmitter<void>();
  @HostBinding('attr.data-shape') dataShape: TButtonShape = this.shape;
  @HostBinding('attr.data-size') dataSize: NbComponentSize = this.size;

  public isDisabled(): boolean {
    return this.loading || this.disabled;
  }

  public action(): void {
    this._action.emit();
  }
}
