import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';
import { NbButtonAppearance, NbComponentSize } from '@nebular/theme';
import { IButtonComponent } from '../utils/interfaces/IButtonComponent';
import { TButton, TButtonShape } from '../utils/types/buttons.types';

@Component({
  selector: 'ng-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent implements IButtonComponent {
  public shape: TButtonShape = 'rectangle';
  public size: NbComponentSize = 'medium';

  @Input() type: TButton = 'button';
  @Input() text: string;
  @Input() disabled: boolean = false;
  @Input() icon: string;
  @Input() appearance: NbButtonAppearance = 'filled';
  @Input('size') set _size(size: NbComponentSize) {
    if (size) {
      this.size = size;
      this.dataSize = size;
    }
  }
  @Input('shape') set _shape(shape: TButtonShape) {
    if (shape) {
      this.shape = shape;
      this.dataShape = shape;
    }
  }

  @Output('action') _action = new EventEmitter<void>();
  @HostBinding('attr.data-shape') dataShape: TButtonShape = this.shape;
  @HostBinding('attr.data-size') dataSize: NbComponentSize = this.size;

  public action(): void {
    this._action.emit();
  }
}
