import { ISimple } from '@core/interfaces/shared/ISimple';
import { ThemeDecor } from '@core/theme/types/theme.types';
import { TButton } from '../utils/types/buttons.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
@Component({
  selector: 'ng-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconButtonComponent {
  @Input() icon: string;
  @Input() type: TButton = 'button';
  @Input() fontSize: string = '1.5rem';
  @Input('disable') disabled: boolean = false;
  @Input() loading: boolean = false;
  @Input('theme') theme: ThemeDecor = 'default';

  @Output('action') _action = new EventEmitter<void>();

  public getIconClasses(): ISimple<boolean> {
    const { disabled, loading } = this;

    return { disabled, loading };
  }

  public getFontSize(): ISimple<string> {
    const { fontSize } = this;
    return { fontSize };
  }

  public action(e: MouseEvent): void {
    e.stopPropagation();
    this._action.emit();
  }
}
