import { TViewState } from '@core/types/view.types';
import { scrollToContent } from '@handlers/components.handlers';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  ANPropShow,
  ANShowBySize,
  ANShowTranslate,
  ANToggleFade,
} from '@core/animations/animations';

@Component({
  selector: 'ng-expand-button',
  templateUrl: './expand-button.component.html',
  styleUrls: ['./expand-button.component.scss'],
  animations: [
    ANShowTranslate(),
    ANToggleFade(),
    ANShowBySize(),
    ANPropShow({
      cssProp: 'width',
      fromValue: '0px',
      toValue: '*',
    }),
  ],
  exportAs: 'expandRef',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExpandButtonComponent {
  @Input('state') state: TViewState = 'hide';
  @Input('text') text: string = 'Создать';
  @Input('icon') icon: string = 'add_circle_outline';
  @Output('changeExpandState') _changeState = new EventEmitter<TViewState>();

  @ViewChild('contentRef', { read: ElementRef, static: true })
  contentRef: ElementRef;

  public getState(): TViewState {
    return this.state;
  }

  public toggleState(): void {
    const currentState = this.getState();

    currentState === 'hide' ? this.show() : this.hide();
    this._changeState.emit(this.state);
  }

  public hide(): void {
    this.state = 'hide';
  }

  public show(isScrollToContent: boolean = true): void {
    this.state = 'show';
    if (isScrollToContent) scrollToContent(this.contentRef, 300);
  }
}
