import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { ThemeDecor } from '@core/theme/types/theme.types';
import {
  ANPropShow,
  ANStateChange,
  ANTransform,
} from '@core/animations/animations';

@Component({
  selector: 'ng-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  animations: [
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'scale(0)',
      toValue: 'scale(1)',
      timingFrom: '0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275)',
    }),
    ANStateChange({ animationRef: ANTransform }),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BadgeComponent {
  @Input() type: 'icon' | 'text' = 'text';
  @Input() size: string = '40px';
  @Input() value: any;
  @Input() fontSize: string = '1rem';
  @Input() theme: ThemeDecor;

  public getSize(): ISimple<string> {
    const { size } = this;

    return {
      height: size,
      width: size,
    };
  }
}
