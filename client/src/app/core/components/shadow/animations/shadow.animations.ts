import { ANFade } from '@core/animations/animations';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import { EnShadow } from '../enums/shadow.enum';
import {
  animateChild,
  group,
  query,
  style,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';

export const MAX_Z_INDEX: string = '1000';

const hostStyles = style({
  position: 'relative',
  zIndex: MAX_Z_INDEX,
});
const containerStyles = query(
  '.shadow',
  [
    style({
      position: 'fixed',
      zIndex: MAX_Z_INDEX,
      overflow: 'hidden',
    }),
  ],
  queryOptions(),
);
const contentStyles = query(
  '.shadow-content',
  [style({ position: 'static' })],
  queryOptions(),
);
const staticStyles = [hostStyles, containerStyles, contentStyles];

export const ANShowShadow = trigger('ANShowShadow', [
  transition(':enter', [
    ...staticStyles,
    group([
      useAnimation(ANFade, { params: { timing: EnShadow.ANIMATION_TIMING } }),
      query(':enter, @*', [animateChild()], queryOptions()),
    ]),
    ...staticStyles,
  ]),
  transition(':leave', [
    ...staticStyles,
    query(':leave, @*', [animateChild()], queryOptions()),
    useAnimation(ANFade, {
      params: {
        fromState: '1',
        toState: '0',
        timing: EnShadow.ANIMATION_TIMING,
      },
    }),
    ...staticStyles,
  ]),
]);
