import { ANShowShadow } from './animations/shadow.animations';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-shadow',
  templateUrl: './shadow.component.html',
  styleUrls: ['./shadow.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowShadow],
})
export class ShadowComponent {
  @Input('contentCenter') contentCenter: boolean = true;
  @Output('close') _close: EventEmitter<void> = new EventEmitter();

  @HostBinding('@ANShowShadow') private ANShowShadow;

  public close(e: MouseEvent): void {
    const isCanClose: boolean = (e.target as HTMLElement).hasAttribute(
      'data-close-element',
    );
    if (isCanClose) {
      this._close.emit();
    }
  }
}
