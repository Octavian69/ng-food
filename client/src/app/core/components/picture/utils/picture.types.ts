export type TPictureShape = 'round' | 'circle' | 'square';
export type TPictureFit = 'contain' | 'cover' | 'fill' | 'scale-down';
export type TPictureLoad = 'default' | 'img';
export type TImgSize = 'small' | 'medium' | 'large' | 'full';
