import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ANToggleFade } from '@core/animations/animations';
import { TPictureFit, TPictureShape } from './utils/picture.types';

@Component({
  selector: 'ng-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.scss'],
  animations: [ANToggleFade()],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PictureComponent {
  @Input('shape')
  shape: TPictureShape = 'round';
  @Input('alt') alt: string = 'img';
  @Input('imgUrl') imgUrl: string;
  @Input('preview') preview: string;
  @Input('defaultUrl') defaultUrl: string;
  @Input('objectFit') objectFit: TPictureFit = 'cover';
}
