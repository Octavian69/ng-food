import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { TLoaderCubeSize } from '@core/directives/loader.directive';

@Component({
  selector: 'ng-square-loader',
  templateUrl: './square-loader.component.html',
  styleUrls: ['./square-loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SquareLoaderComponent {
  @Input() oversize: boolean = false;
  @Input() cubeSize: TLoaderCubeSize = 'medium';
  @Input() contentCenter: boolean = true;

  constructor(private cdr: ChangeDetectorRef) {}

  public detect(): void {
    this.cdr.detectChanges();
  }
}
