import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ISimple } from '@core/interfaces/shared/ISimple';

@Component({
  selector: 'ng-border-wrapper',
  templateUrl: './border-wrapper.component.html',
  styleUrls: ['./border-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BorderWrapperComponent {
  @Input() private height: string = '4rem';
  @Input() private width: string = '3.5rem';

  public getSize(): ISimple<string> {
    const { height, width } = this;

    return { height, width };
  }
}
