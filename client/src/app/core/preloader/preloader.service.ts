import { Injectable } from '@angular/core';
import { TViewState } from '@core/types/view.types';
import { timeoutDelay } from '@handlers/utils.handlers';
import { Observable, Subject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PreloaderService {
  private preload$: Subject<TViewState> = new Subject();

  public listen(): Observable<TViewState> {
    return this.preload$.asObservable();
  }

  public hide(): void {
    this.preload$.next('hide');
  }

  public show(): void {
    this.preload$.next('show');
  }

  public firstLoading<T>(source: Observable<T>, cb?: Function): void {
    this.show();

    const sub$: Subscription = source.pipe(filter(Boolean)).subscribe((v) => {
      if (cb) cb(v);
      this.hide();
      timeoutDelay(() => sub$.unsubscribe());
    });
  }
}
