import {
  ComponentFactory,
  ComponentFactoryResolver,
  AfterViewInit,
  Directive,
  ViewContainerRef,
  Injector,
  ChangeDetectorRef,
  ComponentRef,
} from '@angular/core';
import { delay } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TViewState } from '@core/types/view.types';
import { PreloaderService } from './preloader.service';
import { PreloaderComponent } from './preloader/preloader.component';

@UntilDestroy()
@Directive({
  selector: '[ngPreloader]',
})
export class PreloaderDirective implements AfterViewInit {
  ref$: ComponentRef<PreloaderComponent>;

  constructor(
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private viewContainer: ViewContainerRef,
    private preloaderService: PreloaderService,
  ) {}

  ngAfterViewInit() {
    this.init();
  }

  private init(): void {
    this.preloaderService
      .listen()
      .pipe(untilDestroyed(this), delay(0))
      .subscribe((status: TViewState) => {
        status === 'hide' ? this.hide() : this.show();
      });
  }

  private hide(): void {
    this.ref$?.destroy();
    this.ref$ = null;
  }

  private show(): void {
    if (!this.ref$) {
      const factory: ComponentFactory<PreloaderComponent> = this.resolver.resolveComponentFactory(
        PreloaderComponent,
      );

      this.ref$ = this.viewContainer.createComponent(factory, 0, this.injector);
    }
  }
}
