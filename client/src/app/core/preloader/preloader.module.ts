import { NgModule } from '@angular/core';
import { CoreModule } from '../modules/core.module';
import { PipesModule } from '../modules/pipes.module';
import { PreloaderService } from './preloader.service';
import { PreloaderDirective } from './preloader.directive';
import { PreloaderComponent } from './preloader/preloader.component';

const declarations = [PreloaderDirective, PreloaderComponent];

@NgModule({
  declarations,
  imports: [CoreModule, PipesModule],
  providers: [PreloaderService],
  exports: declarations,
})
export class PreloaderModule {}
