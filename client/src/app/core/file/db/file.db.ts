export const FileDB = {
  errorMsgs: {
    minSize: 'Минимальный размер файла: {{size}} Mb.',
    maxSize: 'Максимальный размер файла: {{size}} Mb.',
    mime: 'Допустимые форматы файла: {{mime}}',
  },
  settings: {
    img: {
      mime: ['image/jpeg', 'image/png'],
      size: {
        max: 5, // Mb
      },
    },
  },
} as const;

export type TFileDB = typeof FileDB;
