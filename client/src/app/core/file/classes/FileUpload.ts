import { TFile, TImgPayload } from '../types/file.types';
export class FileUpload<T extends TFile> {
  constructor(
    public file: File,
    public payload: T extends 'IMG' ? TImgPayload : null = null,
  ) {}
}
