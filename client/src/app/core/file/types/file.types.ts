import { FileError } from '../classes/FileError';

export type TFile = 'IMG';
export type TFileError = 'minSize' | 'maxSize' | 'mime';

export type TFileCheckHandler = (
  file: File,
  params: TFileCheckParams,
) => FileError;

export type TFileValidate = {
  isValid: boolean;
  errors: string[];
};

export type TFileSize = {
  min?: number;
  max?: number;
};

export type TFileCheckParams = {
  mime?: string[];
  min?: number;
  max?: number;
};

export type TImgPayload = {
  preview: string;
};

export type TImgPreview = TImgPayload & {
  default: string;
};
