import { FileRequestBody } from '../classes/FileRequestBody';

export function convertToFormData<T extends object>(
  { json, file }: FileRequestBody<T>,
  fileProp: string = 'file',
  payloadProp: string = 'payload',
) {
  const formData = new FormData();

  if (file) formData.append(fileProp, file, file.name);
  if (json) formData.append(payloadProp, JSON.stringify(json));

  return formData;
}

export function extname(file: File): string {
  return file.name.match(/[.]\w+$/)[0];
}
