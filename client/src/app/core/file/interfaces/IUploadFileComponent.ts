import { IButtonComponent } from '../../components/buttons/utils/interfaces/IButtonComponent';
import { TFile, TFileCheckParams } from '../types/file.types';

export interface IUploadFileComponent
  extends Omit<IButtonComponent, 'type' | 'action'> {
  file: File;
  fileType: TFile;
  fileCheckParams: TFileCheckParams;
  multiple: boolean;

  getFile: () => File;
  changeFile: (e: InputEvent) => void;
}
