import { TButtonShape } from '@core/components/buttons/utils/types/buttons.types';
import { IUploadFileComponent } from '@core/file/interfaces/IUploadFileComponent';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { WINDOW, WindowDocument } from '@core/utils/injection/app-tokens';
import { NbButtonAppearance, NbComponentSize } from '@nebular/theme';
import { FileUpload } from '../../classes/FileUpload';
import { FileService } from '../../services/file.service';
import { TFile, TFileCheckParams } from '../../types/file.types';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'ng-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  providers: [FileService],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: 'uploadRef',
})
export class UploadFileComponent implements IUploadFileComponent {
  public file: File;

  @Input() fileType: TFile = 'IMG';
  @Input() fileCheckParams: TFileCheckParams;
  @Input() multiple: boolean = false;
  @Input() showErrorToastr: boolean = true;
  @Input() text: string;
  @Input() icon: string = 'cloud_upload';
  @Input() disabled: boolean = false;
  @Input() appearance: NbButtonAppearance = 'filled';
  @Input() shape: TButtonShape = 'circle';
  @Input() size: NbComponentSize = 'medium';

  @Output('changeFile') _changeFile = new EventEmitter<FileUpload<TFile>>();

  @ViewChild('fileRef') fileRef$: ElementRef<HTMLInputElement>;

  constructor(
    @Inject(WINDOW) private window: WindowDocument,
    private toastrService: ToastrService,
    private fileService: FileService,
  ) {}

  public getAccept(): string {
    const mime: string[] =
      this.fileCheckParams?.mime || this.getAcceptByType(this.fileType);

    return mime.toString();
  }

  private getAcceptByType(fileType: TFile): string[] {
    switch (fileType) {
      case 'IMG':
        return this.fileService.getDataFromDB(['settings', 'img', 'mime']);
    }
  }

  public changeFile(e: Event): void {
    const file: File = (e.target as HTMLInputElement).files[0];
    const defaultCheckParams: TFileCheckParams = this.fileService.getFileCheckParams(
      this.fileType,
    );
    const checkParams: TFileCheckParams =
      this.fileCheckParams || defaultCheckParams;
    const { isValid, errors } = this.fileService.checkFile(file, checkParams);

    isValid ? this.sendFile(file) : this.showErrors(errors);
  }

  private sendFile(file: File): void {
    const payload: FileUpload<TFile>['payload'] = this.getPayloadByType(file);
    const upload: FileUpload<TFile> = new FileUpload(file, payload);

    this.file = file;
    this._changeFile.emit(upload);
  }

  private showErrors(errors: string[]): void {
    if (this.showErrorToastr) this.toastrService.danger(errors.join('\n'));
  }

  private getPayloadByType(file: File): FileUpload<TFile>['payload'] {
    switch (this.fileType) {
      case 'IMG': {
        const preview: string = this.window.URL.createObjectURL(file);

        return { preview };
      }
      default:
        return null;
    }
  }

  // Output
  public getFile(): File {
    return this.file;
  }

  public select(): void {
    this.fileRef$.nativeElement.click();
  }

  public reset(): void {
    this.file = null;
  }
}
