import { FileUpload } from '@core/file/classes/FileUpload';
import { TFileCheckParams } from '@core/file/types/file.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  TPictureFit,
  TPictureShape,
} from '@core/components/picture/utils/picture.types';

@Component({
  selector: 'ng-upload-picture',
  templateUrl: './upload-picture.component.html',
  styleUrls: ['./upload-picture.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploadPictureComponent {
  public preview: string;

  // Shared params
  @Input('picType') picType: 'empty' | 'theme' = 'empty';
  @Input('objectFit') objectFit: TPictureFit = 'cover';
  @Input('shape') shape: TPictureShape = 'round';
  @Input('alt') alt: string = 'img';
  @Input('imgUrl') imgUrl: string;
  @Input('fileCheckParams') fileCheckParams: TFileCheckParams;
  @Input('showErrorToastr') showErrorToastr: boolean = true;
  @Input('disabled') disabled: boolean = false;

  // Default picture
  @Input('defaultUrl') defaultUrl: string;

  // Theme picture
  @Input('darkUrl') darkUrl: string;
  @Input('lightUrl') lightUrl: string;

  // Shared events
  @Output('changeFile') private _changeFile = new EventEmitter<
    FileUpload<'IMG'>
  >();

  public changeFile(upload: FileUpload<'IMG'>): void {
    const {
      payload: { preview },
    } = upload;
    this.preview = preview;
    this._changeFile.emit(upload);
  }

  public reset(): void {
    this.preview = null;
  }
}
