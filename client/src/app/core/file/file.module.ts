import { NgModule } from '@angular/core';
import { DirectivesModule } from '@core/modules/directives.module';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { UploadPictureComponent } from './components/upload-picture/upload-picture.component';
import { ComponentsModule } from '../modules/components.module';
import { CoreModule } from '../modules/core.module';
import { NebularModule } from '../modules/nebular.module';
import { PipesModule } from '../modules/pipes.module';

const declarations = [UploadFileComponent, UploadPictureComponent];

@NgModule({
  declarations,
  imports: [
    CoreModule,
    NebularModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule,
  ],
  exports: declarations,
})
export class FileModule {}
