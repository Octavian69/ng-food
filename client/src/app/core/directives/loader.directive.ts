import {
  ComponentFactoryResolver,
  Directive,
  ViewContainerRef,
  Input,
  ComponentRef,
  ComponentFactory,
  OnChanges,
  SimpleChanges,
  Injector,
  OnDestroy,
  OnInit,
  SimpleChange,
} from '@angular/core';
import { SquareLoaderComponent } from '@core/components/square-loader/square-loader.component';
import { NbComponentSize } from '@nebular/theme';

export type TLoaderCubeSize = Extract<
  NbComponentSize,
  'small' | 'medium' | 'large'
>;

@Directive({
  selector: '[ngLoader]',
})
export class LoaderDirective implements OnChanges, OnDestroy {
  private instance$: ComponentRef<SquareLoaderComponent>;

  @Input('ngLoader') loadingStatus: boolean;
  @Input('ngLoaderOversize') oversize: boolean;
  @Input('ngLoaderCenter') contentCenter: boolean;
  @Input('ngLoaderCubeSize') cubeSize: TLoaderCubeSize;

  constructor(
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private viewContainer: ViewContainerRef,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    this.makeChanges(changes);
  }

  ngOnDestroy() {
    this.hide();
  }

  public loading(status: boolean): void {
    status ? this.show() : this.hide();
  }

  public hide(): void {
    const idx: number = this.viewContainer.indexOf(this.instance$?.hostView);

    if (~idx) {
      this.viewContainer.remove(idx);
    }
  }

  public show(): void {
    const factory: ComponentFactory<SquareLoaderComponent> = this.resolver.resolveComponentFactory(
      SquareLoaderComponent,
    );
    this.instance$ = this.viewContainer.createComponent(
      factory,
      0,
      this.injector,
    );
  }

  public updateInstance<
    K extends keyof SquareLoaderComponent,
    V extends SquareLoaderComponent[K]
  >(key: K, value: V): void {
    this.instance$.instance[key] = value;
    this.instance$.instance.detect();
  }

  private makeChanges(changes: SimpleChanges): void {
    if (changes.loadingStatus) {
      this.loading(changes.loadingStatus.currentValue);
      delete changes.loading;
    }

    if (this.instance$) {
      Object.entries(changes).forEach(
        ([key, { currentValue }]: [
          keyof SquareLoaderComponent,
          SimpleChange,
        ]) => {
          this.updateInstance(key, currentValue);
        },
      );
    }
  }
}
