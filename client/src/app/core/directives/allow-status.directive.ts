import { Directive, HostBinding, Input } from '@angular/core';
import { TAllowStatus } from '../form/types/form.types';

@Directive({
  selector: '[ngAllowStatus]',
})
export class AllowStatusDirective {
  @HostBinding('attr.data-status') dataStatus: TAllowStatus = 'enable';

  @Input('ngAllowStatus') set status(value: boolean) {
    this.dataStatus = value ? 'disabled' : 'enable';
  }
}
