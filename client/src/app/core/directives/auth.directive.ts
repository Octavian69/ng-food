import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { LoginService } from '@src/auth/services/login.service';
import { TAuthStatus } from '@src/auth/types/auth.types';
import { filter } from 'rxjs/operators';
import {
  ChangeDetectorRef,
  Directive,
  OnDestroy,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@UntilDestroy()
@Directive({
  selector: '[ngIfAuth]',
})
export class AuthDirective implements OnDestroy {
  private status: TAuthStatus;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private cdr: ChangeDetectorRef,
    private loginService: LoginService,
  ) {}

  ngOnDestroy() {}

  ngAfterViewInit() {
    this.init();
  }

  private init(): void {
    this.loginService
      .getStream('auth$')
      .pipe(
        untilDestroyed(this),
        filter((status) => status !== this.status),
      )
      .subscribe((status: TAuthStatus) => {
        this.status = status;
        this.status === 'authorized' ? this.show() : this.hide();
        this.cdr.detectChanges();
      });
  }

  private show(): void {
    this.viewContainer.createEmbeddedView(this.templateRef);
  }

  private hide(): void {
    this.viewContainer.clear();
  }
}
