import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { nativeElement } from '@handlers/components.handlers';

@Directive({
  selector: '[ngMutation]',
  exportAs: 'mutateRef',
})
export class MutationDirective implements AfterViewInit, OnDestroy {
  private mutate$: BehaviorSubject<MutationRecord[]> = new BehaviorSubject(
    null,
  );
  private observer$: MutationObserver;

  @Input('ngMutation') private config: MutationObserverInit;
  @Output('mutate') private _mutate = new EventEmitter<MutationRecord[]>();

  constructor(private el$: ElementRef<HTMLElement>) {}

  ngAfterViewInit() {
    this.initObserver();
  }

  ngOnDestroy() {
    this.observer$.disconnect();
  }

  private initObserver(): void {
    this.observer$ = new MutationObserver(this.mutate);
    this.observer$.observe(nativeElement(this.el$), this.config);
  }

  private mutate = (record: MutationRecord[]): void => {
    this.mutate$.next(record);
    this._mutate.emit(record);
  };

  public listen(): Observable<MutationRecord[]> {
    return this.mutate$.asObservable();
  }
}
