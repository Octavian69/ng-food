import {
  Directive,
  ViewContainerRef,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Type,
  Injector,
} from '@angular/core';

@Directive({
  selector: '[ngInsertComponent]',
})
export abstract class InsertComponentDirective<T extends object> {
  protected instance$: ComponentRef<T>;

  constructor(
    protected resolver: ComponentFactoryResolver,
    protected injector: Injector,
    protected viewContainer: ViewContainerRef,
  ) {}

  protected insert(component: Type<T>): void {
    if (this.instance$) this.reset();

    const factory: ComponentFactory<T> = this.resolver.resolveComponentFactory(
      component,
    );
    this.instance$ = this.viewContainer.createComponent(
      factory,
      0,
      this.injector,
    );
  }

  protected reset(): void {
    this.viewContainer.clear();
    this.instance$ = null;
  }
}
