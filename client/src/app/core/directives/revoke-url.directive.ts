import { Directive, HostListener, Inject, Input } from '@angular/core';
import { WINDOW, WindowDocument } from '@core/utils/injection/app-tokens';

@Directive({
  selector: '[ngRevokeUrl]',
})
export class RevokeUrlDirective {
  @Input('ngRevokeUrl') preview: string;

  constructor(@Inject(WINDOW) private window: WindowDocument) {}

  @HostListener('load')
  private load(): void {
    if (this.preview) {
      this.window.URL.revokeObjectURL(this.preview);
    }
  }
}
