import { nativeElement } from '@handlers/components.handlers';
import { isNotEqual } from '@handlers/conditions.handlers';
import { TSide } from '../types/state.types';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  Renderer2,
} from '@angular/core';
import {
  TAlignPosition,
  TCssDirection,
  TPlacementPosition,
} from '@core/types/state.types';

@Directive({
  selector: '[ngPlacement]',
})
export class PlacementDirective implements AfterViewInit {
  private directionMap: Map<TSide, TCssDirection> = new Map([
    ['left', 'row'],
    ['right', 'row-reverse'],
    ['top', 'column'],
    ['bottom', 'column-reverse'],
  ]);
  private alignMap: Map<TAlignPosition, string> = new Map([
    ['start', 'flex-start'],
    ['center', 'center'],
    ['end', 'flex-end'],
  ]);
  private offsetMap: Map<TSide, TSide> = new Map([
    ['left', 'right'],
    ['right', 'left'],
    ['top', 'bottom'],
    ['bottom', 'top'],
  ]);

  @HostBinding('style.display') display: string = 'flex';
  @HostBinding('style.flexDirection') flexDirection: TCssDirection = 'row';

  @Input('ngPlacement') placement: TPlacementPosition = 'left-start';
  @Input('ngPlacementOffset') offset: string = '1rem';

  constructor(
    private renderer: Renderer2,
    private cdr: ChangeDetectorRef,
    private el$: ElementRef<HTMLElement>,
  ) {}

  ngAfterViewInit() {
    if (isNotEqual(this.placement, 'none')) {
      this.setPlacement(this.placement);
    }
  }

  private setPlacement(placement: TPlacementPosition): void {
    const [side, position] = placement.split('-');
    const direction: TCssDirection = this.directionMap.get(side as TSide);

    this.flexDirection = direction;

    this.alignChild(position as TAlignPosition);
    this.offsetChild(side as TSide);

    this.cdr.detectChanges();
  }

  private alignChild(position: TAlignPosition) {
    const alignChild = nativeElement(this.el$).children[0];
    const alignPosition = this.alignMap.get(position);

    this.renderer.setStyle(alignChild, 'align-self', alignPosition);
  }

  private offsetChild(side: TSide): void {
    const offsetChild = nativeElement(this.el$).children[0];
    const offsetSide: TSide = this.offsetMap.get(side);

    this.renderer.setStyle(offsetChild, `margin-${offsetSide}`, this.offset);
  }
}
