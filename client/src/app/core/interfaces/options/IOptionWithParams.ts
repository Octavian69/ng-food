import { IOption } from './IOption';
import { IOptionParams } from './IOptionParams';

export interface IOptionWithParams<P extends Array<keyof IOptionParams>, S extends object = null> extends IOption {
  params: {
    [K in P[number]]: IOptionParams<S>[K]
  };
}



