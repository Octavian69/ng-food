import { ITooltipOption } from "./ITooltipOption";

export interface IOptionParams<S extends object = null> {
  icon: string;
  path: string;
  link: string;
  action: string;
  tooltip: ITooltipOption;
  img: string;
  special: S;
}