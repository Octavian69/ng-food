import { NbComponentStatus, NbPosition, NbTrigger } from "@nebular/theme";

export interface ITooltipOption {
  text: string,
  placement?: NbPosition,
  trigger?: NbTrigger,
  icon?: string,
  status?: NbComponentStatus
}