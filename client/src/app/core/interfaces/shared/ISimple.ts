export interface ISimple<T> {
  [key: string]: T
}