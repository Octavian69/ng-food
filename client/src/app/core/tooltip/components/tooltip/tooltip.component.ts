import { Component, Input, TemplateRef } from '@angular/core';
import { ANShowFade } from '@core/animations/animations';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { ANShowTooltip } from '@core/tooltip/animations/tooltip.animations';
import { TSide } from '@core/types/state.types';
import { assign } from '@handlers/utils.handlers';

@Component({
  selector: 'ng-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
  animations: [ANShowTooltip, ANShowFade()],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TooltipComponent {
  @Input() public tooltipText: unknown;
  @Input() public tooltipHtml: string;
  @Input() public tooltipTpl: TemplateRef<any>;
  @Input() public tooltipContext: unknown;
  @Input('display') public tooltipPlacement: TSide;
  @Input('posiiton') public position: ISimple<string>;

  public getTooltipStyles(): ISimple<string> {
    const { position } = this;

    return assign(position);
  }

  public getContent(): unknown {
    return this.tooltipHtml || this.tooltipText;
  }
}
