import {
  animate,
  style,
  transition,
  trigger
  } from '@angular/animations';
import { EnAnimation } from '@core/animations/enums/Animation.enum';
import { TSide } from '@core/types/state.types';

const STYLE_MAP = new Map<TSide, any>([
  ['top', { marginTop: '-1rem' }],
  ['bottom', { marginTop: '1rem' }],
  ['left', { marginLeft: '-1rem' }],
  ['right', { marginLeft: '1rem' }],
]);

const item = (side: TSide) => {
  const fromStyles = STYLE_MAP.get(side);
  const [property] = Object.keys(fromStyles);

  return [
    transition(`void => ${side}`, [
      style(fromStyles),
      animate(EnAnimation.DEFAULT_TIMING, style({ [property]: '*' })),
    ]),
    transition(
      `${side} => void`,
      animate(EnAnimation.DEFAULT_TIMING, style(fromStyles)),
    ),
  ];
};

export const ANShowTooltip = trigger('ANShowTooltip', [
  ...item('top'),
  ...item('bottom'),
  ...item('left'),
  ...item('right'),
]);
