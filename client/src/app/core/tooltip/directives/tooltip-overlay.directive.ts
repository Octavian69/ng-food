import { Directive, OnInit, ViewContainerRef } from '@angular/core';
import { TooltipService } from '../services/tooltip.service';

@Directive({
  selector: '[ngTooltipOverlay]',
})
export class TooltipOverlayDirective implements OnInit {
  constructor(
    private overlay$: ViewContainerRef,
    private tooltipService: TooltipService,
  ) {}

  ngOnInit() {
    this.tooltipService.registerOverlay(this.overlay$);
  }
}
