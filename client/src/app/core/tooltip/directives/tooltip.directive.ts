import { Bind } from '@core/form/decorators/decorators';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { TSide } from '@core/types/state.types';
import { nativeElement } from '@handlers/components.handlers';
import { excludeProps, timeoutDelay } from '@handlers/utils.handlers';
import { TooltipComponent } from '../components/tooltip/tooltip.component';
import { TooltipService } from '../services/tooltip.service';
import {
  AfterViewInit,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  ElementRef,
  HostListener,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChange,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';

@Directive({
  selector: '[ngTooltip]',
  exportAs: 'ngTooltipRef',
})
export class TooltipDirective implements AfterViewInit, OnChanges, OnDestroy {
  private tooltipInstance$: ComponentRef<TooltipComponent>;

  @Input('ngTooltip') private tooltipText: unknown;
  @Input('ngTooltipHtml') private tooltipHtml: string;
  @Input('ngTooltipTemplate') private tooltipTpl: TemplateRef<any>;
  @Input('ngTooltipContext') private tooltipContext: unknown;
  @Input('ngTooltipPlacement') private tooltipPlacement: TSide = 'left';
  @Input('ngTooltipDisabled') private disabled: boolean = false;

  constructor(
    private el$: ElementRef,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private tooltipService: TooltipService,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    this.applyChanges(changes);
  }

  ngAfterViewInit(): void {
    this.createInstance();
  }

  ngOnDestroy() {
    this.tooltipService.destroy(this.tooltipInstance$);
  }

  private createInstance(): void {
    const factory: ComponentFactory<TooltipComponent> = this.resolver.resolveComponentFactory(
      TooltipComponent,
    );
    this.tooltipInstance$ = this.tooltipService.append(factory, this.injector);

    this.tooltipInstance$.hostView;

    this.tooltipInstance$.instance.tooltipTpl = this.tooltipTpl;
    this.tooltipInstance$.instance.tooltipContext = this.tooltipContext;
    this.tooltipInstance$.instance.tooltipHtml = this.tooltipHtml;
    this.tooltipInstance$.instance.tooltipText = this.tooltipText;
  }

  private setCoordinates(): void {
    const element$: HTMLElement = nativeElement(this.el$);
    const {
      top,
      bottom,
      left,
      right,
      height,
      width,
    }: DOMRect = element$.getBoundingClientRect();
    const { instance } = this.tooltipInstance$;
    const position: ISimple<string> = {};

    switch (this.tooltipPlacement) {
      case 'top': {
        position.top = `${top}px`;
        position.left = `${left + width / 2}px`;
        position.transform = 'translate(-50%, calc(-100% - 1rem))';
        break;
      }
      case 'bottom': {
        position.top = `${bottom}px`;
        position.left = `${left + width / 2}px`;
        position.transform = 'translate(-50%, 1rem)';
        break;
      }
      case 'left': {
        position.left = `${left}px`;
        position.top = `${top + height / 2}px`;
        position.transform = 'translate(calc(-100% - 1rem), -50%)';
        break;
      }
      case 'right': {
        position.left = `${right}px`;
        position.top = `${top + height / 2}px`;
        position.transform = 'translate(1rem, -50%)';
        break;
      }
    }

    instance.position = position;
  }

  @Bind()
  private applyChanges(changes: SimpleChanges) {
    if (this.tooltipInstance$) {
      const { instance } = this.tooltipInstance$;
      const spyProps = excludeProps<SimpleChanges>(changes, ['disabled']);
      const newComponentState = Object.entries(spyProps).reduce(
        (accum, [key, change]) => {
          accum[key] = (change as SimpleChange).currentValue;

          return accum;
        },
        {},
      );

      timeoutDelay(() => Object.assign(instance, newComponentState), 0);
    }
  }

  @HostListener('mouseenter')
  public show(): void {
    if (!this.disabled) {
      this.setCoordinates();
      this.tooltipInstance$.instance.tooltipPlacement = this.tooltipPlacement;
    }
  }

  @HostListener('mouseleave')
  public hide(): void {
    this.tooltipInstance$.instance.tooltipPlacement = null;
  }
}
