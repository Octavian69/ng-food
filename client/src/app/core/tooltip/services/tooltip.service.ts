import { TooltipComponent } from '../components/tooltip/tooltip.component';
import {
  ComponentFactory,
  ComponentRef,
  Injectable,
  Injector,
  ViewContainerRef,
} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TooltipService {
  private overlay$: ViewContainerRef;

  public registerOverlay(viewContainer: ViewContainerRef): void {
    this.overlay$ = viewContainer;
  }

  public append(
    componentFactory: ComponentFactory<TooltipComponent>,
    injector: Injector,
  ): ComponentRef<TooltipComponent> {
    return this.overlay$.createComponent(componentFactory, 0, injector);
  }

  public destroy(componentFactory: ComponentRef<TooltipComponent>): void {
    const idx: number = this.overlay$.indexOf(componentFactory.hostView);

    if (~idx) {
      this.overlay$.remove(idx);
    }
  }

  public clear(): void {
    this.overlay$.clear();
  }
}
