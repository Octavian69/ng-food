import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule } from '@angular/core';
import { CoreModule } from '@core/modules/core.module';
import { TooltipOverlayDirective } from './directives/tooltip-overlay.directive';
import { TooltipDirective } from './directives/tooltip.directive';

const declarations = [TooltipOverlayDirective, TooltipDirective];

@NgModule({
  declarations,
  imports: [CoreModule, OverlayModule],
  exports: declarations,
})
export class TooltipModule {}
