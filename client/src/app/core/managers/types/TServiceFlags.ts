export type TServiceFlags<T extends object> = {
  [K in keyof T]: boolean
}