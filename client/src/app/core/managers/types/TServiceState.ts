export type TServiceState<T> = {
  [K in keyof T]: T[K]
}