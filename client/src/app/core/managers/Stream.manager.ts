import { HttpErrorResponse } from '@angular/common/http';
import { Enumerable } from '@core/decorators/decorators';
import { BehaviorSubject, MonoTypeOperatorFunction, Observable } from 'rxjs';
import { DetectorManager } from './Detector.manager';
import { ErrorManager } from './Error.manager';
import { IStateAction } from './interfaces/IStateAction';
import { IStreamManagers } from './interfaces/IStreamManagers';
import { StreamError } from './models/StreamError';
import { StateActionManager } from './StateAction.manager';
import { UnsubscribeManager } from './Unsubscribe.manager';
import { TRxSubject } from '../types/libs.types';
import {
  TDestroyStream,
  TDetectionEvent,
  TErrorEvent,
  TStreamManager,
} from './types/managers.types';

function registerManager(managers: TStreamManager[], type: TStreamManager) {
  if (!managers.includes(type)) return null;

  switch (type) {
    case 'unsubscribe':
      return new UnsubscribeManager();
    case 'action':
      return new StateActionManager();
    case 'error':
      return new ErrorManager();
    case 'detector':
      return new DetectorManager();
  }
}

export function StreamManager<A extends IStateAction>(
  managers: Array<TStreamManager> = [
    'unsubscribe',
    'action',
    'error',
    'detector',
  ],
) {
  class StreamController {
    static initializeMethod: string = '_initializeStreamManagers';
    private _streamManagers: IStreamManagers<A>;

    @Enumerable()
    private _initializeStreamManagers(): void {
      this._streamManagers = {
        unsubscribeManager: registerManager(
          managers,
          'unsubscribe',
        ) as IStreamManagers<A>['unsubscribeManager'],

        actionManager: registerManager(
          managers,
          'action',
        ) as IStreamManagers<A>['actionManager'],

        errorManager: registerManager(
          managers,
          'error',
        ) as IStreamManagers<A>['errorManager'],

        detectorManager: registerManager(
          managers,
          'detector',
        ) as IStreamManagers<A>['detectorManager'],
      };
    }

    @Enumerable()
    private getSubject<T>(streamName: string): TRxSubject<T> {
      try {
        const stream$: TRxSubject<T> = this[streamName];

        if (!stream$) throw new Error('Invalid stream name');

        return this[streamName] as TRxSubject<T>;
      } catch (error) {
        console.error(error);
      }
    }

    @Enumerable()
    public exclude(managers: TStreamManager[]): void {
      managers.forEach((manager) => {
        const managerKey = `${manager}Manager`;
        this._streamManagers[managerKey] = null;
      });
    }

    @Enumerable()
    public getStream<T>(streamName: string): Observable<T> {
      return this.getSubject<T>(streamName).asObservable();
    }

    @Enumerable()
    public getStreamValue<T>(streamName: string): T {
      try {
        const sub$: TRxSubject<T> = this.getSubject<T>(streamName);

        if (sub$ instanceof BehaviorSubject) {
          return sub$.getValue();
        }

        throw new Error(
          `${streamName} is not an instance of the class: BehaviorSubject`,
        );
      } catch (e) {
        console.error(e);
      }
    }

    @Enumerable()
    public emitToStream<T>(streamName: string, value: T): void {
      const stream$: TRxSubject<T> = this.getSubject<T>(streamName);

      stream$.next(value);
    }

    @Enumerable()
    public action(stateAction: A): void {
      this._streamManagers.actionManager.action(stateAction);
    }

    @Enumerable()
    public listen(): Observable<A> {
      return this._streamManagers.actionManager.listen();
    }

    @Enumerable()
    public untilDestroyed(): MonoTypeOperatorFunction<unknown> {
      return this._streamManagers.unsubscribeManager.untilDestroyed();
    }

    @Enumerable()
    public untilDestroyedByType(
      type: string,
    ): MonoTypeOperatorFunction<unknown> {
      return this._streamManagers.unsubscribeManager.untilDestroyedByType(type);
    }

    @Enumerable()
    public unsubscribe(): void {
      this._streamManagers.unsubscribeManager.unsubscribe();
    }

    @Enumerable()
    public unsubscribeByTypes(subTypes: string[]): void {
      this._streamManagers.unsubscribeManager.unsubscribeByTypes(subTypes);
    }

    @Enumerable()
    public fullUnsubscribe(): void {
      this._streamManagers.unsubscribeManager.fullUnsubscribe();
    }

    @Enumerable()
    public detect(type: TDetectionEvent<A>): void {
      this._streamManagers.detectorManager.detectChanges(type);
    }

    @Enumerable()
    public detectChanges(): Observable<TDetectionEvent<A>> {
      return this._streamManagers.detectorManager.listenDetectChanges();
    }

    @Enumerable()
    public listenErrors(): Observable<StreamError<TErrorEvent<A>>> {
      return this._streamManagers.errorManager.listen();
    }

    @Enumerable()
    public detectError(action: TErrorEvent<A>): (e: HttpErrorResponse) => void {
      return (e: HttpErrorResponse) =>
        this._streamManagers.errorManager.error(e, action);
    }

    @Enumerable()
    public destroyStreams(streams: TDestroyStream[]): void {
      streams.forEach((stream) => {
        if (typeof stream === 'string') {
          this[stream].next(null);
        } else {
          const { streamKey, value } = stream;

          this[streamKey].next(value);
        }
      });
    }
  }

  return new StreamController();
}
