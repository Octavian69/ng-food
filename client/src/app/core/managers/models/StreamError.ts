import { HttpErrorResponse } from '@angular/common/http';

export class StreamError<E extends string> {
  constructor(error: HttpErrorResponse, type: E) {}
}
