import { DatabaseManager } from '@core/managers/Database.manager';
import { StateManager } from '@core/managers/State.manager';
import { Constructor } from '../types/managers.types';

export interface IStateManagers<
  F extends Constructor<InstanceType<F>> = null,
  S extends Constructor<InstanceType<S>> = null,
  DB extends object = null
> {
  flags: StateManager<F>;
  state: StateManager<S>;
  db: DatabaseManager<DB>;
}
