import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { distinctUntilChanged } from 'rxjs/operators';
import { ThemeService } from '../services/theme.service';
import { TAppTheme } from '../types/theme.types';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';

@UntilDestroy()
@Directive({
  selector: '[ngTheme]',
  exportAs: 'themeRef',
})
export class ThemeDirective implements OnInit, AfterViewInit {
  public currentTheme: TAppTheme;

  @Output('setTheme') public _setTheme = new EventEmitter<TAppTheme>();

  constructor(
    private cdr: ChangeDetectorRef,
    private renderer: Renderer2,
    private el$: ElementRef<HTMLElement>,
    private themeService: ThemeService,
  ) {}

  ngOnInit() {
    this.init();
  }

  ngAfterViewInit() {
    this.applyTheme();
  }

  private init(): void {
    this.themeService
      .onThemeChange()
      .pipe(distinctUntilChanged(), untilDestroyed(this))
      .subscribe(this.setThemeMode);
  }

  get current(): TAppTheme {
    return this.currentTheme;
  }

  private setThemeMode = (themeType: TAppTheme): void => {
    this.currentTheme = themeType;
    this.applyTheme();
    this._setTheme.emit(themeType);
  };

  private applyTheme(): void {
    const { nativeElement } = this.el$;
    this.renderer.setAttribute(nativeElement, 'data-theme', this.currentTheme);
    this.cdr.detectChanges();
  }
}
