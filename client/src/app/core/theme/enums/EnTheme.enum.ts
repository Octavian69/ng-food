export enum EnTheme {
  DARK = 'dark_mode',
  LIGHT = 'light_mode',
}
