import { ModuleWithProviders, NgModule } from '@angular/core';
import { ThemeService } from './services/theme.service';

@NgModule({})
export class ThemeModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return {
      ngModule: ThemeModule,
      providers: [ThemeService],
    };
  }
}
