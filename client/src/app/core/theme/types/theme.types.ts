import { EnTheme } from '../enums/EnTheme.enum';

export type TAppTheme = EnTheme.DARK | EnTheme.LIGHT;
export type ThemeDecor = 'default' | 'reverse';
export type ThemeChanges = {
  name: TAppTheme;
  previous: TAppTheme;
};
export type ThemeOption<T> = {
  [key in TAppTheme]: T;
};
