import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StreamManager } from '@core/managers/Stream.manager';
import { NbThemeService } from '@nebular/theme';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from '../../services/storage.service';
import { EnTheme } from '../enums/EnTheme.enum';
import { TAppTheme } from '../types/theme.types';

@Injectable({
  providedIn: 'root',
})
export class ThemeService extends ExtendsFactory(StreamManager()) {
  private theme$: BehaviorSubject<TAppTheme> = new BehaviorSubject(
    this.getCurrentTheme(),
  );

  constructor(
    private themeService: NbThemeService,
    private storage: StorageService,
  ) {
    super();
    this.init();
  }

  private init(): void {
    const theme: TAppTheme =
      this.storage.get<TAppTheme>('theme') || EnTheme.DARK;

    this.setTheme(theme);
  }

  public onThemeChange(): Observable<TAppTheme> {
    return this.theme$.asObservable();
  }

  public getCurrentTheme(): TAppTheme {
    return this.themeService.currentTheme as TAppTheme;
  }

  public getThemeMessage(): string {
    const theme: TAppTheme = this.getCurrentTheme();

    let themeTag: string;

    switch (theme) {
      case EnTheme.LIGHT: {
        themeTag = 'Темную';
        break;
      }

      case EnTheme.DARK: {
        themeTag = 'Светлую';
        break;
      }
    }

    return `Сменить тему на "${themeTag}"`;
  }

  public toggleTheme(): void {
    const theme =
      this.themeService.currentTheme === EnTheme.DARK
        ? EnTheme.LIGHT
        : EnTheme.DARK;

    this.setTheme(theme);
  }

  private setTheme(theme: TAppTheme): void {
    this.themeService.changeTheme(theme);
    this.theme$.next(theme);
    this.storage.set('theme', theme);
  }
}
