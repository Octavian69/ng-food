import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@core/modules/directives.module';
import { PipesModule } from '@core/modules/pipes.module';
import { TooltipModule } from '@core/tooltip/tooltip.module';
import { FormControlComponent } from './components/form-control/form-control.component';
import { FormDatepickerRangeComponent } from './components/form-datepicker-range/form-datepicker-range.component';
import { FormErrorsComponent } from './components/form-errors/form-errors.component';
import { FormInputNumberComponent } from './components/form-input-number/form-input-number.component';
import { FormInputPasswordComponent } from './components/form-input-password/form-input-password.component';
import { FormInputToggleControlComponent } from './components/form-input-toggle/form-input-toggle.component';
import { FormInputComponent } from './components/form-input/form-input.component';
import { FormMessagesComponent } from './components/form-messages/form-messages.component';
import { FormPendingsComponent } from './components/form-pendings/form-pendings.component';
import { FormQuantityComponent } from './components/form-quantity/form-quantity.component';
import { FormRadioComponent } from './components/form-radio/form-radio.component';
import { FormRangeComponent } from './components/form-range/form-range.component';
import { FormSelectOptionComponent } from './components/form-select/form-select-option/form-select-option.component';
import { FormSelectComponent } from './components/form-select/form-select.component';
import { FormTextareaToggleControlComponent } from './components/form-textarea-toggle/form-textarea-toggle.component';
import { FormTextareaComponent } from './components/form-textarea/form-textarea.component';
import { FormToggleValueComponent } from './components/form-toggle-value/form-toggle-value.component';
import { FormValueLengthComponent } from './components/form-value-length/form-value-length.component';
import { ComponentsModule } from '../modules/components.module';
import { CoreModule } from '../modules/core.module';
import { NebularModule } from '../modules/nebular.module';

const declarations = [
  FormInputComponent,
  FormTextareaComponent,
  FormInputNumberComponent,
  FormInputToggleControlComponent,
  FormRadioComponent,
  FormTextareaToggleControlComponent,
  FormToggleValueComponent,
  FormMessagesComponent,
  FormValueLengthComponent,
  FormControlComponent,
  FormInputPasswordComponent,
  FormErrorsComponent,
  FormPendingsComponent,
  FormRangeComponent,
  FormDatepickerRangeComponent,
  FormQuantityComponent,
  FormSelectComponent,
  FormSelectOptionComponent,
];

@NgModule({
  declarations,
  imports: [
    CoreModule,
    NebularModule,
    DirectivesModule,
    ComponentsModule,
    PipesModule,
    NgxSliderModule,
    TooltipModule,
  ],
  exports: [...declarations, NgxSliderModule],
})
export class FormModule {}
