import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { TextControlComponent } from '@core/form/classes/TextControlComponent';
import { isEqual } from '@handlers/conditions.handlers';

@Component({
  selector: 'ng-form-quantity',
  templateUrl: './form-quantity.component.html',
  styleUrls: ['./form-quantity.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [new FormControlProvider(FormQuantityComponent)],
})
export class FormQuantityComponent extends TextControlComponent {
  @Input() mode: 'horizontal' | 'vertical' = 'horizontal';
  @Input() min: number = 0;
  @Input() max: number = 1000;

  public setValue(value: number): void {
    this.value = value;
    super.setValue(value);
  }

  writeValue(value: any): void {
    if (isEqual(value, null)) {
      this.value = value;
    } else {
      value = Number(value) || this.min || 0;
      this.value =
        value > this.max ? this.max : value < this.min ? this.min : value;
    }

    this.cdr.detectChanges();
  }
}
