import { BaseFormControlComponent } from '@core/form/classes/BaseFormControlComponent';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { RangeValue } from '@core/form/classes/RangeValue';
import {
  ChangeContext,
  Options,
  TranslateFunction,
} from '@angular-slider/ngx-slider';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'ng-form-range',
  templateUrl: './form-range.component.html',
  styleUrls: ['./form-range.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [new FormControlProvider(FormRangeComponent)],
})
export class FormRangeComponent
  extends BaseFormControlComponent
  implements OnInit {
  public minValue: number;
  public maxValue: number;

  @Input('colorTheme') colorTheme: 'default' | 'reverse' = 'default';
  @Input('translate') public set translate(value: TranslateFunction) {
    this.options.translate = value;
  }
  @Input() private set min(value: number) {
    this.options.floor = value;
  }
  @Input() private set max(value: number) {
    this.options.ceil = value;
  }
  @Input() private set step(value: number) {
    this.options.step = value;
  }
  @Input() private set disable(value: boolean) {
    this.options.disabled = value;
  }

  @Input('options') options: Options = {
    floor: 0,
    ceil: 100,
    step: 1,
    disabled: false,
  };

  public setValue({ value: start, highValue: end }: ChangeContext) {
    const value: RangeValue<number> = new RangeValue(start, end);

    this.onChange(value);
    this.onTouched();
    this.updateValue(value);
  }

  private updateValue({ start = null, end = null }: RangeValue<number>): void {
    this.minValue = start;
    this.maxValue = end;
    this.cdr.detectChanges();
  }

  writeValue(newValue: any) {
    const start: number = newValue?.start || this.options.floor;
    const end: number = newValue?.end || this.options.ceil;

    this.updateValue({ start, end });
  }

  setDisabledState(status: boolean): void {
    Object.assign(this.options, { disabled: status });
  }
}
