import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { TrackBy } from '@core/decorators/decorators';
import { TPengingTemplate } from '@core/form/types/form.types';

@Component({
  selector: 'ng-form-pendings',
  templateUrl: './form-pendings.component.html',
  styleUrls: ['./form-pendings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormPendingsComponent {
  public isShowTpls: boolean = false;
  public templates: Exclude<TPengingTemplate, string>[] = [];
  public messages: Extract<TPengingTemplate, string>[] = [];

  @Input('pending') set _pending(value: boolean) {
    this.isShowTpls = value;
  }

  @Input('messages') set _messages(value: TPengingTemplate[]) {
    this.sortMessages(value);
  }

  private sortMessages(value: TPengingTemplate[]): void {
    this.reset();

    value?.forEach((tpl) => {
      if (typeof tpl === 'string') {
        this.messages.push(tpl);
      } else {
        this.templates.push(tpl);
      }
    });
  }

  @TrackBy('index')
  public trackByFn() {}

  private reset(): void {
    this.templates = [];
    this.messages = [];
  }
}
