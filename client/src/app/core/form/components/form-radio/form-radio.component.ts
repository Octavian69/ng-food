import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TrackBy } from '@core/decorators/decorators';
import { BaseFormControlComponent } from '@core/form/classes/BaseFormControlComponent';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { TControlOption } from '@core/form/types/form.types';
import { TCssDirection, TPlacementPosition } from '@core/types/state.types';

@Component({
  selector: 'ng-form-radio',
  templateUrl: './form-radio.component.html',
  styleUrls: ['./form-radio.component.scss'],
  providers: [new FormControlProvider(FormRadioComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormRadioComponent extends BaseFormControlComponent {
  @Input() optionLabelPlacement: TPlacementPosition = 'bottom-center';
  @Input() optionsDirection: Extract<TCssDirection, 'row' | 'column'> = 'row';
  @Input() contentCenter: boolean = true;
  @Input() groupOptions: TControlOption<unknown>[];
  @Input() disabledOptions: Array<unknown> = [];

  public setValue(value: unknown): void {
    if (!this.isDisabledOption(value)) {
      this.value = value;
      this.onChange(this.value);
      this.onTouched();
    }
  }

  public writeValue(value: unknown): void {
    this.value = value;
  }

  @TrackBy('property', 'id')
  public trackByFn() {}

  public isDisabledOption(value: unknown): boolean {
    return this.disabledOptions.includes(value);
  }

  public isActiveOption(value: unknown): boolean {
    return this.value === value;
  }
}
