import { Component, Input } from '@angular/core';
import { isEqual } from '@handlers/conditions.handlers';
import { FormControlProvider } from '../../classes/FormControlProvider';
import { TextControlComponent } from '../../classes/TextControlComponent';
import { TRange } from '../../types/form.types';

@Component({
  selector: 'ng-form-input-number',
  templateUrl: './form-input-number.component.html',
  styleUrls: ['./form-input-number.component.scss'],
  providers: [new FormControlProvider(FormInputNumberComponent)],
})
export class FormInputNumberComponent extends TextControlComponent {
  @Input() min: string;
  @Input() max: string;
  @Input() isShowReset: boolean = true;

  public setValue(value: number): void {
    this.value = this.checkAndGetValue(value);
    super.setValue(value);
  }

  public resetValue(): void {
    this.setValue(null);
  }

  public isDisabledUnary(type: TRange): boolean {
    const { disabled } = this;
    const isDisabledRange: boolean = this[type] && +this[type] === +this.value;

    return disabled || isDisabledRange;
  }

  private checkAndGetValue(value: any): number {
    const { min, max } = this;

    switch (true) {
      case isNaN(value):
      case isEqual(value, null):
        return null;
      case min && +value <= +min:
        return +min;
      case max && +value >= +max:
        return +max;
      default:
        return +value;
    }
  }

  writeValue(value: any): void {
    value = this.checkAndGetValue(value);
    this.value = value;
    this.cdr.detectChanges();
  }

  public increment(): void {
    const value: number = this.value + 1;
    this.setValue(value);
  }

  public decrement(): void {
    const value: number = this.value - 1;
    this.setValue(value);
  }
}
