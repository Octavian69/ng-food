import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { NbComponentShape, NbComponentSize } from '@nebular/theme';
import { ToggleControlComponent } from '../../classes/ToogleControlComponent';
import {
  TContolAffix,
  TInputText,
  TPengingTemplate,
} from '@core/form/types/form.types';

@Component({
  selector: 'ng-form-input-toggle',
  templateUrl: './form-input-toggle.component.html',
  styleUrls: ['./form-input-toggle.component.scss'],
  providers: [new FormControlProvider(FormInputToggleControlComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormInputToggleControlComponent extends ToggleControlComponent {
  @Input('type') type: TInputText = 'text';
  @Input('min') min: string;
  @Input('max') max: string;
  @Input('loading') loading: boolean = false;
  @Input('prefixes') prefixes: TContolAffix[] = [];
  @Input('suffixes') suffixes: TContolAffix[] = [];
  @Input('size') size: NbComponentSize;
  @Input('shape') shape: NbComponentShape;
  @Input('pendingMessages') pendingMessages: TPengingTemplate[];
}
