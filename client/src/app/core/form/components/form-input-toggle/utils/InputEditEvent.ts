import { NgControl } from '@angular/forms';
import { TControlStatus } from '@core/form/types/form.types';

export class InputEditEvent {
  public proprety: string;
  public value: any;
  public status: TControlStatus;

  constructor(control: NgControl) {
    const {
      name,
      value,
      control: { status },
    } = control;

    this.value = value;
    this.proprety = name as string;
    this.status = status as TControlStatus;
  }
}
