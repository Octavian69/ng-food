import { DOCUMENT } from '@angular/common';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { nativeElement } from '@handlers/components.handlers';
import { arraysIsContain } from '@handlers/conditions.handlers';
import { trim } from '@handlers/string.handlers';
import { NbComponentShape, NbComponentSize } from '@nebular/theme';
import { UntilDestroy } from '@ngneat/until-destroy';
import { TextControlComponent } from '../../classes/TextControlComponent';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Injector,
  Input,
  ViewChild,
} from '@angular/core';
import {
  TAffix,
  TAffixTag,
  TAffixTemplate,
  TContolAffix,
  TInputText,
  TPengingTemplate,
} from '../../types/form.types';

@UntilDestroy()
@Component({
  selector: 'ng-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers: [new FormControlProvider(FormInputComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: 'inputRef',
})
export class FormInputComponent extends TextControlComponent {
  public focused: boolean = false;

  @Input('type') type: TInputText = 'text';
  @Input('size') size: NbComponentSize = 'medium';
  @Input('shape') shape: NbComponentShape = 'rectangle';

  @Input('prefixes') prefixes: TContolAffix[] = [];
  @Input('suffixes') suffixes: TContolAffix[] = [];
  @Input('pendingMessages') pendingMessages: TPengingTemplate[];

  @ViewChild('inputRef', { static: true })
  inputRef$: ElementRef<HTMLInputElement>;

  constructor(
    inector: Injector,
    cdr: ChangeDetectorRef,
    @Inject(DOCUMENT) private document: Document,
  ) {
    super(inector, cdr);
  }

  public getCssClasses(): ISimple<boolean> {
    const { shape, disabled } = this;
    const search: boolean = arraysIsContain(
      'search',
      this.prefixes,
      this.suffixes,
    );

    return {
      [status]: true,
      [shape]: true,
      disabled,
      search,
    };
  }

  public getAffixTemplates(affix: TAffix): TAffixTemplate[] {
    const affixesTpls: TContolAffix[] =
      affix === 'prefix' ? this.prefixes : this.suffixes;
    const tpls = affixesTpls.filter(
      (tpl: any) => typeof tpl !== 'string',
    ) as TAffixTemplate[];

    return tpls;
  }

  public getAffixTags(affix: TAffix): TAffixTag[] {
    const affixesTpls: TContolAffix[] =
      affix === 'prefix' ? this.prefixes : this.suffixes;
    const tpls = affixesTpls.filter(
      (tpl: any) => typeof tpl === 'string',
    ) as TAffixTag[];

    return tpls;
  }

  public getAffixContext(affix: TAffix) {
    const templates = this.getAffixTemplates(affix);
    const tags = this.getAffixTags(affix);

    return { templates, tags };
  }

  public setValue(value: string): void {
    this.value = trim(value);
    super.setValue(value);
  }

  public writeValue(value: string): void {
    this.value = trim(value);
  }

  public toggleFocus(): void {
    this.focused ? this.blur() : this.focus();
  }

  public focus(): void {
    if (this.focused) {
      this.blur();
    } else {
      nativeElement(this.inputRef$).focus();
    }

    this.focused = true;
  }

  public blur(): void {
    nativeElement(this.inputRef$).blur();
    this.focused = false;
  }
}
