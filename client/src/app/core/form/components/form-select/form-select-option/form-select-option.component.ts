import { TControlOption } from '@core/form/types/form.types';
import { ISimple } from '@core/interfaces/shared/ISimple';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-form-select-option',
  templateUrl: './form-select-option.component.html',
  styleUrls: ['./form-select-option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [],
})
export class FormSelectOptionComponent {
  @Input() option: TControlOption;
  @Input() multiple: boolean;
  @Input() active: boolean = false;
  @Input() disabled: boolean = false;

  @Output('changeValue') private _change = new EventEmitter<any>();

  public getStatusClasses(): ISimple<boolean> {
    const { active, disabled } = this;

    return { active, disabled };
  }

  public changeValue(): void {
    if (!this.multiple) {
      this._change.emit(this.option.value);
    }
  }

  public toggle(): void {
    this._change.emit(this.option.value);
  }
}
