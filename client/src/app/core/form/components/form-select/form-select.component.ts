import { ANPropShow, ANShowDifferentSides } from '@core/animations/animations';
import { TrackBy } from '@core/decorators/decorators';
import { BaseFormControlComponent } from '@core/form/classes/BaseFormControlComponent';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { TControlOption } from '@core/form/types/form.types';
import { isEqual } from '@handlers/conditions.handlers';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {
  ANShowListOptions,
  ANShowMultipleValue,
} from '@core/form/animations/form.animations';

@Component({
  selector: 'ng-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss'],
  providers: [new FormControlProvider(FormSelectComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'scale(0)',
      toValue: 'scale(1)',
      animationName: 'ANShowMultipleChips',
    }),
    ANShowMultipleValue,
    ANShowListOptions,
    ANShowDifferentSides(),
  ],
})
export class FormSelectComponent
  extends BaseFormControlComponent
  implements OnInit {
  public isShowPanel: boolean = false;

  @Input() options: TControlOption[];
  @Input() multiple: boolean = false;
  @Input() disabledOptionsCb: (opt: TControlOption) => boolean = () => false;

  public getPlaceholder(): string {
    return `Выберите ${this.fieldName || 'значение'}`;
  }

  public getLabel(): string {
    const option: TControlOption = this.options.find(
      (o) => o.value === this.value,
    );

    return option?.label || this.getPlaceholder();
  }

  public getMultipleValueOptions(): TControlOption[] {
    const value: any[] = this.getMultipleValue();
    const options: TControlOption[] = this.options.filter((o) =>
      value.includes(o.value),
    );

    return options;
  }

  public togglePanel(e: MouseEvent): void {
    if (!this.disabled) {
      this.isShowPanel = !this.isShowPanel;
    }
  }

  public isActiveOption({ value }: TControlOption): boolean {
    return this.multiple
      ? this.getMultipleValue().includes(value)
      : isEqual(value, this.value);
  }

  public setValue(optionValue: any): void {
    this.multiple
      ? this.setMultipleValue(optionValue)
      : this.setSingeValue(optionValue);
  }

  private setSingeValue(value: any): void {
    this.changeValue(value);
    this.togglePanel(null);
  }

  private setMultipleValue(optionValue: any): void {
    let value: any[];
    const currentValue: any[] = this.getMultipleValue();

    if (currentValue.includes(optionValue)) {
      value = currentValue.filter((v) => v !== optionValue);

      if (!value.length) value = null;
    } else {
      value = currentValue.concat(optionValue);
    }

    this.changeValue(value);
  }

  public changeValue(value: any | any[]): void {
    this.value = value;
    this.onChange(value);
    this.onTouched();
  }

  public getMultipleValue(): any[] {
    return this.value || [];
  }

  public writeValue(value: any): void {
    this.value = value;
  }

  @TrackBy('property', 'id')
  public trackByFn() {}

  @TrackBy('default')
  public trackByLabel() {}

  public isDisabledOption(option: TControlOption): boolean {
    return this.disabled || Boolean(this?.disabledOptionsCb(option));
  }

  public isDisabledReset(): boolean {
    return this.disabled || isEqual(this.value, null);
  }
}
