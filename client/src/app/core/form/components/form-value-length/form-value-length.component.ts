import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ng-form-value-length',
  templateUrl: './form-value-length.component.html',
  styleUrls: ['./form-value-length.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormValueLengthComponent {
  @Input() value: string = null;
  @Input() maxlength: string;
}
