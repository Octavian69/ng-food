import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';
import { completeFormCtrlErrors } from '@core/form/handlers/form.handlers';
import { TErrorModule } from '@core/form/types/form.types';
import { TControlStatus } from '../../types/form.types';

@Component({
  selector: 'ng-form-messages',
  templateUrl: './form-messages.component.html',
  styleUrls: ['./form-messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormMessagesComponent {
  @Input('control') formControl: FormControl;
  @Input('status') controlStatus: TControlStatus;
  @Input() fieldName: string;
  @Input() pendingMessages: string[];
  @Input() errors: ValidationErrors;
  @Input() customErrors: string[];
  @Input() errorModule: TErrorModule;

  public isShowErrorMessages(): boolean {
    const { invalid, touched, dirty } = this.formControl;

    return invalid && touched && dirty;
  }

  public getErrorMessages(): string[] {
    const errors: string[] = this.customErrors || [];
    const controlErrors: string[] = completeFormCtrlErrors(
      this.formControl.errors,
      this.fieldName,
      this.errorModule,
    );

    return errors.concat(controlErrors);
  }
}
