import { TrackBy } from '@core/decorators/decorators';
import { ANShowFormErrors } from '@core/form/animations/form.animations';
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';

@Component({
  selector: 'ng-form-errors',
  templateUrl: './form-errors.component.html',
  styleUrls: ['./form-errors.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowFormErrors],
})
export class FormErrorsComponent {
  @Input('errors') errors: string[] = [];

  @HostBinding('@ANShowFormErrors') private ANShowFormErrors: unknown;

  @TrackBy('default')
  public trackByFn() {}
}
