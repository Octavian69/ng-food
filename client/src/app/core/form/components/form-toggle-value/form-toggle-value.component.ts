import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TextControl } from '@core/form/types/form.types';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { wrapString } from '@handlers/string.handlers';

@Component({
  selector: 'ng-form-toggle-value',
  templateUrl: './form-toggle-value.component.html',
  styleUrls: ['./form-toggle-value.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormToggleValueComponent {
  @Input() value: string;
  @Input() type: TextControl;
  @Input() fieldName: string;

  public getTypeClass(): ISimple<boolean> {
    return {
      [this.type]: true,
    };
  }

  public getDefaultText(): string {
    return `Редактировать ${wrapString(this.fieldName, '"')}`;
  }
}
