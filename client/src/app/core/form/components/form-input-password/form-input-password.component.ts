import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  Renderer2,
} from '@angular/core';
import { TInputText } from '@core/form/types/form.types';
import { nativeElement } from '@handlers/components.handlers';
import { FormInputComponent } from '../form-input/form-input.component';

@Component({
  selector: 'ng-form-input-password',
  templateUrl: './form-input-password.component.html',
  styleUrls: ['./form-input-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormInputPasswordComponent {
  @Input() hide: boolean = true;
  @Input() disable: boolean = false;

  constructor(
    private renderer: Renderer2,
    private input$: FormInputComponent,
  ) {}

  public getHideTooltipText(): string {
    return this.hide ? 'Показать' : 'Скрыть';
  }

  public getHideIcon(): string {
    return this.hide ? 'visibility_off' : 'visibility';
  }

  public getType(): TInputText {
    return this.hide ? 'password' : 'text';
  }

  public setType(): void {
    this.hide = !this.hide;
    const type: TInputText = this.getType();
    const input$ = nativeElement(this.input$.inputRef$);

    this.renderer.setAttribute(input$, 'type', type);
  }
}
