import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TPlacementPosition } from '@core/types/state.types';

@Component({
  selector: 'ng-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormControlComponent {
  @Input() label: string;
  @Input() labelPlacement: TPlacementPosition = 'top-start';
}
