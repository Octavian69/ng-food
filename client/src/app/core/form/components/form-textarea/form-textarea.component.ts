import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { TextControlComponent } from '@core/form/classes/TextControlComponent';
import { trim } from '@handlers/string.handlers';

@Component({
  selector: 'ng-form-textarea',
  templateUrl: './form-textarea.component.html',
  styleUrls: ['./form-textarea.component.scss'],
  providers: [new FormControlProvider(FormTextareaComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormTextareaComponent extends TextControlComponent {
  @Input('rows') rows: number = 5;
  @Input('showReset') showReset: boolean = true;

  public setValue(value: string): void {
    this.value = trim(value);
    super.setValue(value);
  }

  public writeValue(value: string): void {
    this.value = trim(value);
    this.cdr.detectChanges();
  }
}
