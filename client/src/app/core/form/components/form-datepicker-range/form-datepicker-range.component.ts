import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BaseFormControlComponent } from '@core/form/classes/BaseFormControlComponent';
import { FormControlProvider } from '@core/form/classes/FormControlProvider';
import { RangeValue } from '@core/form/classes/RangeValue';
import { Bind } from '@core/form/decorators/decorators';
import { timeoutDelay } from '@handlers/utils.handlers';
import { NbCalendarRange } from '@nebular/theme';

@Component({
  selector: 'ng-form-datepicker-range',
  templateUrl: './form-datepicker-range.component.html',
  styleUrls: ['./form-datepicker-range.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [new FormControlProvider(FormDatepickerRangeComponent)],
})
export class FormDatepickerRangeComponent extends BaseFormControlComponent {
  public range: Partial<NbCalendarRange<Date | number>> = {
    start: new Date(),
    end: null,
  };

  @Input() public minDate: Date;
  @Input() public maxDate: Date;

  public setValue({ start = null, end = null }: NbCalendarRange<Date>): void {
    const value: RangeValue<Date> = new RangeValue(start, end);
    this.onChange(value);
    this.onTouched();
    this.updateValue(value);
  }

  private updateValue(value: RangeValue<Date>): void {
    this.range = value;
    timeoutDelay(this.detect);
  }

  @Bind()
  private detect(): void {
    this.cdr.detectChanges();
  }

  writeValue(value: any): void {
    const start = value?.start || new Date();
    const end = value?.end || null;

    this.updateValue({ start, end });
  }
}
