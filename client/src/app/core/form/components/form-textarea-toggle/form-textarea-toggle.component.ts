import { Component, Input } from '@angular/core';
import { ToggleControlComponent } from '@core/form/classes/ToogleControlComponent';
import { FormControlProvider } from '../../classes/FormControlProvider';

@Component({
  selector: 'ng-form-textarea-toggle',
  templateUrl: './form-textarea-toggle.component.html',
  styleUrls: ['./form-textarea-toggle.component.scss'],
  providers: [new FormControlProvider(FormTextareaToggleControlComponent)],
})
export class FormTextareaToggleControlComponent extends ToggleControlComponent {
  @Input() rows: number = 5;
  @Input() showReset: boolean = false;
}
