import { ISimple } from '@core/interfaces/shared/ISimple';

export interface IValidationLength
  extends Record<'min' | 'max', ISimple<number>> {}
