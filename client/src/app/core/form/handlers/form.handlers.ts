import { FormGroup } from '@angular/forms';
import { IValidationLength } from '@core/form/interfaces/IValidationLength';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { isEmptyObject } from '@handlers/conditions.handlers';
import { replace } from '@handlers/string.handlers';
import { getDataFromDB } from '@handlers/structutral.handlers';
import { assign, excludeValues } from '@handlers/utils.handlers';
import { AuthDB } from '@src/auth/db/auth.db';
import { GeneralErrorMessagesDB } from '../db/general-error-messages.db';
import { TErrorModule, TFormErrorEntry, TRange } from '../types/form.types';

export function completeFormCtrlErrors<T extends object>(
  errors: T,
  fieldName: string,
  errorsModule: TErrorModule,
): string[] {
  if (!errors) return null;

  const generalDB: ISimple<string> = getErrorsDB('general');
  const moduleDB: ISimple<string> = getErrorsDB(errorsModule);
  const isInvalidModuleKey: string = !moduleDB && errorsModule;

  try {
    if (isInvalidModuleKey)
      throw new Error(`Uncorrect errors module key:  "${errorsModule}"`);

    return Object.entries(errors).reduce(
      (accum: string[], [errorKey, errorValue]: TFormErrorEntry) => {
        const errorWithTags: string =
          generalDB[errorKey] || moduleDB?.[errorKey] || `Некорректный ввод`;
        const tags: string[] = Object.assign({ fieldName }, errorValue);
        const error: string = Object.entries(tags).reduce(
          (accum, [tag, value]) => {
            accum = replace(accum, `{{${tag}}}`, value);

            return accum;
          },
          errorWithTags,
        );

        accum.push(error);

        return accum;
      },
      [],
    );
  } catch (e) {
    console.error(e);
  }
}

function getErrorsDB(type: TErrorModule): ISimple<string> {
  switch (type) {
    case 'general':
      return GeneralErrorMessagesDB;
    case 'auth':
      return getDataFromDB(['messages', 'errors'], AuthDB);
    default:
      return null;
  }
}

export function getRange<
  M extends IValidationLength,
  L extends TRange,
  K extends keyof M[L]
>(validationMap: M, ctrlName: K, rangeType: L): number {
  const lengthMap: IValidationLength[L] = validationMap[rangeType];

  return lengthMap[ctrlName];
}

export function isDisabledForm(form: FormGroup): boolean {
  const { invalid, untouched, dirty } = form;

  return invalid || untouched || !dirty;
}

export function getPlaceholder(
  placehodlerKey: string = 'placeholder',
  fieldNameKey: string = 'fieldName',
): string {
  return this[placehodlerKey] || `Введите "${this[fieldNameKey]}"`;
}

export function getToogleTooltip(statusKey: string = 'isShowControl'): string {
  return this[statusKey] ? 'Закрыть' : 'Редактировать';
}

export function getToogleIcon(statusKey: string = 'isShowControl'): string {
  return this[statusKey] ? 'close' : 'edit';
}

export function makePureFilter<T extends object>(
  filter: T,
  ...values: any[]
): T | Partial<T> | null {
  values = values.length ? values : [null, ''];
  const pureFilter: T | Partial<T> = excludeValues(filter, ...values);

  return isEmptyObject(pureFilter) ? null : pureFilter;
}

export function isEmptyFilter(form: FormGroup) {
  return Boolean(makePureFilter(form.value));
}

export function resetForm(
  form: FormGroup,
  value?: object,
  options: {
    onlySelf?: boolean;
    emitEvent?: boolean;
  } = {},
): any {
  const resetValue = Object.keys(form.controls).reduce(
    (accum: object, controlName: string) => {
      accum[controlName] = null;
      return accum;
    },
    {},
  );
  form.reset(assign(resetValue, value), options);

  return form.value;
}
