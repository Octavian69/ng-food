import {
  ChangeDetectorRef,
  Component,
  Injector,
  Input
  } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { TSide } from '@core/types/state.types';
import { TErrorModule } from '../types/form.types';

@Component({
  selector: 'ng-form-control',
  template: '',
})
export abstract class BaseFormControlComponent implements ControlValueAccessor {
  public value: any;
  public disabled: boolean;
  public formControl: NgControl;

  @Input('fieldName') public fieldName: string = '';
  @Input('label') public label: string;
  @Input('labelPlacement') public labelPlacement: TSide = 'top';
  @Input('errors') public errors: string[];
  @Input('errorModule') public errorModule: TErrorModule;

  protected onChange: (v: any) => void = (v: any) => {};
  public onTouched: () => void = () => {};

  constructor(protected injector: Injector, protected cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.initFormControl();
  }

  private initFormControl(): void {
    this.formControl = this.injector.get(NgControl);
  }

  abstract setValue(value: unknown): void;
  abstract writeValue(value: unknown): void;

  registerOnChange(cb): void {
    this.onChange = cb;
  }

  registerOnTouched(cb): void {
    this.onTouched = cb;
  }

  setDisabledState(status: boolean): void {
    this.disabled = status;
  }
}
