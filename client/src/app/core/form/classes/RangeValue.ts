export class RangeValue<T = unknown> {
  public start?: T;
  public end?: T;

  constructor(start: T, end: T) {
    if (start) this.start = start;
    if (end) this.end = end;
  }
}
