import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { isNotNullish } from '@handlers/conditions.handlers';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, take } from 'rxjs/operators';
import { TAsyncDebounceParams } from '../types/form.types';
export class FormValidator {

  static excludeSpaces(ctrl: AbstractControl): ValidationErrors {
    const { value } = ctrl;

    return isNotNullish(value) && !(~String(value).search(' ')) ? null : { excludeSpaces: true }
  }

  static asyncDebounce<T>([request$, errorChecker]: TAsyncDebounceParams<T>, ms: number = 400): AsyncValidatorFn {
    const sub$: BehaviorSubject<any> = new BehaviorSubject(null);
    const errorCheckSub$: Observable<ValidationErrors> = sub$.pipe(
      distinctUntilChanged(),
      debounceTime(ms),
      switchMap(request$),
      map(errorChecker),
      take(1)
    );

    return (ctrl: AbstractControl): Observable<ValidationErrors> | Promise<ValidationErrors> => {
      sub$.next(ctrl.value);

      return errorCheckSub$;
    }
  }
}