import { TemplateRef } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

//  * Validations

export type TValueChecker<T> = (value: T) => boolean;
export type TRange = 'min' | 'max';

// *Form Controls

export type TControlStatus = 'VALID' | 'INVALID' | 'PENDING' | 'DISABLED';
export type TextControl = 'textarea' | 'input';
export type TInput = 'text' | 'password' | 'file' | 'button' | 'number';
export type TInputText = Extract<TInput, 'text' | 'password'>;
export type TAffix = 'prefix' | 'suffix';
export type TContolAffix = TAffixTemplate | TAffixTag;
export type TAffixTemplate = TemplateRef<any>;
export type TPengingTemplate = TemplateRef<any> | string;
export type TAffixTag = 'hide' | 'reset' | 'email' | 'search';
export type TAllowStatus = 'disabled' | 'enable';
export type TControlOption<T = unknown> = {
  id: number;
  label: string;
  value: T;
};

//  ! Errors types

export type TFormErrorEntry = [string, any];
export type TErrorModule = 'general' | 'auth';

// * FormValidator Types

export type TAsyncDebounceParams<T> = [
  (v: any) => Observable<T>,
  (v: T) => ValidationErrors,
];
