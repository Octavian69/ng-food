import { ISimple } from '@core/interfaces/shared/ISimple';

export const GeneralErrorMessagesDB: ISimple<string> = {
  required: 'Поле "{{fieldName}}" обязательно для заполнения',
  minlength:
    'Поле "{{fieldName}}" не может быть меньше {{requiredLength}} символов.Сейчас {{actualLength}}',
  maxlength:
    'Поле "{{fieldName}}" не может быть меньше {{requiredLength}} символов.Сейчас {{actualLength}}',
  email:
    'Поле "{{fieldName}}" должно соответствовать типу email: "ivanov@mail.ru"',
  min: 'Поле "{{fieldName}}" не может быть меньше {{min}}. Сейчас {{actual}}',
  max: 'Поле "{{fieldName}}" не может быть больше {{max}}. Сейчас {{actual}}',
  excludeSpaces: 'Поле "{{fieldName}}" не должно содержать пробелы',
};
