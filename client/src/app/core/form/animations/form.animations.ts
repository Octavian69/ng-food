import { ANStylesShow } from '@core/animations/animations';
import { EnAnimation } from '@core/animations/enums/Animation.enum';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import {
  animate,
  animateChild,
  query,
  stagger,
  style,
  transition,
  trigger,
} from '@angular/animations';
// *Components

// *ng-form-select
export const ANShowListOptions = trigger('ANShowListOptions', [
  transition(':enter, :leave', [
    style({ overflow: 'hidden' }),
    query(
      ':enter, :leave',
      [stagger(EnAnimation.DEFAULT_STAGGER, [animateChild()])],
      queryOptions(),
    ),
  ]),
]);

export const ANShowMultipleValue = trigger('ANShowMultipleValue', [
  transition(':enter', [
    style({ transform: 'translateY(100%)', height: '0px' }),
    animate(
      EnAnimation.DEFAULT_TIMING,
      style({ transform: 'translateY(0%)', height: '*' }),
    ),
  ]),
  transition(':leave', [
    style({
      height: '0px',
      width: '0px',
      paddingBottom: '0px',
    }),
    animate(
      EnAnimation.DEFAULT_TIMING,
      style({
        transform: 'translateY(100%)',
        height: '0px',
        width: '0px',
        paddingBottom: '0px',
      }),
    ),
  ]),
]);
// *ng-form-select

// *ng-form-errors

export const ANShowFormErrors = ANStylesShow({
  fromStyle: { opacity: '0', transform: 'translateY(-30%)' },
  toStyle: { opacity: '1', transform: 'translateY(0%)' },
  timingFrom: '.3s',
  animationName: 'ANShowFormErrors',
});

// *ng-form-errors

// *Components
