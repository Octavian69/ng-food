import { ISimple } from '@core/interfaces/shared/ISimple';
import { TRxSubject } from '@core/types/libs.types';
import { TInsert } from '@core/types/state.types';
import { environment } from '@env/environment';
import { skip, tap } from 'rxjs/operators';
import { isEqual, isNotNullish } from './conditions.handlers';
import { joinUrl } from './string.handlers';

export function firstTap<T>(cb: Function, skipCount: number = 1) {
  let isFirstCall: boolean = false;

  return (source: TRxSubject<T>) => {
    return source.pipe(
      skip(skipCount),
      tap(() => {
        if (!isFirstCall) {
          isFirstCall = true;
          cb();
        }
      }),
    );
  };
}

export function increment(start: number = 1) {
  return () => start++;
}

export function proxyUrl(...path: string[]): string {
  return joinUrl([environment.API_URL].concat(path));
}

export function insert<T>(v1: T[], v2: T[], type: TInsert): T[] {
  switch (type) {
    case 'begin': {
      return (v2 ?? []).concat(v1);
    }
    default: {
      return (v1 ?? []).concat(v2);
    }
  }
}

export function timeoutDelay(cb: Function, delay: number = 0): void {
  setTimeout(cb, delay);
}

export function assign<T extends object>(...objects: T[]): T {
  return Object.assign({}, ...objects);
}

export function findIndex<T>(
  items: T[],
  elem: Partial<T> | T,
  prop: keyof T,
): number {
  return items.findIndex((item: T) => isEqual(item[prop], elem[prop]));
}

export function findItem<T>(
  items: T[],
  elem: Partial<T> | T,
  prop: keyof T,
): T {
  return items.find((item: T) => isEqual(item[prop], elem[prop]));
}

export function removeItem<T>(items: T[], item: T, prop: keyof T): T[] {
  const idx: number = findIndex(items, item, prop);

  if (~idx) items.splice(idx, 1);

  return items;
}

export function excludeValues<T extends object>(
  object: T,
  ...excludeValues: unknown[]
): Partial<T> {
  return Object.keys(object).reduce(
    (accum: Partial<T>, key: keyof T & string) => {
      const value = object[key];

      if (!excludeValues.includes(value)) {
        accum[key] = value;
      }

      return accum;
    },
    {} as Partial<T>,
  );
}

export function isExistProps<T extends object, K extends keyof T>(
  accum: T,
  ...keys: K[]
): boolean {
  if (!accum) return false;

  return keys.every((k) => isNotNullish(accum[k]));
}

export function unique<T extends Array<any>>(arr: T): T {
  return [...new Set(arr)] as T;
}

export function sum(array: number[]): number {
  return array.reduce((accum, curr) => accum + curr, 0);
}

export function toCountMap(array: any[]): ISimple<number> {
  return array.reduce((accum, curr) => {
    if (!accum[curr]) {
      accum[curr] = 0;
    }
    accum[curr]++;
    return accum;
  }, {});
}

export function percentOfTotal(value: number, total: number) {
  return (value * 100) / total;
}

export function excludeProps<
  T extends object,
  K extends keyof T & string = keyof T & string
>(obj: T, props: Array<K>) {
  return Object.keys(obj).reduce((accum, prop) => {
    const isSkip: boolean = props.includes(prop as K);
    if (!isSkip) {
      accum[prop] = obj[prop];
    }

    return accum;
  }, {});
}
