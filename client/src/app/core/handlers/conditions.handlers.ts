import { EnPageMode } from '@core/enums/state.enums';
import { JSDataType } from '@core/types/libs.types';
import { TPageMode } from '@core/types/state.types';
import { MonoTypeOperatorFunction } from 'rxjs';
import { filter } from 'rxjs/operators';
import { IStateAction } from '../managers/interfaces/IStateAction';

export function isNotNullish(value: unknown): boolean {
  switch (value) {
    case null:
    case undefined:
      return false;
    default:
      return true;
  }
}

export function isNullish(value: unknown) {
  return !isNotNullish(value);
}

export function isJSType(value: any, type: JSDataType): boolean {
  return isEqual(typeof value, type);
}

export function isInstance(value: any, Constructor: { new (): any }): boolean {
  return value instanceof Constructor;
}

export function isEqual(v1: any, v2: any): boolean {
  return Object.is(v1, v2);
}

export function isEqualJSON(v1: any, v2: any): boolean {
  return v1 && v2 && isEqual(JSON.stringify(v1), JSON.stringify(v2));
}

export function isNotEqualJSON(v1: any, v2: any): boolean {
  return !isEqual(JSON.stringify(v1), JSON.stringify(v2));
}

export function isNotEqual(v1: any, v2: any): boolean {
  return !Object.is(v1, v2);
}

export function ofType<T extends string>(
  ...actions: T[]
): MonoTypeOperatorFunction<T> {
  return filter((action: T) => actions.includes(action));
}

export function ofActionType<A extends IStateAction>(
  ...actions: A['type'][]
): MonoTypeOperatorFunction<A> {
  return filter((action: A) => actions.includes(action.type));
}

export function isEditPageMode(id: EnPageMode.Create | string): boolean {
  return id && id !== EnPageMode.Create;
}

export function getPageMode(id: EnPageMode.Create | string): TPageMode {
  return isEditPageMode(id) ? 'edit' : 'create';
}

export function isEmptyObject<T>(object: T): boolean {
  return Object.keys(object).length === 0;
}

export function arraysIsContain(
  searchElement: unknown,
  ...arrays: Array<unknown[]>
): boolean {
  return arrays.some((arr) => arr?.includes(searchElement));
}
