import { ElementRef } from '@angular/core';
import { isJSType } from './conditions.handlers';
import { timeoutDelay } from './utils.handlers';
import {
  TAxis,
  TCssDirection,
  TPlacement,
  TSide
  } from '../types/state.types';

export function getDirection(side: TSide): TCssDirection {
  const axis: TAxis = ['left', 'right'].includes(side) ? 'X' : 'Y';
  const position: TCssDirection = axis === 'X' ? 'row' : 'column';
  const placement: TPlacement = ['right', 'bottom'].includes(side)
    ? 'after'
    : 'before';
  const direction = placement === 'after' ? `${position}-reverse` : position;

  return direction as TCssDirection;
}

export function nativeElement<T extends HTMLElement>(el$: ElementRef<T>): T {
  return el$.nativeElement;
}

export function scrollToContent(nativeEl$: ElementRef, delay?: number): void {
  const el$: HTMLElement = nativeElement(nativeEl$);
  const scrollCb: Function = () => el$.scrollIntoView({ behavior: 'smooth' });

  isJSType(delay, 'number') ? timeoutDelay(scrollCb, delay) : scrollCb();
}

export function wrapToTag(tag: string, content: any, className: string = '') {
  return `<${tag} class='${className}'>${content}</${tag}>`;
}
