import { Titlecase } from '@core/types/libs.types';
import { isJSType } from './conditions.handlers';

export function replace(
  str: string,
  from: string,
  to: string | number,
  flags: string = 'gi',
): string {
  const reg: RegExp = new RegExp(`${from}`, flags);

  return str.replace(reg, String(to));
}

export function prefix(str: string, tag: string): string {
  return `${tag}${str}`;
}

export function suffix(str: string, tag: string): string {
  return `${str}${tag}`;
}

export function trim(str: any): string {
  if (isJSType(str, 'string')) {
    return str.replace(/\s+/g, ' ').trim();
  }

  return str;
}

export function joinUrl(
  args: (string | number)[],
  separator: string = '/',
): string {
  return args.join(separator);
}

export function randomid(random: number = 1): string {
  const id: Uint32Array = window.crypto.getRandomValues(
    new Uint32Array(random),
  );

  return id.toString();
}

export function wrapString(
  str: string,
  startWith: string,
  endWith: string = startWith,
): string {
  return `${startWith}${str}${endWith}`;
}

export function titlecase<T extends string>(str: T): Titlecase<T> {
  return str
    .split(' ')
    .map((letter) => letter[0].toLocaleUpperCase() + letter.substr(1))
    .join(' ') as Titlecase<T>;
}
