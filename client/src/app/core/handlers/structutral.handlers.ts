import { isJSType, isNotNullish } from './conditions.handlers';

export function deepCopy<T>(value: T): T {
  return JSON.parse(JSON.stringify(value));
}

export function getDataFromDB<
  V,
  T extends object,
  K1 extends keyof T = keyof T,
  K2 extends keyof T[K1] = keyof T[K1],
  K3 extends keyof T[K1][K2] = keyof T[K1][K2],
  K4 extends keyof T[K1][K2][K3] = keyof T[K1][K2][K3]
>(keys: [K1, K2?, K3?, K4?], db: T, isCopy: boolean = true, idx = 0): V {
  const value = db[keys[idx] as string];
  const nextKey = keys[idx + 1];

  if (!isJSType(nextKey, 'string') && idx + 1 < keys.length) {
    throw new Error(
      `Invalid path key.The key type must be a string. 
        Key: ${nextKey}, 
        Index: ${idx + 1},
        Path: ${JSON.stringify(keys, (key, value) =>
          !value ? String('undefined') : value,
        )}
      `,
    );
  }

  if (nextKey && value) {
    return getDataFromDB(keys, value, isCopy, idx + 1);
  }

  return isCopy ? deepCopy(value) : value;
}

export function selectedProps<T extends object>(
  obj: T,
  props: (keyof T)[],
): Partial<T> {
  return props.reduce((accum: Partial<T>, prop: keyof T) => {
    accum[prop] = obj[prop];

    return accum;
  }, {});
}

export function selectDefinedProps<T extends object>(obj: T): Partial<T> {
  return Object.entries(obj).reduce((accum, [key, value]) => {
    if (isNotNullish(value)) accum[key] = value;

    return accum;
  }, {});
}
