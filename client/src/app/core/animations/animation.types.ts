import { TAxis } from '@core/types/state.types';
import {
  AnimationOptions,
  AnimationReferenceMetadata,
} from '@angular/animations';

export type TAnimationTiming = string | number;
export type TAnimationTranslateOptions = Partial<{
  timing: TAnimationTiming;
  axis: TAxis;
  from: string;
  to: string;
  animationName: string;
}>;

export type TAnimationRef = {
  animationRef: AnimationReferenceMetadata;
  params?: AnimationOptions;
  name?: string;
};
