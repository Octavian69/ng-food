import { ISimple } from '@core/interfaces/shared/ISimple';
import { IAnimationStateOptions } from './IAnimationStateOptions';

export interface IAnimationStyleOptions extends IAnimationStateOptions {
  fromStyles: ISimple<string | number>;
  toStyles: ISimple<string | number>;
}
