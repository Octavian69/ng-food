import { IAnimationStateOptions } from './IAnimationStateOptions';
import { TAnimationTiming } from '../animation.types';

export interface IAnimationStaggerOptions extends IAnimationStateOptions {
  query$: string;
  staggerTiming: TAnimationTiming;
}
