import { TAnimationTiming } from '../animation.types';

export interface IAnimationStateOptions {
  fromState: string;
  toState: string;
  timing?: TAnimationTiming;
}
