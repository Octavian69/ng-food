import { ISimple } from '@core/interfaces/shared/ISimple';
import { AnimationStateOptions } from './AnimationStateOptions';
import { EnAnimation } from '../enums/Animation.enum';
import { IAnimationStyleOptions } from '../interfaces/IAnimationStyleOptions';

export class AnimationStyleOptions
  extends AnimationStateOptions
  implements IAnimationStyleOptions {
  public fromStyles: ISimple<string | number>;
  public toStyles: ISimple<string | number>;

  constructor(public options: IAnimationStyleOptions) {
    super(
      options.fromState,
      options.toState,
      options.timing || EnAnimation.DEFAULT_TIMING,
    );

    this.fromStyles = options.fromStyles;
    this.toStyles = options.toStyles;
  }
}
