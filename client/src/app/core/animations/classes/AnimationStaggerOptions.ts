import { AnimationStyleOptions } from './AnimationStyleOptions';
import { TAnimationTiming } from '../animation.types';
import { EnAnimation } from '../enums/Animation.enum';
import { IAnimationStaggerOptions } from '../interfaces/IAnimationStaggerOptions';
import { IAnimationStyleOptions } from '../interfaces/IAnimationStyleOptions';

export class AnimationStaggerOptions
  extends AnimationStyleOptions
  implements IAnimationStaggerOptions {
  constructor(
    styleOptions: IAnimationStyleOptions,
    public query$: string,
    public staggerTiming: TAnimationTiming = EnAnimation.DEFAULT_STAGGER,
  ) {
    super(styleOptions);
  }
}
