import { TAnimationTiming } from '../animation.types';
import { EnAnimation } from '../enums/Animation.enum';
import { IAnimationManager } from '../interfaces/IAnimationsManager';
import { IAnimationStateOptions } from '../interfaces/IAnimationStateOptions';

export class AnimationStateOptions
  implements IAnimationStateOptions, IAnimationManager {
  constructor(
    public fromState: string,
    public toState: string,
    public timing: TAnimationTiming = EnAnimation.DEFAULT_TIMING,
  ) {}

  public getTransitonState(): string {
    const { fromState, toState } = this;
    return `${fromState}<=>${toState}`;
  }
}
