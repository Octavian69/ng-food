import { ISimple } from '@core/interfaces/shared/ISimple';
import { TAnimationRef, TAnimationTiming } from './animation.types';
import { EnAnimation } from './enums/Animation.enum';
import { queryOptions } from './handlers/animations.handlers';
import {
  animate,
  animateChild,
  animation,
  AnimationReferenceMetadata,
  AnimationTriggerMetadata,
  group,
  query,
  stagger,
  style,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';

export const ANTransform: AnimationReferenceMetadata = animation(
  [
    style({
      transform: '{{ fromState }}',
    }),
    animate('{{ timing }}', style({ transform: '{{ toState }}' })),
  ],
  {
    params: {
      timing: EnAnimation.DEFAULT_TIMING,
      fromState: 'translateY(-100%)',
      toState: 'translateY(0%)',
    },
  },
);

export const ANFade: AnimationReferenceMetadata = animation(
  [
    style({ opacity: '{{ fromState }}' }),
    animate('{{ timing }}', style({ opacity: '{{ toState }}' })),
  ],
  {
    params: {
      timing: EnAnimation.DEFAULT_TIMING,
      fromState: '0',
      toState: '1',
    },
  },
);

export const ANWidth: AnimationReferenceMetadata = animation(
  [
    style({ width: '{{ fromState }}' }),
    animate('{{ timing }}', style({ width: '{{ toState }}' })),
  ],
  {
    params: {
      timing: EnAnimation.DEFAULT_TIMING,
      fromState: '0%',
      toState: '*',
    },
  },
);

export const ANStateChange = ({
  animationRef,
  params,
  name = 'ANStateChange',
}: TAnimationRef) =>
  trigger(name, [transition('*=>*', [useAnimation(animationRef, params)])]);

export const ANShowFade: (args?) => AnimationTriggerMetadata = ({
  animationName = 'ANShowFade',
  timing = EnAnimation.DEFAULT_TIMING,
  query$ = '@*',
} = {}): AnimationTriggerMetadata =>
  trigger(animationName, [
    transition(':enter', [
      group([
        useAnimation(ANFade, { params: { timing } }),
        query(query$, [animateChild()], queryOptions()),
      ]),
    ]),
    transition(':leave', [
      group([
        query(query$, [animateChild()], queryOptions()),
        useAnimation(ANFade, {
          params: { fromState: '*', toState: '0', timing },
        }),
      ]),
    ]),
  ]);

export const ANToggleFade = (
  timing: TAnimationTiming = EnAnimation.DEFAULT_TIMING,
) =>
  trigger('ANToggleFade', [
    transition('*=>*', [
      style({ opacity: 0 }),
      animate(timing, style({ opacity: 1 })),
    ]),
  ]);

export const ANShowDifferentSides = (
  timing: TAnimationTiming = EnAnimation.DEFAULT_TIMING,
) =>
  trigger('ANShowDifferentSides', [
    transition('void => left', [
      style({ transform: 'translate(-100%, -100%)', opacity: 0 }),
      animate(timing, style({ transform: 'translate(0%, 0%)', opacity: 1 })),
    ]),
    transition('left => void', [
      style({ transform: 'translate(0%, 0%)', opacity: 1 }),
      animate(
        timing,
        style({ transform: 'translate(-100%, -100%)', opacity: 0 }),
      ),
    ]),
    transition('void => right', [
      style({ transform: 'translate(100%, 100%)', opacity: 0 }),
      animate(timing, style({ transform: 'translate(0%, 0%)', opacity: 1 })),
    ]),
    transition('right => void', [
      style({ transform: 'translate(0%, 0%)', opacity: 1 }),
      animate(
        timing,
        style({ transform: 'translate(100%, 100%)', opacity: 0 }),
      ),
    ]),
  ]);

export const ANShowWidth = (
  timing: TAnimationTiming = EnAnimation.DEFAULT_TIMING,
): AnimationTriggerMetadata =>
  trigger('ANShowWidth', [
    transition(':enter', [
      style({ width: 0, overflow: 'hidden' }),
      animate(timing, style({ width: '*', overflow: 'hidden' })),
    ]),
    transition(':leave', [
      animate(timing, style({ width: 0, overflow: 'hidden' })),
    ]),
  ]);

export const ANShowTranslate = ({
  timing = EnAnimation.DEFAULT_TIMING as string,
  axis = 'Y',
  from = '-100%',
  to = '0%',
  animationName = 'ANShowTranslate',
} = {}) => {
  const func: string = `translate${axis}`;
  const valueFrom: string = `${func}(${from})`;
  const valueTo: string = `${func}(${to})`;

  return trigger(animationName, [
    transition(':enter', [
      style({
        transform: valueFrom,
        opacity: 0,
      }),
      animate(timing, style({ transform: valueTo, opacity: 1 })),
    ]),
    transition(':leave', [
      animate(timing, style({ transform: valueFrom, opacity: 0 })),
    ]),
  ]);
};

export const ANPropShow = ({
  cssProp = 'opacity',
  fromValue = '0',
  toValue = '1',
  timingFrom = '300ms',
  timingTo = timingFrom,
  leave = true,
  animationName = 'ANPropShow',
} = {}) => {
  const fromStyle = { [cssProp]: fromValue };
  const toStyle = { [cssProp]: toValue };

  const transitions = [
    transition(`:enter`, [
      style(fromStyle),
      animate(timingFrom, style(toStyle)),
    ]),
  ];

  if (leave) {
    const leaveTransition = transition(
      `:leave`,
      animate(timingTo, style(fromStyle)),
    );

    transitions.push(leaveTransition);
  }

  return trigger(animationName, transitions);
};

export const ANStylesShow = ({
  fromStyle = null,
  toStyle = null,
  timingFrom = '300ms',
  timingTo = timingFrom,
  animationName = 'ANPropShow',
} = {}) => {
  return trigger(animationName, [
    transition(`:enter`, [
      style(fromStyle),
      animate(timingFrom, style(toStyle)),
    ]),
    transition(`:leave`, animate(timingTo, style(fromStyle))),
  ]);
};

export const ANShowBySize = (
  timing: TAnimationTiming = EnAnimation.DEFAULT_TIMING,
  $query: string = '@*',
) =>
  trigger('ANShowBySize', [
    transition(':enter', [
      style({ height: '0px', width: '0px', overflow: 'hidden' }),
      animate(timing, style({ height: '*', width: '*' })),
      query($query, [animateChild()], queryOptions()),
    ]),
    transition(':leave', [
      style({ overflow: 'hidden' }),
      query($query, [animateChild()], queryOptions()),
      animate(
        timing,
        style({ height: '0px', width: '0px', overflow: 'hidden' }),
      ),
    ]),
  ]);

export const ANXaxisTransfromSides = ({
  timing = EnAnimation.DEFAULT_TIMING,
  state = ':enter, * => *',
  staggerTiming = EnAnimation.DEFAULT_STAGGER,
  shift = '100%',
  childSelector = '.stagger-item',
} = {}) =>
  trigger('ANXaxisTransfromSides', [
    transition(state, [
      style({ overflow: 'hidden' }),
      group([
        query(
          `${childSelector}:nth-child(odd):enter`,
          [
            stagger(staggerTiming, [
              useAnimation(ANTransform, {
                params: {
                  timing,
                  fromState: `translateX(-${shift})`,
                },
              }),
            ]),
          ],
          queryOptions(),
        ),
        query(
          `${childSelector}:nth-child(even):enter`,
          [
            stagger(staggerTiming, [
              useAnimation(ANTransform, {
                params: {
                  timing,
                  fromState: `translateX(${shift})`,
                },
              }),
            ]),
          ],
          queryOptions(),
        ),
      ]),
    ]),
  ]);

export const ANStateChildTrigger = (
  state: string = '* => *',
  $query: string = '@*',
) =>
  trigger('ANStateChildTrigger', [
    transition(state, [query($query, [animateChild()], queryOptions())]),
  ]);

export const ANShowStyleStagger = (
  {
    state = ':enter, *=>*',
    fromStyle = null,
    toStyle = null,
    timing = EnAnimation.DEFAULT_TIMING as string,
    staggerTiming = EnAnimation.DEFAULT_STAGGER as string,
    childSelector = '.stagger-item:enter',
    animationName = 'ANShowStyleStagger',
  } = {
    fromStyle: { transform: 'scale(0)' } as ISimple<string | number>,
    toStyle: { transform: 'scale(1)' } as ISimple<string | number>,
  },
) =>
  trigger(animationName, [
    transition(state, [
      query(`${childSelector}`, [style(fromStyle)], queryOptions()),
      query(
        `${childSelector}`,
        [stagger(staggerTiming, [animate(timing, style(toStyle))])],
        queryOptions(),
      ),
    ]),
  ]);
