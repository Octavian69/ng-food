import { ANShowBySize, ANShowTranslate } from '@core/animations/animations';
import { TViewState } from '@core/types/view.types';
import { Pagination } from '@core/utils/fetch/classes/Pagination';
import { SortQuery } from '@core/utils/fetch/classes/SortQuery';
import { IPagination } from '@core/utils/fetch/interfaces/IPagination';
import { isEqual, isNotEqualJSON } from '@handlers/conditions.handlers';
import { assign } from '@handlers/utils.handlers';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  TableColumn,
  TableMessages,
  TablePaginationEvent,
  TableSortEvent,
} from '@core/types/libs.types';

@Component({
  selector: 'ng-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANShowBySize(300),
    ANShowTranslate({
      from: '50%',
    }),
  ],
})
export class TableComponent {
  public pageLimitState: TViewState = 'hide';
  public messages: TableMessages = {
    emptyMessage: 'Список пуст',
  };

  @Input() rows: any[];
  @Input() columns: TableColumn[];
  @Input() count: number;
  @Input() paginate: IPagination;
  @Input() loading: boolean;
  @Input() pageSizeOptions: number[] = [5, 10, 15, 25, 50];
  @Input('messages') public set _messages(value: TableMessages) {
    if (value && isNotEqualJSON(value, this.messages)) {
      this.messages = assign(this.messages, value);
    }
  }

  @Output('pagination') _pagination = new EventEmitter<IPagination>();
  @Output('sort') _sort = new EventEmitter<SortQuery<string>>();

  public togglePageLimit(): void {
    isEqual(this.pageLimitState, 'show')
      ? this.hidePageLimit()
      : this.showPageLimit();
  }

  public hidePageLimit(): void {
    this.pageLimitState = 'hide';
  }

  public showPageLimit(): void {
    this.pageLimitState = 'show';
  }

  public getPageRange(): string {
    const { skip, limit } = this.paginate;
    let from: number = skip * limit + 1;
    let to: number = from + limit - 1;

    if (this.count === 0) from = 0;
    if (this.count <= limit) to = this.count;
    if (this.rows.length < limit && skip !== 0)
      to = from + this.rows.slice(1).length;

    return `${from} / ${to}`;
  }

  public setLimit(pageSize: number): void {
    this.pagination(0, pageSize);
    this.hidePageLimit();
  }

  public setPage({ offset: skip, limit }: TablePaginationEvent) {
    this.pagination(skip, limit);
  }

  public sort(sortEvent: TableSortEvent<string>): void {
    const { prop, dir } = sortEvent.sorts[0];
    const sort = new SortQuery<string>(prop, dir);

    this._sort.emit(sort);
  }

  private pagination(skip: number, limit: number) {
    const paginate = new Pagination(limit, skip);

    this._pagination.emit(paginate);
  }
}
