import { NgModule } from '@angular/core';
import { CoreModule } from '@core/modules/core.module';
import { PipesModule } from '@core/modules/pipes.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TableComponent } from './table/table.component';

@NgModule({
  declarations: [TableComponent],
  imports: [NgxDatatableModule, CoreModule, PipesModule],
  exports: [TableComponent, NgxDatatableModule],
})
export class TableModule {}
