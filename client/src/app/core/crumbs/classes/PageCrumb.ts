import { assign } from '@handlers/utils.handlers';

export class PageCrumb {
  public route: string;
  public active?: boolean;
  public title: string;
  public id: string = '0';

  public assign(): PageCrumb {
    return assign(this, { id: this.getId() } as PageCrumb);
  }

  public getId(): string {
    return `${this.title}[${this.id}]`;
  }
}
