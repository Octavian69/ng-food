import { NgModule } from '@angular/core';
import { SharedModule } from '../modules/shared.module';
import { CrumbsComponent } from './components/crumbs/crumbs.component';
import { CrumbsDirective } from './directives/crumbs.directive';
import { InsertCrumbsDirective } from './directives/insert-crumbs.directive';

const declarations = [CrumbsComponent, InsertCrumbsDirective, CrumbsDirective];

@NgModule({
  declarations,
  imports: [SharedModule],
  exports: declarations,
})
export class CrumbsModule {}
