import { TNavigateDirection } from '@core/types/state.types';
import { TCrumbsDB } from '../db/crumbs.db';
import { PageCrumb } from '../classes/PageCrumb';

export type TCrumbsDirection = TNavigateDirection | 'optional' | 'route';
export type TCrumbsUpdateMode = 'add' | 'set';
export type TCrumbState = 'static' | 'dynamic' | 'reset';
export type TCrumb = keyof TCrumbsDB['static-crumbs'];
export type TStaticCrumb = Omit<PageCrumb, 'active'> & {
  updateMode: TCrumbsUpdateMode;
};
