import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { ANPropShow, ANStateChildTrigger } from '@core/animations/animations';
import { PageCrumb } from '@core/crumbs/classes/PageCrumb';
import { CrumbsService } from '@core/crumbs/crumbs.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'ng-crumbs',
  templateUrl: './crumbs.component.html',
  styleUrls: ['./crumbs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANStateChildTrigger('* => *', ':enter, :leave'),
    ANPropShow({
      cssProp: 'width',
      fromValue: '0px',
      toValue: '*',
    }),
  ],
})
export class CrumbsComponent {
  public crumbs$: Observable<PageCrumb[]> = this.viewService.listen();

  constructor(private router: Router, public viewService: CrumbsService) {}

  public navigate(crumb: PageCrumb): void {
    this.router.navigateByUrl(crumb.route);
  }
}
