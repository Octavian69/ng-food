import { Injectable } from '@angular/core';
import { StorageService } from '@core/services/storage.service';
import { findIndex } from '@handlers/utils.handlers';
import { BehaviorSubject, Observable } from 'rxjs';
import { PageCrumb } from './classes/PageCrumb';

@Injectable({
  providedIn: 'root',
})
export class CrumbsService {
  public crumbs$: BehaviorSubject<PageCrumb[]> = new BehaviorSubject(null);

  constructor(private storage: StorageService) {
    this.init();
  }

  private init(): void {
    const crumbs: PageCrumb[] = this.get() || [];
    this.refresh(crumbs);
  }

  private get(): PageCrumb[] {
    return this.crumbs$.getValue();
  }

  public update(current: PageCrumb, next: PageCrumb = null): void {
    const crumbs: PageCrumb[] = this.get();
    const currentCrumbIdx: number = findIndex(crumbs, current, 'id');
    const newCrumb: PageCrumb = Object.assign(
      {},
      crumbs[currentCrumbIdx],
      current,
      next,
    );

    if (~currentCrumbIdx) {
      crumbs[currentCrumbIdx] = newCrumb;
    } else {
      crumbs.forEach((c) => (c.active = false));
      newCrumb.active = true;
      crumbs.push(newCrumb);
    }

    this.refresh(crumbs);
  }

  public remove(crumbId: string): void {
    const crumbs: PageCrumb[] = this.get();
    const idx: number = crumbs.findIndex((c) => c.id === crumbId);

    if (~idx) crumbs.splice(idx);

    this.activateLast(crumbs);
    this.refresh(crumbs);
  }

  private activateLast(crumbs: PageCrumb[]): void {
    const { length } = crumbs;

    if (length) crumbs[length - 1].active = true;
  }

  public refresh(crumbs: PageCrumb[]): void {
    this.crumbs$.next(crumbs.concat());
    this.storage.set('crumbs', crumbs);
  }

  public listen(): Observable<PageCrumb[]> {
    return this.crumbs$.asObservable();
  }
}
