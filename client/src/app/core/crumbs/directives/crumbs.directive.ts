import { PageCrumb } from '../classes/PageCrumb';
import { CrumbsService } from '../crumbs.service';
import {
  Directive,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';

@Directive({
  selector: '[ngCrumb]',
})
export class CrumbsDirective implements OnChanges, OnDestroy {
  private previousCrumb: PageCrumb = new PageCrumb();
  private currentCrumb: PageCrumb = new PageCrumb();

  @Input('ngCrumb') title: string;
  @Input('ngCrumbRoute') route: string;
  @Input('ngCrumbId') id: string;

  constructor(private viewService: CrumbsService) {}

  ngOnChanges(changes: SimpleChanges) {
    changes?.title.firstChange
      ? this.initialize(changes)
      : this.update(changes);
  }

  ngOnDestroy() {
    const id: string = this.currentCrumb.getId();

    this.viewService.remove(id);
  }

  private initialize(changes: SimpleChanges): void {
    this.makeChanges(changes);
    this.updatePreviousCrumb();

    this.viewService.update(this.currentCrumb.assign());
  }

  private update(changes: SimpleChanges): void {
    this.updatePreviousCrumb();
    this.makeChanges(changes);

    this.viewService.update(
      this.previousCrumb.assign(),
      this.currentCrumb.assign(),
    );
  }

  private makeChanges(changes: SimpleChanges): void {
    Object.entries(changes).forEach(([key, value]) => {
      const { currentValue } = value;

      this.currentCrumb[key] = currentValue;
    });
  }

  private updatePreviousCrumb(): void {
    Object.assign(this.previousCrumb, this.currentCrumb);
  }
}
