import { AfterViewInit, Directive, OnInit } from '@angular/core';
import { InsertComponentDirective } from '@core/directives/insert-component.directive';
import { CrumbsComponent } from '../components/crumbs/crumbs.component';

@Directive({
  selector: '[ngInsertCrumbs]',
})
export class InsertCrumbsDirective
  extends InsertComponentDirective<CrumbsComponent>
  implements AfterViewInit, OnInit {
  ngOnInit() {
    this.insert(CrumbsComponent);
  }
  ngAfterViewInit() {}
}
