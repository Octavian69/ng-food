import { TInsert } from '@core/types/state.types';
import { isEqual } from '@handlers/conditions.handlers';
import { findIndex, insert } from '@handlers/utils.handlers';
import { TAddDataOptions } from '../classes/AddData';
import { TEditDataOptions } from '../classes/EditData';
import { FetchData } from '../classes/FetchData';
import { TRemoveDataOptions } from '../classes/RemoveData';
import { UpdateData } from '../classes/UpdateData';
import { IFetchData } from '../interfaces/IFetchData';
import { IFetchDataManager } from '../interfaces/IFetchDataManager';
import { IPaginationResponse } from '../interfaces/IPaginationResponse';

export class FetchDataManager<T> implements IFetchDataManager<T> {
  private items: T[];
  private totalCount: number;
  private full: boolean;

  public getItems(): IFetchData<T> {
    const { items, totalCount } = this;

    return new FetchData<T>(items, totalCount);
  }

  public replaceItems(response: IPaginationResponse<T>): IFetchData<T> {
    const { items, totalCount } = response;

    this.items = items;
    this.totalCount = totalCount;
    this.full = this.isFullItems();

    return this.getItems();
  }

  public changeItems(
    response: IPaginationResponse<T>,
    insertType: TInsert = 'replace',
  ): IFetchData<T> {
    switch (insertType) {
      case 'replace':
        return this.replaceItems(response);
      case 'begin':
      case 'end':
        return this.insertItems(response, insertType);
    }
  }

  public insertItems(
    response: IPaginationResponse<T>,
    insertType: TInsert = 'end',
  ) {
    const { items: responseItems, totalCount } = response;
    const { items: currentItems } = this;

    this.items = insert(currentItems, responseItems, insertType);
    this.totalCount = totalCount;
    this.full = this.isFullItems();

    return this.getItems();
  }

  public updateItem(updateData: UpdateData<T>): IFetchData<T> {
    if (!this.items) return null;

    switch (updateData.action) {
      case 'add': {
        return this.addItem(
          updateData.payload,
          updateData.options as TAddDataOptions,
        );
      }

      case 'remove': {
        return this.removeItem(
          updateData.payload,
          updateData.options as TRemoveDataOptions<T>,
        );
      }

      case 'edit': {
        return this.editItem(
          updateData.payload,
          updateData.options as TEditDataOptions<T>,
        );
      }
    }
  }

  public addItem(item: T, options: TAddDataOptions): IFetchData<T> {
    const method = isEqual(options.insert, 'begin') ? 'unshift' : 'push';

    this.items[method](item);
    this.totalCount++;

    return this.getItems();
  }

  public removeItem(item: T, options: TRemoveDataOptions<T>): IFetchData<T> {
    const idx: number = findIndex(this.items, item, options.findKey);

    if (~idx) {
      this.items.splice(idx, 1);
      this.totalCount--;
    }

    return this.getItems();
  }

  public editItem(item: T, options: TEditDataOptions<T>): IFetchData<T> {
    const idx: number = findIndex(this.items, item, options.findKey);

    if (~idx) {
      const current: T = this.items[idx];
      const updateItem: T = Object.assign({}, current, item);
      this.items[idx] = updateItem;
    }

    return this.getItems();
  }

  public isFullItems(): boolean {
    return !(this.items.length < this.totalCount);
  }

  public resetItems(): IFetchData<T> {
    this.items = [];
    this.totalCount = 0;
    this.full = true;

    return this.getItems();
  }
}
