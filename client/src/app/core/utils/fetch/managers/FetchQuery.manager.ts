import { deepCopy, selectDefinedProps } from '@handlers/structutral.handlers';
import { assign } from '@handlers/utils.handlers';
import { FetchQuery } from '../classes/FetchQuery';
import { SortQuery } from '../classes/SortQuery';
import { IFetchQuery } from '../interfaces/IFetchQuery';
import { IFetchQueryManager } from '../interfaces/IFetchQueryManager';
import { IPagination } from '../interfaces/IPagination';
import { TPaginateOperation } from '../types/fetch.types';

export class FetchQueryManager<Q extends IFetchQuery>
  implements IFetchQueryManager<Q> {
  private initialQuery: Q;
  private currentQuery: Q;

  constructor(query: Q) {
    this.initialize(query);
  }

  private initialize(query) {
    this.currentQuery = query;
    this.makeSnapshotInitState(query);
  }

  private makeSnapshotInitState(query: Q): void {
    const snapshot = selectDefinedProps(query) as Q;
    this.initialQuery = assign(new FetchQuery(), snapshot as IFetchQuery) as Q;
  }

  public nextPage(): Q {
    this.pagination('increment');

    return this.getQuery() as Q;
  }

  public toggleSort(prop: string): Q['sort'] {
    const currentSort: Q['sort'] = this.getQueryItem('sort');
    if (!currentSort) {
      this.updateQuery('sort', new SortQuery(prop, 'asc'));
    } else {
      const nextSortValue: number = currentSort[prop] === 1 ? -1 : null;
      const sortValue = nextSortValue
        ? assign(currentSort, { [prop]: nextSortValue })
        : null;

      this.updateQuery('sort', sortValue);
    }

    return this.getQueryItem('sort');
  }

  public pagination(action?: TPaginateOperation): IPagination {
    switch (action) {
      case 'decrement': {
        this.decrement();
        break;
      }
      case 'increment': {
        this.increment();
        break;
      }
      case 'reset': {
        this.setPage(0);
        break;
      }
    }
    return this.getPagination();
  }

  public setPagination(paginate: IPagination): void {
    this.currentQuery.paginate = paginate;
  }

  public getPagination(): IPagination {
    return this.currentQuery.paginate;
  }

  public increment(): void {
    this.currentQuery.paginate.skip++;
  }

  public decrement(): void {
    if (this.currentQuery.paginate.skip > 0) this.currentQuery.paginate.skip--;
  }

  public setPage(skip: number): void {
    this.currentQuery.paginate.skip = skip;
  }

  public getQuery(): Q {
    return selectDefinedProps(this.currentQuery) as Q;
  }

  public getQueryItem<K extends keyof Q>(key: K): Q | Q[K] {
    return this.currentQuery[key];
  }

  public setQuery(query: Q): Q {
    this.currentQuery = assign(this.currentQuery, query);
    return this.currentQuery;
  }

  public updateQuery<K extends keyof Q>(queryKey: K, queryValue: Q[K]): Q {
    this.currentQuery[queryKey] = queryValue;

    return this.currentQuery;
  }

  public reset(): void {
    const initialState = deepCopy(this.initialQuery);
    this.currentQuery = assign(this.currentQuery, initialState);
  }

  public destroy(): void {
    this.currentQuery.sort = null;
    this.currentQuery.filters = null;
  }
}
