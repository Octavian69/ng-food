import { IFetchData } from '../interfaces/IFetchData';

export function isCanMakeFetch<T extends object>(
  fetchData: IFetchData<T>,
): boolean {
  return !fetchData?.full;
}
