import { TInsert } from '@core/types/state.types';
import { IFetchData } from './IFetchData';
import { IPaginationResponse } from './IPaginationResponse';
import { TAddDataOptions } from '../classes/AddData';
import { TEditDataOptions } from '../classes/EditData';
import { TRemoveDataOptions } from '../classes/RemoveData';
import { UpdateData } from '../classes/UpdateData';

export interface IFetchDataManager<T> {
  updateItem(updateData: UpdateData<T>): IFetchData<T>;
  addItem(item: T, options: TAddDataOptions): IFetchData<T>;
  removeItem(item: T, options: TRemoveDataOptions<T>): IFetchData<T>;
  editItem(item: T, options: TEditDataOptions<T>): IFetchData<T>;
  getItems(): IFetchData<T>;
  replaceItems(response: IPaginationResponse<T>): IFetchData<T>;
  changeItems(response: IPaginationResponse<T>, type?: TInsert): IFetchData<T>;
  insertItems(response: IPaginationResponse<T>, type?: TInsert): IFetchData<T>;
  isFullItems(): boolean;
  resetItems(): IFetchData<T>;
}
