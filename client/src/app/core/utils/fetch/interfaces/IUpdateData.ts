import { TUpdateDataAction } from '@core/types/data.types';

export interface IUpdateData<A extends TUpdateDataAction, P> {
  action: A;
  payload: P;
  options: any;
}
