import { IPagination } from './IPagination';
import { SortQuery } from '../classes/SortQuery';

export interface IFetchQuery<
  T extends object = unknown & object,
  F extends object = unknown & object,
  S extends keyof T & string = keyof T & string
> {
  paginate: IPagination;
  filters?: F;
  sort?: SortQuery<S>;
}
