export interface IPaginationResponse<T> {
  items: T[];
  totalCount: number;
}
