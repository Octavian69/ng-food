import { IPaginationResponse } from './IPaginationResponse';

export interface IFetchData<T> extends IPaginationResponse<T> {
  full: boolean; // @param обозначает возможность следующего вызова с пагинацией. (length === totalCount)
  isCanMakeFetch(): boolean;
}
