import { IFetchQuery } from './IFetchQuery';
import { IPagination } from './IPagination';
import { TPaginateOperation } from '../types/fetch.types';

export interface IFetchQueryManager<Q extends IFetchQuery> {
  pagination(action?: TPaginateOperation): IPagination;
  getPagination(): IPagination;
  setPagination(paginate: IPagination): void;
  increment(): void;
  toggleSort(prop: string): Q['sort'];
  decrement(): void;
  nextPage(): Q;
  setPage(skip: number): void;
  getQuery(): Q;
  getQueryItem<K extends keyof Q>(key: K): Q | Q[K];
  setQuery(query: Q): Q;
  updateQuery<K extends keyof Q>(queryKey: K, queryValue: Q[K]): Q;
  reset(): void;
  destroy(): void;
}
