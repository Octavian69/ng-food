import { TInsert } from '@core/types/state.types';
import { UpdateData } from './UpdateData';

export type TAddDataOptions = {
  insert: TInsert;
};

export class AddData<P> extends UpdateData<P, 'add'> {
  constructor(payload: P, options: TAddDataOptions) {
    super('add', payload, options);
  }
}
