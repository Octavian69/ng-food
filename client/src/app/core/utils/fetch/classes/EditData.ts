import { UpdateData } from './UpdateData';

export type TEditDataOptions<P> = {
  findKey: keyof P;
};

export class EditData<P> extends UpdateData<P, 'edit'> {
  constructor(payload: P, options: TEditDataOptions<P>) {
    super('edit', payload, options);
  }
}
