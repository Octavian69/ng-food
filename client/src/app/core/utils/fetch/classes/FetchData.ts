import { IFetchData } from '../interfaces/IFetchData';
export class FetchData<T> implements IFetchData<T> {
  public full: boolean = false;

  constructor(public items: T[], public totalCount: number) {
    this.full = this.items.length >= totalCount;
  }

  public isCanMakeFetch(): boolean {
    return !this.full;
  }
}
