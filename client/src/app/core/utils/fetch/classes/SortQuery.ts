import { TSortDirection } from '@core/types/libs.types';

export class SortQuery<T extends string = string> {
  constructor(prop: T, direction: TSortDirection) {
    return {
      [prop]: direction === 'asc' ? 1 : -1,
    };
  }
}
