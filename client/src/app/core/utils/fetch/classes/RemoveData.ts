import { TEditDataOptions } from './EditData';
import { UpdateData } from './UpdateData';

export type TRemoveDataOptions<P> = TEditDataOptions<P>;

export class RemoveData<P> extends UpdateData<P, 'remove'> {
  constructor(payload: P, options: TRemoveDataOptions<P>) {
    super('remove', payload, options);
  }
}
