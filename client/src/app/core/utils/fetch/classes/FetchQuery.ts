import { isJSType } from '@handlers/conditions.handlers';
import { SortQuery } from './SortQuery';
import { IFetchQuery } from '../interfaces/IFetchQuery';
import { IPagination } from '../interfaces/IPagination';

export class FetchQuery<
  T extends object,
  F extends object,
  S extends keyof T & string
> implements IFetchQuery<T, F, S> {
  public filters: F;
  public sort: SortQuery<S>;

  constructor(
    public paginate: IPagination = null,
    filters?: F,
    sort?: SortQuery<S>,
  ) {
    if (!isJSType(filters, 'undefined')) this.filters = filters;
    if (!isJSType(filters, 'undefined')) this.sort = sort;
  }
}
