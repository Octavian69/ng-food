import { TUpdateDataAction } from '@core/types/data.types';
import { TAddDataOptions } from './AddData';
import { TEditDataOptions } from './EditData';
import { TRemoveDataOptions } from './RemoveData';

type TUpdateDataOptions<A extends TUpdateDataAction, P> = A extends 'remove'
  ? TRemoveDataOptions<P>
  : A extends 'add'
  ? TAddDataOptions
  : A extends 'edit'
  ? TEditDataOptions<P>
  : never;

export class UpdateData<P, A extends TUpdateDataAction = TUpdateDataAction> {
  constructor(
    public action: A,
    public payload: P,
    public options: TUpdateDataOptions<A, P>,
  ) {}
}
