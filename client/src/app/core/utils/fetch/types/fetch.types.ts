import { TUpdateDataAction } from '@core/types/data.types';
import { TUnaryOperation } from '@core/types/view.types';

export type TFetchAction = TUpdateDataAction | 'expand';
export type TPaginateOperation = TUnaryOperation | 'reset';
