import { InjectionToken } from '@angular/core';
import * as moment from 'moment';

export type WindowDocument = typeof window;
export const WINDOW = new InjectionToken('window', {
  providedIn: 'root',
  factory(): WindowDocument {
    return window;
  },
});

// *Libs

export type Moment = typeof moment;
export const MOMENT = new InjectionToken('moment', {
  providedIn: 'root',
  factory(): Moment {
    moment.locale('ru');
    return moment;
  },
});
