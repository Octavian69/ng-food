import { IMessage } from './IMessage';

export class Message implements IMessage {
  constructor(public message: string) {}
}
