export class KeyValue<K extends string = string, V extends any = unknown> {
  constructor(public key: K, public value: V) {}
}
