export function TrackBy<T extends unknown>(
  type: 'index' | 'property' | 'default' = 'index',
  propName?: keyof T,
) {
  return (
    _,
    methodName: string,
    descriptor: PropertyDescriptor,
  ): PropertyDescriptor => {
    return {
      ...descriptor,
      value: function (idx: number, accum: T): T | T[keyof T] | number {
        switch (type) {
          case 'default':
            return accum;
          case 'property':
            return accum[propName];
          case 'index':
            return idx;
        }
      },
    };
  };
}

export function isCreatedMode(
  stateName: string = 'viewState',
  stateKey: string = 'pageMode',
): MethodDecorator {
  return (
    target: object,
    methodName: string,
    descriptor: PropertyDescriptor,
  ) => {
    descriptor.value = function (): boolean {
      return this[stateName][stateKey] === 'create';
    };

    return descriptor;
  };
}

function InitializeOnInit(target, args) {
  Object.defineProperty(target, '__initializeHandlers__', {
    value: [],
    enumerable: false,
    configurable: false,
    writable: false,
  });
  const { ngOnInit: originalOnInit } = target;
  Object.defineProperty(target, 'ngOnInit', {
    value: function () {
      target.__initializeHandlers__.forEach((handler) => {
        handler.call(this, args);
      });
      originalOnInit?.call(this);
    },
  });
}

export function Initialize(...args: any[]): MethodDecorator {
  return function (
    target: any,
    methodName: string,
    descriptor: PropertyDescriptor,
  ): void {
    if (!target.__initializeHandlers__) {
      InitializeOnInit(target, args);
    }

    const { value: initializeHandler } = descriptor;
    target.__initializeHandlers__.push(initializeHandler);
  };
}

export function Enumerable(): MethodDecorator {
  return function (
    target,
    methodName,
    descriptor: PropertyDescriptor,
  ): PropertyDescriptor {
    return { ...descriptor, enumerable: true };
  };
}

export function Skip(value: number): MethodDecorator {
  return function (
    target,
    methodName,
    descriptor: PropertyDescriptor,
  ): PropertyDescriptor {
    let count = 0;
    const { value: originalValue } = descriptor;

    return {
      value: function (...args) {
        const isCanCall: boolean = count >= value;

        if (isCanCall) {
          return originalValue.apply(this, args);
        } else {
          count++;
        }
      },
    };
  };
}

export function Cache(): MethodDecorator {
  return function (
    target: any,
    methodName: string,
    descriptor: PropertyDescriptor,
  ): PropertyDescriptor {
    const { value: originalValue } = descriptor;
    const store = new Map();

    return {
      value: function (...args: any[]) {
        const jsonArgs: string = JSON.stringify(args);
        const fromCache = store.get(jsonArgs);

        if (fromCache) {
          return fromCache;
        }

        const value = originalValue.apply(this, args);
        store.set(jsonArgs, value);

        return value;
      },
    };
  };
}
