export type TUpdateDataAction = 'add' | 'remove' | 'edit';

export type TIndexItem<T> = {
  index: number;
  item: T;
};
