export type TShowStatus = 'close' | 'open';
export type TViewState = 'hide' | 'show';
export type TListType = 'list' | 'table';
export type TUnaryOperation = 'increment' | 'decrement';
