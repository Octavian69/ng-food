export type TSide = 'left' | 'right' | 'top' | 'bottom';
export type TCssDirection = 'row' | 'column' | 'row-reverse' | 'column-reverse';
export type TPlacement = 'after' | 'before';
export type TPlacementPosition =
  | 'none'
  | 'left-start'
  | 'left-center'
  | 'left-end'
  | 'right-start'
  | 'right-center'
  | 'right-end'
  | 'top-start'
  | 'top-center'
  | 'top-end'
  | 'bottom-start'
  | 'bottom-center'
  | 'bottom-end';
export type TAxis = 'X' | 'Y' | '';
export type TAlignPosition = 'start' | 'center' | 'end';
export type TPageMode = 'create' | 'edit' | 'unknown';
export type TNavigateDirection = 'back' | 'forward';
export type TInsert = 'begin' | 'end' | 'replace';
