export class AuthDataFlags {
  public isAuthenticate: boolean = false;
  public isRegistration: boolean = false;
  public isRemove: boolean = false;
}