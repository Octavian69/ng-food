import { Injectable } from '@angular/core';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { environment } from '@env/environment';
import { prefix, replace } from '@handlers/string.handlers';
import { Observable, Subject, throwError } from 'rxjs';
import {
  catchError,
  first,
  switchMap,
  takeUntil
  } from 'rxjs/operators';
import { remainedTokenTimePercent } from '../handlers/auth.handlers';
import { ITokens } from '../interfaces/ITokens';
import { AuthTokensService } from '../services/auth-tokens.service';
import { AuthService } from '../services/auth.service';
import { LoginService } from '../services/login.service';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private refresh$: Subject<string> = new Subject();
  private error$: Subject<HttpErrorResponse> = new Subject();
  private isRefresh: boolean = false;

  constructor(
    private toastr: ToastrService,
    private authService: AuthService,
    private loginService: LoginService,
    private tokensService: AuthTokensService,
  ) {}

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const url: string = this.replaceUrl(req.url);
    const clone: HttpRequest<any> = req.clone({ url });

    return next.handle(clone).pipe(
      catchError((err: HttpErrorResponse) => {
        return this.errorHandler(err, clone, next);
      }),
    );
  }

  private replaceUrl(shortUrl: string): string {
    return replace(shortUrl, '@', environment.API_URL);
  }

  private errorHandler = (
    err: HttpErrorResponse,
    req?: HttpRequest<any>,
    next?: HttpHandler,
  ): Observable<never | HttpEvent<any>> => {
    const { status } = err;
    let message = err.error?.message || err.message || String(err);

    switch (status) {
      case 401: {
        return this.authenticateRequest(req, next);
      }

      case 403: {
        this.unauthorized(err);
        message = this.loginService.getDataFromDB(['messages', 'unauthorized']);

        break;
      }
    }

    this.toastr.danger(message);

    return throwError(err);
  };

  private unauthorized(err: HttpErrorResponse): void {
    this.isRefresh = false;
    this.loginService.logout();
    this.error$.next(err);
  }

  private authenticateRequest(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return this.isRefresh
      ? this.getPendingRequest(req, next)
      : this.getRefreshRequest(req, next);
  }

  private getRefreshRequest(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    this.isRefresh = true;
    const refresh_token: string = this.tokensService.getToken('refresh_token');

    return this.authService.refreshTokens(refresh_token).pipe(
      switchMap((tokens: ITokens) => {
        const clone = this.setAuthHeaders(req, tokens.access_token);

        this.tokensService.updateTokens(tokens);
        this.refresh$.next(tokens.access_token);
        this.isRefresh = false;

        return next.handle(clone);
      }),
    );
  }

  private getPendingRequest(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return this.refresh$.asObservable().pipe(
      first(),
      switchMap((access_token: string) => {
        const clone: HttpRequest<any> = this.setAuthHeaders(req, access_token);

        return next.handle(clone);
      }),
      takeUntil(
        this.error$.pipe(
          switchMap((err) => throwError(err)),
          catchError((err) => throwError(err)),
        ),
      ),
    );
  }

  private setAuthHeaders(
    req: HttpRequest<any>,
    access_token: string,
  ): HttpRequest<any> {
    const clone = req.clone({
      headers: new HttpHeaders({
        Authorization: prefix(access_token, 'Bearer '),
      }),
    });

    return clone;
  }

  // ! Функционал не используется.
  private canRefresh(): boolean {
    const { iat, exp } = this.tokensService.getDecodeToken('access_token');
    const remainedPercent: number = remainedTokenTimePercent(iat, exp);
    const refreshTreshold: number = 10;
    //  * Если токену осталось жить меньше 10% от общего времени жизни, то обновляем его
    return remainedPercent < refreshTreshold;
  }
}
