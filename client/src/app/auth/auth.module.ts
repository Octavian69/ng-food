import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../core/modules/shared.module';
import { AuthPageComponent } from './components/auth-page/auth-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginService } from './services/login.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { AuthDataService } from './services/auth.data.service';
import { AuthTokensService } from './services/auth-tokens.service';
import { AuthSocialModule } from './modules/auth-social/auth-social.module';

@NgModule({
  declarations: [AuthPageComponent, LoginComponent, RegistrationComponent],
  imports: [SharedModule, AuthSocialModule, AuthRoutingModule],
  providers: [
    AuthGuard,
    AuthService,
    AuthDataService,
    LoginService,
    AuthTokensService,
  ],
})
export class AuthModule {}
