import { Router } from '@angular/router';
import { Initialize } from '@core/decorators/decorators';
import { FormValidator } from '@core/form/classes/FormValidator';
import { getRange } from '@core/form/handlers/form.handlers';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitDetectionChanges } from '@core/managers/interfaces/hooks/OnInitDetectionChanges';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { IUserInfo } from '@core/user/interfaces/IUserInfo';
import { User } from '@core/user/models/User.model';
import { UserService } from '@core/user/services/user.service';
import { isEqual, ofActionType, ofType } from '@handlers/conditions.handlers';
import { NbComponentStatus } from '@nebular/theme';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NSRegistrationValidation } from '../../namespaces/auth-form.namespace';
import { AuthDataService } from '../../services/auth.data.service';
import { AuthDataFlags } from '../../states/Auth.data.flags';
import { TAuthDataAction, TRegistrationGroup } from '../../types/auth.types';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import {
  DisabledFormStatus,
  RequiredRange,
} from '@core/form/decorators/decorators';
import {
  TAsyncDebounceParams,
  TRange,
  TValueChecker,
} from '@core/form/types/form.types';
@UntilDestroy()
@Component({
  selector: 'ng-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationComponent
  implements
    OnInit,
    OnDestroy,
    OnInitActionListeners,
    OnInitDetectionChanges,
    OnInitStates {
  public form: FormGroup;
  @DisabledFormStatus() public disabled: boolean;

  public flags: AuthDataFlags;
  public passwordDIfConditions: string[] = this.dataService.getDataFromDB([
    'passwordDifConditions',
  ]);
  private passwordDifChecks: TValueChecker<string>[] = this.dataService.getDataFromDB(
    ['passwordDifChecks'],
    false,
  );
  private passwordDifStatusTitles: ISimple<string> = this.dataService.getDataFromDB(
    ['passwordDifStatusTitles'],
  );
  private passwordDifCssStatuses: ISimple<NbComponentStatus> = this.dataService.getDataFromDB(
    ['passwordDifCssStatuses'],
  );

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private userService: UserService,
    private dataService: AuthDataService,
  ) {}

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {}

  private init(): void {
    this.initForm();
  }

  @Initialize()
  initActionListeners(): void {
    this.dataService
      .listen()
      .pipe(ofActionType<TAuthDataAction>('registration'), untilDestroyed(this))
      .subscribe((action) => {
        const message: string = [
          action.payload,
          'Используйте свои данные для входа в кабинет',
        ].join('.');

        this.toastrService.success(message);
        this.router.navigateByUrl('/login');
      });
  }

  @Initialize()
  initDetectionChanges(): void {
    this.dataService
      .detectChanges()
      .pipe(
        ofType<TAuthDataAction['type']>('registration'),
        untilDestroyed(this),
      )
      .subscribe((_) => this.cdr.detectChanges());
  }

  @Initialize()
  initStates(): void {
    this.flags = this.dataService.getFullState('flags');
  }

  private initForm(): void {
    this.form = this.fb.group({
      loginGroup: this.fb.group({
        Login: [
          null,
          [
            Validators.required,
            Validators.email,
            FormValidator.excludeSpaces,
            Validators.maxLength(
              getRange(NSRegistrationValidation, 'Login', 'max'),
            ),
          ],
          [
            FormValidator.asyncDebounce<IUserInfo>(
              this.getExistsLoginCheck(),
              600,
            ),
          ],
        ],
        Name: [
          null,
          [
            Validators.required,
            Validators.minLength(
              getRange(NSRegistrationValidation, 'Name', 'min'),
            ),
            Validators.maxLength(
              getRange(NSRegistrationValidation, 'Name', 'max'),
            ),
          ],
        ],
      }),
      passwordGroup: this.fb.group(
        {
          Password: [
            null,
            [
              Validators.required,
              FormValidator.excludeSpaces,
              Validators.minLength(
                getRange(NSRegistrationValidation, 'Password', 'min'),
              ),
              Validators.maxLength(
                getRange(NSRegistrationValidation, 'Password', 'max'),
              ),
            ],
          ],
          ConfirmPassword: [
            null,
            [
              Validators.required,
              FormValidator.excludeSpaces,
              Validators.minLength(
                getRange(NSRegistrationValidation, 'Password', 'min'),
              ),
              Validators.maxLength(
                getRange(NSRegistrationValidation, 'Password', 'max'),
              ),
            ],
          ],
        },
        { validators: [this.isEqualPasswords] },
      ),
    });
  }

  private getExistsLoginCheck = (): TAsyncDebounceParams<IUserInfo> => {
    const request$ = this.userService.getByLogin.bind(this.userService);
    const errorChecker = (user: IUserInfo) => {
      return user ? { existLogin: true } : null;
    };

    return [request$, errorChecker];
  };

  public isDisabledGroup(group: TRegistrationGroup): boolean {
    const { invalid, pending } = this.form.get(group);

    return invalid || pending;
  }

  public getGroupStatus(
    group: TRegistrationGroup | 'form',
  ): Partial<NbComponentStatus> {
    const { invalid, valid, pending } =
      group === 'form' ? this.form : this.form.get(group);

    switch (true) {
      case invalid:
        return 'danger';
      case pending:
        return 'warning';
      case valid:
        return 'success';
    }
  }

  @RequiredRange(NSRegistrationValidation)
  public getRange(controlName: string, rangeType: TRange) {}

  public isValidFormControl(groupName: TRegistrationGroup, ctrlName: string) {
    return this.form.get(groupName).valid;
  }

  public getDifPasswordStatus(): NbComponentStatus {
    const difficultPercent: number = this.getDifPasswordPercent();

    return this.passwordDifCssStatuses?.[difficultPercent];
  }

  public getPasswordDifStatusLabel(): string {
    const difficultPercent: number = this.getDifPasswordPercent();

    return this.passwordDifStatusTitles[difficultPercent];
  }

  public getDifPasswordClass(): ISimple<boolean> {
    const className: NbComponentStatus = this.getDifPasswordStatus();

    return { [className]: true };
  }

  private getDifPasswordPercent(): number {
    const { value } = this.form.get('passwordGroup').get('Password');
    const percentStep: number = 100 / this.passwordDifChecks.length;

    return this.passwordDifChecks.reduce(
      (accum: number, cb: TValueChecker<string>) => {
        if (value && cb(value)) accum += percentStep;

        return accum;
      },
      0,
    );
  }

  private isEqualPasswords = (form: FormGroup): ValidationErrors => {
    const {
      value: { Password, ConfirmPassword },
    } = form;

    return isEqual(Password, ConfirmPassword)
      ? null
      : { isNotEqualPasswords: true };
  };

  public getEqualPasswordsError(): string[] {
    const { touched } = this.form.get('passwordGroup');
    const isShowError: boolean =
      touched && this.form.get('passwordGroup').hasError('isNotEqualPasswords');

    return isShowError ? ['Пароли не совпадают'] : null;
  }

  public registration(): void {
    const {
      value: {
        loginGroup: { Login, Name },
        passwordGroup: { Password },
      },
    } = this.form;

    const user: User = new User(Login, Name, Password);

    this.dataService.registration(user);
  }
}
