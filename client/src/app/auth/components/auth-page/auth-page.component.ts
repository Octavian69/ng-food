import { Component, OnDestroy } from '@angular/core';
import { AuthDataService } from '../../services/auth.data.service';

@Component({
  selector: 'ng-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
})
export class AuthPageComponent implements OnDestroy {
  constructor(private authDataService: AuthDataService) {}

  ngOnDestroy() {
    this.authDataService.destroy();
  }
}
