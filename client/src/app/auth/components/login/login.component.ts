import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Initialize } from '@core/decorators/decorators';
import { FormValidator } from '@core/form/classes/FormValidator';
import { RequiredRange } from '@core/form/decorators/decorators';
import { getRange } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { OnInitDetectionChanges } from '@core/managers/interfaces/hooks/OnInitDetectionChanges';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { IUserCredentials } from '@core/user/interfaces/IUserCredentials';
import { ofType } from '@handlers/conditions.handlers';
import { NbComponentStatus } from '@nebular/theme';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TSocialWeb } from '@src/auth/modules/auth-social/types/auth-social.types';
import { TAuthDataAction } from '@src/auth/types/auth.types';
import { NSRegistrationValidation } from '../../namespaces/auth-form.namespace';
import { AuthDataService } from '../../services/auth.data.service';
import { AuthDataFlags } from '../../states/Auth.data.flags';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
@UntilDestroy()
@Component({
  selector: 'ng-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent
  implements OnInit, OnDestroy, OnInitStates, OnInitDetectionChanges {
  public form: FormGroup;
  public flags: AuthDataFlags;

  constructor(
    private cdr: ChangeDetectorRef,
    private dataService: AuthDataService,
  ) {}

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy() {
    this.dataService.fullUnsubscribe();
  }

  @Initialize()
  initDetectionChanges(): void {
    this.dataService
      .detectChanges()
      .pipe(
        ofType<TAuthDataAction['type']>('authenticate'),
        untilDestroyed(this),
      )
      .subscribe((_) => this.cdr.detectChanges());
  }

  @Initialize()
  initStates(): void {
    this.flags = this.dataService.getFullState('flags');
  }

  private init(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = new FormGroup({
      Login: new FormControl(null, [
        Validators.required,
        Validators.email,
        FormValidator.excludeSpaces,
        Validators.maxLength(
          getRange(NSRegistrationValidation, 'Login', 'max'),
        ),
      ]),
      Password: new FormControl(null, [
        Validators.required,
        FormValidator.excludeSpaces,
        Validators.minLength(
          getRange(NSRegistrationValidation, 'Password', 'min'),
        ),
        Validators.maxLength(
          getRange(NSRegistrationValidation, 'Password', 'max'),
        ),
      ]),
    });
  }

  @RequiredRange(NSRegistrationValidation)
  public getRange(controlName: string, rangeType: TRange) {}

  public getFormStatus(): NbComponentStatus {
    const { invalid, valid } = this.form;

    switch (true) {
      case invalid:
        return 'danger';
      case valid:
        return 'success';
      default:
        return 'basic';
    }
  }

  public loginBySocialWeb(socialWebType: TSocialWeb): void {
    this.dataService.loginBySocialWeb(socialWebType);
  }

  public login(): void {
    const credentials: IUserCredentials = this.form.value;

    this.dataService.login(credentials);
  }
}
