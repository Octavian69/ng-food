import { IStateAction } from '@core/managers/interfaces/IStateAction';
import { IMessage } from '@core/message/IMessage';

// Services Actions
export type TAuthDataAction =
  | IStateAction<'authenticate', null>
  | IStateAction<'registration' | 'remove', IMessage>;

export type TLoginAction =
  | IStateAction<'login', 'authorized'>
  | IStateAction<'logout', 'unauthorized'>;

export type TAuthStatus = 'authorized' | 'unauthorized';
export type TRegistrationGroup = 'loginGroup' | 'passwordGroup';
