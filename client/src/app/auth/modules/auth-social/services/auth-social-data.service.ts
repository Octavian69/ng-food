import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { EnToastrTitle } from '@core/toastr/enums/toastr.enum';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { ITokens } from '@src/auth/interfaces/ITokens';
import { SocialUser } from 'angularx-social-login';
import { Observable, of } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { AuthSocialService } from './auth-social.service';
import { AuthSocialDB } from '../db/auth-social.db';
import { ISocialCandidate } from '../interfaces/ISocialCandidate';
import { SocialCandidate } from '../models/SocialCandidate.model';
import { TSocialWeb } from '../types/auth-social.types';

@Injectable()
export class AuthSocialDataService extends ExtendsFactory(
  State({ db: AuthSocialDB }),
) {
  constructor(
    private authSocialService: AuthSocialService,
    private toastr: ToastrService,
  ) {
    super(null, AuthSocialDB);
  }

  public login(socialWebType: TSocialWeb): Observable<ITokens> {
    return this.getRequestByProvider(socialWebType).pipe(
      mergeMap((socialUser: SocialUser) => {
        const candidate: ISocialCandidate = new SocialCandidate(socialUser);

        return this.authSocialService.login(candidate).pipe(
          mergeMap((tokens: ITokens) => {
            return tokens ? of(tokens) : this.registration(candidate);
          }),
        );
      }),
    );
  }

  public registration(candidate: ISocialCandidate): Observable<ITokens> {
    return this.authSocialService.registration(candidate).pipe(
      tap((_) => {
        const successMessage: string = this.getDataFromDB([
          'messages',
          'registration',
        ]);
        this.toastr.success(successMessage, EnToastrTitle.SUCCESS, {
          duration: 5000,
        });
      }),
    );
  }

  private getRequestByProvider(
    socialWebType: TSocialWeb,
  ): Observable<SocialUser> {
    switch (socialWebType) {
      case 'facebook': {
        return this.authSocialService.signInWithFacebook();
      }
      case 'google': {
        return this.authSocialService.signInWithGoogle();
      }
      // case 'vk': {
      //   return this.authSocialService.signInWithVK();
      // }
    }
  }
}
