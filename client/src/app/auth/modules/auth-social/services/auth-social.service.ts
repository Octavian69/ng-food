import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ITokens } from '@src/auth/interfaces/ITokens';
import { defer } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { ISocialCandidate } from '../interfaces/ISocialCandidate';
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialAuthService,
  SocialUser,
  VKLoginProvider,
} from 'angularx-social-login';

@Injectable()
export class AuthSocialService {
  constructor(
    private http: HttpClient,
    private socialAuthService: SocialAuthService,
  ) {}

  public signInWithVK(): Observable<SocialUser> {
    // https://github.com/veliksergey/angularx-social-login-vk#readme
    return defer(() =>
      this.socialAuthService.signIn(VKLoginProvider.PROVIDER_ID),
    );
  }

  public signInWithFacebook(): Observable<SocialUser> {
    return defer(() =>
      this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID),
    );
  }

  public signInWithGoogle(): Observable<SocialUser> {
    return defer(() =>
      this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID),
    );
  }

  public login(candidate: ISocialCandidate): Observable<ITokens> {
    return this.http.post<ITokens>(
      '@/auth-social/login-by-social-web',
      candidate,
    );
  }

  public registration(candidate: ISocialCandidate): Observable<ITokens> {
    return this.http.post<ITokens>(
      '@/auth-social/registration-by-social-web',
      candidate,
    );
  }
}
