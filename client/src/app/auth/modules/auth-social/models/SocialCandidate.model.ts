import { SocialUser } from 'angularx-social-login';
import { ISocialCandidate } from '../interfaces/ISocialCandidate';
import { TSocialWeb } from '../types/auth-social.types';

export class SocialCandidate implements ISocialCandidate {
  public AccountID: string;
  public Name: string;
  public Login: string;
  public Avatar: string;
  public Provider: TSocialWeb;

  constructor(user: SocialUser) {
    const { id, name, email, photoUrl, provider } = user;

    this.AccountID = id;
    this.Name = name;
    this.Login = email;
    this.Avatar = photoUrl;
    this.Provider = provider.toLocaleUpperCase() as TSocialWeb;
  }
}
