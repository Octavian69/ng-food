import { IOptionWithParams } from "@core/interfaces/options/IOptionWithParams";

export type TSocialWeb = 'facebook' | 'google' | 'vk';
export type TSpecialSocialOption = { socialWebType: TSocialWeb };
export type TAuthSocialOption = IOptionWithParams<['img', 'special'], TSpecialSocialOption>;
export type TAuthSocialOptions = TAuthSocialOption[];