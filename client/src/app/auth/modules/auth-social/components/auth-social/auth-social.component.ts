import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { TrackBy } from '@core/decorators/decorators';
import { prefix } from '@handlers/string.handlers';
import { AuthSocialDataService } from '../../services/auth-social-data.service';
import {
  TAuthSocialOption,
  TAuthSocialOptions,
} from '../../types/auth-social.types';

@Component({
  selector: 'ng-auth-social',
  templateUrl: './auth-social.component.html',
  styleUrls: ['./auth-social.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthSocialComponent {
  public options: TAuthSocialOptions = this.dataService.getDataFromDB([
    'authSocialOptions',
  ]);

  @Output('auth') _auth = new EventEmitter<string>();

  constructor(private dataService: AuthSocialDataService) {}

  public getOptionTooltip(opt: TAuthSocialOption): string {
    return prefix(opt.title, 'Войти с помощью: ');
  }

  public getOptionUrl(opt: TAuthSocialOption) {
    return `auth/social-webs/${opt.params.img}`;
  }

  public auth(socialWebType: string): void {
    this._auth.emit(socialWebType);
  }

  @TrackBy('property', 'id')
  public trackByFn() {}
}
