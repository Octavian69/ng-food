import { NgModule } from '@angular/core';
import { SocialLoginModule } from 'angularx-social-login';
import { SharedModule } from '@core/modules/shared.module';
import { getAuthServiceCfg } from './utils/social.config';
import { AuthSocialDataService } from './services/auth-social-data.service';
import { AuthSocialService } from './services/auth-social.service';
import { AuthSocialComponent } from './components/auth-social/auth-social.component';

@NgModule({
  declarations: [AuthSocialComponent],
  imports: [SharedModule, SocialLoginModule],
  providers: [
    AuthSocialDataService,
    AuthSocialService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: getAuthServiceCfg(),
    },
  ],
  exports: [AuthSocialComponent],
})
export class AuthSocialModule {}
