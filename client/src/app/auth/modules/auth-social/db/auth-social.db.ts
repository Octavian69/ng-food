export const AuthSocialDB = {
  messages: {
    registration:
      'Создан личный кабинет. Учетные данные отправлены Вам на почту.',
  },
  authSocialOptions: [
    {
      id: 1,
      title: 'Facebook',
      params: {
        img: 'facebook.png',
        special: {
          socialWebType: 'facebook',
        },
      },
    },
    {
      id: 2,
      title: 'Google',
      params: {
        img: 'google.png',
        special: {
          socialWebType: 'google',
        },
      },
    },
    // {
    //   id: 3,
    //   title: 'ВКонтакте',
    //   params: {
    //     img: 'vk.png',
    //     special: {
    //       socialWebType: 'vk',
    //     },
    //   },
    // },
  ],
} as const;

export type TAuthSocialDB = typeof AuthSocialDB;
