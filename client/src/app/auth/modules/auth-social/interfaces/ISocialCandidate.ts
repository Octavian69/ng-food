import { TSocialWeb } from "../types/auth-social.types";

export interface ISocialCandidate {
  AccountID: string;
  Provider: TSocialWeb;
  Name: string;
  Login: string;
  Avatar: string;
}