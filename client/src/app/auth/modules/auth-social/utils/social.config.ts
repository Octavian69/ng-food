import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialAuthServiceConfig,
  VKLoginProvider,
} from 'angularx-social-login';
import { environment } from '@env/environment';

export function getAuthServiceCfg(): SocialAuthServiceConfig {
  return {
    autoLogin: false,
    providers: [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(environment.FB_APP_ID),
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(environment.GOOGLE_APP_ID),
      },
      // {
      //   id: VKLoginProvider.PROVIDER_ID,
      //   provider: new VKLoginProvider(environment.VK_APP_ID),
      // },
    ],
  };
}
