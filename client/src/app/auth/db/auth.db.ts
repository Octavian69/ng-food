export const AuthDB = {
  tokensKey: '_t',
  messages: {
    errors: {
      existLogin: 'Пользотватель с таким логином сущствует. Попробуйте другой',
    },
    unauthorized:
      'Истекло время сессии.Используйте личные данные для входа в кабинет',
  },
  disallowedRoutes: ['auth/login', 'auth/registration', 'user/get-by-login'],
  passwordDifConditions: [
    'Одной строчной буквы',
    'Одной заглавной буквы',
    'Одной цифры',
    'Одного спецсимвола: " !@#$%^&*()|/,._=+-?<> "',
    'Иметь не меньше 10 символов',
  ],
  passwordDifStatusTitles: {
    '20': 'Минимальная',
    '40': 'Низкая',
    '60': 'Средняя',
    '80': 'Высокая',
    '100': 'Максимальная',
  },
  passwordDifCssStatuses: {
    '20': 'danger',
    '40': 'warning',
    '60': 'basic',
    '80': 'primary',
    '100': 'success',
  },
  passwordDifChecks: [
    (v: string): boolean => Boolean(~v.search(/[a-zа-яё]/)),
    (v: string): boolean => Boolean(~v.search(/[A-ZА-ЯЁ]/)),
    (v: string): boolean => Boolean(~v.search(/[0-9]/)),
    (v: string): boolean => Boolean(~v.search(/[@<>#$%^&*)(/,._=!+?-]/)),
    (v: string): boolean => v.length >= 10,
  ],
};

export type TAuthDB = typeof AuthDB;
