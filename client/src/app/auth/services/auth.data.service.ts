import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { IMessage } from '@core/message/IMessage';
import { IUserCredentials } from '@core/user/interfaces/IUserCredentials';
import { User } from '@core/user/models/User.model';
import { finalize } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { LoginService } from './login.service';
import { AuthDB } from '../db/auth.db';
import { ITokens } from '../interfaces/ITokens';
import { AuthSocialDataService } from '../modules/auth-social/services/auth-social-data.service';
import { TSocialWeb } from '../modules/auth-social/types/auth-social.types';
import { AuthDataFlags } from '../states/Auth.data.flags';
import { TAuthDataAction } from '../types/auth.types';

@Injectable()
export class AuthDataService extends ExtendsFactory(
  StreamManager<TAuthDataAction>(),
  State({
    flags: AuthDataFlags,
    db: AuthDB,
  }),
) {
  constructor(
    private loginService: LoginService,
    private authService: AuthService,
    private authSocialService: AuthSocialDataService,
  ) {
    super();
  }

  public loginBySocialWeb(socialWebType: TSocialWeb): void {
    this.setState('flags', 'isAuthenticate', true);

    this.authSocialService
      .login(socialWebType)
      .pipe(
        finalize(() => {
          this.setState('flags', 'isAuthenticate', false);
          this.detect('authenticate');
        }),
      )
      .subscribe((tokens: ITokens) => {
        this.loginService.login(tokens);
      });
  }

  public login(credentials: IUserCredentials): void {
    this.setState('flags', 'isAuthenticate', true);

    const next = (tokens: ITokens) => {
      this.loginService.login(tokens);
    };

    this.authService
      .login(credentials)
      .pipe(
        this.untilDestroyed(),
        finalize(() => {
          this.setState('flags', 'isAuthenticate', false);
          this.detect('authenticate');
        }),
      )
      .subscribe(next);
  }

  public registration(user: User): void {
    this.setState('flags', 'isRegistration', true);

    const next = (response: IMessage) => {
      this.action(new StateAction('registration', response));
    };

    this.authService
      .registration(user)
      .pipe(
        this.untilDestroyed(),
        finalize(() => {
          this.setState('flags', 'isRegistration', false);
          this.detect('registration');
        }),
      )
      .subscribe(next);
  }

  public remove(): void {
    this.setState('flags', 'isRemove', true);
    const next = (response: IMessage) => {
      this.action(new StateAction('remove', response));
      this.loginService.logout();
    };

    this.authService
      .remove()
      .pipe(
        this.untilDestroyed(),
        finalize(() => this.setState('flags', 'isRemove', false)),
      )
      .subscribe(next);
  }

  public destroy(): void {
    this.destroyStates('flags');
  }
}
