import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { StorageService } from '@core/services/storage.service';
import { UserDataService } from '@core/user/services/user.data.service';
import { BehaviorSubject } from 'rxjs';
import { AuthTokensService } from './auth-tokens.service';
import { AuthDB } from '../db/auth.db';
import { ITokens } from '../interfaces/ITokens';
import { TAuthStatus } from '../types/auth.types';

@Injectable()
export class LoginService extends ExtendsFactory(
  State({ db: AuthDB }),
  StreamManager(),
) {
  private auth$: BehaviorSubject<TAuthStatus> = new BehaviorSubject(
    this.getAuthStatus(),
  );

  constructor(
    private router: Router,
    private storage: StorageService,
    private userService: UserDataService,
    private tokensService: AuthTokensService,
  ) {
    super(null, AuthDB);
  }

  public login(tokens: ITokens): void {
    this.tokensService.updateTokens(tokens);
    this.userService.login(this.tokensService.getDecodeToken());
    this.storage.set('isAuth', true);
    this.action(new StateAction('login', 'authorized'));
    this.emitToStream('auth$', 'authorized');
    this.navigateToHomePage();
  }

  public logout(): void {
    this.storage.clear();
    this.action(new StateAction('logout', 'unauthorized'));
    this.emitToStream('auth$', 'unauthorized');
    this.navigateToHomePage();
  }

  public getAuthStatus(): TAuthStatus {
    return this.isLogged() ? 'authorized' : 'unauthorized';
  }

  public isLogged(): boolean {
    return Boolean(this.storage.get<boolean>('isAuth'));
  }

  public navigateToHomePage(): void {
    const homePage: string = this.isLogged() ? '/analytics' : '/login';

    this.router.navigateByUrl(homePage);
  }
}
