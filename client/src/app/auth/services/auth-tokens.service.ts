import { Injectable } from "@angular/core";
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from "@core/services/storage.service";
import { getDataFromDB } from "@handlers/structutral.handlers";
import { AuthDB } from "../db/auth.db";
import { ITokens } from "../interfaces/ITokens";

@Injectable()
export class AuthTokensService {
  constructor(
    private storage: StorageService,
    private jwtHelper: JwtHelperService
  ) { }

  public getToken(key: keyof ITokens): string {
    const tokensKey: string = getDataFromDB(['tokensKey'], AuthDB);
    const tokens: ITokens = this.storage.get(tokensKey);

    return tokens?.[key];
  }

  public updateTokens(tokens: ITokens): void {
    const tokensKey: string = getDataFromDB(['tokensKey'], AuthDB);

    this.storage.set(tokensKey, tokens);
  }

  public getDecodeToken<T>(key: keyof ITokens = 'access_token'): T {
    const token = this.getToken(key);

    return this.jwtHelper.decodeToken(token);
  }

  public decode<T>(token: string): T {
    return this.jwtHelper.decodeToken(token);
  }
}