import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IMessage } from '@core/message/IMessage';
import { IUserCredentials } from '@core/user/interfaces/IUserCredentials';
import { User } from '@core/user/models/User.model';
import { Observable } from 'rxjs';
import { ITokens } from '../interfaces/ITokens';
@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  public registration(user: User): Observable<IMessage> {
    return this.http.post<IMessage>('@/auth/registration', user);
  }

  public login(credentials: IUserCredentials): Observable<ITokens> {
    return this.http.post<ITokens>('@/auth/login', credentials);
  }

  public refreshTokens(refresh_token: string): Observable<ITokens> {
    return this.http.patch<ITokens>('@/auth/refresh-tokens', { refresh_token });
  }

  public remove(): Observable<IMessage> {
    return this.http.delete<IMessage>('@/auth/remove');
  }
}
