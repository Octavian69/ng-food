import { getDataFromDB } from '@handlers/structutral.handlers';
import { AuthDB, TAuthDB } from '../db/auth.db';
import { ITokens } from '../interfaces/ITokens';

export function tokenGetter(): string {
  const tokensKey: string = getDataFromDB<string, TAuthDB>(['tokensKey'], AuthDB);
  const tokens: ITokens = JSON.parse(window.sessionStorage.getItem(tokensKey));

  return tokens?.access_token;
}

export function getDisallowedRoutes(): RegExp[] {
  const disallowedRoutes: string[] = getDataFromDB<string[], TAuthDB>(['disallowedRoutes'], AuthDB);

  return disallowedRoutes.map((route: string) => new RegExp(route));
}

export function remainedTokenTimePercent(iat: number, exp: number): number {
  const fullTime: number = exp - iat;
  const remainedTime: number = Math.floor(((exp * 1000) - Date.now()) / 1000);
  const remainedPercent: number = Math.floor(remainedTime * 100 / fullTime);

  return remainedPercent;
}
