import { ExtractKeys } from '@core/types/libs.types';
import { IUser } from '@core/user/interfaces/IUser';

type MINRange = ExtractKeys<IUser, 'Name' | 'Password'>;
type MAXRange = ExtractKeys<IUser, 'Login' | 'Name' | 'Password'>;
export namespace NSRegistrationValidation {
  export const min: Record<MINRange, number> = {
    Name: 3,
    Password: 7,
  };

  export const max: Record<MAXRange, number> = {
    Login: 50,
    Name: 40,
    Password: 25,
  };
}
