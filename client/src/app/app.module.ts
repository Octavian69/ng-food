import { OverlayModule } from '@angular/cdk/overlay';
import { registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import ruLocale from '@angular/common/locales/ru';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from '@auth0/angular-jwt';
import { NotFoundComponent } from '@core/components/not-found/not-found.component';
import { ConfirmModule } from '@core/confirm/confirm.module';
import { CrumbsModule } from '@core/crumbs/crumbs.module';
import { ServicesModule } from '@core/modules/services.module';
import { SharedModule } from '@core/modules/shared.module';
import { PreloaderModule } from '@core/preloader/preloader.module';
import { ThemeModule } from '@core/theme/theme.module';
import { ToastrModule } from '@core/toastr/toastr.module';
import { HeadBoardModule } from './app-page/headboard/headboard.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AuthInterceptor } from './auth/interceptors/auth.interceptor';
import { TooltipComponent } from './core/tooltip/components/tooltip/tooltip.component';
import { SystemModule } from './system/system.module';
import {
  getDisallowedRoutes,
  tokenGetter,
} from './auth/handlers/auth.handlers';

registerLocaleData(ruLocale, 'ru');
@NgModule({
  declarations: [AppComponent, NotFoundComponent, TooltipComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    PreloaderModule,
    HeadBoardModule,
    AuthModule,
    SystemModule,
    AppRoutingModule,
    ConfirmModule,
    CrumbsModule,
    ThemeModule.forRoot(),
    ServicesModule.forRoot(),
    ToastrModule.forRoot(),
    OverlayModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        disallowedRoutes: getDisallowedRoutes(),
      },
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: LOCALE_ID,
      useValue: 'ru-RU',
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
