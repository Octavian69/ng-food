import { NgModule } from '@angular/core';
import { SharedModule } from '@core/modules/shared.module';
import { CrumbsModule } from '@core/crumbs/crumbs.module';
import { HeadboardViewService } from './services/headboard.view.service';
import { HeadboardComponent } from './components/headboard/headboard.component';
import { HeadboardMenuComponent } from './components/headboard-menu/headboard-menu.component';
import { HeadboardMenuItemComponent } from './components/headboard-menu-item/headboard-menu-item.component';

@NgModule({
  declarations: [
    HeadboardComponent,
    HeadboardMenuComponent,
    HeadboardMenuItemComponent,
  ],
  imports: [SharedModule, CrumbsModule],
  providers: [HeadboardViewService],
  exports: [HeadboardComponent],
})
export class HeadBoardModule {}
