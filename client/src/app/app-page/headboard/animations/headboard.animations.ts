import { ANTransform } from '@core/animations/animations';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import { isEqual } from '@handlers/conditions.handlers';
import {
  group,
  query,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';

const CHILD_STYLE_MAP = new Map([
  ['1', 'translate(-100%, -100%)'],
  ['2', 'translate(100%, -100%)'],
  ['3', 'translate(-100%, 100%)'],
  ['4', 'translate(100%, 100%)'],
]);

const child = (state: 'enter' | 'leave', child: string) => {
  let fromState = CHILD_STYLE_MAP.get(child);
  let toState = 'translate(0%)';

  if (isEqual(state, 'leave')) {
    [fromState, toState] = [toState, fromState];
  }

  return query(
    `.stagger-item:nth-child(${child}):${state}`,
    [
      group([
        useAnimation(ANTransform, {
          params: { fromState, toState },
        }),
      ]),
    ],
    queryOptions(),
  );
};

const childs: string[] = Array.from(CHILD_STYLE_MAP.keys());
const enter = childs.map((k) => child('enter', k));
const leave = childs.map((k) => child('leave', k));

export const ANMenuList = trigger('ANMenuList', [
  transition(':enter', [group(enter)]),
  transition(':leave', [group(leave)]),
]);
