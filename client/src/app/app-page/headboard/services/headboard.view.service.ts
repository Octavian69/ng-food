import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { LoginService } from '@src/auth/services/login.service';
import { TAuthStatus } from '@src/auth/types/auth.types';
import { BehaviorSubject } from 'rxjs';
import { HeadboardDB } from '../db/headboard.db';
import { HeadboardFlags } from '../state/Headbord.flags';
import {
  THeadboardNavOptions,
  THeadboardStateAction,
} from '../types/headboard.types';

@Injectable()
export class HeadboardViewService extends ExtendsFactory(
  State({
    flags: HeadboardFlags,
    db: HeadboardDB,
  }),
  StreamManager<THeadboardStateAction>(),
) {
  navOptions$: BehaviorSubject<THeadboardNavOptions> = new BehaviorSubject([]);

  constructor(private loginService: LoginService) {
    super();
  }

  public initNavOptions(): void {
    const authStatus: TAuthStatus = this.loginService.getAuthStatus();

    this.setNavOptions(authStatus);
  }

  public setNavOptions(status: TAuthStatus): void {
    const optionsKey =
      status === 'authorized' ? 'systemNavOptions' : 'authNavOptions';
    const options: THeadboardNavOptions = this.getDataFromDB([optionsKey]);

    this.emitToStream('navOptions$', options);
  }
}
