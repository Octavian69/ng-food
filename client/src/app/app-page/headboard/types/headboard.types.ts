import { IOptionWithParams } from '@core/interfaces/options/IOptionWithParams';

export type THeadboardStateAction = { type: 'change-title' };
export type THeadboardNavOption = IOptionWithParams<['icon', 'link']>;
export type THeadboardNavOptions = THeadboardNavOption[];
