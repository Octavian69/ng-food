export const HeadboardDB = {
  authNavOptions: [
    {
      id: 1,
      title: 'Вход',
      params: {
        icon: 'follow_the_signs',
        link: '/login',
      }
    },
    {
      id: 2,
      title: 'Регистрация',
      params: {
        icon: 'how_to_reg',
        link: '/registration',
      }
    },
  ],
  systemNavOptions: [
    {
      id: 3,
      title: 'Аналитика',
      params: {
        icon: 'stacked_line_chart',
        link: '/analytics',
      }
    },
    {
      id: 4,
      title: 'История',
      params: {
        icon: 'history_edu',
        link: '/history',
      }
    },
    {
      id: 5,
      title: 'Заказы',
      params: {
        icon: 'local_dining',
        link: '/orders',
      }
    },
    {
      id: 6,
      title: 'Категории',
      params: {
        icon: 'list_alt',
        link: '/categories',
      }
    },
  ]
};

export type THeadboardDB = typeof HeadboardDB;