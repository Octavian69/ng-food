import { Location } from '@angular/common';
import { TrackBy } from '@core/decorators/decorators';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  THeadboardNavOption,
  THeadboardNavOptions,
} from '../../types/headboard.types';

@Component({
  selector: 'ng-headboard-menu',
  templateUrl: './headboard-menu.component.html',
  styleUrls: ['./headboard-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeadboardMenuComponent {
  @Input() options: THeadboardNavOptions = [];
  @Output('navigate') _navigate = new EventEmitter<string>();

  constructor(private location: Location) {}

  @TrackBy('property', 'id')
  public trackByFn() {}

  public isActiveLink(opt: THeadboardNavOption): boolean {
    return this.location.isCurrentPathEqualTo(opt.params.link);
  }

  public navigate(link: string): void {
    this._navigate.emit(link);
  }
}
