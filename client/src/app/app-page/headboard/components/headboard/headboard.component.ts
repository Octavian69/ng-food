import { Router } from '@angular/router';
import { ANShowWidth } from '@core/animations/animations';
import { ConfirmService } from '@core/confirm/confirm.service';
import { Initialize } from '@core/decorators/decorators';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { ThemeService } from '@core/theme/services/theme.service';
import { TShowStatus } from '@core/types/view.types';
import { UserDataService } from '@core/user/services/user.data.service';
import { TUserDataAction } from '@core/user/types/user.types';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { LoginService } from '@src/auth/services/login.service';
import { TLoginAction } from '@src/auth/types/auth.types';
import { Observable } from 'rxjs';
import { ANMenuList } from '../../animations/headboard.animations';
import { HeadboardViewService } from '../../services/headboard.view.service';
import { HeadboardFlags } from '../../state/Headbord.flags';
import { THeadboardNavOptions } from '../../types/headboard.types';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';

@UntilDestroy()
@Component({
  selector: 'ng-headboard',
  templateUrl: './headboard.component.html',
  styleUrls: ['./headboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowWidth(), ANMenuList],
})
export class HeadboardComponent
  implements OnInit, OnDestroy, OnInitActionListeners, OnInitStates {
  public flags: HeadboardFlags;
  public navOptions$: Observable<THeadboardNavOptions> = this.viewService.getStream(
    'navOptions$',
  );

  constructor(
    private router: Router,
    private loginService: LoginService,
    private themeService: ThemeService,
    private userDataService: UserDataService,
    private confirmService: ConfirmService,
    private viewService: HeadboardViewService,
  ) {}

  ngOnInit(): void {
    this.init();
  }

  logout() {
    this.loginService.logout();
  }

  ngOnDestroy() {}

  @Initialize()
  initActionListeners(): void {
    this.initLoginListener();
    this.initUserListener();
  }

  @Initialize()
  initStates(): void {
    this.flags = this.viewService.getFullState('flags');
  }

  private initLoginListener(): void {
    this.userDataService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe(({ type }: TUserDataAction) => {
        switch (type) {
          case 'remove': {
            this.confirmService.hide();
            break;
          }
        }
      });
  }

  private initUserListener(): void {
    this.loginService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe(({ payload }: TLoginAction) => {
        this.viewService.setNavOptions(payload);
      });
  }

  private init(): void {
    this.viewService.initNavOptions();
  }

  public getLogoCss(): ISimple<string> {
    return {
      boxShadow: 'var(--theme-box-shadow)',
      height: '50px',
      width: '50px',
    };
  }

  public menu(status: TShowStatus): void {
    const value: boolean = status === 'close' ? false : true;

    this.viewService.setState('flags', 'isShowMenu', value);
  }

  public home(): void {
    this.loginService.navigateToHomePage();
  }

  public getThemeMessage(): string {
    return this.themeService.getThemeMessage();
  }

  public setTheme(): void {
    this.themeService.toggleTheme();
  }

  public navigate(link: string): void {
    this.router.navigateByUrl(link);
    this.viewService.setState('flags', 'isShowMenu', false);
  }

  public async remove(): Promise<void> {
    const result = await this.confirmService.confirm(
      'Вы действительно хотите удалить аккаунт?',
      true,
    );
    if (result) {
      this.userDataService.remove();
    }
  }
}
