import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { THeadboardNavOption } from '../../types/headboard.types';

@Component({
  selector: 'ng-headboard-menu-item',
  templateUrl: './headboard-menu-item.component.html',
  styleUrls: ['./headboard-menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadboardMenuItemComponent {
  @Input() option: THeadboardNavOption;
  @Input('active') isActive: boolean = false;
  @Output('navigate') _navigate = new EventEmitter<string>();

  public navigate(): void {
    this._navigate.emit(this.option.params.link);
  }
}
