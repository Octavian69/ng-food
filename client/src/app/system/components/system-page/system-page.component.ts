import { Component } from '@angular/core';

@Component({
  selector: 'ng-system-page',
  templateUrl: './system-page.component.html',
  styleUrls: ['./system-page.component.scss'],
})
export class SystemPageComponent {}
