import { IStateAction } from '@core/managers/interfaces/IStateAction';
import { IFetchDataManager } from '@core/utils/fetch/interfaces/IFetchDataManager';
import { IFetchQuery } from '@core/utils/fetch/interfaces/IFetchQuery';
import { IFetchQueryManager } from '@core/utils/fetch/interfaces/IFetchQueryManager';
import { TCategoriesFilter } from '../components/categoies-filter/types/categories.filter.types';
import { ICategory } from '../interfaces/ICategory';
// Requests

export type TCategoriesSortFields = 'Created' | 'TotalPositions';
export type TCategoriesQuery = IFetchQuery<
  ICategory,
  TCategoriesFilter,
  TCategoriesSortFields
>;
export type TCategoriesDataManager = IFetchDataManager<ICategory>;
export type TCategoriesQueryManager = IFetchQueryManager<TCategoriesQuery>;
export type TCategoriesDataAction = IStateAction<'fetch', TCategoriesQuery>;
