import { ICategory } from '../interfaces/ICategory';

export class CategoryModel implements ICategory {
  constructor(
    public Title: string = null,
    public Description: string = null,
    public Preview: string = null,
    public TotalPositions: number = 0,
    public Created: Date = new Date(),
    public User: string = null,
  ) {}
}
