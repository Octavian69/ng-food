import { NgModule } from '@angular/core';
import { CrumbsModule } from '@core/crumbs/crumbs.module';
import { FileModule } from '@core/file/file.module';
import { SharedModule } from '@core/modules/shared.module';
import { TableModule } from '@core/table/table.module';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesFilterComponent } from './components/categoies-filter/categories-filter.component';
import { CategoriesListCardComponent } from './components/categories-list-card/categories-list-card.component';
import { CategoriesListTableComponent } from './components/categories-list-table/categories-list-table.component';
import { CategoriesListComponent } from './components/categories-list/categories-list.component';
import { CategoriesPageComponent } from './components/categories-page/categories-page.component';
import { CategoryCreateComponent } from './components/category-create/category-create.component';
import { CategoryEditComponent } from './components/category-edit/category-edit.component';
import { CategoryComponent } from './components/category/category.component';
import { CategoryDataService } from './components/category/services/category.data.service';
import { CategoryViewService } from './components/category/services/category.view.service';
import { PositionsModule } from './modules/positions/positions.module';
import { CategoriesDataService } from './services/categories.data.service';
import { CategoriesViewService } from './services/categories.view.service';

@NgModule({
  declarations: [
    CategoriesPageComponent,
    CategoriesListComponent,
    CategoriesListCardComponent,
    CategoryComponent,
    CategoryEditComponent,
    CategoryCreateComponent,
    CategoriesFilterComponent,
    CategoriesListTableComponent,
  ],
  imports: [
    SharedModule,
    CategoriesRoutingModule,
    PositionsModule,
    CrumbsModule,
    FileModule,
    TableModule,
  ],
  providers: [
    CategoriesDataService,
    CategoriesViewService,
    CategoryDataService,
    CategoryViewService,
  ],
})
export class CategoriesModule {}
