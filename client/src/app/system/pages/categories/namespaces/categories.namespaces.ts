import { ExtractKeys } from '@core/types/libs.types';
import { ICategory } from '../interfaces/ICategory';

type MINRange = ExtractKeys<ICategory, 'Title'>;
type MAXRange = ExtractKeys<ICategory, 'Title' | 'Description'>;

export namespace NSCategoriesValidation {
  export const min: Record<MINRange, number> = {
    Title: 3,
  };

  export const max: Record<MAXRange, number> = {
    Title: 50,
    Description: 300,
  };
}
