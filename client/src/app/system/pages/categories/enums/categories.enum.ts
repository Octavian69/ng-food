export enum EnCategories {
  FETCH_LIMIT = 15,
  STORAGE_QUERY_KEY = 'categories_query',
  STORAGE_LIST_TYPE = 'categories_list_type',
}
