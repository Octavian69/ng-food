import { ANShowStyleStagger } from '@core/animations/animations';

export const ANShowCategories = ANShowStyleStagger({
  animationName: 'ANShowCategories',
  fromStyle: {
    opacity: '0',
    transform: 'translateY(-20%)',
  },
  toStyle: {
    opacity: '1',
    transform: 'translateY(0%)',
  },
});

export const ANShowCategoriesNav = ANShowStyleStagger({
  state: ':enter',
  animationName: 'ANShowCategoriesNav',
  childSelector: '.stagger-item',
  fromStyle: {
    opacity: '0',
    transform: 'scale(0)',
  },
  toStyle: {
    opacity: '1',
    transform: 'scale(1)',
  },
});
