export interface ICategory {
  Title: string;
  Description: string;
  TotalPositions: number;
  Preview: string;
  Created: Date;
  User: string;
  _id?: string;
}
