import { FetchQuery } from '@core/utils/fetch/classes/FetchQuery';
import { Pagination } from '@core/utils/fetch/classes/Pagination';
import { FetchDataManager } from '@core/utils/fetch/managers/FetchData.manager';
import { FetchQueryManager } from '@core/utils/fetch/managers/FetchQuery.manager';
import { EnPositions } from '../enums/positions.enum';
import {
  TPositionsDataManager,
  TPositionsQuery,
  TPositionsQueryManager,
} from '../types/positions.types';
export class PositionsDataState {
  public listDataManager: TPositionsDataManager = null;
  public listQueryManager: TPositionsQueryManager = null;
  public removedPositionIds: string[] = [];

  constructor() {
    this.initializeDataManager();
    this.initializeQueryManager();
  }

  private initializeDataManager(): void {
    this.listDataManager = new FetchDataManager();
  }

  private initializeQueryManager(): void {
    const initialQuery: TPositionsQuery = new FetchQuery(
      new Pagination(EnPositions.FETCH_LIMIT),
    );
    this.listQueryManager = new FetchQueryManager(initialQuery);
  }

  public reset(): void {
    this.initializeDataManager();
    this.initializeQueryManager();
    this.removedPositionIds = [];
  }
}
