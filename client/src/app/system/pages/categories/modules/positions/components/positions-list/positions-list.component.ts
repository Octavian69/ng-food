import { ConfirmService } from '@core/confirm/confirm.service';
import { Initialize, TrackBy } from '@core/decorators/decorators';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { StorageService } from '@core/services/storage.service';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { TUpdateDataAction } from '@core/types/data.types';
import { UserDataService } from '@core/user/services/user.data.service';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { EnPositions } from '../../enums/positions.enum';
import { IPosition } from '../../interfaces/IPosition';
import { PositionsDataService } from '../../services/positions.data.service';
import { PositionsViewService } from '../../services/positions.view.service';
import { PositionsDataFlags } from '../../states/Positions.data.flags';
import { PositionsDataState } from '../../states/Positions.data.state';
import { PositionsViewState } from '../../states/Positions.view.state';
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  ANShowPositionAddBtn,
  ANShowPositionItem,
} from '../../animations/positions.animations';
import {
  TPositionsActions,
  TPositionsQuery,
  TPositionsQueryManager,
} from '../../types/positions.types';
@UntilDestroy()
@Component({
  selector: 'ng-positions-list',
  templateUrl: './positions-list.component.html',
  styleUrls: ['./positions-list.component.scss'],
  animations: [ANShowPositionAddBtn, ANShowPositionItem],
})
export class PositionsListComponent
  implements OnInit, OnDestroy, OnInitStates, OnInitActionListeners {
  public positions$: Observable<
    IFetchData<IPosition>
  > = this.dataService.getStream('positions$');
  public editPosition$: Observable<IPosition> = this.dataService.getStream(
    'editPosition$',
  );

  public dataFlags: PositionsDataFlags;
  public dataState: PositionsDataState;
  public viewState: PositionsViewState;

  @Input('categoryId') categoryId: string;
  @Output('update') _update = new EventEmitter<TUpdateDataAction>();

  constructor(
    private storage: StorageService,
    private confirmService: ConfirmService,
    private toastrService: ToastrService,
    private dataService: PositionsDataService,
    public userDataService: UserDataService,
    public viewService: PositionsViewService,
  ) {}

  ngOnInit(): void {
    this.load();
  }

  ngOnDestroy() {
    this.dataService.destroy();
    this.viewService.destroyStates('flags');
  }

  @Initialize()
  initActionListeners() {
    this.dataService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe((action: TPositionsActions) => {
        switch (action.type) {
          case 'create': {
            this.afterCreate(action.payload);
            break;
          }
          case 'edit': {
            this.afterEdit(action.payload);
            break;
          }
          case 'remove': {
            this.afterRemove(action.payload);
            break;
          }
        }
      });
  }

  @Initialize()
  initStates(): void {
    this.dataFlags = this.dataService.getFullState('flags');
    this.dataState = this.dataService.getFullState('state');
    this.viewState = this.viewService.getFullState('state');
  }

  @TrackBy('property', '_id')
  public trackByFn() {}

  public load(): void {
    const storageQuery: TPositionsQuery = this.storage.get(
      EnPositions.STORAGE_QUERY_KEY,
    );
    this.dataService.load(this.categoryId, storageQuery);
  }

  public expand(): void {
    const current: IFetchData<IPosition> = this.dataService.getStreamValue(
      'positions$',
    );

    if (current.isCanMakeFetch()) {
      const queryManager: TPositionsQueryManager = this.dataService.getState(
        'state',
        'listQueryManager',
      );
      this.dataService.fetch(this.categoryId, queryManager.nextPage(), 'end');
    }
  }

  public create(body: FileRequestBody<IPosition>): void {
    this.dataService.create(body);
  }

  private afterCreate(position: IPosition): void {
    this.viewService.setCreationViewState('hide');
    this._update.emit('add');
  }

  public async remove(position: IPosition) {
    const isRemoved: boolean = await this.confirmService.confirm(
      `Вы действительно хотите удалить позицию "${position.Title}"`,
      true,
    );

    if (isRemoved) this.dataService.remove(position);
  }

  private afterRemove(position: IPosition): void {
    this.toastrService.success('Позиция удалена');
    this.confirmService.loading('hide');
    this._update.emit('remove');
  }

  public isRemoved(positionId: string): boolean {
    const removedIds: string[] = this.dataService.getState(
      'state',
      'removedPositionIds',
    );

    return removedIds.includes(positionId);
  }

  public toggleEditModal(position: IPosition): void {
    this.dataService.emitToStream('editPosition$', position);
  }

  public edit(body: FileRequestBody<IPosition>): void {
    this.dataService.edit(body);
  }

  public afterEdit(position: IPosition): void {
    this.dataService.emitToStream('editPosition$', null);
  }
}
