import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit
  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileUpload } from '@core/file/classes/FileUpload';
import { getRange } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { NSPositionValidation } from '../../namespaces/Position.namespaces';
import {
  DisabledFormStatus,
  RequiredRange,
} from '@core/form/decorators/decorators';

@Component({
  selector: 'ng-positions-form',
  template: '',
})
export abstract class PositionsFormComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public file: File;
  public preview: string;

  @DisabledFormStatus()
  public disabled: boolean;
  constructor(protected fb: FormBuilder, protected cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.reset();
  }

  protected init(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      Title: [
        null,
        [
          Validators.required,
          Validators.minLength(getRange(NSPositionValidation, 'Title', 'min')),
          Validators.maxLength(getRange(NSPositionValidation, 'Title', 'max')),
        ],
      ],
      Cost: [
        null,
        [
          Validators.required,
          Validators.min(getRange(NSPositionValidation, 'Cost', 'min')),
        ],
      ],
      Description: [
        null,
        Validators.maxLength(
          getRange(NSPositionValidation, 'Description', 'max'),
        ),
      ],
    });
  }

  @RequiredRange(NSPositionValidation)
  public getRange(controlName: string, rangeType: TRange) {}

  public changeFile({ file, payload: { preview } }: FileUpload<'IMG'>): void {
    this.preview = preview;
    this.file = file;
  }

  public reset(): void {
    this.form.reset();
    this.form.markAsUntouched();
    this.file = null;
    this.preview = null;
  }

  public abstract submit(): void;
}
