import { ANPropShow } from '@core/animations/animations';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import {
  animate,
  group,
  query,
  stagger,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const ANShowPositionAddBtn = ANPropShow({
  animationName: 'ANShowPositionAddBtn',
  cssProp: 'transform',
  fromValue: 'scale(0)',
  toValue: 'scale(1)',
  timingFrom: '10s 1s cubic-bezier(0.175, 0.885, 0.32, 1.275)',
  leave: false,
});

export const ANShowPositionItem = trigger('ANShowPositionItem', [
  transition(':enter, *=>*', [
    query(
      '.stagger-item:enter',
      [
        query(
          '.position',
          [
            style({
              transform: 'perspective(400px) rotateY(-90deg)',
            }),

            query(
              '.img',
              [
                style({
                  opacity: '0',
                  transform: 'scale(0)',
                }),
              ],
              queryOptions(),
            ),
            query(
              '.description-toggle-icon',
              [
                style({
                  opacity: '0',
                  transform: 'translate(0%, -50%) rotate(90deg)',
                }),
              ],
              queryOptions(),
            ),
            query(
              '.title',
              [
                style({
                  opacity: '0',
                  transform: 'translateY(-1rem)',
                }),
              ],
              queryOptions(),
            ),
            query(
              '.cost',
              [
                style({
                  opacity: '0',
                  transform: 'translateY(1rem)',
                }),
              ],
              queryOptions(),
            ),
            query(
              '.separator',
              [
                style({
                  height: '0',
                  width: '0',
                }),
              ],
              queryOptions(),
            ),
            query(
              '.edit-btn',
              [
                style({
                  transform: 'translateX(-150%)',
                }),
              ],
              queryOptions(),
            ),
            query(
              '.remove-btn',
              [
                style({
                  transform: 'translateX(150%)',
                }),
              ],
              queryOptions(),
            ),
          ],
          queryOptions(),
        ),
      ],
      queryOptions(),
    ),

    query(
      '.stagger-item:enter',
      stagger('.2s', [
        query(
          '.position',
          [
            animate(
              '400ms',
              style({
                transform: 'perspective(400px) rotateY(0deg)',
              }),
            ),
            group([
              query(
                '.img',
                [
                  animate(
                    '400ms',
                    style({
                      opacity: 1,
                      transform: 'scale(1)',
                    }),
                  ),
                ],
                queryOptions(),
              ),
              query(
                '.description-toggle-icon',
                [
                  animate(
                    '400ms',
                    style({
                      opacity: '1',
                      transform: 'translate(-100%, -50%) rotate(90deg)',
                    }),
                  ),
                ],
                queryOptions(),
              ),
              query(
                '.title',
                [
                  animate(
                    '400ms',
                    style({
                      opacity: '1',
                      transform: 'translateY(0rem)',
                    }),
                  ),
                ],
                queryOptions(),
              ),
              query(
                '.cost',
                [
                  animate(
                    '400ms',
                    style({
                      opacity: '1',
                      transform: 'translateY(0rem)',
                    }),
                  ),
                ],
                queryOptions(),
              ),
              query(
                '.separator',
                [
                  animate(
                    '400ms',
                    style({
                      height: '*',
                      width: '*',
                    }),
                  ),
                ],
                queryOptions(),
              ),
              query(
                '.edit-btn, .remove-btn',
                [
                  animate(
                    '400ms',
                    style({
                      transform: 'translateX(0%)',
                    }),
                  ),
                ],
                queryOptions(),
              ),
            ]),
          ],
          queryOptions(),
        ),
      ]),
      queryOptions(),
    ),
  ]),
]);
