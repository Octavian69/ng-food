import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { TViewState } from '@core/types/view.types';
import { PositionsViewState } from '../states/Positions.view.state';

@Injectable()
export class PositionsViewService extends ExtendsFactory(
  State({ state: PositionsViewState }),
) {
  constructor() {
    super();
  }

  public setCreationViewState(state: TViewState): void {
    this.setState('state', 'creationState', state);
  }
}
