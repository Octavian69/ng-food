import { ExtractKeys } from '@core/types/libs.types';
import { IPosition } from '../interfaces/IPosition';

type MINRange = ExtractKeys<IPosition, 'Title' | 'Cost'>;
type MAXRange = ExtractKeys<IPosition, 'Title' | 'Description'>;

export namespace NSPositionValidation {
  export const min: Record<MINRange, number> = {
    Title: 3,
    Cost: 1,
  };

  export const max: Record<MAXRange, number> = {
    Title: 40,
    Description: 70,
  };
}
