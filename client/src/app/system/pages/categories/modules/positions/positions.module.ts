import { NgModule } from '@angular/core';
import { FileModule } from '@core/file/file.module';
import { SharedModule } from '@core/modules/shared.module';
import { PositionsCreateComponent } from './components/positions-create/positions-create.component';
import { PositionsEditComponent } from './components/positions-edit/positions-edit.component';
import { PositionsItemComponent } from './components/positions-item/positions-item.component';
import { PositionsListComponent } from './components/positions-list/positions-list.component';
import { PositionsDataService } from './services/positions.data.service';
import { PositionsService } from './services/positions.service';
import { PositionsViewService } from './services/positions.view.service';

@NgModule({
  declarations: [
    PositionsListComponent,
    PositionsItemComponent,
    PositionsCreateComponent,
    PositionsEditComponent,
  ],
  imports: [SharedModule, FileModule],
  providers: [PositionsService, PositionsDataService, PositionsViewService],
  exports: [PositionsListComponent],
})
export class PositionsModule {}
