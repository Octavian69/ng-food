import { IPosition } from '../interfaces/IPosition';

export class PositionModel implements IPosition {
  public Title: string = null;
  public Cost: number = null;
  public Description: string = null;
  public Preview: string = null;
  public Created: Date = new Date();
  public Updated: Date = new Date();

  constructor(public Category: string, public User: string) {}
}
