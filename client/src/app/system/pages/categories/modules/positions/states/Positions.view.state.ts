import { TViewState } from '@core/types/view.types';

export class PositionsViewState {
  public creationState: TViewState = 'hide';
}
