import { Injectable } from '@angular/core';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { convertToFormData } from '@core/file/handlers/file.handlers';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { TInsert } from '@core/types/state.types';
import { AddData } from '@core/utils/fetch/classes/AddData';
import { RemoveData } from '@core/utils/fetch/classes/RemoveData';
import { UpdateData } from '@core/utils/fetch/classes/UpdateData';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { PositionsService } from './positions.service';
import { IPosition } from '../interfaces/IPosition';
import { PositionsDataFlags } from '../states/Positions.data.flags';
import { PositionsDataState } from '../states/Positions.data.state';
import {
  TPositionsActions,
  TPositionsDataManager,
  TPositionsQuery,
  TPositionsQueryManager,
} from '../types/positions.types';

@Injectable()
export class PositionsDataService extends ExtendsFactory(
  State({
    state: PositionsDataState,
    flags: PositionsDataFlags,
  }),
  StreamManager<TPositionsActions>(),
) {
  private positions$: BehaviorSubject<
    IFetchData<IPosition>
  > = new BehaviorSubject(null);
  private editPosition$: BehaviorSubject<IPosition> = new BehaviorSubject(null);

  constructor(private positionsService: PositionsService) {
    super();
  }

  public load(categoryId: string, storageQuery: TPositionsQuery): void {
    const dataManager: TPositionsDataManager = this.getState(
      'state',
      'listDataManager',
    );
    const queryManager: TPositionsQueryManager = this.getState(
      'state',
      'listQueryManager',
    );
    const query: TPositionsQuery = queryManager.setQuery(storageQuery);

    const next = (response: IPaginationResponse<IPosition>) => {
      const positions: IFetchData<IPosition> = dataManager.changeItems(
        response,
      );

      this.emitToStream('positions$', positions);
    };

    this.positionsService
      .fetch(categoryId, query)
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public fetch(
    categoryId: string,
    query: TPositionsQuery,
    inserType: TInsert,
  ): void {
    this.setState('flags', 'isFetched', true);

    const dataManager: TPositionsDataManager = this.getState(
      'state',
      'listDataManager',
    );

    const next = (response: IPaginationResponse<IPosition>) => {
      const positions: IFetchData<IPosition> = dataManager.changeItems(
        response,
        inserType,
      );
      this.emitToStream('positions$', positions);
    };

    this.positionsService
      .fetch(categoryId, query)
      .pipe(
        finalize(() => this.setState('flags', 'isFetched', false)),
        this.untilDestroyed(),
      )
      .subscribe(next, this.detectError('error-expand'));
  }

  public create(candidate: FileRequestBody<IPosition>): void {
    this.setState('flags', 'isCreated', true);
    const body: FormData = convertToFormData(candidate, 'preview', 'position');

    const next = (position: IPosition) => {
      this.update(new AddData(position, { insert: 'begin' }));
      this.action(new StateAction('create', position));
    };

    this.positionsService
      .create(body)
      .pipe(
        this.untilDestroyed(),
        finalize(() => this.setState('flags', 'isCreated', false)),
      )
      .subscribe(next, this.detectError('error-create'));
  }

  public edit(candidate: FileRequestBody<IPosition>): void {
    this.setState('flags', 'isEdited', true);
    const body: FormData = convertToFormData(candidate, 'preview', 'position');

    const next = (position: IPosition) => {
      this.update(new UpdateData('edit', position, { findKey: '_id' }));
      this.action(new StateAction('edit', position));
      this.setState('flags', 'isEdited', false);
    };

    this.positionsService
      .edit(body, candidate.json._id)
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public remove({ _id: positionId }: IPosition): void {
    const removedIds: string[] = this.getState('state', 'removedPositionIds');
    removedIds.push(positionId);

    const next = (position: IPosition) => {
      const filtered: string[] = removedIds.filter((id) => id !== positionId);
      this.setState('state', 'removedPositionIds', filtered);
      this.update(new RemoveData(position, { findKey: '_id' }));
      this.action(new StateAction('remove', position));
    };

    this.positionsService
      .remove(positionId)
      .subscribe(next, this.detectError('error-remove'));
  }

  public update(data: UpdateData<IPosition>): void {
    const dataManager: TPositionsDataManager = this.getState(
      'state',
      'listDataManager',
    );
    const positions: IFetchData<IPosition> = dataManager.updateItem(data);
    this.emitToStream('positions$', positions);
  }

  public destroy(): void {
    this.destroyStates('flags');
    this.getFullState('state').reset();
    this.unsubscribe();
    this.destroyStreams([
      {
        streamKey: 'positions$',
        value: null,
      },
    ]);
  }
}
