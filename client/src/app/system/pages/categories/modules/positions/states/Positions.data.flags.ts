export class PositionsDataFlags {
  public isFetched: boolean = false;
  public isCreated: boolean = false;
  public isEdited: boolean = false;
}
