import { RangeValue } from '@core/form/classes/RangeValue';
import { IStateAction } from '@core/managers/interfaces/IStateAction';
import { ExtractKeys } from '@core/types/libs.types';
import { SortQuery } from '@core/utils/fetch/classes/SortQuery';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IFetchDataManager } from '@core/utils/fetch/interfaces/IFetchDataManager';
import { IFetchQuery } from '@core/utils/fetch/interfaces/IFetchQuery';
import { IFetchQueryManager } from '@core/utils/fetch/interfaces/IFetchQueryManager';
import { IPosition } from '../interfaces/IPosition';

export type TPositionsFilter = { Title?: string; Cost?: RangeValue<number> };
export type TPositionsSort = SortQuery<
  ExtractKeys<IPosition, 'Created' | 'Cost'>
>;
export type TPositionsQuery = IFetchQuery<TPositionsFilter, TPositionsSort>;
export type TPositionsDataManager = IFetchDataManager<IPosition>;
export type TPositionsQueryManager = IFetchQueryManager<TPositionsQuery>;
export type TPositionsCostStats = {
  MinCost: number;
  MaxCost: number;
};

export type TPositionsActions =
  | IStateAction<'create' | 'add' | 'remove' | 'edit', IPosition>
  | IStateAction<'expand', IFetchData<IPosition>>;
