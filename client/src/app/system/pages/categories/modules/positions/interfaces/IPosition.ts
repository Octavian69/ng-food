export interface IPosition {
  Title: string;
  Cost: number;
  Description: string;
  Created: Date;
  Updated: Date;
  Preview: string;
  Category: string;
  User: string;
  _id?: string;
}
