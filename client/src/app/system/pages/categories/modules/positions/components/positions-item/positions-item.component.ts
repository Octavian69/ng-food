import { TViewState } from '@core/types/view.types';
import { IPosition } from '../../interfaces/IPosition';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-positions-item',
  templateUrl: './positions-item.component.html',
  styleUrls: ['./positions-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PositionsItemComponent {
  public descriptionShowState: TViewState = 'hide';

  @Input() position: IPosition;
  @Input() isRemoved: boolean = false;
  @Input() isEdited: boolean = false;

  @Output('remove') private _remove = new EventEmitter<IPosition>();
  @Output('edit') private _edit = new EventEmitter<IPosition>();

  public toggleDecriptionShowState(): void {
    this.descriptionShowState =
      this.descriptionShowState === 'hide' ? 'show' : 'hide';
  }

  public edit(): void {
    this._edit.emit(this.position);
  }

  public remove(): void {
    this._remove.emit(this.position);
  }
}
