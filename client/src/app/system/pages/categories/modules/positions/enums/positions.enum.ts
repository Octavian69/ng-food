export enum EnPositions {
  FETCH_LIMIT = 15,
  STORAGE_QUERY_KEY = 'category_positions_query',
}
