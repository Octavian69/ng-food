import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { Observable } from 'rxjs';
import { IPosition } from '../interfaces/IPosition';
import { TPositionsCostStats, TPositionsQuery } from '../types/positions.types';

@Injectable()
export class PositionsService {
  constructor(private http: HttpClient) {}

  public fetch(
    categoryId: string,
    query: TPositionsQuery,
  ): Observable<IPaginationResponse<IPosition>> {
    return this.http.post<IPaginationResponse<IPosition>>(
      `@/positions/fetch/${categoryId}`,
      query,
    );
  }

  public create(body: FormData): Observable<IPosition> {
    return this.http.post<IPosition>('@/positions/create', body);
  }

  public edit(body: FormData, positionId: string): Observable<IPosition> {
    return this.http.patch<IPosition>(`@/positions/edit/${positionId}`, body);
  }

  public remove(positionId: string): Observable<IPosition> {
    return this.http.delete<IPosition>(`@/positions/remove/${positionId}`);
  }

  public getCostStats(categoryId: string): Observable<TPositionsCostStats> {
    return this.http.get<TPositionsCostStats>(
      `@/positions/get-stats/${categoryId}`,
    );
  }
}
