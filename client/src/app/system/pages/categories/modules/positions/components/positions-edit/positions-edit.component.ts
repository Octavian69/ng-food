import { FormGroup } from '@angular/forms';
import { ANShowTranslate } from '@core/animations/animations';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { assign } from '@handlers/utils.handlers';
import { IPosition } from '../../interfaces/IPosition';
import { PositionsFormComponent } from '../positions-form/positions-form.component';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-positions-edit',
  templateUrl: './positions-edit.component.html',
  styleUrls: ['./positions-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANShowTranslate({
      from: '-20%',
      animationName: 'ANShowContent',
    }),
    ANShowTranslate({
      from: '20%',
      animationName: 'ANShowFooter',
    }),
  ],
})
export class PositionsEditComponent
  extends PositionsFormComponent
  implements OnInit {
  public form: FormGroup;

  @Input() public position: IPosition;
  @Input() public isEdited: boolean = false;

  @Output('edit') private _edit = new EventEmitter<
    FileRequestBody<IPosition>
  >();

  ngOnInit(): void {
    super.init();
    this.form.patchValue(this.position);
  }

  public isDisabledForm(): boolean {
    return !(this.form.valid && this.file) && this.disabled;
  }

  public submit(): void {
    const { value } = this.form;
    const json = assign(this.position, value);
    const body = new FileRequestBody<IPosition>(json, this.file);

    this._edit.emit(body);
  }
}
