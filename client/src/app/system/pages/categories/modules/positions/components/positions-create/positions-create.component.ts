import { ANPropShow } from '@core/animations/animations';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { TViewState } from '@core/types/view.types';
import { isEqual } from '@handlers/conditions.handlers';
import { IPosition } from '../../interfaces/IPosition';
import { PositionModel } from '../../models/Position.model';
import { PositionsFormComponent } from '../positions-form/positions-form.component';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-positions-create',
  templateUrl: './positions-create.component.html',
  styleUrls: ['./positions-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'translate(-150%, 50%)',
      toValue: 'translate(-50%, -50%)',
    }),
  ],
})
export class PositionsCreateComponent extends PositionsFormComponent {
  public expandState: TViewState = 'hide';

  @Input('userId') private User: string;
  @Input('categoryId') private Category: string;
  @Input('expandState') private set _expandState(expandState: TViewState) {
    this.updateState(expandState);
  }
  @Input('isCreated') public isCreatedRequest: boolean = false;
  @Output('create') private _create = new EventEmitter<
    FileRequestBody<IPosition>
  >();

  public updateState(newState: TViewState): void {
    const { expandState: currentState } = this;
    const isReset: boolean =
      isEqual(currentState, 'show') && isEqual(newState, 'hide');

    if (isReset) this.reset();
    this.expandState = newState;
  }

  public submit(): void {
    const { value } = this.form;
    const { Category, User } = this;
    const json = Object.assign(new PositionModel(Category, User), value);
    const body = new FileRequestBody<IPosition>(json, this.file);

    this._create.emit(body);
  }
}
