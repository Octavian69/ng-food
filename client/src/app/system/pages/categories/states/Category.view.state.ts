import { TPageMode } from '@core/types/state.types';

export class CategoryViewState {
  public pageMode: TPageMode = 'unknown';
}
