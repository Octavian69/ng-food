import { FetchQuery } from '@core/utils/fetch/classes/FetchQuery';
import { Pagination } from '@core/utils/fetch/classes/Pagination';
import { FetchDataManager } from '@core/utils/fetch/managers/FetchData.manager';
import { FetchQueryManager } from '@core/utils/fetch/managers/FetchQuery.manager';
import { EnCategories } from '../enums/categories.enum';
import {
  TCategoriesDataManager,
  TCategoriesQuery,
  TCategoriesQueryManager,
} from '../types/categories.types';

export class CategoriesDataState {
  public listDataManager: TCategoriesDataManager;
  public listQueryManager: TCategoriesQueryManager;

  constructor() {
    this.initializeDataManager();
    this.initializeQueryManager();
  }

  private initializeDataManager(): void {
    this.listDataManager = new FetchDataManager();
  }

  private initializeQueryManager(): void {
    const initialQuery: TCategoriesQuery = new FetchQuery(
      new Pagination(EnCategories.FETCH_LIMIT),
    );
    this.listQueryManager = new FetchQueryManager(initialQuery);
  }

  public reset(): void {
    this.initializeDataManager();
    this.initializeQueryManager();
  }
}
