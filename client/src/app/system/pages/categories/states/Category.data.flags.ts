export class CategoryDataFlags {
  public isFetchedById: boolean = false;
  public isCreated: boolean = false;
  public isEdit: boolean = false;
  public isRemove: boolean = false;
}
