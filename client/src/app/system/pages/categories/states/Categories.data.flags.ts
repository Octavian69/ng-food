export class CategoriesDataFlags {
  public isLoad: boolean = false;
  public isNeededUpdate: boolean = false;
  public isFetched: boolean = false;
}
