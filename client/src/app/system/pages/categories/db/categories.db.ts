export const CategoriesDB = {
  'category-details': {
    TotalPositions: {
      id: 1,
      title: 'Позиции',
    },
    Description: {
      id: 2,
      title: 'Описание',
    },
    Created: {
      id: 3,
      title: 'Создана',
    },
  },

  'categories-list': {
    'table-columns': [
      { prop: 'Title', name: 'Название', sortable: false },
      { prop: 'TotalPositions', name: 'Позиции', sortable: true },
      { prop: 'Created', name: 'Создана', sortable: true },
      { prop: 'Description', name: 'Описание', sortable: false },
      { prop: 'Actions', name: 'Действия', sortable: false },
    ],
  },
};
