import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IMessage } from '@core/message/IMessage';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { Observable } from 'rxjs';
import { ICategory } from '../interfaces/ICategory';
import { TCategoriesQuery } from '../types/categories.types';

@Injectable()
export class CategoriesService {
  constructor(private http: HttpClient) {}

  public fetch(
    query: TCategoriesQuery,
  ): Observable<IPaginationResponse<ICategory>> {
    return this.http.post<IPaginationResponse<ICategory>>(
      `@/categories/fetch`,
      query,
    );
  }

  public getById(categoryId: string): Observable<ICategory> {
    return this.http.get<ICategory>(`@/categories/get-by-id/${categoryId}`);
  }

  public create(candidate: FormData): Observable<ICategory> {
    return this.http.post<ICategory>('@/categories/create', candidate);
  }

  public edit(candidate: FormData, categoryId: string): Observable<ICategory> {
    return this.http.patch<ICategory>(
      `@/categories/edit/${categoryId}`,
      candidate,
    );
  }

  public remove(categoryId: string): Observable<IMessage> {
    return this.http.delete<IMessage>(`@/categories/remove/${categoryId}`);
  }
}
