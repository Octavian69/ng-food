import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { CategoriesDB } from '../db/categories.db';
import { CatergoriesViewState } from '../states/Categories.view.state';

export class CategoriesViewService extends ExtendsFactory(
  State({
    state: CatergoriesViewState,
    db: CategoriesDB,
  }),
) {}
