import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { TInsert } from '@core/types/state.types';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CategoriesService } from './categories.service';
import { ICategory } from '../interfaces/ICategory';
import { CategoriesDataFlags } from '../states/Categories.data.flags';
import { CategoriesDataState } from '../states/Categories.data.state';
import {
  TCategoriesDataAction,
  TCategoriesDataManager,
  TCategoriesQuery,
  TCategoriesQueryManager,
} from '../types/categories.types';

@Injectable()
export class CategoriesDataService extends ExtendsFactory(
  State({
    state: CategoriesDataState,
    flags: CategoriesDataFlags,
  }),
  StreamManager<TCategoriesDataAction>(),
) {
  private categories$: Observable<IFetchData<ICategory>> = new BehaviorSubject(
    null,
  );

  constructor(private categoriesService: CategoriesService) {
    super();
  }

  public load(storageQuery: TCategoriesQuery): void {
    const dataManager: TCategoriesDataManager = this.getState(
      'state',
      'listDataManager',
    );
    const queryManager: TCategoriesQueryManager = this.getState(
      'state',
      'listQueryManager',
    );
    const query: TCategoriesQuery = queryManager.setQuery(storageQuery);

    const next = (response: IPaginationResponse<ICategory>) => {
      const categories: IFetchData<ICategory> = dataManager.replaceItems(
        response,
      );

      this.emitToStream('categories$', categories);
      this.setState('flags', 'isLoad', true);
      this.action(new StateAction('fetch', query));
    };

    this.categoriesService
      .fetch(query)
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public fetch(query: TCategoriesQuery, insert: TInsert): void {
    this.setState('flags', 'isFetched', true);

    const next = (response: IPaginationResponse<ICategory>) => {
      const dataManager: TCategoriesDataManager = this.getState(
        'state',
        'listDataManager',
      );
      const categories: IFetchData<ICategory> = dataManager.changeItems(
        response,
        insert,
      );

      this.emitToStream('categories$', categories);
      this.action(new StateAction('fetch', query));
    };

    this.categoriesService
      .fetch(query)
      .pipe(
        this.untilDestroyed(),
        finalize(() => {
          this.setState('flags', 'isFetched', false);
        }),
      )
      .subscribe(next);
  }

  public destroy(): void {
    this.destroyStates('flags');
    this.getFullState('state').reset();
    this.unsubscribe();
    this.destroyStreams([{ streamKey: 'categories$', value: null }]);
  }
}
