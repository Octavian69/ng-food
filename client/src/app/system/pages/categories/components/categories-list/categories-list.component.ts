import { Router } from '@angular/router';
import { ANShowTranslate, ANShowWidth } from '@core/animations/animations';
import { Initialize, TrackBy } from '@core/decorators/decorators';
import { IOption } from '@core/interfaces/options/IOption';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { PreloaderService } from '@core/preloader/preloader.service';
import { StorageService } from '@core/services/storage.service';
import { TableColumn } from '@core/types/libs.types';
import { TListType } from '@core/types/view.types';
import { SortQuery } from '@core/utils/fetch/classes/SortQuery';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IPagination } from '@core/utils/fetch/interfaces/IPagination';
import { isEqual, isNotEqual } from '@handlers/conditions.handlers';
import { timeoutDelay } from '@handlers/utils.handlers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { EnCategories } from '../../enums/categories.enum';
import { ICategory } from '../../interfaces/ICategory';
import { CategoriesDataService } from '../../services/categories.data.service';
import { CategoriesViewService } from '../../services/categories.view.service';
import { CategoriesDataFlags } from '../../states/Categories.data.flags';
import { CategoriesDataState } from '../../states/Categories.data.state';
import { CatergoriesViewState } from '../../states/Categories.view.state';
import { TCategoriesFilter } from '../categoies-filter/types/categories.filter.types';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import {
  ANShowCategories,
  ANShowCategoriesNav,
} from '../../animations/categories.animations';
import {
  TCategoriesDataAction,
  TCategoriesQuery,
  TCategoriesQueryManager,
  TCategoriesSortFields,
} from '../../types/categories.types';

@UntilDestroy()
@Component({
  selector: 'ng-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANShowCategories,
    ANShowCategoriesNav,
    ANShowTranslate(),
    ANShowWidth(),
  ],
})
export class CategoriesListComponent
  implements OnInit, OnInitStates, OnInitActionListeners {
  public categories$: Observable<
    IFetchData<ICategory>
  > = this.dataService.getStream('categories$');

  public dataFlags: CategoriesDataFlags;
  public dataState: CategoriesDataState;
  public viewState: CatergoriesViewState;

  // * Table options START
  public tableColumns: TableColumn[];
  // * Table options END

  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private preloaderService: PreloaderService,
    private dataService: CategoriesDataService,
    private viewService: CategoriesViewService,
    private storage: StorageService,
  ) {}

  ngOnInit(): void {
    this.initListType();
    this.initTableOptions();
    this.load();
  }

  @Initialize()
  initStates(): void {
    this.dataState = this.dataService.getFullState('state');
    this.dataFlags = this.dataService.getFullState('flags');
    this.viewState = this.viewService.getFullState('state');
  }

  @Initialize()
  initActionListeners() {
    this.dataService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe((action: TCategoriesDataAction) => {
        switch (action.type) {
          case 'fetch': {
            this.afterFetch(action.payload);
            break;
          }
        }
        timeoutDelay(() => this.cdr.detectChanges());
      });
  }

  private initListType(): void {
    const storageListType: TListType = this.storage.get(
      EnCategories.STORAGE_LIST_TYPE,
    );

    if (storageListType) {
      this.viewService.setState('state', 'listType', storageListType);
    } else {
      const deaultListType: TListType = this.viewService.getState(
        'state',
        'listType',
      );
      this.storage.set(EnCategories.STORAGE_LIST_TYPE, deaultListType);
    }
  }

  private initTableOptions(): void {
    this.tableColumns = this.viewService.getDataFromDB([
      'categories-list',
      'table-columns',
    ]);
  }

  @TrackBy('property', '_id')
  public trackByFn() {}

  private load(): void {
    const { listType } = this.viewState;
    const { isLoad, isNeededUpdate } = this.dataFlags;

    this.viewService.setState('state', 'listType', listType);

    if (isLoad) {
      if (isNeededUpdate) {
        isEqual(listType, 'list') ? this.loadList() : this.loadTable();
        this.dataService.setState('flags', 'isNeededUpdate', false);
      }
    } else {
      const storageQuery: TCategoriesQuery = this.storage.get(
        EnCategories.STORAGE_QUERY_KEY,
      );
      const query: TCategoriesQuery = isEqual(listType, 'table')
        ? storageQuery
        : null;
      this.preloaderService.firstLoading(this.categories$);
      this.dataService.load(query);
    }
  }

  private loadList(): void {
    const queryManager: TCategoriesQueryManager = this.dataService.getState(
      'state',
      'listQueryManager',
    );

    queryManager.setPage(0);
    this.dataService.fetch(queryManager.getQuery(), 'replace');
  }

  private loadTable(): void {
    const queryManager: TCategoriesQueryManager = this.dataService.getState(
      'state',
      'listQueryManager',
    );
    const { skip } = queryManager.getPagination();

    if (isNotEqual(skip, 0)) queryManager.decrement();

    this.dataService.fetch(queryManager.getQuery(), 'replace');
  }

  public expand(): void {
    const current: IFetchData<ICategory> = this.dataService.getStreamValue(
      'categories$',
    );

    if (current.isCanMakeFetch()) {
      const queryManager: TCategoriesQueryManager = this.dataService.getState(
        'state',
        'listQueryManager',
      );
      this.dataService.fetch(queryManager.nextPage(), 'end');
    }
  }

  public filter(filterValue: TCategoriesFilter): void {
    const currentQuery: TCategoriesQueryManager = this.dataService.getState(
      'state',
      'listQueryManager',
    );
    currentQuery.pagination('reset');

    this.dataService.fetch(
      currentQuery.updateQuery('filters', filterValue),
      'replace',
    );
  }

  public pagination(paginate: IPagination): void {
    const currentQuery: TCategoriesQueryManager = this.dataService.getState(
      'state',
      'listQueryManager',
    );

    currentQuery.setPagination(paginate);
    this.dataService.fetch(currentQuery.getQuery(), 'replace');
  }

  public sort(event: SortQuery<TCategoriesSortFields>): void {
    const currentQuery: TCategoriesQueryManager = this.dataService.getState(
      'state',
      'listQueryManager',
    );
    currentQuery.updateQuery('sort', event);

    this.dataService.fetch(currentQuery.getQuery(), 'replace');
  }

  public afterFetch(query: TCategoriesQuery) {
    this.storage.set(EnCategories.STORAGE_QUERY_KEY, query);
  }

  public getListDetailsOptions(): Map<keyof ICategory, IOption> {
    const details: Record<
      keyof ICategory,
      number
    > = this.viewService.getDataFromDB(['category-details']);

    return new Map(Object.entries(details) as any);
  }

  public getTablePaginate(): IPagination {
    return this.dataService
      .getState('state', 'listQueryManager')
      .getPagination();
  }

  public setListType(type: TListType): void {
    const currentType: TListType = this.viewService.getState(
      'state',
      'listType',
    );
    const notEqual: boolean = isNotEqual(currentType, type);

    if (notEqual) {
      const queryManager: TCategoriesQueryManager = this.dataService.getState(
        'state',
        'listQueryManager',
      );
      this.dataService.destroyStreams(['categories$']);
      this.preloaderService.firstLoading(this.categories$);
      this.viewService.setState('state', 'listType', type);
      this.storage.set(EnCategories.STORAGE_LIST_TYPE, type);
      queryManager.setPage(0);
      this.dataService.fetch(queryManager.getQuery(), 'replace');
    }
  }

  public navigateToCategory(categoryId: string): void {
    this.router.navigateByUrl(`/categories/${categoryId}`);
  }
}
