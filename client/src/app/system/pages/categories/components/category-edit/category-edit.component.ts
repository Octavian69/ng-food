import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { FileUpload } from '@core/file/classes/FileUpload';
import { InputEditEvent } from '@core/form/components/form-input-toggle/utils/InputEditEvent';
import { RequiredRange } from '@core/form/decorators/decorators';
import { getRange } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { TUpdateDataAction } from '@core/types/data.types';
import { ICategory } from '../../interfaces/ICategory';
import { NSCategoriesValidation } from '../../namespaces/categories.namespaces';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    // AnPro
  ],
})
export class CategoryEditComponent implements OnInit {
  public form: FormGroup;

  @Input() category: ICategory;

  @Output('edit') _edit = new EventEmitter<FileRequestBody<ICategory>>();
  @Output('remove') _remove = new EventEmitter<string>();
  @Output('updatePositions') _updatePositions = new EventEmitter();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    const { Title, Description } = this.category;

    this.form = this.fb.group({
      Title: [
        Title,
        [
          Validators.required,
          Validators.minLength(
            getRange(NSCategoriesValidation, 'Title', 'min'),
          ),
          Validators.maxLength(
            getRange(NSCategoriesValidation, 'Title', 'max'),
          ),
        ],
      ],
      Description: [
        Description,
        [
          Validators.maxLength(
            getRange(NSCategoriesValidation, 'Description', 'max'),
          ),
        ],
      ],
    });
  }

  @RequiredRange(NSCategoriesValidation)
  public getRange(controlName: string, rangeType: TRange) {}

  public changeFile(upload: FileUpload<'IMG'>): void {
    const { file } = upload;

    this.edit(null, file);
  }

  public changeValue({ proprety, value }: InputEditEvent): void {
    this.edit({ [proprety]: value });
  }

  public edit(edited: Partial<ICategory>, file: File = null): void {
    const json: ICategory = Object.assign({}, this.category, edited);
    const body: FileRequestBody<ICategory> = new FileRequestBody(json, file);

    this._edit.emit(body);
  }

  public confirmRemove(): void {
    const message: string = 'Вы действительно хотите удалить категорию ?';

    this._remove.emit(message);
  }

  public updateCategoryPositions(action: TUpdateDataAction): void {
    this._updatePositions.emit(action);
  }
}
