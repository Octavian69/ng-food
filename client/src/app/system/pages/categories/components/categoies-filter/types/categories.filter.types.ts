import { CategoriesFilter } from '../classes/CategoriesFilter';

export type TCategoriesFilter = Partial<CategoriesFilter>;
