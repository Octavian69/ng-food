import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ANShowTranslate } from '@core/animations/animations';
import { getRange, makePureFilter } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { TViewState } from '@core/types/view.types';
import { isEqual } from '@handlers/conditions.handlers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TCategoriesFilter } from './types/categories.filter.types';
import { ICategory } from '../../interfaces/ICategory';
import { NSCategoriesValidation } from '../../namespaces/categories.namespaces';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  Bind,
  DisabledFormStatus,
  RequiredRange,
} from '@core/form/decorators/decorators';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
} from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'ng-categories-filter',
  templateUrl: './categories-filter.component.html',
  styleUrls: ['./categories-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANShowTranslate({
      timing: '0.2s',
      axis: 'Y',
      from: '2rem',
    }),
  ],
})
export class CategoriesFilterComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public filterViewState: TViewState = 'hide';
  @DisabledFormStatus('form')
  public disabled: boolean;

  @Input() filterValue: TCategoriesFilter;
  @Input() loading: boolean = false;
  @Output('filter') _filter = new EventEmitter<TCategoriesFilter>();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
    this.listenSearchTitleFilter();
  }

  ngOnDestroy() {}

  @RequiredRange(NSCategoriesValidation)
  public getRange(controlName: keyof ICategory, rangeType: TRange) {}

  private initForm(): void {
    this.form = this.fb.group(
      {
        Title: [
          null,
          [
            Validators.maxLength(
              getRange(NSCategoriesValidation, 'Title', 'max'),
            ),
          ],
        ],
        TotalPositions: [null],
        Created: [null],
      },
      { updateOn: 'change' },
    );

    if (this.filterValue) {
      this.form.patchValue(this.filterValue);
    }
  }

  private listenSearchTitleFilter(): void {
    this.form
      .get('Title')
      .valueChanges.pipe(
        distinctUntilChanged(),
        debounceTime(300),
        filter((v) => v !== null),
        map((_) => this.form.value),
        untilDestroyed(this),
      )
      .subscribe(this.filter);
  }

  public toogleViewFilterState(): void {
    this.form.get('Title').markAsUntouched();
    this.filterViewState = isEqual(this.filterViewState, 'show')
      ? 'hide'
      : 'show';
  }

  public reset(): void {
    this.form.reset();
    this.filter(this.form.value);
  }

  @Bind()
  public filter(value: TCategoriesFilter): void {
    const filter: TCategoriesFilter = makePureFilter(value);

    this._filter.emit(filter);
    this.form.markAsUntouched();
  }
}
