import { RangeValue } from '@core/form/classes/RangeValue';

export class CategoriesFilter {
  public Title: string = null;
  public Created: RangeValue<Date> = null;
  public TotalPositions: RangeValue<number> = null;
}
