import { formatDate } from '@angular/common';
import { TrackBy } from '@core/decorators/decorators';
import { IOption } from '@core/interfaces/options/IOption';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { TKeyValue } from '@core/types/libs.types';
import { joinUrl } from '@handlers/string.handlers';
import { ICategory } from '../../interfaces/ICategory';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-categories-list-card',
  templateUrl: './categories-list-card.component.html',
  styleUrls: ['./categories-list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesListCardComponent {
  public DESCRIPTION_LENGTH = 100;
  public isOpenInfo: boolean = false;

  @Input() category: ICategory;
  @Input() detailsOptions: Map<keyof ICategory, IOption>;

  @Output('navigate') private _navigate = new EventEmitter<string>();

  public getdefaultUrl(theme: 'dark' | 'default'): string {
    const themeImg: string =
      theme === 'dark' ? 'dark-preview.png' : 'light-preview.png';
    const path: string[] = ['categories', themeImg];

    return joinUrl(path);
  }

  @TrackBy('property', '_id')
  public trackByFn() {}

  public toggleInfo(): void {
    this.isOpenInfo = !this.isOpenInfo;
  }

  public sortDetails<
    K extends keyof ICategory,
    D extends TKeyValue<K, ICategory[K]>
  >(details: D[]): D[] {
    return details.sort((a, b) => {
      const { id: positionA } = this.detailsOptions.get(a.key);
      const { id: positionB } = this.detailsOptions.get(b.key);

      return positionA - positionB;
    });
  }

  @TrackBy()
  public trackByDetails() {}

  public getDetailsKeys(): (keyof ICategory)[] {
    return ['TotalPositions', 'Description', 'Created'];
  }

  public mapDetailsValue<K extends keyof ICategory>(
    key: K,
    value: ICategory[K],
  ): unknown {
    switch (key) {
      case 'TotalPositions': {
        return this.mapTotalPositions(value as number);
      }
      case 'Description': {
        return this.mapDescription(value as string);
      }
      case 'Created': {
        return this.mapCreated(value as Date);
      }
    }
  }

  private mapTotalPositions(value: number): string | number {
    return value || 'Отсутствуют';
  }

  private mapDescription(value: string): string {
    if (value?.length > this.DESCRIPTION_LENGTH) {
      return value.slice(0, this.DESCRIPTION_LENGTH).concat('...');
    }

    return value;
  }

  private mapCreated(value: Date): string {
    return formatDate(value, 'mediumDate', 'ru');
  }

  public getDetailClass(key: keyof ICategory): ISimple<boolean> {
    return {
      [key.toLocaleLowerCase()]: true,
    };
  }

  public getDetailTitle(titleKey: keyof ICategory): string {
    return this.detailsOptions.get(titleKey).title;
  }

  public navigateToCategory(): void {
    this._navigate.emit(this.category._id);
  }
}
