import { TableColumn } from '@core/types/libs.types';
import { SortQuery } from '@core/utils/fetch/classes/SortQuery';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IPagination } from '@core/utils/fetch/interfaces/IPagination';
import { prefix } from '@handlers/string.handlers';
import { ICategory } from '../../interfaces/ICategory';
import { TCategoriesSortFields } from '../../types/categories.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
@Component({
  selector: 'ng-categories-list-table',
  templateUrl: './categories-list-table.component.html',
  styleUrls: ['./categories-list-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesListTableComponent implements OnInit {
  public completedColumns: TableColumn[];

  @Input() public categories: IFetchData<ICategory>;
  @Input() private columns: TableColumn[];
  @Input() public paginate: IPagination;
  @Input() public loading: boolean;

  @Output('pagination') _pagination = new EventEmitter<IPagination>();
  @Output('sort') _sort = new EventEmitter<SortQuery<TCategoriesSortFields>>();
  @Output('navigate') private _navigate = new EventEmitter<string>();

  // * Table cell templates
  @ViewChild('titleTpl', { static: true, read: TemplateRef })
  titleTpl: TemplateRef<any>;
  @ViewChild('totalPositionsTpl', { static: true, read: TemplateRef })
  totalPositionsTpl: TemplateRef<any>;
  @ViewChild('createdTpl', { static: true, read: TemplateRef })
  createdTpl: TemplateRef<any>;
  @ViewChild('descriptionTpl', { static: true, read: TemplateRef })
  descriptionTpl: TemplateRef<any>;
  @ViewChild('actionsTpl', { static: true, read: TemplateRef })
  actionsTpl: TemplateRef<any>;
  // * Table cell templates

  ngOnInit() {
    this.initColumns();
  }

  private initColumns(): void {
    this.completedColumns = this.getModifiedColumns(this.columns);
  }

  private getModifiedColumns(columns: TableColumn[]): TableColumn[] {
    const tableCellTemplates: Record<string, TemplateRef<null>> = {
      Title: this.titleTpl,
      TotalPositions: this.totalPositionsTpl,
      Created: this.createdTpl,
      Description: this.descriptionTpl,
      Actions: this.actionsTpl,
    };

    return columns.map((column: TableColumn) => {
      const cellTemplate: TemplateRef<null> = tableCellTemplates[column.prop];
      const headerClass: string = prefix(
        column.prop.toLocaleLowerCase(),
        'header-',
      );
      const cellClass: string = prefix(
        column.prop.toLocaleLowerCase(),
        'cell-',
      );

      return Object.assign({}, column, {
        cellTemplate,
        headerClass,
        cellClass,
      });
    });
  }

  public sort(sorted: SortQuery<TCategoriesSortFields>): void {
    this._sort.emit(sorted);
  }

  public pagination(paginate: IPagination): void {
    this._pagination.emit(paginate);
  }

  public navigateToCategory(categoryId: string): void {
    this._navigate.emit(categoryId);
  }
}
