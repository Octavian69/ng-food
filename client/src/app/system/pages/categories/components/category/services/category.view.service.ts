import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { CategoryViewState } from '../../../states/Category.view.state';

@Injectable()
export class CategoryViewService extends ExtendsFactory(
  State({ state: CategoryViewState }),
) {}
