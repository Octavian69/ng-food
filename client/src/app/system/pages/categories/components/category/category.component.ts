import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ANPropShow } from '@core/animations/animations';
import { ConfirmService } from '@core/confirm/confirm.service';
import { Initialize, isCreatedMode } from '@core/decorators/decorators';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitDetectionChanges } from '@core/managers/interfaces/hooks/OnInitDetectionChanges';
import { OnInitErrorListener } from '@core/managers/interfaces/hooks/OnInitErrorListener';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { PreloaderService } from '@core/preloader/preloader.service';
import { StorageService } from '@core/services/storage.service';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { TUpdateDataAction } from '@core/types/data.types';
import { getPageMode } from '@handlers/conditions.handlers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { CategoryDataService } from './services/category.data.service';
import { CategoryViewService } from './services/category.view.service';
import { TCategoryDataAction } from './types/category.types';
import { ICategory } from '../../interfaces/ICategory';
import { CategoriesDataService } from '../../services/categories.data.service';
import { CategoriesViewService } from '../../services/categories.view.service';
import { CategoryDataFlags } from '../../states/Category.data.flags';
import { CategoryViewState } from '../../states/Category.view.state';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  TDetectionEvent,
  TErrorEvent,
} from '@core/managers/types/managers.types';
@UntilDestroy()
@Component({
  selector: 'ng-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANPropShow()],
})
export class CategoryComponent
  implements
    OnInit,
    OnDestroy,
    OnInitActionListeners,
    OnInitErrorListener,
    OnInitDetectionChanges,
    OnInitStates {
  public category$: Observable<ICategory> = this.dataService.getStream(
    'category$',
  );

  public viewState: CategoryViewState;
  public dataFlags: CategoryDataFlags;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private toastr: ToastrService,
    private preloader: PreloaderService,
    private storage: StorageService,
    private confirmService: ConfirmService,
    private dataService: CategoryDataService,
    private viewService: CategoryViewService,
    private categoriesDataService: CategoriesDataService,
    private categoriesViewService: CategoriesViewService,
  ) {}

  ngOnInit(): void {
    this.initActiveRouteSub();
  }

  ngOnDestroy() {
    this.dataService.destroy();
    this.viewService.destroyStates('state');
  }

  @Initialize()
  initStates(): void {
    this.viewState = this.viewService.getFullState('state');
    this.dataFlags = this.dataService.getFullState('flags');
  }

  @Initialize()
  initActionListeners(): void {
    this.dataService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe((action: TCategoryDataAction) => {
        switch (action.type) {
          case 'get-by-id': {
            this.afterGetById(action.payload);
            break;
          }
          case 'create': {
            this.afterCreate(action.payload);
            break;
          }
          case 'edit': {
            this.afterEdit(action.payload);
            break;
          }
          case 'remove': {
            this.afterRemove(action.payload);
            break;
          }
        }
      });
  }

  @Initialize()
  initErrorListener(): void {
    this.dataService
      .listenErrors()
      .pipe(untilDestroyed(this))
      .subscribe((type: TErrorEvent<TCategoryDataAction>) => {
        switch (type) {
          case 'error-create':
          case 'error-get-by-id': {
            this.preloader.hide();
            this.router.navigateByUrl('/categories');
            break;
          }
        }

        this.cdr.detectChanges();
      });
  }

  @Initialize()
  initDetectionChanges(): void {
    this.dataService
      .detectChanges()
      .pipe(untilDestroyed(this))
      .subscribe((type: TDetectionEvent<TCategoryDataAction>) => {
        this.cdr.detectChanges();
      });
  }

  private initActiveRouteSub(): void {
    this.activeRoute.paramMap.subscribe((paramMap: ParamMap) => {
      this.preloader.show();
      this.dataService.getById(paramMap.get('id'));
    });
  }

  @isCreatedMode()
  public isCreatedMode() {}

  private afterGetById(category: ICategory): void {
    const mode = getPageMode(category._id);

    this.viewService.setState('state', 'pageMode', mode);
    this.preloader.hide();
  }

  public create(body: FileRequestBody<ICategory>) {
    this.dataService.create(body);
  }

  private afterCreate(category: ICategory): void {
    const message: string = `Категория ${category.Title} создана.`;

    this.toastr.success(message);
    this.updateCategories();
    this.router.navigateByUrl(`/categories/${category._id}`);
  }

  public edit(body: FileRequestBody<ICategory>): void {
    this.dataService.edit(body);
  }

  private afterEdit(category: ICategory): void {
    const message: string = `Категория "${category.Title} обновлена."`;
    this.toastr.success(message);
    this.updateCategories();
  }

  public remove(): void {
    this.dataService.remove();
  }

  public afterRemove(category: ICategory): void {
    const message: string = `Категория ${category.Title} удалена.`;

    this.toastr.success(message);
    this.confirmService.loading('hide');
    this.router.navigate(['/categories']);
    this.updateCategories();
  }

  public async confirmRemove(message: string): Promise<void> {
    const isCanRemove: boolean = await this.confirmService.confirm(
      message,
      true,
    );

    if (isCanRemove) this.remove();
  }

  public updatePositions(action: TUpdateDataAction): void {
    this.dataService.updateCategoryPositions(action);
  }

  private updateCategories(): void {
    const isLoad: boolean = this.categoriesDataService.getState(
      'flags',
      'isLoad',
    );

    if (isLoad) {
      this.categoriesDataService.setState('flags', 'isNeededUpdate', true);
    }
  }
}
