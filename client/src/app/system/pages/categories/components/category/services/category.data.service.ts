import { Injectable } from '@angular/core';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { convertToFormData } from '@core/file/handlers/file.handlers';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { TUpdateDataAction } from '@core/types/data.types';
import { isEditPageMode } from '@handlers/conditions.handlers';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ICategory } from '../../../interfaces/ICategory';
import { CategoryModel } from '../../../models/Category.model';
import { CategoriesService } from '../../../services/categories.service';
import { CategoryDataFlags } from '../../../states/Category.data.flags';
import { TCategoryDataAction } from '../types/category.types';

@Injectable()
export class CategoryDataService extends ExtendsFactory(
  State({ flags: CategoryDataFlags }),
  StreamManager<TCategoryDataAction>(),
) {
  private category$: BehaviorSubject<ICategory> = new BehaviorSubject(null);

  constructor(private categoriesService: CategoriesService) {
    super(new CategoryDataFlags());
  }

  public getById(categoryId: string): void {
    this.setState('flags', 'isFetchedById', true);

    if (isEditPageMode(categoryId)) {
      this.getEditedCategory(categoryId);
    } else {
      this.getCreatedCategory();
    }
  }

  private getCreatedCategory(): void {
    this.pushCategory(new CategoryModel());
    this.detect('get-by-id');
  }

  private getEditedCategory(categoryId: string): void {
    this.categoriesService
      .getById(categoryId)
      .pipe(
        this.untilDestroyed(),
        finalize(() => this.detect('get-by-id')),
      )
      .subscribe(this.pushCategory, this.detectError('error-get-by-id'));
  }

  private pushCategory = (category: ICategory) => {
    this.emitToStream('category$', category);
    this.action(new StateAction('get-by-id', category));
    this.setState('flags', 'isFetchedById', false);
  };

  public create(candidate: FileRequestBody<ICategory>): void {
    this.setState('flags', 'isCreated', true);

    const body: FormData = convertToFormData(candidate, 'preview', 'category');

    const next = (category: ICategory) => {
      this.setState('flags', 'isCreated', false);
      this.action(new StateAction('create', category));
    };

    this.categoriesService
      .create(body)
      .pipe(
        this.untilDestroyed(),
        finalize(() => this.detect('create')),
      )
      .subscribe(next, this.detectError('error-create'));
  }

  public edit(candidate: FileRequestBody<ICategory>): void {
    this.setState('flags', 'isEdit', true);

    const body: FormData = convertToFormData(candidate, 'preview', 'category');

    const next = (category: ICategory) => {
      this.emitToStream('category$', category);
      this.setState('flags', 'isEdit', false);
      this.action(new StateAction('edit', category));
    };

    this.categoriesService
      .edit(body, candidate.json._id)
      .pipe(
        this.untilDestroyed(),
        finalize(() => this.detect('edit')),
      )
      .subscribe(next, this.detectError('error-edit'));
  }

  public remove(): void {
    this.setState('flags', 'isRemove', true);

    const { _id: categoryId } = this.getStreamValue('category$');

    const next = (category: ICategory) => {
      this.action(new StateAction('remove', category));
      this.setState('flags', 'isRemove', false);
    };

    this.categoriesService
      .remove(categoryId)
      .pipe(
        this.untilDestroyed(),
        finalize(() => this.detect('remove')),
      )
      .subscribe(next, this.detectError('error-remove'));
  }

  public updateCategoryPositions(action: TUpdateDataAction): void {
    const category: ICategory = this.getStreamValue('category$');
    switch (action) {
      case 'add': {
        category.TotalPositions++;
        break;
      }

      case 'remove': {
        category.TotalPositions--;
        break;
      }
    }

    this.emitToStream('category$', category);
  }

  public destroy(): void {
    this.destroyStates('flags');
    this.destroyStreams(['category$']);
  }
}
