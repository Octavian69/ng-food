import { IStateAction } from '@core/managers/interfaces/IStateAction';
import { ICategory } from '../../../interfaces/ICategory';

export type TCategoryDataAction = IStateAction<
  'create' | 'remove' | 'edit' | 'get-by-id',
  ICategory
>;
