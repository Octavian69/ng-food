import { Component, OnDestroy } from '@angular/core';
import { StorageService } from '@core/services/storage.service';
import { EnCategories } from '../../enums/categories.enum';
import { CategoriesDataService } from '../../services/categories.data.service';

@Component({
  selector: 'ng-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss'],
})
export class CategoriesPageComponent implements OnDestroy {
  constructor(
    private dataService: CategoriesDataService,
    private storage: StorageService,
  ) {}

  ngOnDestroy() {
    this.dataService.destroy();
    this.storage.remove(EnCategories.STORAGE_QUERY_KEY);
  }
}
