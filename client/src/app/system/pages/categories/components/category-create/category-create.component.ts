import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileRequestBody } from '@core/file/classes/FileRequestBody';
import { FileUpload } from '@core/file/classes/FileUpload';
import { getRange } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { ICategory } from '../../interfaces/ICategory';
import { NSCategoriesValidation } from '../../namespaces/categories.namespaces';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {
  DisabledFormStatus,
  RequiredRange,
} from '@core/form/decorators/decorators';

@Component({
  selector: 'ng-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryCreateComponent implements OnInit {
  public file: File;
  public form: FormGroup;
  public preview: string = null;
  @DisabledFormStatus() public disabled: boolean;
  @Input() category: ICategory;
  @Input() loading: boolean = false;

  @Output('create') _create = new EventEmitter<FileRequestBody<ICategory>>();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    const { Title, Description } = this.category;

    this.form = this.fb.group({
      Title: [
        Title,
        [
          Validators.required,
          Validators.minLength(
            getRange(NSCategoriesValidation, 'Title', 'min'),
          ),
          Validators.maxLength(
            getRange(NSCategoriesValidation, 'Title', 'max'),
          ),
        ],
      ],
      Description: [
        Description,
        [
          Validators.maxLength(
            getRange(NSCategoriesValidation, 'Description', 'max'),
          ),
        ],
      ],
    });
  }

  public isEmptyPreview(): boolean {
    return !this.preview && !this.category.Preview;
  }

  @RequiredRange(NSCategoriesValidation)
  public getRange(controlName: string, rangeType: TRange) {}

  public changeFile(upload: FileUpload<'IMG'>): void {
    const {
      file,
      payload: { preview },
    } = upload;

    this.preview = preview;
    this.file = file;
  }

  public create(): void {
    const { value } = this.form;
    const json: ICategory = Object.assign({}, this.category, value);
    const body: FileRequestBody<ICategory> = new FileRequestBody(
      json,
      this.file,
    );

    this._create.emit(body);
  }
}
