import { NgModule } from '@angular/core';
import { CrumbsModule } from '@core/crumbs/crumbs.module';
import { SharedModule } from '@core/modules/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { AnalyticsChartHeatMapComponent } from './components/analytics-chart-heat-map/analytics-chart-heat-map.component';
import { AnalyticsFilterChartsComponent } from './components/analytics-filter-charts/analytics-filter-charts.component';
import { AnalyticsFilterPeriodComponent } from './components/analytics-filter-period/analytics-filter-period.component';
import { AnalyticsFilterComponent } from './components/analytics-filter/analytics-filter.component';
import { AnalyticsOrdersComponent } from './components/analytics-orders/analytics-orders.component';
import { AnalyticsPageComponent } from './components/analytics-page/analytics-page.component';
import { AnalyticsPieAdvancedChartComponent } from './components/analytics-pie-advanced-chart/analytics-pie-advanced-chart.component';
import { AnalyticsPieChartComponent } from './components/analytics-pie-chart/analytics-pie-chart.component';
import { AnalyticsPieGridChartComponent } from './components/analytics-pie-grid-chart/analytics-pie-grid-chart.component';
import { AnalyticsPositionsComponent } from './components/analytics-positions/analytics-positions.component';
import { AnalyticsDataSerivce } from './services/analytics.data.service';
import { AnalyticsViewService } from './services/analytics.view.service';

@NgModule({
  declarations: [
    AnalyticsPageComponent,
    AnalyticsOrdersComponent,
    AnalyticsPieGridChartComponent,
    AnalyticsPositionsComponent,
    AnalyticsPieChartComponent,
    AnalyticsFilterComponent,
    AnalyticsFilterChartsComponent,
    AnalyticsFilterPeriodComponent,
    AnalyticsPieAdvancedChartComponent,
    AnalyticsChartHeatMapComponent,
  ],
  imports: [
    SharedModule,
    CrumbsModule,
    AnalyticsRoutingModule,
    NgxChartsModule,
  ],
  providers: [AnalyticsDataSerivce, AnalyticsViewService],
})
export class AnalyticsModule {}
