export enum EnAnalyticsFilter {
  ORDERS_PERIOD_TOTAL_AMOUNT = 'orders:TotalAmount',
  ORDERS_DAYS_TOTAL_AMOUNT = 'orders:DaysTotalAmount',
  POSITIONS_QUANTITY = 'positions:Quantity',
  POSITIONS_TOTAL_COST = 'positions:TotalAmount',
}
