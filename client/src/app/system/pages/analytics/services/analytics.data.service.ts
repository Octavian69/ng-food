import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { AnalyticsService } from './analytics.service';
import { EnAnalyticsFilter } from '../enums/analytics-filter.enum';
import { IAnalyticsOrders } from '../interfaces/IAnalyticsOrders';
import { IAnalyticsPositionItem } from '../interfaces/IAnalyticsPositionItem';
import { AnalyticsDataFlags } from '../states/analytics.data.flags';
import { AnalyticsDataState } from '../states/analytics.data.state';
import {
  TAnalyticsFilterResponse,
  TAnalyticsForPeriod,
  TAnanlyticsFilterPeriod,
} from '../types/analytics.types';

@Injectable()
export class AnalyticsDataSerivce extends ExtendsFactory(
  State({
    flags: AnalyticsDataFlags,
    state: AnalyticsDataState,
  }),
  StreamManager(['unsubscribe']),
) {
  private ordersAnalytics$: BehaviorSubject<IAnalyticsOrders> = new BehaviorSubject(
    null,
  );
  private positionsAnalytics$: BehaviorSubject<
    IAnalyticsPositionItem[]
  > = new BehaviorSubject(null);
  private filter$: BehaviorSubject<
    TAnalyticsFilterResponse[]
  > = new BehaviorSubject(null);

  constructor(private analyticsService: AnalyticsService) {
    super();
  }

  public fetchOrdersAnalytics(): void {
    this.setState('flags', 'isLoadOrders', false);
    const next = (ordersAnalytics: IAnalyticsOrders) => {
      this.emitToStream('ordersAnalytics$', ordersAnalytics);
      this.setState('flags', 'isLoadOrders', true);
    };

    this.analyticsService
      .fetchOrdersAnalytics()
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public fetchPositionsAnalytics(): void {
    this.setState('flags', 'isLoadPositions', false);
    const next = (positionsAnalytics: IAnalyticsPositionItem[]) => {
      this.emitToStream('positionsAnalytics$', positionsAnalytics);
      this.setState('flags', 'isLoadPositions', true);
    };

    this.analyticsService
      .fetchPositionsAnalytics()
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public filter(
    method: keyof AnalyticsService,
    periods: TAnanlyticsFilterPeriod[],
    type: EnAnalyticsFilter,
  ): void {
    this.getFullState('state').toggleFiltered(type);

    const next = (response: TAnalyticsForPeriod[]) => {
      const responsePeriods: TAnalyticsFilterResponse[] = response.map(
        (value, i) => {
          const { id } = periods[i];
          return { id, value };
        },
      );
      this.emitToStream('filter$', responsePeriods);

      this.getFullState('state').toggleFiltered(type);
    };
    const requests: Observable<TAnalyticsForPeriod>[] = periods.map((p) =>
      this.analyticsService[method](p.query),
    );

    forkJoin(requests).pipe(this.untilDestroyed()).subscribe(next);
  }

  public destroy(): void {
    this.destroyStreams(['ordersAnalytics$', 'positionsAnalytics$', 'filter$']);
  }
}
