import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IAnalyticsOrders } from '../interfaces/IAnalyticsOrders';
import { IAnalyticsPositionItem } from '../interfaces/IAnalyticsPositionItem';
import {
  TAnalayticsForPositions,
  TAnalyticsFilterQuery,
  TAnalyticsForOrdersDaysAmount,
  TAnalyticsForOrdersTotalAmount,
} from '../types/analytics.types';

@Injectable()
export class AnalyticsService {
  constructor(private http: HttpClient) {}

  // *Orders

  public fetchOrdersAnalytics(): Observable<IAnalyticsOrders> {
    return this.http.get<IAnalyticsOrders>('@/analytics/fetch-orders-profit');
  }

  public filterOrdersPeriodProfit(
    period: TAnalyticsFilterQuery<'orders'>,
  ): Observable<TAnalyticsForOrdersTotalAmount> {
    return this.http.post<TAnalyticsForOrdersTotalAmount>(
      '@/analytics/filter-orders-period-profit',
      period,
    );
  }
  public filterOrdersDaysProfit(
    period: TAnalyticsFilterQuery<'orders'>,
  ): Observable<TAnalyticsForOrdersDaysAmount[]> {
    return this.http.post<TAnalyticsForOrdersDaysAmount[]>(
      '@/analytics/filter-orders-days-profit',
      period,
    );
  }

  // *Positions

  public fetchPositionsAnalytics(): Observable<IAnalyticsPositionItem[]> {
    return this.http.get<IAnalyticsPositionItem[]>(
      '@/analytics/fetch-positions',
    );
  }

  public filterPositionsAnalytics(
    period: TAnalyticsFilterQuery<'positions'>,
  ): Observable<TAnalayticsForPositions> {
    return this.http.post<TAnalayticsForPositions>(
      '@/analytics/filter-positions',
      period,
    );
  }
}
