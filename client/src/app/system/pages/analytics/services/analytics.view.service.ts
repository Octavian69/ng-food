import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { AnalyticsDB } from '../db/analytics.db';
import { AnalyticsViewFlags } from '../states/analytics.view.flags';

@Injectable()
export class AnalyticsViewService extends ExtendsFactory(
  State({
    flags: AnalyticsViewFlags,
    db: AnalyticsDB,
  }),
) {}
