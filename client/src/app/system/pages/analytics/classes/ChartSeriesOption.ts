export class ChartSeriesOption {
  constructor(
    public name: string,
    public value: number,
    public extra?: object,
  ) {}
}
