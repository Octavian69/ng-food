import { ChartSeriesOption } from './ChartSeriesOption';

export class ChartOption {
  constructor(
    public name: string,
    public series: ChartSeriesOption[],
    public extra?: object,
  ) {}
}
