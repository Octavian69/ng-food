import { randomid } from '@handlers/string.handlers';
import {
  TAnalyticsFilterChangeValue,
  TAnalyticsFilterFormValue,
  TAnalyticsForPeriod,
  TAnalyticsPeriodSplitValue,
} from '../types/analytics.types';

export class AnalyticsFilterPeriod {
  public id: string = randomid();
  public valid: boolean = false;
  public analytics: TAnalyticsForPeriod;
  public isExistChart: boolean = false;

  constructor(public value: TAnalyticsFilterFormValue, public dirty: boolean) {}

  public transformToChangeValue(
    value: TAnalyticsFilterFormValue,
    valid: boolean,
  ): TAnalyticsFilterChangeValue {
    const { id } = this;
    return { id, value, valid };
  }

  public getSplittedValue(): TAnalyticsPeriodSplitValue {
    return this.value.type.split(':') as TAnalyticsPeriodSplitValue;
  }

  public setAnanlytics(analytics: TAnalyticsForPeriod): void {
    this.analytics = analytics;
    this.isExistChart = true;
  }

  public update(value, valid: boolean): AnalyticsFilterPeriod {
    this.value = value;
    this.valid = valid;
    this.dirty = true;

    return this;
  }

  public markAsUndirty(): void {
    this.dirty = false;
  }

  public markAsDirty(): void {
    this.dirty = true;
  }
}
