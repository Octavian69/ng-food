export interface IAnalyticsPositionItem {
  Title: string;
  TotalAmount: number;
  Quantity: number;
}
