import { IAnalyticsOrders } from './IAnalyticsOrders';

export interface IAnalyticsFilterOrders
  extends Pick<IAnalyticsOrders, 'TotalAmount'> {}
