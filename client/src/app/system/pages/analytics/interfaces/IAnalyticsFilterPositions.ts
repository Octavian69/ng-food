import { ExtractKeys } from '@core/types/libs.types';
import { IAnalyticsPositionItem } from './IAnalyticsPositionItem';

type TPositonsKeys = ExtractKeys<
  IAnalyticsPositionItem,
  'Quantity' | 'TotalAmount'
>;
type TPositionsAnalytics = {
  [K in TPositonsKeys]?: number;
};

export interface IAnalyticsFilterPositions
  extends TPositionsAnalytics,
    Pick<IAnalyticsPositionItem, 'Title'> {}
