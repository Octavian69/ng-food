import { TAnalyticsValueType } from '../types/analytics.types';

export class AnalyticsDataState {
  public filteredCharts: TAnalyticsValueType[] = [];

  public toggleFiltered(type: TAnalyticsValueType): void {
    const idx: number = this.filteredCharts.indexOf(type);

    if (~idx) {
      this.filteredCharts.splice(idx, 1);
    } else {
      this.filteredCharts.push(type);
    }
  }
}
