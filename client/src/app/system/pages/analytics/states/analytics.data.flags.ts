export class AnalyticsDataFlags {
  public isLoadOrders: boolean = false;
  public isLoadPositions: boolean = false;
  public isFiltered: boolean = false;
}
