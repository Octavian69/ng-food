import { RangeValue } from '@core/form/classes/RangeValue';
import { IfElse } from '@core/managers/types/managers.types';
import { ExtractKeys } from '@core/types/libs.types';
import { ChartOption } from '../classes/ChartOption';
import { ChartSeriesOption } from '../classes/ChartSeriesOption';
import { EnAnalyticsFilter } from '../enums/analytics-filter.enum';
import { IAnalyticsOrders } from '../interfaces/IAnalyticsOrders';
import { IAnalyticsPositionItem } from '../interfaces/IAnalyticsPositionItem';

// *  --------------   --------------  --------------  --------------

export type TAnalyticsChartType =
  | 'pie'
  | 'pie-grid'
  | 'pie-advanced'
  | 'heat-map';

//  *Orders
export type TAnalyticsOrderPeriod = 'year' | 'month' | 'day';
export type TAnalyticsOrderPeriodOption = {
  periodTitle: string;
  moment: TAnalyticsOrderMoment;
  period: TAnalyticsOrderPeriod;
  groupOptions: TOrderPeriodGroupOption[];
};

export type TAnalyticsOrderMoment = {
  time: 'y' | 'M' | 'd';
  format: string;
};
export type TOrderPeriodGroupOption = {
  title: string;
  field: Exclude<keyof IAnalyticsOrders, 'TotalAmount'>;
  add?: number;
};

//  *Orders

// *  --------------   --------------  --------------  --------------

//  *Positions

export type TAnalyticsPositionsOption = {
  title: string;
  field: ExtractKeys<IAnalyticsPositionItem, 'Quantity' | 'TotalAmount'>;
  extra: {
    optionsValueSuffix: string;
  };
};

//  *Positions

// *  --------------   --------------  --------------  --------------

//  *Filters

export type TAnalyticsChartParser<
  T extends string
> = T extends `${infer Type}:${string}` ? Type : never;
export type TAnalyticsTypeParser<
  T extends string
> = T extends `${string}:${infer Type}` ? Type : never;

export type TAnalyticsPeriodSplitValue = [
  TAnalyticsChartParser<TAnalyticsValueType>,
  TAnalyticsTypeParser<TAnalyticsValueType>,
];
export type TAnalyticsValueType =
  | TAnalyticsOrdersValue
  | TAnalyticsPositionsValue;

export type TAnalyticsOrdersValue =
  | 'orders:TotalAmount'
  | 'orders:DaysTotalAmount';
export type TAnalyticsForOrdersTotalAmount = {
  TotalAmount: number;
};
export type TAnalyticsForOrdersDaysAmount = TAnalyticsForOrdersTotalAmount & {
  Date: Date;
};

export type TAnalyticsForOrders =
  | TAnalyticsForOrdersTotalAmount
  | TAnalyticsForOrdersDaysAmount[];

export type TAnalyticsPositionsValue =
  | 'positions:TotalAmount'
  | 'positions:Quantity';

export type TAnalyticsPositionsValueField = TAnalyticsTypeParser<TAnalyticsPositionsValue>;

export type TAnalayticsForPositions = ({
  [K in TAnalyticsPositionsValueField]?: number;
} & { Title: string })[];

export type TAnalyticsForPeriod = TAnalyticsForOrders | TAnalayticsForPositions;

export type TAnalyticsFilterFormValue = {
  type: EnAnalyticsFilter;
  range: Required<RangeValue<Date>>;
};

export type TAnalyticsFilterChangeValue = {
  id: string;
  value: TAnalyticsFilterFormValue;
  valid: boolean;
};

export type TAnanlyticsFilterPeriod = {
  id: string;
  type: TAnalyticsValueType;
  query: TAnalyticsFilterQuery;
};

export type TAnalyticsFilterQuery<
  T extends TAnalyticsChartParser<TAnalyticsValueType> = TAnalyticsChartParser<TAnalyticsValueType>
> = {
  type: TAnalyticsTypeParser<
    IfElse<T, 'orders', TAnalyticsOrdersValue, TAnalyticsPositionsValue>
  >;
  range: TAnalyticsFilterFormValue['range'];
};

export type TAnalyticsFilterPeriodChart = {
  periodId: string;
  type: TAnalyticsChartType;
  title: string;
  value: TAnalyticsFilterFormValue;
  series: Array<ChartOption | ChartSeriesOption>;
};

export type TAnalyticsFilterResponse = {
  id: string;
  value: TAnalyticsForPeriod;
};

export type TAnalyticsPeriodTypes = {
  [K in EnAnalyticsFilter]: TAnanlyticsFilterPeriod[];
};

export type TAnalyticsFilterPeriodOption = {
  title: string;
};
//  *Filters

// *  --------------   --------------  --------------  --------------

// *Libs

// *ngx-charts

export type TNgxColorScheme = {
  domain: string[];
};

// *ngx-charts
