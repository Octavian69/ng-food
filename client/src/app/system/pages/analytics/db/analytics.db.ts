import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { EnAnalyticsFilter } from '../enums/analytics-filter.enum';

export const AnalyticsDB = {
  charts: {
    colors: {
      pie: {
        [EnTheme.DARK]: [
          '#F4DAB8',
          '#fff8dc',
          '#F08244',
          '#f5deb3',
          '#F5EFA9',
          '#F2AC4E',
          '#ffebcd',
          '#F0AA83',
          '#FDD8A7',
          '#ffd200',
          '#ffe4c4',
          '#ffe4b5',
          '#C4985E',
          '#f7971e',
          '#ffe4e1',
        ],
        [EnTheme.LIGHT]: [
          '#afeeee',
          '#00b4db',
          '#A2FCF7',
          '#0083b0',
          '#40A9E5',
          '#42EC6D',
          '#819BFB',
          '#1e90ff',
          '#00ced1',
          '#66cdaa',
          '#00ffff',
          '#46A5F9',
          '#00ffff',
          '#46F8CF',
          '#227874',
        ],
      },
    },
    controls: {
      typeOptions: [
        {
          id: 1,
          value: EnAnalyticsFilter.ORDERS_PERIOD_TOTAL_AMOUNT,
          label: 'Выручка(за период)',
        },
        {
          id: 2,
          value: EnAnalyticsFilter.ORDERS_DAYS_TOTAL_AMOUNT,
          label: 'Выручка(по дням)',
        },
        {
          id: 3,
          value: EnAnalyticsFilter.POSITIONS_QUANTITY,
          label: 'Позиции(топ полупулярых)',
        },
        {
          id: 4,
          value: EnAnalyticsFilter.POSITIONS_TOTAL_COST,
          label: 'Позиции(топ прибыльных)',
        },
      ],
    },
    orders: {
      // ? Цвета взяты из градиентов темы: theme.scss => var(--theme-gradient)
      options: [
        {
          periodTitle: 'День',
          period: 'day',
          moment: {
            time: 'd',
            format: '(Do)',
          },
          groupOptions: [
            { title: 'Сегодня', field: 'CurrentDay' },
            { title: 'Вчера', field: 'PreviousDay', add: -1 },
          ],
        },
        {
          periodTitle: 'Месяц',
          period: 'month',
          moment: {
            time: 'M',
            format: '(MMMM)',
          },
          groupOptions: [
            { title: 'Текущий', field: 'CurrentMonth' },
            { title: 'Прошлый', field: 'PreviousMonth', add: -1 },
          ],
        },
        {
          periodTitle: 'Год',
          period: 'year',
          moment: {
            time: 'y',
            format: '(YYYY)',
          },
          groupOptions: [
            { title: 'Текущий', field: 'CurrentYear' },
            { title: 'Предыдущий', field: 'PreviousYear', add: -1 },
          ],
        },
      ],
    },
    positions: {
      options: [
        {
          field: 'TotalAmount',
          title: 'Топ прибыльных позиций',
          extra: {
            optionsValueSuffix: '(₽)',
          },
        },
        {
          field: 'Quantity',
          title: 'Топ популярных позиций',
          extra: {
            optionsValueSuffix: '(шт.)',
          },
        },
      ],
    },

    filter: {
      options: {
        //  *Orders
        [EnAnalyticsFilter.ORDERS_PERIOD_TOTAL_AMOUNT]: {
          title: 'Выручка (за период)',
        },
        [EnAnalyticsFilter.ORDERS_DAYS_TOTAL_AMOUNT]: {
          title: 'Выручка (по дням)',
        },
        //  * Positions
        [EnAnalyticsFilter.POSITIONS_QUANTITY]: {
          title: 'Топ популярных позиций',
        },
        [EnAnalyticsFilter.POSITIONS_TOTAL_COST]: {
          title: 'Топ прибыльных позиций',
        },
      },
    },

    suffixes: [
      { suffix: '(шт.)', fields: [EnAnalyticsFilter.POSITIONS_QUANTITY] },
      {
        suffix: '(₽)',
        fields: [
          EnAnalyticsFilter.ORDERS_PERIOD_TOTAL_AMOUNT,
          EnAnalyticsFilter.ORDERS_DAYS_TOTAL_AMOUNT,
          EnAnalyticsFilter.POSITIONS_TOTAL_COST,
        ],
      },
    ],
  },
} as const;

export type TAnalyicsDB = typeof AnalyticsDB;
