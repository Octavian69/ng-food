import { ANFade, ANTransform } from '@core/animations/animations';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import {
  animate,
  AnimationTriggerMetadata,
  group,
  query,
  style,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';

export const ANAnalyticsHeader = trigger('ANAnalyticsHeader', [
  transition(':enter', [
    group([
      useAnimation(ANFade, { params: { timing: '.5s' } }),
      useAnimation(ANTransform, { params: { timing: '.7s' } }),
    ]),
  ]),
]);

export const ANAnanlyticsPositions = trigger('ANAnanlyticsPositions', [
  transition(':enter', [
    query(
      '.chart-content:enter',
      group([
        useAnimation(ANTransform, {
          params: {
            fromState: 'perspective(600px) rotateX(0deg) scale(.3)',
            toState: 'perspective(600px) rotateX(-360deg) scale(1)',
            timing: '.7s',
          },
        }),
        useAnimation(ANFade, {
          params: {
            timing: '.7s',
          },
        }),
      ]),
      queryOptions(),
    ),
  ]),
]);

export const ANAnalyticsOrders = trigger('ANAnalyticsOrders', [
  transition(':enter', [
    group([
      useAnimation(ANFade, { params: { timing: '.5s' } }),
      query(
        '.chart-content:nth-child(1):enter',
        [
          useAnimation(ANTransform, {
            params: { fromState: 'translateX(-30%)', timing: '.5s' },
          }),
        ],
        queryOptions(),
      ),
      query(
        '.chart-content:nth-child(2):enter',
        [
          useAnimation(ANTransform, {
            params: {
              fromState: 'perspective(600px) rotateY(-360deg) scale(.5)',
              toState: 'perspective(600px) rotateY(0deg) scale(1)',
              timing: '.5s',
            },
          }),
        ],
        queryOptions(),
      ),
      query(
        '.chart-content:nth-child(3):enter',
        [
          useAnimation(ANTransform, {
            params: { fromState: 'translateX(30%)', timing: '.5s' },
          }),
        ],
        queryOptions(),
      ),
    ]),
  ]),
]);

export const ANFilterChart: () => AnimationTriggerMetadata = () =>
  trigger('ANFilterChart', [
    transition(':enter, *=>*', [
      // *Enter
      group([
        query(
          '.chart:enter .header .period',
          [
            group([
              useAnimation(ANFade, { params: { timing: '.6s' } }),
              useAnimation(ANTransform, {
                params: {
                  fromState: 'translateY(-100%)',
                  toState: 'translateY(0%)',
                  timing: '.6s',
                },
              }),
            ]),
          ],
          queryOptions(),
        ),

        query(
          '.chart:enter .header .time',
          [
            group([
              useAnimation(ANFade, { params: { timing: '.6s' } }),
              useAnimation(ANTransform, {
                params: {
                  fromState: 'translateY(100%)',
                  toState: 'translateY(0%)',
                  timing: '.6s',
                },
              }),
            ]),
          ],
          queryOptions(),
        ),
        query(
          '[data-type="pie"]:enter .chart-item, [data-type="pie-grid"]:enter .chart-item',
          [
            group([
              useAnimation(ANFade, { params: { timing: '.6s' } }),
              style({
                transform: 'perspective(600px) rotateY(180deg) scale(.7)',
                transformOrigin: 'center',
              }),
              animate(
                '1s',
                style({
                  transform: 'perspective(600px) rotateY(0deg) scale(1)',
                  transformOrigin: 'center',
                }),
              ),
            ]),
          ],
          queryOptions(),
        ),

        query(
          '[data-type="pie"]:nth-child(odd):enter,[data-type="pie-grid"]:nth-child(odd):enter',
          [
            group([
              useAnimation(ANFade),
              useAnimation(ANTransform, {
                params: {
                  fromState: 'translateX(-50%)',
                  toState: 'translateX(0%)',
                },
              }),
            ]),
          ],
          queryOptions(),
        ),
        query(
          '[data-type="pie"]:nth-child(even):enter, [data-type="pie-grid"]:nth-child(even):enter',
          [
            group([
              useAnimation(ANFade),
              useAnimation(ANTransform, {
                params: {
                  fromState: 'translateX(50%)',
                  toState: 'translateX(0%)',
                },
              }),
            ]),
          ],
          queryOptions(),
        ),
        query(
          '[data-type="heat-map"]:enter',
          [
            group([
              useAnimation(ANFade),
              useAnimation(ANTransform, {
                params: {
                  fromState: 'scale(0)',
                  toState: 'scale(1)',
                },
              }),
            ]),
          ],
          queryOptions(),
        ),
      ]),

      // * Leave
      query(
        '[data-type="pie"]:nth-child(odd):leave, [data-type="pie-grid"]:nth-child(odd):leave',
        [
          group([
            useAnimation(ANFade, { params: { fromState: '*', toState: '0' } }),
            useAnimation(ANTransform, {
              params: {
                fromState: '*',
                toState: 'translateX(-50%)',
              },
            }),
          ]),
        ],
        queryOptions(),
      ),
      query(
        '[data-type="pie"]:nth-child(even):leave, [data-type="pie-grid"]:nth-child(even):leave',
        [
          group([
            useAnimation(ANFade, { params: { fromState: '*', toState: '0' } }),
            useAnimation(ANTransform, {
              params: {
                fromState: '*',
                toState: 'translateX(50%)',
              },
            }),
          ]),
        ],
        queryOptions(),
      ),
      query(
        '[data-type="heat-map"]:leave',
        [
          group([
            useAnimation(ANFade, {
              params: { fromState: '*', toState: '0' },
            }),
            useAnimation(ANTransform, {
              params: {
                fromState: '*',
                toState: 'scale(0)',
              },
            }),
          ]),
        ],
        queryOptions(),
      ),
    ]),
  ]);
