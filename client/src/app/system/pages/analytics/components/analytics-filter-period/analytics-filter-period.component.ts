import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
  } from '@angular/core';
import { RangeValue } from '@core/form/classes/RangeValue';
import { Bind } from '@core/form/decorators/decorators';
import { TControlOption } from '@core/form/types/form.types';
import { isExistProps } from '@handlers/utils.handlers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { distinctUntilChanged } from 'rxjs/operators';
import { AnalyticsFilterPeriod } from '../../classes/AnalyticsFilterPeriod';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import {
  TAnalyticsFilterChangeValue,
  TAnalyticsFilterFormValue,
} from '../../types/analytics.types';
@UntilDestroy()
@Component({
  selector: 'ng-analytics-filter-period',
  templateUrl: './analytics-filter-period.component.html',
  styleUrls: ['./analytics-filter-period.component.scss'],
})
export class AnalyticsFilterPeriodComponent implements OnInit {
  public form: FormGroup;

  @Input() period: AnalyticsFilterPeriod;
  @Input() count: number;
  @Input() isCanRemoved: boolean;
  @Input() typeOptions: TControlOption[];

  @Output('remove') private _remove = new EventEmitter<string>();
  @Output('change')
  private _change = new EventEmitter<TAnalyticsFilterChangeValue>();

  ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    const { value } = this.period;
    this.form = new FormGroup(
      {
        type: new FormControl(null, [Validators.required]),
        range: new FormControl(null, [
          Validators.required,
          this.rangeErrorValidator,
        ]),
      },
      { updateOn: 'change' },
    );

    if (value) {
      this.form.patchValue(value, { emitEvent: false });
    }

    this.form.valueChanges
      .pipe(distinctUntilChanged(), untilDestroyed(this))
      .subscribe(this.changeValue);
  }

  private rangeErrorValidator(control: AbstractControl): ValidationErrors {
    const range: RangeValue<Date> = control.value;
    const isCompleteValue: boolean = isExistProps(range, 'end', 'start');

    return isCompleteValue ? null : { incomplete: true };
  }

  public remove(): void {
    this._remove.emit(this.period.id);
  }

  @Bind()
  private changeValue(value: TAnalyticsFilterFormValue): void {
    const { valid } = this.form;
    const periodValue: TAnalyticsFilterChangeValue = this.period.transformToChangeValue(
      value,
      valid,
    );

    this._change.emit(periodValue);
  }
}
