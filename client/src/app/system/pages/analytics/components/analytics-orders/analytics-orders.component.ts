import { Moment, MOMENT } from '@core/utils/injection/app-tokens';
import { suffix } from '@handlers/string.handlers';
import { ChartOption } from '../../classes/ChartOption';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { IAnalyticsOrders } from '../../interfaces/IAnalyticsOrders';
import { AnalyticsViewService } from '../../services/analytics.view.service';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
} from '@angular/core';
import {
  ANAnalyticsHeader,
  ANAnalyticsOrders,
} from '../../animations/analytics.animations';
import {
  TAnalyticsOrderMoment,
  TAnalyticsOrderPeriodOption,
  TOrderPeriodGroupOption,
} from '../../types/analytics.types';

@Component({
  selector: 'ng-analytics-orders',
  templateUrl: './analytics-orders.component.html',
  styleUrls: ['./analytics-orders.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANAnalyticsHeader, ANAnalyticsOrders],
})
export class AnalyticsOrdersComponent {
  public analytics: IAnalyticsOrders;

  public periods: ChartOption[];
  public options: TAnalyticsOrderPeriodOption[] = this.viewService.getDataFromDB(
    ['charts', 'orders', 'options'],
  );
  @Input('analytics') public set _analytics(value: IAnalyticsOrders) {
    this.analytics = value;
    if (value) {
      this.transformData();
    }
  }
  @Input() public load: boolean = false;

  constructor(
    @Inject(MOMENT) private moment: Moment,
    private viewService: AnalyticsViewService,
  ) {}

  private transformData(): void {
    this.periods = this.transformToPeriods();
  }

  private transformToPeriods(): ChartOption[] {
    return this.options.map((option: TAnalyticsOrderPeriodOption) => {
      const { periodTitle, groupOptions, moment: match } = option;
      const series: ChartSeriesOption[] = this.transformToOrdersSeries(
        groupOptions,
        match,
      );

      return new ChartOption(periodTitle, series);
    });
  }

  private transformToOrdersSeries(
    groupOptions: TOrderPeriodGroupOption[],
    match: TAnalyticsOrderMoment,
  ): ChartSeriesOption[] {
    const { time, format } = match;

    return groupOptions.map(
      ({ title, field, add = 0 }: TOrderPeriodGroupOption) => {
        const timeLabel: string = this.moment().add(add, time).format(format);
        const optionTitle: string = `${title}${timeLabel}`;
        const value: number = this.analytics[field];

        return new ChartSeriesOption(optionTitle, value);
      },
    );
  }

  public valueFormatter(value: number): string {
    return suffix(String(value), '₽');
  }

  public getTotalAmount(period: ChartOption): number {
    return period.series.reduce((accum, curr) => (accum += curr.value), 0);
  }
}
