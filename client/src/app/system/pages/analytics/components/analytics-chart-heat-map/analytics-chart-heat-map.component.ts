import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Cache, TrackBy } from '@core/decorators/decorators';
import { Bind } from '@core/form/decorators/decorators';
import { TRange } from '@core/form/types/form.types';
import { ISimple } from '@core/interfaces/shared/ISimple';
import { IfElse } from '@core/managers/types/managers.types';
import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { Entries } from '@core/types/libs.types';
import { isEqual } from '@handlers/conditions.handlers';
import { getDataFromDB } from '@handlers/structutral.handlers';
import { ChartOption } from '../../classes/ChartOption';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { AnalyticsDB } from '../../db/analytics.db';
import { TNgxColorScheme } from '../../types/analytics.types';
import {
  percentOfTotal,
  sum,
  toCountMap,
  unique,
} from '@handlers/utils.handlers';

type TChartSeriesMap = Map<string, ChartSeriesOption[]>;

@Component({
  selector: 'ng-analytics-chart-heat-map',
  templateUrl: './analytics-chart-heat-map.component.html',
  styleUrls: ['./analytics-chart-heat-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnalyticsChartHeatMapComponent {
  public seriesMap: TChartSeriesMap;
  public points: string[];
  public colors: TNgxColorScheme;
  public rectColorsMap: ISimple<string>;
  public legendBackground: string = '';
  public averageSeriesValue: number = 0;
  public minSeriesValue: number = 0;
  public maxSeriesValue: number = 0;

  @Input() public size: string = '2.5rem';
  @Input() public legend: boolean = true;
  @Input() public legendTitle: string;
  @Input() public legendPosition: 'before' | 'after' = 'after';
  @Input() public xAxis: boolean = true;
  @Input() public yAxis: boolean = true;
  @Input() public xAxisLabel: boolean = true;
  @Input() public yAxisLabel: boolean = true;
  @Input() public showXAxisLabel: boolean = true;
  @Input() public showYAxisLabel: boolean = true;
  @Input() public maxLabelLength: number = 20;
  @Input() public rotateXAxisTicks: boolean = false;
  @Input() public ladderXAxisTicks: boolean = false;
  @Input() public showTooltip: boolean = true;
  @Input() public innerPadding: number = 3;
  @Input() public valueFormatter: (
    v: number,
    opt?: ChartSeriesOption,
  ) => string;
  @Input() public xAxisLabelFormatter: (v: number) => string;
  @Input() public yAxisLabelFormatter: (v: number) => string;
  @Input() public legendLabelFormatter: (v: number, range: TRange) => string;

  @Input('colors') public colorsScheme: TNgxColorScheme;
  @Input('series') public set _series(series: ChartOption[]) {
    if (series) {
      this.repaint(series);
    }
  }

  private getSeriesValuesByProp<T extends 'name' | 'value'>(
    seriesMap: TChartSeriesMap,
    key: T,
  ): Array<IfElse<T, 'name', string, number>> {
    return Array.from(seriesMap.values()).reduce((accum, curr) => {
      curr.forEach((item) => accum.push(item[key]));

      return accum;
    }, [] as any);
  }

  @Bind()
  public getRectBackground(value: number): string {
    const averageValuePercent: number = Math.round(
      (value * 100) / this.averageSeriesValue,
    );
    const colorsEntries: Entries<ISimple<string>> = Object.entries(
      this.rectColorsMap,
    );
    const [, color] =
      colorsEntries.find(([percent]) => averageValuePercent < +percent) ||
      colorsEntries.pop();

    return color;
  }

  public computedLegendBackground(seriesMap: TChartSeriesMap): void {
    const values: number[] = this.getSeriesValuesByProp(seriesMap, 'value');
    const colors: string[] = values.map(this.getRectBackground);
    const countColors: ISimple<number> = toCountMap(colors);
    const sumColorsCount: number = sum(Object.values(countColors));
    const gradientPoints: string[] = Object.entries(countColors)
      .sort(([, percentA], [, percentB]) => percentA - percentB)
      .map(([color, count]) => [color, percentOfTotal(count, sumColorsCount)])
      .map(([color]: [string, number], i, array) => {
        const lastPoint = array
          .slice(0, i)
          .map(([, percent]) => percent)
          .reduce((a: number, b: number) => a + b, 0) as number;
        const currentPoint: number = lastPoint;
        return `${color} ${currentPoint}%`;
      });

    const bg: string =
      gradientPoints.length < 2
        ? gradientPoints.shift()
        : `linear-gradient(to top, ${gradientPoints.join(', ')})`;

    this.legendBackground = bg;
  }

  public getSeriesOptionByName(
    colName: string,
    pointName: string,
  ): ChartSeriesOption {
    const series = this.seriesMap.get(colName);

    return series.find(({ name }) => isEqual(name, pointName));
  }

  @Cache()
  public getCssSize(): string {
    const size: string = `calc(${this.size} + .5rem)`;

    return size;
  }

  @Cache()
  public setColorsScheme(theme: EnTheme): void {
    const domain: string[] = getDataFromDB(
      ['charts', 'colors', 'pie', theme],
      AnalyticsDB,
    );
    this.colors = { domain };
    this.createRectColorsRange(this.seriesMap, domain);
    this.computedLegendBackground(this.seriesMap);
  }

  @TrackBy('default')
  public trackByPoints() {}

  private repaint(series: ChartOption[]): void {
    this.seriesMap = this.parseToSeriesMap(series);

    this.createPoints(this.seriesMap);
    this.defineSeriesValuesRange(this.seriesMap);
  }

  private parseToSeriesMap(series: ChartOption[]): TChartSeriesMap {
    const seriesMap: TChartSeriesMap = series.reduce(
      (accum, curr: ChartOption) => {
        const { name, series } = curr;
        const existSeries: ChartSeriesOption[] = accum.get(name) || [];

        accum.set(name, existSeries.concat(series));

        return accum;
      },
      new Map(),
    );

    return seriesMap;
  }

  private createPoints(seriesMap: TChartSeriesMap): void {
    const allPoints: string[] = this.getSeriesValuesByProp(seriesMap, 'name');

    this.points = unique(allPoints).sort((a, b) => (a > b ? 1 : -1));
  }

  public createRectColorsRange(
    seriesMap: TChartSeriesMap,
    colors: string[],
  ): void {
    const FULL_PERCENT: number = 100;
    const values: number[] = this.getSeriesValuesByProp(seriesMap, 'value');
    const valuesSum: number = sum(values);
    const averageSum: number = valuesSum / values.length;
    const averageColorRange: number = FULL_PERCENT / colors.length;
    const colorsMap: ISimple<string> = colors.reduce((accum, color, i) => {
      const percentStep: string = (averageColorRange * (i + 1)).toFixed(0);

      accum[percentStep] = color;

      return accum;
    }, {});

    this.averageSeriesValue = averageSum;
    this.rectColorsMap = colorsMap;
  }

  private defineSeriesValuesRange(seriesMap: TChartSeriesMap): void {
    const values: number[] = this.getSeriesValuesByProp(seriesMap, 'value');

    this.minSeriesValue = Math.min.apply(null, values);
    this.maxSeriesValue = Math.max.apply(null, values);
  }
}
