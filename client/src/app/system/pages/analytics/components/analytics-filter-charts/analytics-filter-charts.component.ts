import { formatDate } from '@angular/common';
import { Cache, TrackBy } from '@core/decorators/decorators';
import { Moment, MOMENT } from '@core/utils/injection/app-tokens';
import { isEqual } from '@handlers/conditions.handlers';
import { suffix } from '@handlers/string.handlers';
import { ANFilterChart } from '../../animations/analytics.animations';
import { AnalyticsFilterPeriod } from '../../classes/AnalyticsFilterPeriod';
import { ChartOption } from '../../classes/ChartOption';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { EnAnalyticsFilter } from '../../enums/analytics-filter.enum';
import { AnalyticsViewService } from '../../services/analytics.view.service';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
} from '@angular/core';
import {
  TAnalayticsForPositions,
  TAnalyticsChartType,
  TAnalyticsFilterPeriodChart,
  TAnalyticsForOrdersDaysAmount,
  TAnalyticsForOrdersTotalAmount,
} from '../../types/analytics.types';

@Component({
  selector: 'ng-analytics-filter-charts',
  templateUrl: './analytics-filter-charts.component.html',
  styleUrls: ['./analytics-filter-charts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANFilterChart()],
})
export class AnalyticsFilterChartsComponent {
  public charts: TAnalyticsFilterPeriodChart[] = [];

  @Input('periods') private set _periods(periods: AnalyticsFilterPeriod[]) {
    if (periods) {
      this.updateCharts(periods);
    }
  }

  constructor(
    @Inject(MOMENT) private moment: Moment,
    private viewService: AnalyticsViewService,
  ) {}

  @Cache()
  public getChartType(type: EnAnalyticsFilter): TAnalyticsChartType {
    switch (type) {
      case EnAnalyticsFilter.POSITIONS_QUANTITY:
      case EnAnalyticsFilter.POSITIONS_TOTAL_COST: {
        return 'pie';
      }
      case EnAnalyticsFilter.ORDERS_PERIOD_TOTAL_AMOUNT: {
        return 'pie-grid';
      }
      case EnAnalyticsFilter.ORDERS_DAYS_TOTAL_AMOUNT: {
        return 'heat-map';
      }
    }
  }

  @Cache()
  public getChartTitle(type: EnAnalyticsFilter) {
    const options: Record<
      EnAnalyticsFilter,
      { title: string }
    > = this.viewService.getDataFromDB(['charts', 'filter', 'options']);

    return options[type].title;
  }

  @Cache()
  public getSuffix(type: EnAnalyticsFilter): string {
    const suffixes: Array<{
      suffix: string;
      fields: EnAnalyticsFilter[];
    }> = this.viewService.getDataFromDB(['charts', 'suffixes']);
    const { suffix } = suffixes.find((item) => item.fields.includes(type));

    return suffix;
  }

  public updateCharts(periods: AnalyticsFilterPeriod[]): void {
    this.charts = periods.reduce((accum, period, i) => {
      if (period.isExistChart) {
        const { id: periodId, value } = period;
        const title: string = String(i + 1);
        const type: TAnalyticsChartType = this.getChartType(value.type);
        const series: TAnalyticsFilterPeriodChart['series'] = this.transformToChartSeries(
          period,
        );
        const chart: TAnalyticsFilterPeriodChart = {
          periodId,
          type,
          title,
          value,
          series,
        };

        accum.push(chart);
      }
      return accum;
    }, [] as TAnalyticsFilterPeriodChart[]);
  }

  @TrackBy('property', 'periodId')
  public trackByCharts() {}

  private transformToChartSeries(
    period: AnalyticsFilterPeriod,
  ): Array<ChartOption | ChartSeriesOption> {
    const { type } = period.value;

    switch (type) {
      case EnAnalyticsFilter.POSITIONS_QUANTITY:
      case EnAnalyticsFilter.POSITIONS_TOTAL_COST:
        return this.toPositionsSeries(period);
      case EnAnalyticsFilter.ORDERS_PERIOD_TOTAL_AMOUNT:
        return this.toOrdersAmountSeries(period);
      case EnAnalyticsFilter.ORDERS_DAYS_TOTAL_AMOUNT: {
        return this.toOrdersDaysAmountSeries(
          period.analytics as TAnalyticsForOrdersDaysAmount[],
        );
      }
      default:
        throw new Error(`Chart value type : "${type}" is not defined.`);
    }
  }

  private toPositionsSeries(
    period: AnalyticsFilterPeriod,
  ): ChartSeriesOption[] {
    const analytics = period.analytics as TAnalayticsForPositions;
    const options: ChartSeriesOption[] = analytics.map((opt) => {
      const { Title: name } = opt;
      const [, valueField] = period.getSplittedValue();
      const value: number = opt[valueField];

      return { name, value };
    });

    return options;
  }

  private toOrdersAmountSeries(
    period: AnalyticsFilterPeriod,
  ): ChartSeriesOption[] {
    const analytics = period.analytics as TAnalyticsForOrdersTotalAmount;
    const [, valueField] = period.getSplittedValue();
    const {
      range: { start, end },
    } = period.value;
    const value: number = analytics?.[valueField];

    if (value) {
      const from: string = formatDate(start, 'mediumDate', 'ru');
      const to: string = formatDate(end, 'mediumDate', 'ru');
      const name: string = `C ${from} по ${to}`;
      return [{ name, value }];
    }

    return [];
  }

  private toOrdersDaysAmountSeries(
    analytics: TAnalyticsForOrdersDaysAmount[],
  ): ChartOption[] {
    if (!analytics.length) return [];

    const MIN_MONTH_DATE: number = 1;
    const MAX_MONTH_DATE: number = 31;

    const chartSeries: ChartOption[] = [];

    for (let i = MIN_MONTH_DATE; i <= MAX_MONTH_DATE; i++) {
      const name: string = String(i);
      const series: ChartSeriesOption[] = analytics
        .filter(({ Date }) => {
          const date: number = this.moment(Date).date();
          return isEqual(date, i);
        })
        .sort(({ Date: D1 }, { Date: D2 }) => +new Date(D1) - +new Date(D2))
        .map(({ TotalAmount: value, Date }) => {
          const name: string = this.moment(Date).format('MMM, YYYY');

          return new ChartSeriesOption(name, value);
        });

      chartSeries.push(new ChartOption(name, series));
    }

    return chartSeries;
  }

  @Cache()
  public valueFormatter(chartValue: EnAnalyticsFilter): (v: number) => string {
    const tag: string = this.getSuffix(chartValue);

    return (value: number) => {
      return suffix(String(value), tag);
    };
  }
}
