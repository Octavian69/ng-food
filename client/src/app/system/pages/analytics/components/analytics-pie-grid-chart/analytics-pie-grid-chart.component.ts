import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Cache } from '@core/decorators/decorators';
import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { getDataFromDB } from '@handlers/structutral.handlers';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { AnalyticsDB } from '../../db/analytics.db';
import { TNgxColorScheme } from '../../types/analytics.types';

@Component({
  selector: 'ng-analytics-pie-grid-chart',
  templateUrl: './analytics-pie-grid-chart.component.html',
  styleUrls: ['./analytics-pie-grid-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnalyticsPieGridChartComponent {
  @Input() public view: [number, number] = [350, 250];
  @Input() public label: string;
  @Input() public series: ChartSeriesOption[];
  @Input('colors') public colorsScheme: TNgxColorScheme;
  @Input() public valueFormatter: (v: number) => string;
  @Input() public nameFormatter: (v: string) => string;

  @Cache()
  public getColorsScheme(theme: EnTheme): TNgxColorScheme {
    const domain: string[] = getDataFromDB(
      ['charts', 'colors', 'pie', theme],
      AnalyticsDB,
    );

    return { domain };
  }
}
