import { Initialize } from '@core/decorators/decorators';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { EnAnalyticsFilter } from '../../enums/analytics-filter.enum';
import { IAnalyticsOrders } from '../../interfaces/IAnalyticsOrders';
import { IAnalyticsPositionItem } from '../../interfaces/IAnalyticsPositionItem';
import { AnalyticsDataSerivce } from '../../services/analytics.data.service';
import { AnalyticsService } from '../../services/analytics.service';
import { AnalyticsDataFlags } from '../../states/analytics.data.flags';
import { AnalyticsDataState } from '../../states/analytics.data.state';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  TAnalyticsPeriodTypes,
  TAnanlyticsFilterPeriod,
} from '../../types/analytics.types';
@UntilDestroy()
@Component({
  selector: 'ng-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnalyticsPageComponent implements OnInit, OnInitStates, OnDestroy {
  public filter$: Observable<any> = this.dataService.getStream('filter$');

  public dataFlags: AnalyticsDataFlags;
  public dataState: AnalyticsDataState;

  // *Orders
  public ordersAnalytics$: Observable<IAnalyticsOrders> = this.dataService.getStream(
    'ordersAnalytics$',
  );

  // * Positions
  public positionsAnalytics$: Observable<
    IAnalyticsPositionItem[]
  > = this.dataService.getStream('positionsAnalytics$');

  constructor(private dataService: AnalyticsDataSerivce) {}

  ngOnInit(): void {
    this.loadAnalytics();
  }

  @Initialize()
  initStates(): void {
    this.dataFlags = this.dataService.getFullState('flags');
    this.dataState = this.dataService.getFullState('state');
  }

  ngOnDestroy(): void {
    this.dataService.destroy();
  }

  private loadAnalytics(): void {
    this.dataService.fetchOrdersAnalytics();
    this.dataService.fetchPositionsAnalytics();
  }

  private getFilterRequestMethod(
    type: EnAnalyticsFilter,
  ): keyof AnalyticsService {
    switch (type) {
      case EnAnalyticsFilter.ORDERS_PERIOD_TOTAL_AMOUNT: {
        return 'filterOrdersPeriodProfit';
      }
      case EnAnalyticsFilter.ORDERS_DAYS_TOTAL_AMOUNT: {
        return 'filterOrdersDaysProfit';
      }
      case EnAnalyticsFilter.POSITIONS_QUANTITY:
      case EnAnalyticsFilter.POSITIONS_TOTAL_COST: {
        return 'filterPositionsAnalytics';
      }
    }
  }

  public filter(periods: TAnalyticsPeriodTypes): void {
    Object.entries(periods).forEach(
      ([type, value]: [EnAnalyticsFilter, TAnanlyticsFilterPeriod[]]) => {
        const method: keyof AnalyticsService = this.getFilterRequestMethod(
          type,
        );

        this.dataService.filter(method, value, type);
      },
    );
  }
}
