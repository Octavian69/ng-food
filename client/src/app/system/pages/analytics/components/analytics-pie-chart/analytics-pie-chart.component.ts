import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Cache } from '@core/decorators/decorators';
import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { getDataFromDB } from '@handlers/structutral.handlers';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { AnalyticsDB } from '../../db/analytics.db';
import { TNgxColorScheme } from '../../types/analytics.types';

@Component({
  selector: 'ng-analytics-pie-chart',
  templateUrl: './analytics-pie-chart.component.html',
  styleUrls: ['./analytics-pie-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnalyticsPieChartComponent {
  @Input() public view: [number, number] = [400, 300];
  @Input() public legendTitle: string;
  @Input() public legendPosition: 'right' | 'below' = 'right';
  @Input() public labels: boolean = true;
  @Input() public maxLabelLength: number = 20;
  @Input() public valueFormatter: (v: number) => string;
  @Input() public labelFormatter: (v: string) => string = (v) => v;

  @Input() public series: ChartSeriesOption[];
  @Input('colors') public colorsScheme: TNgxColorScheme;

  @Cache()
  public getColorsScheme(theme: EnTheme): TNgxColorScheme {
    const domain: string[] = getDataFromDB(
      ['charts', 'colors', 'pie', theme],
      AnalyticsDB,
    );

    return { domain };
  }
}
