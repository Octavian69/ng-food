import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Bind } from '@core/form/decorators/decorators';
import { suffix } from '@handlers/string.handlers';
import { assign } from '@handlers/utils.handlers';
import { ChartOption } from '../../classes/ChartOption';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { IAnalyticsPositionItem } from '../../interfaces/IAnalyticsPositionItem';
import { AnalyticsViewService } from '../../services/analytics.view.service';
import { TAnalyticsPositionsOption } from '../../types/analytics.types';
import {
  ANAnalyticsHeader,
  ANAnanlyticsPositions,
} from '../../animations/analytics.animations';

@Component({
  selector: 'ng-analytics-positions',
  templateUrl: './analytics-positions.component.html',
  styleUrls: ['./analytics-positions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANAnalyticsHeader, ANAnanlyticsPositions],
})
export class AnalyticsPositionsComponent {
  public analytics: IAnalyticsPositionItem[];
  public chartOptions: ChartOption[] = [];
  public options: TAnalyticsPositionsOption[] = this.viewService.getDataFromDB([
    'charts',
    'positions',
    'options',
  ]);

  @Input('analytics') public set _analytics(value: IAnalyticsPositionItem[]) {
    this.analytics = value;

    if (value) {
      this.transformData();
    }
  }
  @Input() public load: boolean = false;

  constructor(private viewService: AnalyticsViewService) {}

  private transformData(): void {
    this.chartOptions = this.options.map(this.transformByType);
  }

  @Bind()
  private transformByType({
    title,
    field,
    extra,
  }: TAnalyticsPositionsOption): ChartOption {
    const series: ChartSeriesOption[] = this.analytics.map(
      (item: IAnalyticsPositionItem) => {
        const { Title } = item;
        const value: number = item[field];

        return new ChartSeriesOption(Title, value);
      },
    );
    return new ChartOption(title, series, assign(extra));
  }

  public valueFormatter({ optionsValueSuffix }): (v: string) => string {
    return (label: string | number) =>
      suffix(String(label), ` ${optionsValueSuffix}`);
  }
}
