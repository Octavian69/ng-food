import { Component, Input } from '@angular/core';
import { Cache } from '@core/decorators/decorators';
import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { getDataFromDB } from '@handlers/structutral.handlers';
import { ChartSeriesOption } from '../../classes/ChartSeriesOption';
import { AnalyticsDB } from '../../db/analytics.db';
import { TNgxColorScheme } from '../../types/analytics.types';

@Component({
  selector: 'ng-analytics-pie-advanced-chart',
  templateUrl: './analytics-pie-advanced-chart.component.html',
  styleUrls: ['./analytics-pie-advanced-chart.component.scss'],
})
export class AnalyticsPieAdvancedChartComponent {
  @Input() public view: [number, number] = [350, 250];
  @Input() public label: string;
  @Input() public series: ChartSeriesOption[];
  @Input('colors') public colorsScheme: TNgxColorScheme;
  @Input() public valueFormatter: (v: number) => string;

  @Cache()
  public getColorsScheme(theme: EnTheme): TNgxColorScheme {
    const domain: string[] = getDataFromDB(
      ['charts', 'colors', 'pie', theme],
      AnalyticsDB,
    );

    return { domain };
  }
}
