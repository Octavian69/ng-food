import { Initialize } from '@core/decorators/decorators';
import { RangeValue } from '@core/form/classes/RangeValue';
import { TControlOption } from '@core/form/types/form.types';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { Moment, MOMENT } from '@core/utils/injection/app-tokens';
import { scrollToContent } from '@handlers/components.handlers';
import { findIndex } from '@handlers/utils.handlers';
import { AnalyticsFilterPeriod } from '../../classes/AnalyticsFilterPeriod';
import { EnAnalyticsFilter } from '../../enums/analytics-filter.enum';
import { EnAnalytics } from '../../enums/analytics.enum';
import { AnalyticsViewService } from '../../services/analytics.view.service';
import { AnalyticsViewFlags } from '../../states/analytics.view.flags';
import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  ANPropShow,
  ANShowBySize,
  ANShowFade,
} from '@core/animations/animations';
import {
  TAnalyticsFilterChangeValue,
  TAnalyticsFilterFormValue,
  TAnalyticsFilterQuery,
  TAnalyticsFilterResponse,
  TAnalyticsForPeriod,
  TAnalyticsPeriodTypes,
  TAnanlyticsFilterPeriod,
} from '../../types/analytics.types';

const EXPAND_FILTER_TIME: number = 700;

@Component({
  selector: 'ng-analytics-filter',
  templateUrl: './analytics-filter.component.html',
  styleUrls: ['./analytics-filter.component.scss'],
  animations: [
    ANShowFade(),
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'scale(0)',
      toValue: 'scale(1)',
      animationName: 'ANShowChart',
    }),
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'translateY(-100%)',
      toValue: 'translateY(0%)',
      animationName: 'ANShowHeader',
      timingFrom: '.7s',
      leave: false,
    }),
    ANShowBySize(EXPAND_FILTER_TIME),
  ],
})
export class AnalyticsFilterComponent implements OnInit, OnInitStates {
  public periods: AnalyticsFilterPeriod[] = [];
  public isShowFilter: boolean = false;
  public viewFlags: AnalyticsViewFlags;
  public typeOptions: TControlOption[] = this.viewService.getDataFromDB([
    'charts',
    'controls',
    'typeOptions',
  ]);

  @Input('loading') public loading: boolean = false;
  @Input('analytics') private set _analytics(
    analytics: TAnalyticsFilterResponse[],
  ) {
    if (analytics) {
      this.updatePeriodsAnalytics(analytics);
    }
  }

  @Output('filter') private _filter = new EventEmitter<TAnalyticsPeriodTypes>();
  @ViewChild('periodsRef') private periodsRef: ElementRef;

  constructor(
    @Inject(MOMENT) private moment: Moment,
    private viewService: AnalyticsViewService,
  ) {}

  ngOnInit(): void {
    this.addPeriod();
  }

  @Initialize()
  initStates(): void {
    this.viewFlags = this.viewService.getFullState('flags');
  }

  public toggleFilter(): void {
    this.isShowFilter = !this.isShowFilter;

    if (this.isShowFilter) {
      scrollToContent(this.periodsRef, EXPAND_FILTER_TIME);
    }
  }

  public addPeriod(): void {
    const start: Date = this.moment().add(-1, 'd').toDate();
    const end: Date = this.moment().toDate();
    const range = new RangeValue(
      start,
      end,
    ) as TAnalyticsFilterFormValue['range'];
    const newPeriod: AnalyticsFilterPeriod = new AnalyticsFilterPeriod(
      { type: null, range },
      true,
    );

    this.periods = this.periods.concat(newPeriod);
  }

  public removePeriod(periodId: string): void {
    this.periods = this.periods.filter((p) => {
      return p.id !== periodId;
    });
  }

  public updatePeriodsAnalytics(analytics: TAnalyticsFilterResponse[]): void {
    const map: Map<string, TAnalyticsForPeriod> = analytics.reduce(
      (accum, { id, value }) => {
        accum.set(id, value);
        return accum;
      },
      new Map(),
    );

    this.periods = this.periods.map((period) => {
      const isExistPeriod: boolean = map.has(period.id);

      if (isExistPeriod) {
        const periodAnalytics: TAnalyticsForPeriod = map.get(period.id);
        period.setAnanlytics(periodAnalytics);
      }

      return period;
    });
  }

  public isCanAddPeriod(): boolean {
    return this.periods.length < EnAnalytics.MAX_FILTER_PERIODS;
  }

  public isCanSubmit(): boolean {
    return this.periods.some((p: AnalyticsFilterPeriod) => p.dirty && p.valid);
  }

  public createPeriod(): void {
    if (this.isCanAddPeriod()) {
      this.addPeriod();
    }
  }

  public changePeriod(candidate: TAnalyticsFilterChangeValue): void {
    const candidateIdx: number = findIndex(this.periods, candidate, 'id');

    if (~candidateIdx) {
      this.periods[candidateIdx].update(candidate.value, candidate.valid);
    }
  }

  private splitPeriodsByTypes(
    periods: AnalyticsFilterPeriod[],
  ): TAnalyticsPeriodTypes {
    const splittedPeriods = periods.reduce((accum, period) => {
      const {
        value: { type, range },
        id,
      } = period;
      const [, queryType] = period.getSplittedValue();
      const query: TAnalyticsFilterQuery = {
        type: queryType,
        range,
      };
      const filterPeriod: TAnanlyticsFilterPeriod = { id, query, type };

      if (!accum[type]) {
        accum[type as EnAnalyticsFilter] = [];
      }

      accum[type].push(filterPeriod);

      return accum;
    }, {}) as TAnalyticsPeriodTypes;

    return splittedPeriods;
  }

  public submit(): void {
    const dirtyPeriods: AnalyticsFilterPeriod[] = this.periods.filter(
      (p) => p.dirty && p.valid,
    );
    const splittedPeriods: TAnalyticsPeriodTypes = this.splitPeriodsByTypes(
      dirtyPeriods,
    );

    this._filter.emit(splittedPeriods);
    dirtyPeriods.forEach((p) => p.markAsUndirty());
  }
}
