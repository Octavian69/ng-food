import { NgModule } from '@angular/core';
import { CrumbsModule } from '@core/crumbs/crumbs.module';
import { SharedModule } from '@core/modules/shared.module';
import { OrderPositionComponent } from './components/order-position/order-position.component';
import { OrderComponent } from './components/order/order.component';
import { OrdersCategoriesItemComponent } from './components/orders-categories-item/orders-categories-item.component';
import { OrdersCategoriesListComponent } from './components/orders-categories-list/orders-categories-list.component';
import { OrdersPageComponent } from './components/orders-page/orders-page.component';
import { OrdersPositionsItemComponent } from './components/orders-positions-item/orders-positions-item.component';
import { OrdersPositionsListComponent } from './components/orders-positions-list/orders-positions-list.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersCategoriesDataService } from './services/orders-categories.data.service';
import { OrdersViewService } from './services/orders.view.service';
import { PositionsDataService } from '../categories/modules/positions/services/positions.data.service';
import { PositionsService } from '../categories/modules/positions/services/positions.service';

@NgModule({
  declarations: [
    OrderComponent,
    OrdersPageComponent,
    OrdersCategoriesListComponent,
    OrdersCategoriesItemComponent,
    OrdersPositionsListComponent,
    OrdersPositionsItemComponent,
    OrderPositionComponent,
  ],
  imports: [SharedModule, OrdersRoutingModule, CrumbsModule],
  providers: [
    OrdersCategoriesDataService,
    OrdersViewService,
    PositionsService,
    PositionsDataService,
  ],
})
export class OrdersModule {}
