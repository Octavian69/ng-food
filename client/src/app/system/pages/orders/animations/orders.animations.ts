import { EnAnimation } from '@core/animations/enums/Animation.enum';
import { queryOptions } from '@core/animations/handlers/animations.handlers';
import { EnShadow } from '@core/components/shadow/enums/shadow.enum';
import {
  animate,
  animateChild,
  group,
  query,
  style,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';
import {
  ANFade,
  ANPropShow,
  ANShowStyleStagger,
  ANTransform,
} from '@core/animations/animations';

export const ANShowOrdersCategories = ANShowStyleStagger({
  animationName: 'ANShowOrdersCategories',
  fromStyle: {
    opacity: '0',
    transform: 'translateY(-20%)',
  },
  toStyle: {
    opacity: '1',
    transform: 'translateY(0%)',
  },
});

export const ANShowOrdersModal = trigger('ANShowOrdersModal', [
  transition(':enter', [
    group([
      query('.shadow, :enter, @*', [animateChild()], queryOptions()),
      query(
        '.animation-header, .animation-footer',
        [
          useAnimation(ANFade, {
            params: { timing: EnShadow.ANIMATION_TIMING },
          }),
        ],
        queryOptions(),
      ),
      query(
        '.animation-header',
        [
          useAnimation(ANTransform, {
            params: {
              fromState: 'translateY(-30%)',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ],
        queryOptions(),
      ),
      query(
        '.animation-footer',
        [
          useAnimation(ANTransform, {
            params: {
              fromState: 'translateY(30%)',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ],
        queryOptions(),
      ),
    ]),
  ]),
  transition(':leave', [
    group([
      query('.shadow:leave, @*, :leave', [animateChild()], queryOptions()),
      query(
        '.animation-header, .animation-list, .animation-footer',
        [
          useAnimation(ANFade, {
            params: {
              fromState: '*',
              toState: '0',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ],
        queryOptions(),
      ),
      query(
        '.animation-header',
        [
          useAnimation(ANTransform, {
            params: {
              fromState: '*',
              toState: 'translateY(-30%)',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ],
        queryOptions(),
      ),
      query(
        '.animation-footer',
        [
          useAnimation(ANTransform, {
            params: {
              fromState: '*',
              toState: 'translateY(30%)',
              timing: EnShadow.ANIMATION_TIMING,
            },
          }),
        ],
        queryOptions(),
      ),
    ]),
  ]),
]);

export const AnShowOrdersList = trigger('AnShowOrdersList', [
  transition(':enter', [
    query(
      ':self',
      [
        style({
          overflow: 'hidden',
        }),
        query(
          '.animation-list-item:nth-child(odd)',
          [style({ transform: 'translateX(-110%)' })],
          queryOptions(),
        ),
        query(
          '.animation-list-item:nth-child(even)',
          [style({ transform: 'translateX(110%)' })],
          queryOptions(),
        ),
        useAnimation(ANTransform, {
          params: {
            fromState: 'scale(0)',
            toState: 'scale(1)',
            timing: EnShadow.ANIMATION_TIMING,
          },
        }),
        query(
          '.animation-list-item:nth-child(odd), .animation-list-item:nth-child(even)',
          [
            animate(
              EnAnimation.DEFAULT_TIMING,
              style({ transform: 'translateX(0%)' }),
            ),
          ],
          queryOptions(),
        ),
        style({
          overflow: 'hidden',
        }),
      ],
      queryOptions(),
    ),
  ]),
  transition(':leave', [
    query(
      ':self',
      [
        useAnimation(ANTransform, {
          params: {
            fromState: '*',
            toState: 'scale(0)',
            timing: EnShadow.ANIMATION_TIMING,
          },
        }),
      ],
      queryOptions(),
    ),
  ]),
]);

export const ANShowOrdersModalHeader = ANPropShow({
  cssProp: 'transform',
  fromValue: 'translateY(-100%)',
  toValue: 'translateY(0%)',
  timingFrom: '.5s',
  animationName: 'ANShowOrdersModalHeader',
});
export const ANShowOrdersModalFooter = ANPropShow({
  cssProp: 'transform',
  fromValue: 'translateY(100%)',
  toValue: 'translateY(0%)',
  animationName: 'ANShowOrdersModalFooter',
  timingFrom: '.5s',
});
