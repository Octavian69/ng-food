import { IOrder, IOrderPosition } from '../interface/IOrder';

export class OrderModel implements IOrder {
  constructor(
    public Positions: IOrderPosition[],
    public TotalAmount: number,
    public Created: Date = new Date(),
  ) {}
}
