export namespace NSOrdersValidation {
  export const min = {
    PositionCount: 1,
  } as const;

  export const max = {
    PositionCount: 10,
  } as const;
}
