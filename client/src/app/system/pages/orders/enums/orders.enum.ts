export enum EnOrders {
  FETCH_LIMIT = 5,
  STORAGE_QUERY_KEY = 'orders_categories_query',
  STORAGE_ORDER_KEY = 'orders_order_item',
}
