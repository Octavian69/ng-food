export interface IOrdersTotalStats {
  MinTotalAmount: number;
  MaxTotalAmount: number;
  MinTotalPositions: number;
  MaxTotalPositions: number;
  MinCreated: Date;
  MaxCreated: Date;
}
