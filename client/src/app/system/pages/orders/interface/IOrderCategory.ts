import { ICategory } from '../../categories/interfaces/ICategory';

export interface IOrderCategory extends ICategory {
  MinPositionCost: number;
  MaxPositionCost: number;
}
