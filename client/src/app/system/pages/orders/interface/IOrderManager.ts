import { MixedRequired } from '@core/types/libs.types';
import { IPosition } from '../../categories/modules/positions/interfaces/IPosition';

export interface IOrderManager {
  Positions: IOrderCandidatePosition[];
  TotalAmount: number;
}

export interface IOrderCandidatePosition {
  Quantity: number;
  Item: MixedRequired<IPosition, '_id' | 'Cost' | 'Title'>;
}
