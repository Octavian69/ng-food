export interface IOrder {
  Positions: IOrderPosition[];
  TotalAmount: number;
  Created: Date;
  User?: string;
  _id?: string;
}

export interface IOrderPosition {
  PositionId: string;
  PositionCost: number;
  PositionTitle: string;
  Quantity: number;
  TotalCost: number;
}
