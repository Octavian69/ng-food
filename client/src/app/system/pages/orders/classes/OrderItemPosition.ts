import { getRange } from '@core/form/handlers/form.handlers';
import { IOrderCandidatePosition } from '../interface/IOrderManager';
import { NSOrdersValidation } from '../namespaces/orders.namespaces';

export class OrderItemPosition implements IOrderCandidatePosition {
  constructor(
    public Item: IOrderCandidatePosition['Item'],
    public Quantity: number = getRange(
      NSOrdersValidation,
      'PositionCount',
      'min',
    ),
  ) {}
}
