import { IOrderPosition } from '../interface/IOrder';
import { IOrderCandidatePosition } from '../interface/IOrderManager';

export class OrderPosition implements IOrderPosition {
  public PositionId: string;
  public PositionCost: number;
  public PositionTitle: string;
  public TotalCost: number;

  constructor(
    public Quantity: number,
    position: IOrderCandidatePosition['Item'],
  ) {
    const { Cost, Title, _id } = position;
    this.PositionId = _id;
    this.PositionTitle = Title;
    this.PositionCost = Cost;
    this.TotalCost = Cost * Quantity;
  }
}
