import { OrderPosition } from './OrderPosition';
import { IOrderPosition } from '../interface/IOrder';
import { IOrderCandidatePosition } from '../interface/IOrderManager';
import { OrderModel } from '../models/Order.model';

export class OrderManager {
  public Positions: IOrderCandidatePosition[] = [];
  public TotalAmount: number = 0;

  public addPosition(position: IOrderCandidatePosition): OrderManager {
    const idx: number = this.Positions.findIndex(
      (p) => p.Item._id === position.Item._id,
    );

    if (~idx) this.Positions[idx].Quantity = position.Quantity;
    else this.Positions.push(position);
    this.recalculateTotalAmount();

    return this;
  }

  public removePosition(positionId: string): OrderManager {
    this.Positions = this.Positions.filter((p) => p.Item._id !== positionId);
    this.recalculateTotalAmount();

    return this;
  }

  public repeatOrder(positions: IOrderCandidatePosition[]): OrderManager {
    const newPositions = [];

    positions.forEach((position: IOrderCandidatePosition) => {
      const existPosition: IOrderCandidatePosition = this.Positions.find(
        (pos) => pos.Item._id === position.Item._id,
      );

      if (existPosition) existPosition.Quantity += position.Quantity;
      else newPositions.push(position);
    });

    this.Positions = this.Positions.concat(newPositions);
    this.recalculateTotalAmount();

    return this;
  }

  public recalculateTotalAmount(): OrderManager {
    this.TotalAmount = this.Positions.reduce((amount, position) => {
      const positionTotalCost = position.Item.Cost * position.Quantity;

      return (amount += positionTotalCost);
    }, 0);

    return this;
  }

  public mapToOrderModel(): OrderModel {
    const { TotalAmount } = this;
    const Positions: IOrderPosition[] = this.Positions.map(
      (position: IOrderCandidatePosition) => {
        return new OrderPosition(position.Quantity, position.Item);
      },
    );

    return new OrderModel(Positions, TotalAmount);
  }
}
