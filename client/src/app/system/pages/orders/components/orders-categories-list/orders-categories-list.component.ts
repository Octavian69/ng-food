import {
  Component,
  Input,
  OnDestroy,
  OnInit
  } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ANPropShow, ANShowFade } from '@core/animations/animations';
import { Initialize, TrackBy } from '@core/decorators/decorators';
import { RequiredRange } from '@core/form/decorators/decorators';
import { makePureFilter } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { PreloaderService } from '@core/preloader/preloader.service';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ICategory } from '@src/system/pages/categories/interfaces/ICategory';
import { NSCategoriesValidation } from '@src/system/pages/categories/namespaces/categories.namespaces';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ANShowOrdersCategories } from '../../animations/orders.animations';
import { IOrderCategory } from '../../interface/IOrderCategory';
import { OrdersCategoriesDataService } from '../../services/orders-categories.data.service';
import { OrdersViewService } from '../../services/orders.view.service';
import { OrdersCategoriesDataFlags } from '../../states/OrdersCategories.data.flags';
import {
  TOrdersCategoriesFilter,
  TOrdersCategoriesQuery,
  TOrdersCategoriesQueryManager,
} from '../../types/orders.types';

@UntilDestroy()
@Component({
  selector: 'ng-orders-categories-list',
  templateUrl: './orders-categories-list.component.html',
  styleUrls: ['./orders-categories-list.component.scss'],
  animations: [
    ANShowFade(),
    ANShowOrdersCategories,
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'scale(0)',
      toValue: 'scale(1)',
    }),
  ],
})
export class OrdersCategoriesListComponent
  implements OnInit, OnInitStates, OnDestroy {
  public categories$: Observable<
    IFetchData<IOrderCategory>
  > = this.dataService.getStream('categories$');
  public category$: Observable<ICategory> = this.viewService.getStream(
    'category$',
  );

  public dataFlags: OrdersCategoriesDataFlags;
  public searchControl: FormControl;

  @Input() isShowOrderBtn: boolean = false;

  constructor(
    private preloaderService: PreloaderService,
    private dataService: OrdersCategoriesDataService,
    private viewService: OrdersViewService,
  ) {}

  ngOnInit(): void {
    this.initFormControl();
    this.load();
  }

  @Initialize()
  initStates(): void {
    this.dataFlags = this.dataService.getFullState('flags');
  }

  ngOnDestroy() {
    this.dataService.destroy();
  }

  @TrackBy('property', '_id')
  public trackByFn() {}

  private initFormControl(): void {
    this.searchControl = new FormControl(null);

    this.searchControl.valueChanges
      .pipe(debounceTime(400), distinctUntilChanged(), untilDestroyed(this))
      .subscribe((Title: string) => {
        this.filter({ Title });
      });
  }

  @RequiredRange(NSCategoriesValidation)
  public getRange(controlName: keyof ICategory, rangeType: TRange): number {
    return;
  }

  private load(): void {
    const query: TOrdersCategoriesQuery = this.dataService
      .getState('state', 'categoriesQuery')
      .getQuery();
    this.preloaderService.firstLoading(this.categories$);
    this.dataService.fetchCategories(query, 'replace');
  }

  public expand(): void {
    const current: IFetchData<ICategory> = this.dataService.getStreamValue(
      'categories$',
    );
    if (current.isCanMakeFetch()) {
      const queryManager: TOrdersCategoriesQueryManager = this.dataService.getState(
        'state',
        'categoriesQuery',
      );
      this.dataService.fetchCategories(queryManager.nextPage(), 'end');
    }
  }

  public filter(filterValue: TOrdersCategoriesFilter): void {
    const pureFilter: Partial<TOrdersCategoriesFilter> = makePureFilter(
      filterValue,
    );
    const currentQuery: TOrdersCategoriesQueryManager = this.dataService.getState(
      'state',
      'categoriesQuery',
    );
    currentQuery.pagination('reset');
    this.dataService.fetchCategories(
      currentQuery.updateQuery('filters', pureFilter),
      'replace',
    );
  }

  public togglePositionsModal(category: ICategory): void {
    this.viewService.togglePositionsModal(category);
  }

  public toggleOrderModal(): void {
    this.viewService.toggleOrderModal();
  }
}
