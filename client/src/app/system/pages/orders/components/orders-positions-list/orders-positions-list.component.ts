import { LabelType } from '@angular-slider/ngx-slider';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ANXaxisTransfromSides } from '@core/animations/animations';
import { Initialize, TrackBy } from '@core/decorators/decorators';
import { Bind, RequiredRange } from '@core/form/decorators/decorators';
import { makePureFilter } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { UserDataService } from '@core/user/services/user.data.service';
import { FetchData } from '@core/utils/fetch/classes/FetchData';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { wrapToTag } from '@handlers/components.handlers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { IPosition } from '@src/system/pages/categories/modules/positions/interfaces/IPosition';
import { NSPositionValidation } from '@src/system/pages/categories/modules/positions/namespaces/Position.namespaces';
import { PositionsDataService } from '@src/system/pages/categories/modules/positions/services/positions.data.service';
import { PositionsDataFlags } from '@src/system/pages/categories/modules/positions/states/Positions.data.flags';
import { PositionsDataState } from '@src/system/pages/categories/modules/positions/states/Positions.data.state';
import { TPositionsQueryManager } from '@src/system/pages/categories/modules/positions/types/positions.types';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { OrderItemPosition } from '../../classes/OrderItemPosition';
import { OrderManager } from '../../classes/OrderManager';
import { IOrderPosition } from '../../interface/IOrder';
import { IOrderCategory } from '../../interface/IOrderCategory';
import { IOrderCandidatePosition } from '../../interface/IOrderManager';
import { OrdersDataService } from '../../services/orders.data.service';
import { TOrdersPositionsFilter } from '../../types/orders.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  AnShowOrdersList,
  ANShowOrdersModal,
} from '../../animations/orders.animations';

@UntilDestroy()
@Component({
  selector: 'ng-orders-positions-list',
  templateUrl: './orders-positions-list.component.html',
  styleUrls: ['./orders-positions-list.component.scss'],
  providers: [PositionsDataService, PositionsDataService],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANXaxisTransfromSides(), ANShowOrdersModal, AnShowOrdersList],
})
export class OrdersPositionsListComponent
  implements OnInit, OnInitStates, OnDestroy {
  public positions$: Observable<
    IFetchData<IOrderCandidatePosition>
  > = this.initPositonsStream();

  public filterForm: FormGroup;

  public dataFlags: PositionsDataFlags;
  public dataState: PositionsDataState;

  @Input() public category: IOrderCategory;
  @Output('hide') private _hide = new EventEmitter<void>();

  @HostBinding('@ANShowOrdersModal') private ANShowOrdersModal: unknown;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    public userDataService: UserDataService,
    public ordersDataService: OrdersDataService,
    private positionsDataService: PositionsDataService,
  ) {}

  ngOnInit(): void {
    this.initFilterForm();
    this.load();
  }

  ngOnDestroy() {
    this.positionsDataService.destroy();
  }

  @Initialize()
  initStates(): void {
    this.dataFlags = this.positionsDataService.getFullState('flags');
    this.dataState = this.positionsDataService.getFullState('state');
  }

  private initPositonsStream(): Observable<
    IFetchData<IOrderCandidatePosition>
  > {
    return this.positionsDataService
      .getStream('positions$')
      .pipe(map(this.modifiedToOrderPositions));
  }

  private initFilterForm(): void {
    this.filterForm = this.fb.group(
      {
        Title: [null],
        Cost: [null],
      },
      { updateOn: 'change' },
    );

    this.filterForm.valueChanges
      .pipe(debounceTime(300), untilDestroyed(this))
      .subscribe((value: TOrdersPositionsFilter) => {
        this.filter(value);
      });
  }

  @TrackBy('property', '_id')
  public trackByFn() {}

  @RequiredRange(NSPositionValidation)
  public getRange(
    controlName: keyof TOrdersPositionsFilter,
    rangeType: TRange,
  ): number {
    return;
  }

  public isHiddenControl(controlKey: 'Title' | 'Cost'): boolean {
    const positions: IFetchData<IOrderPosition> = this.positionsDataService.getStreamValue(
      'positions$',
    );
    const totalPositonsCount: number = positions?.totalCount ?? 0;

    return !totalPositonsCount && !Boolean(this.filterForm.value[controlKey]);
  }

  public isActiveSort(): boolean {
    const sort = this.positionsDataService
      .getState('state', 'listQueryManager')
      .getQueryItem('sort');

    return Boolean(sort);
  }

  public load(): void {
    this.positionsDataService.load(this.category._id, null);
  }

  public expand(): void {
    const current: IFetchData<IPosition> = this.positionsDataService.getStreamValue(
      'positions$',
    );

    if (current.isCanMakeFetch()) {
      const queryManager: TPositionsQueryManager = this.positionsDataService.getState(
        'state',
        'listQueryManager',
      );
      this.positionsDataService.fetch(
        this.category._id,
        queryManager.nextPage(),
        'end',
      );
    }
  }

  public filter(filterValue: TOrdersPositionsFilter): void {
    const pureFilter: Partial<TOrdersPositionsFilter> = makePureFilter(
      filterValue,
    );
    const queryManager: TPositionsQueryManager = this.positionsDataService.getState(
      'state',
      'listQueryManager',
    );

    queryManager.setPage(0);
    this.positionsDataService.fetch(
      this.category._id,
      queryManager.updateQuery('filters', pureFilter),
      'replace',
    );
  }

  public isAddedPositionToCart(position: IOrderCandidatePosition): boolean {
    const order: OrderManager = this.ordersDataService.getStreamValue('order$');

    return (
      Boolean(order) &&
      order.Positions.some((p) => p.Item._id === position.Item._id)
    );
  }

  public resetFilter(): void {
    this.filterForm.reset();
  }

  public sortByCost(): void {
    const queryManager: TPositionsQueryManager = this.positionsDataService.getState(
      'state',
      'listQueryManager',
    );

    queryManager.toggleSort('Cost');
    queryManager.setPage(0);

    this.positionsDataService.fetch(
      this.category._id,
      queryManager.getQuery(),
      'replace',
    );
  }

  @Bind()
  private modifiedToOrderPositions(
    positions: IFetchData<IPosition>,
  ): IFetchData<IOrderCandidatePosition> {
    const order: OrderManager = this.ordersDataService.getStreamValue('order$');

    if (positions) {
      const items: IOrderCandidatePosition[] = positions.items.map(
        (position: IPosition) => {
          const itemPosition: IOrderCandidatePosition = new OrderItemPosition(
            position,
          );
          const addedPostion: IOrderCandidatePosition = order?.Positions.find(
            (p: IOrderCandidatePosition) => p.Item._id === position._id,
          );
          if (addedPostion) {
            itemPosition.Quantity = addedPostion.Quantity;
          }

          return itemPosition;
        },
      );

      const updatedPositons: IFetchData<IOrderCandidatePosition> = new FetchData(
        items,
        positions.totalCount,
      );

      return updatedPositons;
    }

    return null;
  }

  public hide(): void {
    this._hide.emit(null);
  }

  public addPosition(position: IOrderCandidatePosition): void {
    this.ordersDataService.addPosition(position);
    this.toastr.info(`Позиция "${position.Item.Title}" добавлена.`);
  }

  public removePosition(positionId: string): void {
    this.ordersDataService.removePosition(positionId);
  }

  public translateRangeCb(value: number, label: LabelType): string {
    return wrapToTag('b', `${value} ₽`);
  }
}
