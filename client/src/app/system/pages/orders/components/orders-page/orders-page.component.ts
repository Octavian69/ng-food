import { Component, OnDestroy, OnInit } from '@angular/core';
import { Initialize } from '@core/decorators/decorators';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { StorageService } from '@core/services/storage.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { OrderManager } from '../../classes/OrderManager';
import { EnOrders } from '../../enums/orders.enum';
import { IOrderManager } from '../../interface/IOrderManager';
import { OrdersDataService } from '../../services/orders.data.service';
import { OrdersViewService } from '../../services/orders.view.service';
import { OrdersViewFlags } from '../../states/Orders.view.flags';

@Component({
  selector: 'ng-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss'],
})
export class OrdersPageComponent implements OnInit, OnInitStates, OnDestroy {
  public viewFlags: OrdersViewFlags;
  public order$: Observable<OrderManager> = this.getOrderStream();

  constructor(
    private storage: StorageService,
    private orderDataService: OrdersDataService,
    private viewService: OrdersViewService,
  ) {}

  ngOnInit(): void {
    this.initOrder();
  }

  ngOnDestroy() {
    this.orderDataService.destroy();
  }

  @Initialize()
  initStates() {
    this.viewFlags = this.viewService.getFullState('flags');
  }

  private initOrder(): void {
    const order: IOrderManager = this.orderDataService.getStreamValue('order$');
    const candidate: IOrderManager = this.storage.get(
      EnOrders.STORAGE_ORDER_KEY,
    );

    if (candidate && !order) {
      const order: OrderManager = Object.assign(new OrderManager(), candidate);

      this.orderDataService.emitToStream('order$', order);
    }
  }

  private getOrderStream(): Observable<OrderManager> {
    return this.orderDataService.getStream('order$').pipe(
      tap((order: OrderManager) => {
        this.storage.set(EnOrders.STORAGE_ORDER_KEY, order);
      }),
    );
  }

  public toggleOrderModal(): void {
    this.viewService.toggleOrderModal();
  }
}
