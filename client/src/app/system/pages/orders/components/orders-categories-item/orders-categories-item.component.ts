import {
  Component,
  EventEmitter,
  Input,
  Output
  } from '@angular/core';
import { ICategory } from '@src/system/pages/categories/interfaces/ICategory';
import { IOrderCategory } from '../../interface/IOrderCategory';

@Component({
  selector: 'ng-orders-categories-item',
  templateUrl: './orders-categories-item.component.html',
  styleUrls: ['./orders-categories-item.component.scss'],
})
export class OrdersCategoriesItemComponent {
  public isShowDescription: boolean = false;

  @Input('category') public category: IOrderCategory;

  @Output('selectPositions')
  private _selectPositions = new EventEmitter<ICategory>();

  public selectPositions(): void {
    this._selectPositions.emit(this.category);
  }

  public toggleDescription(): void {
    this.isShowDescription = !this.isShowDescription;
  }
}
