import {
  Component,
  EventEmitter,
  Input,
  Output
  } from '@angular/core';
import { RequiredRange } from '@core/form/decorators/decorators';
import { TRange } from '@core/form/types/form.types';
import { IOrderCandidatePosition } from '../../interface/IOrderManager';
import { NSOrdersValidation } from '../../namespaces/orders.namespaces';

@Component({
  selector: 'ng-order-position',
  templateUrl: './order-position.component.html',
  styleUrls: ['./order-position.component.scss'],
})
export class OrderPositionComponent {
  @Input() public position: IOrderCandidatePosition;

  @Output('setQuantity')
  _setQuantity = new EventEmitter<IOrderCandidatePosition>();
  @Output('remove') _remove = new EventEmitter<string>();

  @RequiredRange(NSOrdersValidation)
  public getRange(controlName: 'PositionCount', rangeType: TRange) {}

  public setQuantity(Quantity: number): void {
    const { Item } = this.position;
    const position: IOrderCandidatePosition = { Item, Quantity };

    this._setQuantity.emit(position);
  }

  public remove(): void {
    this._remove.emit(this.position.Item._id);
  }
}
