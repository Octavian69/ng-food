import { FormControl, Validators } from '@angular/forms';
import { RequiredRange } from '@core/form/decorators/decorators';
import { getRange } from '@core/form/handlers/form.handlers';
import { TRange } from '@core/form/types/form.types';
import { isJSType } from '@handlers/conditions.handlers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { distinctUntilChanged } from 'rxjs/operators';
import { OrderItemPosition } from '../../classes/OrderItemPosition';
import { IOrderCandidatePosition } from '../../interface/IOrderManager';
import { NSOrdersValidation } from '../../namespaces/orders.namespaces';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
@UntilDestroy()
@Component({
  selector: 'ng-orders-positions-item',
  templateUrl: './orders-positions-item.component.html',
  styleUrls: ['./orders-positions-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersPositionsItemComponent implements OnInit {
  public formControl: FormControl;

  @Input() public position: IOrderCandidatePosition;
  @Input() public isAddedToCart: boolean = false;

  @Output('add') _add = new EventEmitter<IOrderCandidatePosition>();
  @Output('remove') _remove = new EventEmitter<string>();

  ngOnInit() {
    this.initFormControl();
  }

  private initFormControl(): void {
    this.formControl = new FormControl(this.position.Quantity, [
      Validators.min(getRange(NSOrdersValidation, 'PositionCount', 'min')),
      Validators.max(getRange(NSOrdersValidation, 'PositionCount', 'max')),
    ]);

    this.formControl.valueChanges
      .pipe(distinctUntilChanged(), untilDestroyed(this))
      .subscribe((value) => {
        if (isJSType(value, 'number')) this.add(value);
      });
  }

  @RequiredRange(NSOrdersValidation)
  public getRange(controlName: 'PositionCount', rangeType: TRange): number {
    return;
  }

  public action(): void {
    this.isAddedToCart ? this.remove() : this.add(this.formControl.value);
  }

  private remove(): void {
    this.position.Quantity = this.getRange('PositionCount', 'min');
    this.formControl.setValue(this.position.Quantity, { emitEvent: false });
    this._remove.emit(this.position.Item._id);
  }

  public add(Quantity: number): void {
    const orderItemPosition: IOrderCandidatePosition = new OrderItemPosition(
      this.position.Item,
      Quantity,
    );

    this._add.emit(orderItemPosition);
  }
}
