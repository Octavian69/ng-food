import { ANXaxisTransfromSides } from '@core/animations/animations';
import { Initialize, TrackBy } from '@core/decorators/decorators';
import { OnInitActionListeners } from '@core/managers/interfaces/hooks/OnInitActionListeners';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { OrderManager } from '../../classes/OrderManager';
import { IOrder } from '../../interface/IOrder';
import { IOrderCandidatePosition } from '../../interface/IOrderManager';
import { OrdersDataService } from '../../services/orders.data.service';
import { OrdersDataFlags } from '../../states/Orders.data.flags';
import { TOrdersDataAction } from '../../types/orders.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Output,
} from '@angular/core';
import {
  AnShowOrdersList,
  ANShowOrdersModal,
} from '../../animations/orders.animations';

@UntilDestroy()
@Component({
  selector: 'ng-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANXaxisTransfromSides(), ANShowOrdersModal, AnShowOrdersList],
})
export class OrderComponent implements OnInitStates, OnInitActionListeners {
  public order$: Observable<OrderManager> = this.dataService.getStream(
    'order$',
  );
  @Output('hide') private _hide = new EventEmitter<void>();

  @HostBinding('@ANShowOrdersModal') private ANShowOrdersModal: unknown;
  public dataFlags: OrdersDataFlags;

  constructor(
    private dataService: OrdersDataService,
    private toastr: ToastrService,
  ) {}

  @Initialize()
  initStates(): void {
    this.dataFlags = this.dataService.getFullState('flags');
  }

  @Initialize()
  initActionListeners(): void {
    this.dataService
      .listen()
      .pipe(untilDestroyed(this))
      .subscribe((action: TOrdersDataAction) => {
        switch (action.type) {
          case 'save': {
            this.afterSave(action.payload);
            break;
          }
        }
      });
  }

  @TrackBy('property', '_id')
  public trackByFn() {}

  public setQuantity(position: IOrderCandidatePosition): void {
    this.dataService.addPosition(position);
  }

  public removePosition(positionId: string): void {
    this.dataService.removePosition(positionId);
  }

  public hide(): void {
    this._hide.emit();
  }

  public save(): void {
    this.dataService.saveOrder();
  }

  private afterSave(order: IOrder): void {
    this.hide();
    this.toastr.success(
      `Заказ на сумму ${order.TotalAmount}р. успешно совершен.`,
    );
  }
}
