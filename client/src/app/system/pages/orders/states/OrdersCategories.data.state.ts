import { FetchQuery } from '@core/utils/fetch/classes/FetchQuery';
import { Pagination } from '@core/utils/fetch/classes/Pagination';
import { FetchDataManager } from '@core/utils/fetch/managers/FetchData.manager';
import { FetchQueryManager } from '@core/utils/fetch/managers/FetchQuery.manager';
import { EnOrders } from '../enums/orders.enum';
import { IOrderCategory } from '../interface/IOrderCategory';
import {
  TOrdersCategoriesDataManager,
  TOrdersCategoriesQuery,
  TOrdersCategoriesQueryManager,
} from '../types/orders.types';

export class OrdersCategoriesDataState {
  public categoriesData: TOrdersCategoriesDataManager;
  public categoriesQuery: TOrdersCategoriesQueryManager;

  constructor() {
    this.initializeCategoriesManagers();
  }

  private initializeCategoriesManagers(): void {
    const initalQuery: TOrdersCategoriesQuery = new FetchQuery(
      new Pagination(EnOrders.FETCH_LIMIT),
    );

    this.categoriesData = new FetchDataManager<IOrderCategory>();
    this.categoriesQuery = new FetchQueryManager(initalQuery);
  }
}
