import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { Observable } from 'rxjs';
import { IOrder } from '../interface/IOrder';
import { IOrderCategory } from '../interface/IOrderCategory';
import { IOrdersTotalStats } from '../interface/IOrdersTotalStats';
import { OrderModel } from '../models/Order.model';
import { TOrdersCategoriesQuery } from '../types/orders.types';

@Injectable()
export class OrdersService {
  constructor(private http: HttpClient) {}

  public fetchCategories(
    query: TOrdersCategoriesQuery,
  ): Observable<IPaginationResponse<IOrderCategory>> {
    return this.http.post<IPaginationResponse<IOrderCategory>>(
      '@/orders/fetch-order-categories',
      query,
    );
  }

  public saveOrder(order: OrderModel): Observable<IOrder> {
    return this.http.post<IOrder>('@/orders/save-order', order);
  }

  public getTotalStats(): Observable<IOrdersTotalStats> {
    return this.http.get<IOrdersTotalStats>('@/orders/total-stats');
  }
}
