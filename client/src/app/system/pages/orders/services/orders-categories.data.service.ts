import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { TInsert } from '@core/types/state.types';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { BehaviorSubject } from 'rxjs';
import { OrdersService } from './orders.service';
import { IOrderCategory } from '../interface/IOrderCategory';
import { OrdersCategoriesDataFlags } from '../states/OrdersCategories.data.flags';
import { OrdersCategoriesDataState } from '../states/OrdersCategories.data.state';
import {
  TOrdersCategoriesDataManager,
  TOrdersCategoriesQuery,
} from '../types/orders.types';

@Injectable()
export class OrdersCategoriesDataService extends ExtendsFactory(
  State({
    flags: OrdersCategoriesDataFlags,
    state: OrdersCategoriesDataState,
  }),
  StreamManager(),
) {
  private categories$: BehaviorSubject<
    IFetchData<IOrderCategory>
  > = new BehaviorSubject(null);

  constructor(private ordersService: OrdersService) {
    super();
  }

  public fetchCategories(query: TOrdersCategoriesQuery, insert: TInsert): void {
    this.setState('flags', 'isFetched', true);

    const next = (response: IPaginationResponse<IOrderCategory>) => {
      const dataManager: TOrdersCategoriesDataManager = this.getState(
        'state',
        'categoriesData',
      );
      const categories: IFetchData<IOrderCategory> = dataManager.changeItems(
        response,
        insert,
      );

      this.emitToStream('categories$', categories);
    };

    this.ordersService
      .fetchCategories(query)
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public destroy(): void {
    this.destroyStates('state', 'flags');
    this.destroyStreams(['categories$']);
  }
}
