import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { StateAction } from '@core/managers/models/StateAction';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { BehaviorSubject } from 'rxjs';
import { OrdersService } from './orders.service';
import { OrderManager } from '../classes/OrderManager';
import { IOrder } from '../interface/IOrder';
import { IOrderCandidatePosition } from '../interface/IOrderManager';
import { OrdersDataFlags } from '../states/Orders.data.flags';
import { TOrdersDataAction } from '../types/orders.types';

@Injectable()
export class OrdersDataService extends ExtendsFactory(
  State({ flags: OrdersDataFlags }),
  StreamManager<TOrdersDataAction>(),
) {
  private order$: BehaviorSubject<OrderManager> = new BehaviorSubject(null);

  constructor(private ordersService: OrdersService) {
    super();
  }

  private get order(): OrderManager {
    return this.getStreamValue('order$') || new OrderManager();
  }

  public saveOrder(): void {
    this.setState('flags', 'isSaved', true);

    const next = (order: IOrder) => {
      this.updateOrder(null);
      this.action(new StateAction('save', order));
      this.setState('flags', 'isSaved', false);
    };

    this.ordersService
      .saveOrder(this.order.mapToOrderModel())
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public addPosition(orderPosition: IOrderCandidatePosition): void {
    const order: OrderManager = this.order.addPosition(orderPosition);
    this.updateOrder(order);
  }

  public removePosition(positionId: string): void {
    const order: OrderManager = this.order.removePosition(positionId);
    this.updateOrder(order);
  }

  public recalculateTotalAmount(): void {
    const order: OrderManager = this.order.recalculateTotalAmount();
    this.updateOrder(order);
  }

  public repeatOrder(positions: IOrderCandidatePosition[]): void {
    const order: OrderManager = this.order.repeatOrder(positions);

    this.updateOrder(order);
  }

  public updateOrder(order: OrderManager): void {
    this.emitToStream('order$', order);
  }

  public destroy(): void {
    this.unsubscribe();
    this.destroyStates('flags');
  }
}
