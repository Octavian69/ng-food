import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { BehaviorSubject } from 'rxjs';
import { ICategory } from '../../categories/interfaces/ICategory';
import { OrdersViewFlags } from '../states/Orders.view.flags';

export class OrdersViewService extends ExtendsFactory(
  State({
    flags: OrdersViewFlags,
  }),
  StreamManager(),
) {
  private category$: BehaviorSubject<ICategory> = new BehaviorSubject(null);

  public togglePositionsModal(category: ICategory): void {
    this.emitToStream('category$', category);
  }

  public toggleOrderModal(): void {
    const currentValue: boolean = this.getState('flags', 'isShowOrder');

    this.setState('flags', 'isShowOrder', !currentValue);
  }
}
