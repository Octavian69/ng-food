import { RangeValue } from '@core/form/classes/RangeValue';
import { IStateAction } from '@core/managers/interfaces/IStateAction';
import { IFetchDataManager } from '@core/utils/fetch/interfaces/IFetchDataManager';
import { IFetchQuery } from '@core/utils/fetch/interfaces/IFetchQuery';
import { IFetchQueryManager } from '@core/utils/fetch/interfaces/IFetchQueryManager';
import { CategoriesFilter } from '../../categories/components/categoies-filter/classes/CategoriesFilter';
import { IPosition } from '../../categories/modules/positions/interfaces/IPosition';
import { IOrder } from '../interface/IOrder';
import { IOrderCategory } from '../interface/IOrderCategory';

// * Categories query
export type TOrdersCategoriesFilter = Partial<Pick<CategoriesFilter, 'Title'>>;

export type TOrdersCategoriesQuery = Omit<
  IFetchQuery<IOrderCategory, TOrdersCategoriesFilter>,
  'sort'
>;

export type TOrdersCategoriesDataManager = IFetchDataManager<IOrderCategory>;
export type TOrdersCategoriesQueryManager = IFetchQueryManager<TOrdersCategoriesQuery>;

// * Positions query

export type TOrdersPositionsFilter = {
  Title: string;
  CostRange: RangeValue<number>;
};
export type TOrdersPositionsSortFields = 'Cost';
export type TOrdersPositionsQuery = IFetchQuery<
  IPosition,
  TOrdersPositionsFilter,
  TOrdersPositionsSortFields
>;
export type TOrdersPositionsDataManager = IFetchDataManager<IPosition>;
export type TOrdersPositionsQueryManager = IFetchQueryManager<TOrdersPositionsQuery>;

// * Data service

export type TOrdersDataAction = IStateAction<'save', IOrder>;
