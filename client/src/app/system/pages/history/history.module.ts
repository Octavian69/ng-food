import { NgModule } from '@angular/core';
import { CrumbsModule } from '@core/crumbs/crumbs.module';
import { SharedModule } from '@core/modules/shared.module';
import { HistoryFilterComponent } from './components/history-filter/history-filter.component';
import { HistoryListOrderComponent } from './components/history-list-order/history-list-order.component';
import { HistoryListComponent } from './components/history-list/history-list.component';
import { HistoryPageComponent } from './components/history-page/history-page.component';
import { HistorySortComponent } from './components/history-sort/history-sort.component';
import { HistoryRoutingModule } from './history-routing.module';
import { HistoryDataService } from './services/history.data.service';
import { HistoryViewService } from './services/history.view.service';

@NgModule({
  declarations: [
    HistoryPageComponent,
    HistoryListComponent,
    HistoryListOrderComponent,
    HistoryFilterComponent,
    HistorySortComponent,
  ],
  imports: [SharedModule, HistoryRoutingModule, CrumbsModule],
  providers: [HistoryDataService, HistoryViewService],
})
export class HistoryModule {}
