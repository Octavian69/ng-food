import { ANShowStyleStagger } from '@core/animations/animations';

export const ANShowHistoryOrders = ANShowStyleStagger({
  fromStyle: { transform: 'scale(.7)', opacity: 0 },
  toStyle: { transform: 'scale(1)', opacity: 1 },
  animationName: 'ANShowHistoryOrders',
});
