import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { Observable } from 'rxjs';
import { IHistoryOrder } from '../interfaces/IHistoryOrder';
import { THistoryQuery } from '../types/history.types';

@Injectable()
export class HistoryService {
  constructor(private http: HttpClient) {}

  public fetch(
    query: THistoryQuery,
  ): Observable<IPaginationResponse<IHistoryOrder>> {
    return this.http.post<IPaginationResponse<IHistoryOrder>>(
      '@/history/fetch',
      query,
    );
  }
}
