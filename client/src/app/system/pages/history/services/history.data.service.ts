import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { StreamManager } from '@core/managers/Stream.manager';
import { TInsert } from '@core/types/state.types';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { IPaginationResponse } from '@core/utils/fetch/interfaces/IPaginationResponse';
import { BehaviorSubject } from 'rxjs';
import { HistoryService } from './history.service';
import { IOrdersTotalStats } from '../../orders/interface/IOrdersTotalStats';
import { OrdersService } from '../../orders/services/orders.service';
import { IHistoryOrder } from '../interfaces/IHistoryOrder';
import { HistoryDataFlags } from '../states/History.data.flags';
import { HistoryDataState } from '../states/History.data.state';
import { THistoryDataManager, THistoryQuery } from '../types/history.types';

@Injectable()
export class HistoryDataService extends ExtendsFactory(
  State({
    flags: HistoryDataFlags,
    state: HistoryDataState,
  }),
  StreamManager(['unsubscribe']),
) {
  private historyOrders$: BehaviorSubject<
    IFetchData<IHistoryOrder>
  > = new BehaviorSubject(null);
  private ordersStats$: BehaviorSubject<IOrdersTotalStats> = new BehaviorSubject(
    null,
  );

  constructor(
    private historyService: HistoryService,
    private ordersService: OrdersService,
  ) {
    super();
  }

  public fetch(query: THistoryQuery, insertType: TInsert): void {
    this.setState('flags', 'isFetched', true);

    const next = (response: IPaginationResponse<IHistoryOrder>) => {
      const fetchManger: THistoryDataManager = this.getState(
        'state',
        'fetchManager',
      );
      const historyOrders: IFetchData<IHistoryOrder> = fetchManger.changeItems(
        response,
        insertType,
      );

      this.emitToStream('historyOrders$', historyOrders);
      this.setState('flags', 'isFetched', false);
    };

    this.historyService
      .fetch(query)
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public getOrdersTotalStats(): void {
    const next = (stats: IOrdersTotalStats) => {
      this.emitToStream('ordersStats$', stats);
    };

    this.ordersService
      .getTotalStats()
      .pipe(this.untilDestroyed())
      .subscribe(next);
  }

  public destroy(): void {
    this.destroyStates();
    this.unsubscribe();
    this.destroyStreams(['historyOrders$', 'ordersStats$']);
  }
}
