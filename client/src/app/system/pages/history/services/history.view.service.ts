import { Injectable } from '@angular/core';
import { ExtendsFactory } from '@core/managers/handlers/managers.handlers';
import { State } from '@core/managers/StateService.manager';
import { HistoryDB } from '../db/history.db';
import { HistoryViewState } from '../states/history.view.state';

@Injectable()
export class HistoryViewService extends ExtendsFactory(
  State({
    state: HistoryViewState,
    db: HistoryDB,
  }),
) {
  constructor() {
    super();
  }
}
