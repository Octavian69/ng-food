import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { randomid } from '@handlers/string.handlers';

export const HistoryDB = {
  list: {
    'position-statuses': [
      {
        id: randomid(),
        status: 'default',
        label: 'Не изменялась',
        color: {
          [EnTheme.LIGHT]: 'rgba(70, 239, 231, 0.25)',
          [EnTheme.DARK]: 'rgba(255, 255, 255, 0.2)',
        },
      },
      {
        id: randomid(),
        status: 'changed',
        label: 'Изменялась',
        color: {
          [EnTheme.LIGHT]: 'rgb(1 255 44 / 30%)',
          [EnTheme.DARK]: 'rgb(1 255 44 / 30%)',
        },
      },
      {
        id: randomid(),
        status: 'removed',
        label: 'Удалена',
        color: {
          [EnTheme.LIGHT]: 'rgb(255 1 1 / 12%)',
          [EnTheme.DARK]: 'rgb(165 42 42 / 50%)',
        },
      },
    ],
  },
  sortOptions: [
    { id: 1, label: 'Общая стоимость', value: 'TotalAmount' },
    { id: 2, label: 'Дата заказа', value: 'Created' },
    { id: 3, label: 'Количество позиций', value: 'Positions' },
  ],
} as const;

export type THistoryDB = typeof HistoryDB;
