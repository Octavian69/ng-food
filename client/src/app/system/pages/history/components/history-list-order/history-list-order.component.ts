import { ANXaxisTransfromSides } from '@core/animations/animations';
import { KeyValue } from '@core/classes/KeyValue';
import { Cache } from '@core/decorators/decorators';
import { Bind } from '@core/form/decorators/decorators';
import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { isEqual } from '@handlers/conditions.handlers';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  IHistoryOrder,
  IHistoryOrderPosition,
} from '../../interfaces/IHistoryOrder';
import {
  THistoryPositionStatus,
  THistoryPositionStatusOption,
} from '../../types/history.types';

@Component({
  selector: 'ng-history-list-order',
  templateUrl: './history-list-order.component.html',
  styleUrls: ['./history-list-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANXaxisTransfromSides()],
})
export class HistoryListOrderComponent {
  public order: IHistoryOrder;
  public RecalculateAmount: number = 0;

  @Input('order')
  private set _order(order: IHistoryOrder) {
    this.order = order;
    this.recalculateTotalAmount(order.Positions);
  }
  @Input() private positionsStatusesOptions: THistoryPositionStatusOption[];
  @Output('repeat') private _repeat = new EventEmitter<IHistoryOrder>();

  @Cache()
  public getPositionIcon(status: THistoryPositionStatus): string {
    return isEqual(status, 'active')
      ? 'published_with_changes'
      : 'do_disturb_on';
  }

  public getPosStatus({
    Status,
    NewState,
  }: IHistoryOrderPosition): THistoryPositionStatusOption['status'] {
    switch (Status) {
      case 'active':
        return NewState ? 'changed' : 'default';
      case 'removed':
        return 'removed';
    }
  }

  @Cache()
  public getPositionStatusColor(
    status: THistoryPositionStatusOption['status'],
    theme: EnTheme,
  ): string {
    const { color } = this.positionsStatusesOptions.find((p) =>
      isEqual(p.status, status),
    );

    return color[theme];
  }

  @Cache()
  private getPositionMeta<
    T extends Exclude<keyof IHistoryOrderPosition['NewState'], 'Updated'>
  >({ key, value }: KeyValue<T, string>) {
    switch (key) {
      case 'PositionCost': {
        return new KeyValue('Новая цена', `${value}₽`);
      }
      case 'PositionTitle': {
        return new KeyValue('Новое название', value);
      }
    }
  }

  @Bind()
  public convertToChanges(
    items: KeyValue[],
    position: IHistoryOrderPosition,
  ): KeyValue<string>[] {
    return items
      .filter(({ key, value }) => position[key] !== value)
      .map(this.getPositionMeta);
  }

  @Cache()
  public isShowPositionBadge(position: IHistoryOrderPosition): boolean {
    return Boolean(position.NewState) || isEqual(position.Status, 'removed');
  }

  public isShowAmountIcon(): boolean {
    return (
      this.RecalculateAmount !== this.order.TotalAmount &&
      this.RecalculateAmount > 0
    );
  }

  private recalculateTotalAmount(positions: IHistoryOrderPosition[]): void {
    this.RecalculateAmount = positions.reduce((accum, position) => {
      let { Quantity, NewState, PositionCost, Status } = position;
      if (Status === 'removed') return accum;
      if (NewState?.PositionCost) PositionCost = NewState.PositionCost;

      return (accum += Quantity * PositionCost);
    }, 0);
  }

  public repeat(): void {
    this._repeat.emit(this.order);
  }
}
