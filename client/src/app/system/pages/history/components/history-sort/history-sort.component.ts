import { FormControl } from '@angular/forms';
import { ANPropShow, ANShowFade } from '@core/animations/animations';
import { TControlOption } from '@core/form/types/form.types';
import { isEqual } from '@handlers/conditions.handlers';
import { THistoryQuery, THistorySortFields } from '../../types/history.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-history-sort',
  templateUrl: './history-sort.component.html',
  styleUrls: ['./history-sort.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'translateX(-30%)',
      toValue: '*',
    }),
    ANShowFade(),
  ],
})
export class HistorySortComponent implements OnInit {
  public formControl: FormControl;

  @Input('isOpen') public isOpen: boolean = false;
  @Input('sort') public sortValue: THistoryQuery['sort'];
  @Input('options') public sortOptions: TControlOption<THistorySortFields>[];
  @Output('sort')
  private _sort = new EventEmitter<THistoryQuery['sort']>();
  @Output('toggleShow')
  private _toggleShow = new EventEmitter<void>();

  ngOnInit(): void {
    this.initFormControl();
  }

  private initFormControl(): void {
    const value = this.getParseSortValue('value');
    this.formControl = new FormControl(value);
  }

  private getParseSortValue(
    type: 'key' | 'value',
  ): THistorySortFields | number {
    if (this.sortValue) {
      const [[key, value]] = Object.entries(this.sortValue) as [
        [THistorySortFields, number],
      ];

      return isEqual(type, 'key') ? key : value;
    }

    return null;
  }

  public isDesc(): boolean {
    const value = this.getParseSortValue('value');

    return Boolean(value) && isEqual(value, -1);
  }

  public toggleSortDirection(): void {
    const value = this.getParseSortValue('value');
    const sortValue = value === 1 ? -1 : 1;
    const sortKey = this.getParseSortValue('key');

    this._sort.emit({ [sortKey]: sortValue });
  }

  public toggleShow(): void {
    this._toggleShow.emit();
  }

  public reset(): void {
    this.formControl.reset();
    this._sort.emit(null);
  }

  public sort(sortKey: THistorySortFields): void {
    const value = this.getParseSortValue('value');
    const sortValue = value || 1;

    this._sort.emit({ [sortKey]: sortValue });
  }
}
