import { FormBuilder, FormGroup } from '@angular/forms';
import { ANPropShow, ANShowFade } from '@core/animations/animations';
import { DisabledFormStatus } from '@core/form/decorators/decorators';
import { makePureFilter, resetForm } from '@core/form/handlers/form.handlers';
import { IOrdersTotalStats } from '@src/system/pages/orders/interface/IOrdersTotalStats';
import { THistoryFilter } from '../../types/history.types';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'ng-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    ANPropShow({
      cssProp: 'transform',
      fromValue: 'translateX(30%)',
      toValue: '*',
    }),
    ANShowFade(),
  ],
})
export class HistoryFilterComponent implements OnInit {
  public form: FormGroup;
  @DisabledFormStatus('form')
  public disabledFilter: boolean;

  @Input('filter') public filterValue: THistoryFilter;
  @Input('ordersStats') public ordersStats: IOrdersTotalStats;
  @Input('isOpen') public isOpen: boolean = false;
  @Output('filter') private _filter = new EventEmitter<THistoryFilter>();
  @Output('toggleShow') private _toggleShow = new EventEmitter<void>();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initFilterForm();
  }

  private initFilterForm(): void {
    this.form = this.fb.group(
      {
        TotalAmount: [null],
        Positions: [null],
        Created: [null],
      },
      { updateOn: 'change' },
    );

    if (this.filterValue) {
      this.form.patchValue(this.filterValue);
    }
  }

  public toggleShow(): void {
    this._toggleShow.emit();
  }

  public reset(): void {
    const resetValue = resetForm(this.form, null, { emitEvent: false });
    this.filter(resetValue);
  }

  public filter(value: THistoryFilter): void {
    const filterValue: THistoryFilter = makePureFilter(value);

    this.form.markAsUntouched();
    this._filter.emit(filterValue);
  }
}
