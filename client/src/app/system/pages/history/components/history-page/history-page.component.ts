import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { StorageService } from '@core/services/storage.service';
import { EnHistory } from '../../enums/history.enum';
import { HistoryDataService } from '../../services/history.data.service';
import { HistoryViewService } from '../../services/history.view.service';

@Component({
  selector: 'ng-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryPageComponent implements OnDestroy {
  constructor(
    private storage: StorageService,
    private dataService: HistoryDataService,
    private viewService: HistoryViewService,
  ) {}

  ngOnDestroy() {
    this.dataService.destroy();
    this.viewService.destroyStates();
    this.storage.remove(EnHistory.FETCH_QUERY_KEY);
  }
}
