import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Initialize, TrackBy } from '@core/decorators/decorators';
import { TControlOption } from '@core/form/types/form.types';
import { OnInitStates } from '@core/managers/interfaces/hooks/OnInitStates';
import { IfElse } from '@core/managers/types/managers.types';
import { PreloaderService } from '@core/preloader/preloader.service';
import { StorageService } from '@core/services/storage.service';
import { ToastrService } from '@core/toastr/services/toastr.service';
import { IFetchData } from '@core/utils/fetch/interfaces/IFetchData';
import { isEqual } from '@handlers/conditions.handlers';
import { OrderItemPosition } from '@src/system/pages/orders/classes/OrderItemPosition';
import { IOrderCandidatePosition } from '@src/system/pages/orders/interface/IOrderManager';
import { IOrdersTotalStats } from '@src/system/pages/orders/interface/IOrdersTotalStats';
import { OrdersDataService } from '@src/system/pages/orders/services/orders.data.service';
import { Observable } from 'rxjs';
import { ANShowHistoryOrders } from '../../animations/history.animnations';
import { EnHistory } from '../../enums/history.enum';
import { HistoryDataService } from '../../services/history.data.service';
import { HistoryViewService } from '../../services/history.view.service';
import { HistoryDataFlags } from '../../states/History.data.flags';
import { HistoryDataState } from '../../states/History.data.state';
import { HistoryViewState } from '../../states/history.view.state';
import {
  IHistoryOrder,
  IHistoryOrderPosition,
} from '../../interfaces/IHistoryOrder';
import {
  THistoryDataManager,
  THistoryFilter,
  THistoryPositionStatusOption,
  THistoryQuery,
  THistoryQueryManager,
  THistorySortFields,
} from '../../types/history.types';

@Component({
  selector: 'ng-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANShowHistoryOrders],
})
export class HistoryListComponent implements OnInit, OnInitStates {
  public historyOrders$: Observable<
    IFetchData<IHistoryOrder>
  > = this.dataService.getStream('historyOrders$');
  public ordersStats$: Observable<IOrdersTotalStats> = this.dataService.getStream(
    'ordersStats$',
  );

  public dataState: HistoryDataState;
  public dataFlags: HistoryDataFlags;
  public viewState: HistoryViewState;

  public positionStatusOptions: THistoryPositionStatusOption[] = this.viewService.getDataFromDB(
    ['list', 'position-statuses'],
  );
  public sortOptions: TControlOption<THistorySortFields>[] = this.viewService.getDataFromDB(
    ['sortOptions'],
  );

  constructor(
    private toastr: ToastrService,
    private storage: StorageService,
    private preloaderService: PreloaderService,
    private viewService: HistoryViewService,
    private dataService: HistoryDataService,
    private orderDataService: OrdersDataService,
  ) {}

  ngOnInit(): void {
    this.load();
  }

  @Initialize()
  initStates(): void {
    this.dataFlags = this.dataService.getFullState('flags');
    this.dataState = this.dataService.getFullState('state');
    this.viewState = this.viewService.getFullState('state');
  }

  @TrackBy('property', 'id')
  public trackByStatuses() {}

  public toggleShowQuery(queryType: 'filters' | 'sort'): void {
    const value = this.viewState.activeQuery === queryType ? null : queryType;

    this.viewService.setState('state', 'activeQuery', value);
  }

  private load(): void {
    const storageQuery: THistoryQuery = this.storage.get(
      EnHistory.FETCH_QUERY_KEY,
    );
    const queryManager: THistoryQueryManager = this.dataService.getState(
      'state',
      'fetchQueryManager',
    );

    queryManager.setQuery(storageQuery);

    this.preloaderService.firstLoading(this.historyOrders$);
    this.dataService.fetch(queryManager.getQuery(), 'replace');
    this.dataService.getOrdersTotalStats();
  }

  public expand(): void {
    const fetchManager: THistoryDataManager = this.dataService.getState(
      'state',
      'fetchManager',
    );

    if (!fetchManager.isFullItems()) {
      const queryManager: THistoryQueryManager = this.dataService.getState(
        'state',
        'fetchQueryManager',
      );

      this.dataService.fetch(queryManager.nextPage(), 'end');
    }
  }

  public setQuery<
    K extends 'filters' | 'sort',
    V extends IfElse<K, 'filters', THistoryFilter, THistoryQuery['sort']>
  >(key: K, query: V): void {
    const queryManager: THistoryQueryManager = this.dataService.getState(
      'state',
      'fetchQueryManager',
    );
    const requestQuery: THistoryQuery = queryManager.updateQuery(key, query);
    const { sort, filters } = requestQuery;

    queryManager.setPage(0);

    this.dataService.fetch(requestQuery, 'replace');
    this.storage.set(EnHistory.FETCH_QUERY_KEY, { sort, filters });

    if (isEqual(key, 'filters')) {
      this.viewService.setState('state', 'activeQuery', null);
    }
  }

  public repeatOrder(order: IHistoryOrder): void {
    const candidatePositions: IOrderCandidatePosition[] = order.Positions.filter(
      (p) => p.Status === 'active',
    ).map((position: IHistoryOrderPosition) => {
      let {
        PositionId: _id,
        PositionCost: Cost,
        PositionTitle: Title,
        Quantity,
        NewState,
      } = position;

      if (NewState) {
        Title = NewState.PositionTitle;
        Cost = NewState.PositionCost;
      }

      const candidatePosition = new OrderItemPosition(
        { _id, Cost, Title },
        Quantity,
      );

      return candidatePosition;
    });

    this.orderDataService.repeatOrder(candidatePositions);
    this.toastr.info(`Заказ на сумму "${order.TotalAmount}₽" добавлен.`);
  }
}
