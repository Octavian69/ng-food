import { RangeValue } from '@core/form/classes/RangeValue';
import { EnTheme } from '@core/theme/enums/EnTheme.enum';
import { IFetchDataManager } from '@core/utils/fetch/interfaces/IFetchDataManager';
import { IFetchQuery } from '@core/utils/fetch/interfaces/IFetchQuery';
import { IFetchQueryManager } from '@core/utils/fetch/interfaces/IFetchQueryManager';
import { IHistoryOrder } from '../interfaces/IHistoryOrder';

export type THistoryPositionStatus = 'active' | 'removed';
export type THistoryPositionStatusOption = {
  id: string;
  status: 'default' | 'changed' | 'removed';
  label: string;
  color: {
    [EnTheme.LIGHT]: string;
    [EnTheme.DARK]: string;
  };
};
export type THistoryFilter = {
  TotalAmount?: RangeValue<number>;
  Positions?: RangeValue<number>;
  Created?: RangeValue<number>;
};
export type THistorySortFields = 'Created' | 'TotalAmount' | 'Positions';
export type THistoryQuery = IFetchQuery<
  IHistoryOrder,
  THistoryFilter,
  THistorySortFields
>;
export type THistoryDataManager = IFetchDataManager<IHistoryOrder>;
export type THistoryQueryManager = IFetchQueryManager<THistoryQuery>;
