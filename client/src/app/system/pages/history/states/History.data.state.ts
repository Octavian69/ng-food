import { FetchQuery } from '@core/utils/fetch/classes/FetchQuery';
import { Pagination } from '@core/utils/fetch/classes/Pagination';
import { FetchDataManager } from '@core/utils/fetch/managers/FetchData.manager';
import { FetchQueryManager } from '@core/utils/fetch/managers/FetchQuery.manager';
import { EnHistory } from '../enums/history.enum';
import {
  THistoryDataManager,
  THistoryQuery,
  THistoryQueryManager,
} from '../types/history.types';

export class HistoryDataState {
  public fetchManager: THistoryDataManager = new FetchDataManager();
  public fetchQueryManager: THistoryQueryManager;

  constructor() {
    this.initFetchQueryManager();
  }

  private initFetchQueryManager(): void {
    const defaultQuery: THistoryQuery = new FetchQuery(
      new Pagination(EnHistory.FETCH_LIMIT),
    );
    this.fetchQueryManager = new FetchQueryManager(defaultQuery);
  }
}
