import { THistoryPositionStatus } from '../types/history.types';

export interface IHistoryOrder {
  Positions: IHistoryOrderPosition[];
  TotalAmount: number;
  Created: Date;
  User: string;
  _id: string;
}

export interface IHistoryOrderPosition {
  PositionId: string;
  PositionCost: number;
  PositionTitle: string;
  Quantity: number;
  TotalCost: number;
  Status: THistoryPositionStatus;
  NewState: {
    PositionTitle: string;
    PositionCost: number;
    PositionUpdated: Date;
  };
}
