import { NgModule } from '@angular/core';
import { DirectivesModule } from '@core/modules/directives.module';
import { UserModule } from '@core/user/user.module';
import { SystemPageComponent } from './components/system-page/system-page.component';
import { SystemGuard } from './guards/system.guard';
import { AnalyticsService } from './pages/analytics/services/analytics.service';
import { CategoriesService } from './pages/categories/services/categories.service';
import { HistoryService } from './pages/history/services/history.service';
import { OrdersDataService } from './pages/orders/services/orders.data.service';
import { OrdersService } from './pages/orders/services/orders.service';
import { SystemRoutingModule } from './system-routing.module';

@NgModule({
  declarations: [SystemPageComponent],
  imports: [DirectivesModule, UserModule, SystemRoutingModule],
  providers: [
    SystemGuard,
    CategoriesService,
    OrdersService,
    OrdersDataService,
    HistoryService,
    AnalyticsService,
  ],
})
export class SystemModule {}
