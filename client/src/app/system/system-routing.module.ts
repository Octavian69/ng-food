import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SystemGuard } from './guards/system.guard';
import { SystemPageComponent } from './components/system-page/system-page.component';

const routes: Routes = [
  {
    path: '',
    component: SystemPageComponent,
    canActivate: [SystemGuard],
    children: [
      { path: '', redirectTo: '/analytics', pathMatch: 'full' },
      {
        path: 'analytics',
        loadChildren: () =>
          import('./pages/analytics/analytics.module').then(
            ({ AnalyticsModule }) => AnalyticsModule,
          ),
      },
      {
        path: 'history',
        loadChildren: () =>
          import('./pages/history/history.module').then(
            ({ HistoryModule }) => HistoryModule,
          ),
      },
      {
        path: 'orders',
        loadChildren: () =>
          import('./pages/orders/orders.module').then(
            ({ OrdersModule }) => OrdersModule,
          ),
      },
      {
        path: 'categories',
        loadChildren: () =>
          import('./pages/categories/categories.module').then(
            ({ CategoriesModule }) => CategoriesModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemRoutingModule {}
