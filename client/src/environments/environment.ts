// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { IEnvironment } from './interfaces/IEnvironment';

export const environment: IEnvironment = {
  production: false,
  API_URL: 'http://localhost:3000/api',
  VK_APP_ID: '7735562',
  FB_APP_ID: '2878651239055101',
  GOOGLE_APP_ID: '1039309084599-eq1jlu7lpst15n56p9trvp2fl3e2thvk.apps.googleusercontent.com',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
