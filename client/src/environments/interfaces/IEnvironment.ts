export interface IEnvironment {
  production: boolean;
  API_URL: string;
  VK_APP_ID: string;
  FB_APP_ID: string;
  GOOGLE_APP_ID: string;
}