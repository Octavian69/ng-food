import { IEnvironment } from './interfaces/IEnvironment';

export const environment: IEnvironment = {
  production: true,
  API_URL: 'https://ng-food.herokuapp.com/api',
  VK_APP_ID: '7735562',
  FB_APP_ID: '2878651239055101',
  GOOGLE_APP_ID:
    '1039309084599-eq1jlu7lpst15n56p9trvp2fl3e2thvk.apps.googleusercontent.com',
};
