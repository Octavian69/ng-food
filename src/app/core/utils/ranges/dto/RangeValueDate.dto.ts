import { isType } from '@core/validators/handlers/dto-validator.handlers';
import { IsDateString, IsOptional } from 'class-validator';
import { IRangeValue } from '../interfaces/IRangeValue';

export class RangeValueDateDto implements IRangeValue<Date> {
  @IsOptional()
  @IsDateString(isType('start', 'date'))
  public start: Date;

  @IsOptional()
  @IsDateString(isType('end', 'date'))
  public end: Date;
}
