export interface IRangeValue<T = unknown> {
  start: T;
  end: T;
}
