import { IMessage } from "./interfaces/IMessage";

export class Message implements IMessage {
  public message: string;

  constructor(message: any) {
    this.message = String(message);
  }
}