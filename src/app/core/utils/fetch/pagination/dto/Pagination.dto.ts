import { isNumber } from '@core/validators/handlers/dto-validator.handlers';
import { IsNumber } from 'class-validator';
import { IPagination } from '../interfaces/IPagination';

export class PaginationDto implements IPagination {
  @IsNumber(...isNumber('skip'))
  public skip: number;

  @IsNumber(...isNumber('limit'))
  public limit: number;
}
