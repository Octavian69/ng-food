import { IPagination } from './interfaces/IPagination';

export class Pagination implements IPagination {
  constructor(public limit: number = 10, public skip: number = 0) {}
}
