export type TPaginationValue<T> = {
  items: T[];
  totalCount: number;
};
