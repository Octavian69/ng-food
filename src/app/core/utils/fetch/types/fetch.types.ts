export type TSortQuery<T extends string = string> = { [K in T]?: 1 | -1 };
