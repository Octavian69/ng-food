import { isType } from '@core/validators/handlers/dto-validator.handlers';
import { IsObject, IsOptional, ValidateNested } from 'class-validator';
import { PaginationDto } from '../pagination/dto/Pagination.dto';
import { TSortQuery } from '../types/fetch.types';

export class FetchQueryDto<
  T = unknown,
  F extends object = null,
  S extends keyof T & string = null
> {
  public payload: T;

  @IsOptional()
  @ValidateNested()
  public paginate: PaginationDto;

  @IsOptional()
  @IsObject(isType('sort', 'object'))
  public sort: TSortQuery<S>;

  @IsOptional()
  @IsObject(isType('filters', 'object'))
  public filters: F;
}
