import { toDate } from './transform.handlers';

export function equal(v1: any, v2: any): boolean {
  return Object.is(v1, v2);
}

export function isCorrectlyDate(value: any): boolean {
  const date: Date = toDate(value);

  return date.toJSON() ? true : false;
}

export function isNotNullish(value: any): boolean {
  switch (value) {
    case undefined:
    case null:
      return false;
    default:
      return true;
  }
}

export function isNullish(value: any): boolean {
  return !isNotNullish(value);
}

export function isEveryNotNullish(...params: any[]): boolean {
  return params.every(isNotNullish);
}
