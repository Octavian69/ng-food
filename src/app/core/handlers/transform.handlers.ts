export function toDate(value: any): Date {
  return new Date(value);
}

export function toDateArray(dates: (string | number | Date)[]): Date[] {
  return dates && dates.map((date) => new Date(date));
}
