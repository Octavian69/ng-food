import { promises } from 'fs';
import { randomPassword, lower, upper, digits } from 'secure-random-password';
import { RandomPasswordOptions } from 'secure-random-password';
import { config } from '@config/handlers/config.handlers';
import { EnUser } from '@module/user/enums/User.enum';

export function url(...uri: string[]): string {
  const globalPrefix: string = config.get('API_PREFIX');

  return `/${globalPrefix}/` + uri.join('/');
}

export function nodePromisify<F extends Function, T = null>(
  fn: F,
  ...args
): Promise<T> {
  return new Promise((res, rej) => {
    fn(...args, (err: NodeJS.ErrnoException, result?: T) => {
      if (err) rej(err);
      else res(result);
    });
  });
}

export function getRandomPassword(
  passwordLength: number = EnUser.DEFAULT_PASSWORD_LENGTH,
  options?: RandomPasswordOptions,
): string {
  const config: RandomPasswordOptions = options || {
    characters: [lower, upper, digits],
  };

  return randomPassword({
    length: passwordLength,
    ...config,
  });
}

export async function createDirectories(
  directories: string[],
  recursive: boolean = false,
): Promise<void> {
  const toPromises: Promise<string>[] = directories.map((path: string) =>
    promises.mkdir(path, { recursive }),
  );

  await Promise.all(toPromises);
}
