import { ISimple } from '@core/interfaces/shared/ISimple';
//  Replace Tags feature

export type TReplaceTags = ISimple<string | number>;
export type TReplaceStrMeta<T extends ISimple<string | number> = null> = {
  string: string;
  tags: T;
};

export function tag(key: string): string {
  return `{-!{${key}}!-}`;
}

export function replaceStringTags(
  str: string,
  tags: TReplaceTags,
  regexFlags: string = 'gi',
): string {
  return Object.entries(tags).reduce(
    (accum, [key, value]: [string, string | number]) => {
      // ! Все теги должны проходить через функцию-конвенцию: "tag(key)".Как в шаблонах так и при поиске.
      // ! Функция глобальна для всего проекта.
      const regex = new RegExp(tag(key), regexFlags);

      return accum.replace(regex, String(value));
    },
    str,
  );
}

//  Replace Tags feature
export function globalReplaceTags<T extends object>(
  obj: T,
  tags: TReplaceTags,
): T {
  const replacedObjectString: string = replaceStringTags(
    JSON.stringify(obj),
    tags,
    'g',
  );

  return JSON.parse(replacedObjectString);
}

export function pickAndJoin<T extends object, K extends keyof T>(
  obj: T,
  separator: string,
  ...keys: K[]
): string {
  return keys
    .reduce((accum: Array<T[K]>, key: K) => {
      accum.push(obj[key]);
      return accum;
    }, [])
    .join(separator);
}
