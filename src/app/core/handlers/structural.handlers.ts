export function deepCopy<T>(data: T): T {
  return JSON.parse(JSON.stringify(data));
}

export function getDataFromDB<
  V,
  T extends object,
  K1 extends keyof T & string = keyof T & string,
  K2 extends keyof T[K1] & string = keyof T[K1] & string,
  K3 extends keyof T[K1][K2] & string = keyof T[K1][K2] & string,
  K4 extends keyof T[K1][K2][K3] & string = keyof T[K1][K2][K3] & string
>(db: T, path: [K1, K2?, K3?, K4?], isCopy: boolean = true): V {
  let value: any = db[path[0]];
  // @ts-ignore
  path = path.slice(1);

  while (path.length) {
    const key: string = path.shift();
    value = value?.[key];

    if (!value) break;
  }

  const isUndefined: boolean = typeof value === 'undefined';

  return isCopy && !isUndefined ? deepCopy(value) : value;
}

export function keys<T extends object = null>(obj: T): Array<keyof T> {
  return Object.keys(obj) as Array<keyof T>;
}

export function selectProps<T extends object, K extends keyof T = null>(
  obj: T,
  keys: K[],
): Partial<T> {
  return keys.reduce((accum, key) => {
    if (key in obj) {
      accum[key] = obj[key];
    }

    return accum;
  }, {} as Partial<T>);
}

export function makeMergeInstance<T extends object>(
  constructor: Partial<T>,
  selectObj: Partial<T>,
): T {
  const constructorKeys: Array<keyof T> = keys(constructor);
  const partialInstance = selectProps(selectObj, constructorKeys);

  const instance: T = Object.assign(constructor, partialInstance) as T;

  return instance;
}
