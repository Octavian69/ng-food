import { baseErrorHandler } from '@core/errors/handlers/error.handlers';
import { isEveryNotNullish } from '@core/handlers/condition.handlers';
import { Message } from '@core/utils/messages/Message';
import ValidatorJS from 'validator';
import { IValidationLength } from '../interfaces/IValidationLength';
import { TValidatorErrorOptions } from '../types/validatior.types';
import {
  IsNumberOptions,
  validate,
  ValidationError,
  ValidationOptions,
} from 'class-validator';

export async function validateDto<T>(dto: T): Promise<T> {
  const errors: ValidationError[] = await validate(dto);

  if (errors.length) {
    throw errors;
  }

  return dto;
}

export function isInvalid(
  message: string = 'Неккорректные данные',
): ValidationOptions {
  return new Message(message);
}

export function isStringLength<T extends object>(
  title: string,
  field: keyof T,
  schema: IValidationLength<T>,
): [number, ...TValidatorErrorOptions<number>] {
  const min: number = schema.min[field];
  const max: number = schema.max[field];
  const options: ValidationOptions = new Message(
    `Поле "${title}" должно содержать от ${min} до ${max} символов`,
  );

  if (!isEveryNotNullish(min, max))
    baseErrorHandler('parameter is not defined');

  return [min, max, options];
}

export function isMinLength<T extends object, K extends keyof T>(
  title: string,
  field: K,
  schema: Record<K, number>,
): TValidatorErrorOptions<number> {
  const min: number = schema[field];
  const options: ValidationOptions = new Message(
    `Поле:" ${title}" должно содержать не меньше ${min} символов`,
  );

  return [min, options];
}

export function isMaxLength<T extends object, K extends keyof T>(
  title: string,
  field: K,
  schema: Record<K, number>,
): TValidatorErrorOptions<number> {
  const max: number = schema[field];
  const options: ValidationOptions = new Message(
    `Поле "${title}" должно содержать не больше ${max} символов`,
  );

  return [max, options];
}

export function isRequired(title: string): ValidationOptions {
  return new Message(`Поле "${title}" обязательно для заполнения`);
}

export function isEmailField(
  title: string,
  emailOptions: ValidatorJS.IsEmailOptions = { allow_display_name: false },
): TValidatorErrorOptions<ValidatorJS.IsEmailOptions> {
  const message: string = `Поле "${title}" должно быть типа email: "ivanov@mail.ru"`;

  return [emailOptions, new Message(message)];
}

export function isContain(
  title: string,
  contains: Array<string | number> = [],
): TValidatorErrorOptions<Array<string | number>> {
  const containStr = contains.reduce(
    (accum: string, current: string | number, idx: number) => {
      return (accum += `\n${idx + 1}.${String(current)}.`);
    },
    ``,
  ) as string;

  const message: string = `Поле "${title}" может содержать в себе только следующие значения: \n ${containStr}`;

  return [contains, new Message(message)];
}

export function isType(title: string, type: string): ValidationOptions {
  return new Message(`Поле "${title}" должно быть типа: ${type}`);
}

export function isNumber(
  field: string,
  numberOptions: IsNumberOptions = { maxDecimalPlaces: 2 },
): [IsNumberOptions, ValidationOptions] {
  return [numberOptions, isType(field, 'number')];
}

export function isMin(min: number, title: string): [number, ValidationOptions] {
  const message = new Message(`Поле "${title}" не может быть меньше ${min}`);

  return [min, message];
}

export function isMinArrayLength(
  min: number,
  title: string,
): [number, ValidationOptions] {
  const message = new Message(
    `Поле "${title}" должно содержать не меньше ${min} элем.`,
  );

  return [min, message];
}

export function isMaxArrayLength(
  max: number,
  title: string,
): [number, ValidationOptions] {
  const message = new Message(
    `Поле "${title}" должно содержать не меньше ${max} элем.`,
  );

  return [max, message];
}
