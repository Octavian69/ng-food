import { IsDefined, IsNumber, IsString } from 'class-validator';
import { IValidationLength } from '@core/validators/interfaces/IValidationLength';
import { isRequired } from '../../dto-validator.handlers';
import { TValidationRange } from '@core/validators/types/validatior.types';

export class MockDto {
  @IsDefined(isRequired('Имя'))
  @IsString()
  public name: string;

  @IsDefined(isRequired('Возраст'))
  @IsNumber()
  public age: number;
}

export namespace NSMockLength {
  export const min: TValidationRange<MockDto, 'name'> = {
    name: 5,
  };

  export const max: TValidationRange<MockDto, 'name'> = {
    name: 30,
  };
}
