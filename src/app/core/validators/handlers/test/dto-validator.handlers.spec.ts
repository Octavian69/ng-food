import { ValidationError, ValidationOptions } from 'class-validator';
import ValidatorJS from 'validator';
import {
  validateDto,
  isInvalid,
  isStringLength,
  isMinLength,
  isMaxLength,
  isRequired,
  isEmailField,
  isContain,
  isType,
} from '../dto-validator.handlers';
import { MockDto, NSMockLength } from './__mocks__/dto-validator.handlers.mock';
import { TValidatorErrorOptions } from '../../types/validatior.types';
import { Message } from '@core/utils/messages/Message';
import {
  isEveryNotNullish,
  isNullish,
} from '@core/handlers/condition.handlers';
import { baseErrorHandler } from '@core/errors/handlers/error.handlers';

describe('Test "dto-validator" handlers', () => {
  const title: string = 'Имя';
  const field: string = 'name';

  // "validateDto" function
  describe('Test "validateDto" function', () => {
    it('should be defined', () => {
      expect(validateDto).toBeDefined();
      expect(typeof validateDto).toBe('function');
    });

    it('should create correctly dto without errors', async () => {
      const name: string = 'John Doe';
      const age: number = 35;
      const params = { name, age };
      const candidate: MockDto = Object.assign(new MockDto(), params);
      const dto: MockDto = await validateDto(candidate);

      expect(dto).toBeInstanceOf(MockDto);
      expect(dto).toEqual(params);
    });

    it('should throw error with invalid params', async () => {
      try {
        const invalidDto: MockDto = Object.assign(new MockDto(), {
          name: null,
          age: '31',
        });

        await validateDto(invalidDto);
      } catch (e) {
        const [firstErr, lastErr] = e;

        expect(e).toBeInstanceOf(Array);
        expect(e.length).toBe(2);
        expect(firstErr).toBeInstanceOf(ValidationError);
        expect(lastErr).toBeInstanceOf(ValidationError);
      }
    });
  });

  // "isInvalid" function
  describe('Test "isInvalid" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
    });

    it('should be defined', () => {
      expect(isInvalid).toBeDefined();
      expect(typeof isInvalid).toBe('function');
    });

    it('should return correctly error message', () => {
      const result: ValidationOptions = isInvalid();

      expect(result).toBeInstanceOf(Message);
      expect(typeof result.message).toBe('string');
    });
  });

  // "isStringLength" function
  describe('Test "isStringLength" function', () => {
    test('checking the existence of dependencies', () => {
      expect(isEveryNotNullish).toBeDefined();
      expect(baseErrorHandler).toBeDefined();
    });

    it('should be defined', () => {
      expect(isStringLength).toBeDefined();
      expect(typeof isStringLength).toBe('function');
    });

    it('should return correctly value, when called with correctly params', () => {
      const [min, max, options]: [
        number,
        ...TValidatorErrorOptions<number>
      ] = isStringLength(title, 'name', NSMockLength);

      expect(min).toBe(NSMockLength.min['name']);
      expect(max).toBe(NSMockLength.max['name']);
      expect(options.message).toBeDefined();
      expect(typeof options.message).toBe('string');
      expect(options.message).toMatch(new RegExp(`${title}`));
      expect(options.message).toMatch(new RegExp(`${min}`));
      expect(options.message).toMatch(new RegExp(`${max}`));
    });

    it('should throw error if length params not defined', () => {
      try {
        isStringLength('Title', 'name', NSMockLength);
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
        expect(e.message).toBe('parameter is not defined');
      }
    });
  });

  // "isMinLength" function
  describe('Test "isMinLength" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
      expect(isNullish).toBeDefined();
    });

    it('should be defined', () => {
      expect(isMinLength).toBeDefined();
      expect(typeof isMinLength).toBeDefined();
    });

    it('should return correctly value if called correctly params', () => {
      const [min, options]: TValidatorErrorOptions<number> = isMinLength(
        title,
        'name',
        NSMockLength.min,
      );

      expect(typeof min).toBe('number');
      expect(min).toBe(NSMockLength.min.name);
      expect(options.message).toBeDefined();
      expect(typeof options.message).toBe('string');
      expect(options.message).toMatch(new RegExp(`${title}`));
      expect(options.message).toMatch(new RegExp(`${min}`));
    });

    it('should throw error if length params not defined', () => {
      try {
        isMinLength('Title', 'name', NSMockLength.min);
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
        expect(e.message).toBe('parameter is not defined');
      }
    });
  });

  // "isMaxLength" function
  describe('Test "isMaxLength" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
      expect(isNullish).toBeDefined();
    });

    it('should be defined', () => {
      expect(isMaxLength).toBeDefined();
      expect(typeof isMaxLength).toBeDefined();
    });

    it('should return correctly value if called correctly params', () => {
      const [max, options]: TValidatorErrorOptions<number> = isMaxLength(
        title,
        'name',
        NSMockLength.max,
      );

      expect(typeof max).toBe('number');
      expect(max).toBe(NSMockLength.max['name']);
      expect(options.message).toBeDefined();
      expect(typeof options.message).toBe('string');
      expect(options.message).toMatch(new RegExp(`${title}`));
      expect(options.message).toMatch(new RegExp(`${max}`));
    });

    it('should throw error if length params not defined', () => {
      try {
        isMaxLength('Title', 'name', NSMockLength.max);
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
        expect(e.message).toBe('parameter is not defined');
      }
    });
  });

  // "isRequired" function
  describe('Test "isRequired" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
    });

    it('should be defined', () => {
      expect(isRequired).toBeDefined();
      expect(typeof isRequired).toBe('function');
    });

    it('should return correctly value, which contain transmitted title', () => {
      const result: ValidationOptions = isRequired(title);

      expect(result).toBeInstanceOf(Message);
      expect(typeof result.message).toBe('string');
      expect(result.message).toMatch(new RegExp(title));
    });
  });

  // "isEmailField" function
  describe('Test "isEmailField" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
    });

    it('should be defined', () => {
      expect(isEmailField).toBeDefined();
      expect(typeof isEmailField).toBe('function');
    });

    it('should return correctly default email options', () => {
      const [emailOptions] = isEmailField(title);

      expect(emailOptions).toBeInstanceOf(Object);
      expect(emailOptions).toEqual({ allow_display_name: false });
    });

    it('should return correctly custom email options', () => {
      const customOptions: ValidatorJS.IsEmailOptions = {
        allow_display_name: true,
      };
      const [emailOptions] = isEmailField(title, customOptions);

      expect(emailOptions).toBeInstanceOf(Object);
      expect(emailOptions).toEqual({ allow_display_name: true });
    });

    it('should return correctly message', () => {
      const [, options] = isEmailField(title);

      expect(options).toBeInstanceOf(Object);
      expect(options.message).toBeDefined();
      expect(typeof options.message).toBe('string');
      expect(options.message).toMatch(new RegExp(`${title}`));
    });
  });

  // "isContain" function
  describe('Test "isContain" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
    });

    it('should be defined', () => {
      expect(isContain).toBeDefined();
      expect(typeof isContain).toBe('function');
    });

    it('should return default empty contains array', () => {
      const [contains] = isContain(title);

      expect(contains).toBeInstanceOf(Array);
      expect(contains.length).toBe(0);
    });

    it('should return correctly result options', () => {
      const contains: string[] = ['contain', 'params'];
      const [resultContains, options] = isContain(title, contains);

      expect(resultContains).toEqual(contains);
      expect(options).toBeDefined();
      expect(typeof options.message).toBe('string');
      expect(options.message).toMatch(new RegExp(`${title}`));
      expect(options.message).toMatch(new RegExp(`${resultContains[0]}`));
      expect(options.message).toMatch(new RegExp(`${resultContains[1]}`));
    });
  });

  // "isType" function
  describe('Test "isType" function', () => {
    test('checking the existence of dependencies', () => {
      expect(Message).toBeDefined();
    });

    it('should be defined', () => {
      expect(isType).toBeDefined();
      expect(typeof isType).toBe('function');
    });

    it('should return correct options message which contain title and type param', () => {
      const type: string = 'string';
      const result: ValidationOptions = isType(title, type);

      expect(result).toBeInstanceOf(Object);
      expect(typeof result.message).toBe('string');
      expect(result.message).toMatch(new RegExp(`${type}`));
    });
  });
});
