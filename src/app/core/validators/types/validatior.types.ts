import { ValidationOptions } from 'class-validator';

export type TValidatorErrorOptions<T> = [T, ValidationOptions];
export type TValidationRange<T extends object, P extends keyof T> = Record<
  P,
  number
>;
