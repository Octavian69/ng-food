import { isCorrectlyDate } from '@core/handlers/condition.handlers';
import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ async: false })
export class IsStringDate implements ValidatorConstraintInterface {
  public validate(value: any): boolean {
    return isCorrectlyDate(value);
  }

  public defaultMessage(args: ValidationArguments): string {
    const title = args.constraints?.[0] || args.property;

    return `Поле "${title}" должно быть типа: "date"`;
  }
}
