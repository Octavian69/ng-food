import {
  ValidationArguments,
  ValidatorConstraintInterface,
} from 'class-validator';
import { isCorrectlyDate } from '@core/handlers/condition.handlers';
import { IsStringDate } from '../StringDate.constraint';

describe('Test "StringDate" constraint', () => {
  const validator: ValidatorConstraintInterface = new IsStringDate();

  // "validate" method
  describe('Test "validate" method', () => {
    test('should be defiend', () => {
      expect(validator.validate).toBeTruthy();
      expect(typeof validator.validate).toBe('function');
    });

    test('check exitstence of dependencies', () => {
      expect(isCorrectlyDate).toBeTruthy();
      expect(typeof isCorrectlyDate).toBe('function');
    });

    it('should return correctly value', () => {
      const falsy = validator.validate('dasd') as boolean;
      const truthy = validator.validate(new Date()) as boolean;

      expect(falsy).toBe(false);
      expect(truthy).toBe(true);
    });
  });

  // "defaultMessage" method
  describe('Test "defaultMessage" method', () => {
    test('should be defined', () => {
      expect(validator.defaultMessage).toBeTruthy();
      expect(typeof validator.defaultMessage).toBe('function');
    });

    it('should return correct message WITHOUT args', () => {
      const property: string = 'Test property';
      const args = { property } as ValidationArguments;
      const result: string = validator.defaultMessage(args);

      expect(result).toBeTruthy();
      expect(typeof result).toBe('string');
      expect(result).toMatch(new RegExp(`${property}`));
    });

    it('should return correct message WITH args', () => {
      const title: string = 'Test title';
      const args = { constraints: [title] } as ValidationArguments;
      const result: string = validator.defaultMessage(args);

      expect(result).toBeTruthy();
      expect(typeof result).toBe('string');
      expect(result).toMatch(new RegExp(`${title}`));
    });
  });
});
