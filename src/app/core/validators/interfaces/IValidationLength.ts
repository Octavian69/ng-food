import { TKeyOfValue } from '@core/types/object.types';

export interface IValidationLength<T extends object> {
  min?: TKeyOfValue<T, number>;
  max?: TKeyOfValue<T, number>;
}
