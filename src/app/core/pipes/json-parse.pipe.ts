import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  HttpStatus,
} from '@nestjs/common';
import { validateDto } from '@core/validators/handlers/dto-validator.handlers';
import { apiMessageException } from '@core/errors/handlers/error.handlers';

@Injectable()
export class JSONParsePipe<T extends object = null> implements PipeTransform {
  constructor(private DTO: { new (): T }) {}

  async transform(value: string, metadata: ArgumentMetadata) {
    try {
      const parseValue = JSON.parse(value);
      const validateValue = await validateDto(
        Object.assign(new this.DTO(), parseValue),
      );

      return validateValue;
    } catch (e) {
      apiMessageException(String(e), HttpStatus.BAD_REQUEST);
    }
  }
}
