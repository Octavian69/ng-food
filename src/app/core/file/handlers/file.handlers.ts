import { NSFile } from '@core/file/namespaces/file.namespaces';
import { TFile } from '@core/file/types/file.types';
import { nodePromisify } from '@core/handlers/api.handlers';
import { replaceStringTags } from '@core/handlers/string.handlers';
import { getDataFromDB } from '@core/handlers/structural.handlers';
import { IUserRequest } from '@module/user/interfaces/IUserRequest';
import { HttpException, HttpStatus } from '@nestjs/common';
import { join } from 'path';
import * as rimraf from 'rimraf';
import { FileDB } from '../db/file.db';
import { EnFile } from '../enums/file.enum';
import {
  apiMessageException,
  getErrorMessage,
} from '@core/errors/handlers/error.handlers';
import {
  diskStorage,
  DiskStorageOptions,
  FileFilterCallback,
  Options as MulterOptions,
  StorageEngine,
} from 'multer';

export function covertToBytes(mb: number): number {
  return mb * Math.pow(1024, 2);
}

export async function removeFile(path: string): Promise<void> {
  return await nodePromisify(rimraf, path);
}

export async function updateFileStorage(
  file: Express.Multer.File,
  previousPath: string = null,
): Promise<string> {
  if (file) {
    if (previousPath) removeFile(previousPath);

    return file.path;
  }

  return previousPath;
}

export function getDestination(filePath: string[] = []) {
  return (req: IUserRequest, file: Express.Multer.File, cb: Function) => {
    const { _id } = req.user;
    const path: string = join(EnFile.FILES_ROOT, _id.toString(), ...filePath);

    return cb(null, path);
  };
}

export function filename(
  req: IUserRequest,
  file: Express.Multer.File,
  cb: Function,
): string {
  const fileName: string = `[${Date.now()}]__${file.originalname}`;

  return cb(null, fileName);
}

export function getFileFilter(type: TFile) {
  return (
    req: IUserRequest,
    file: Express.Multer.File,
    cb: Function,
  ): FileFilterCallback => {
    const mime: string[] = NSFile.getMIME(type);
    const isValid: boolean = mime.includes(file.mimetype);
    const message: string = replaceStringTags(
      getDataFromDB(FileDB, ['messages', 'invalid-mime']),
      { mime: mime.toString() },
    );

    return isValid
      ? cb(null, true)
      : cb(new HttpException(message, HttpStatus.BAD_REQUEST));
  };
}

export function multerOptions(
  fileType: TFile,
  filePath: string[],
  fileSize: number,
): MulterOptions {
  try {
    const fileFilter = getFileFilter(fileType) as any;
    const destination = getDestination(filePath);
    const limits = { fileSize: covertToBytes(fileSize) };
    const options: DiskStorageOptions = {
      // @ts-ignore
      destination,
      // @ts-ignore
      filename,
    };
    const storage: StorageEngine = diskStorage(options);

    return {
      limits, // todo Не срабатывает валидация на размер файла
      fileFilter,
      storage,
    };
  } catch (e) {
    apiMessageException(getErrorMessage(e), HttpStatus.BAD_REQUEST);
  }
}
