export type TFile = 'IMG';
export type TFileOptions = {
  fileType: TFile;
  filePath: string[];
  fileSize: number; //? размер указывается в мегабайтах
};
