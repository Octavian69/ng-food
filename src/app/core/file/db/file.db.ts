import { tag } from '@core/handlers/string.handlers';

export const FileDB = {
  messages: {
    'invalid-mime': `Некорректный формат файла.Допустимы следующие расширения: ${tag(
      'mime',
    )}.`,
  },
};

export type TFileDB = typeof FileDB;
