import {
  FileFilterCallback,
  StorageEngine,
  Options as MulterOptions,
} from 'multer';
import { multerOptions } from '../handlers/file.handlers';
import { TFileOptions } from '../types/file.types';

export class FileOptions {
  public fileFilter: (
    req: Request,
    file: Express.Multer.File,
    callback: FileFilterCallback,
  ) => void;
  public storage: StorageEngine;

  constructor(options: TFileOptions) {
    const { fileType, filePath, fileSize } = options;
    const { fileFilter, storage }: MulterOptions = multerOptions(
      fileType,
      filePath,
      fileSize,
    );

    // @ts-ignore
    this.fileFilter = fileFilter;
    this.storage = storage;
  }
}
