import { TFile } from '../types/file.types';

export namespace NSFile {
  const MIME = {
    IMG: ['image/jpeg', 'image/png'],
  };

  export function getMIME(fileType: TFile): string[] {
    return MIME[fileType];
  }
}
