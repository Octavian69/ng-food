// * Typescript
export type ObjectValues<
  T extends object = object,
  K extends keyof T & string = keyof T & string
> = T[K];
