import { Request } from 'express';

export type TMode = 'production' | 'development' | 'testing';
export type ExpressRequest = Request;
