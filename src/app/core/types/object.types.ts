export type TKeyOfValue<T extends object, V> = {
  [K in keyof T]?: V;
};

export type TObjectPath<
  T extends object,
  K1 extends keyof T & string = keyof T & string,
  K2 extends keyof T[K1] & string = keyof T[K1] & string,
  K3 extends keyof T[K1][K2] & string = keyof T[K1][K2] & string,
  K4 extends keyof T[K1][K2][K3] & string = keyof T[K1][K2][K3] & string
> = [K1, K2?, K3?, K4?];

type Grow<T, A extends Array<T>> = ((x: T, ...xs: A) => void) extends (
  ...a: infer X
) => void
  ? X
  : never;
type GrowToSize<T, A extends Array<T>, N extends number> = {
  0: A;
  1: GrowToSize<T, Grow<T, A>, N>;
}[A['length'] extends N ? 0 : 1];

export type FixedArray<T, N extends number> = GrowToSize<T, [], N>;
