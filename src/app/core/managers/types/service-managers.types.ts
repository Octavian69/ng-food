
export type TServiceManager = 'database';
export type TServiceManagerMeta = [{ new(meta: object): any }, object, TServiceManager];