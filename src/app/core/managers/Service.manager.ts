import { DatabaseManager } from './Database.manager';
export class ServiceManager<DB extends object = null> {

  private db: DatabaseManager<DB>;

  constructor(
    db: DB = null
  ) {
    this.initManagers(db);
  }

  private initManagers(db: DB): void {
    this.initDB(db);
  }

  private initDB(db: DB): void {
    if (db) this.db = new DatabaseManager<DB>(db);
  }

  public getDataFromDB<
    V,
    K1 extends keyof DB & string = keyof DB & string,
    K2 extends keyof DB[K1] & string = keyof DB[K1] & string,
    K3 extends keyof DB[K1][K2] & string = keyof DB[K1][K2] & string,
    K4 extends keyof DB[K1][K2][K3] & string = keyof DB[K1][K2][K3] & string,
    >(keys: [K1, K2?, K3?, K4?], isCopy: boolean = true): V {
    return this.db.get<V, K1, K2, K3, K4>(keys, isCopy);
  }
}