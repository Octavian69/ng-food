import { getDataFromDB } from "../handlers/structural.handlers";


export class DatabaseManager<T extends object> {
  constructor(private db: T) { }

  public get<
    V,
    K1 extends keyof T & string = keyof T & string,
    K2 extends keyof T[K1] & string = keyof T[K1] & string,
    K3 extends keyof T[K1][K2] & string = keyof T[K1][K2] & string,
    K4 extends keyof T[K1][K2][K3] & string = keyof T[K1][K2][K3] & string,
    >(path: [K1, K2?, K3?, K4?], isCopy: boolean = true): V {

    return getDataFromDB<V, T, K1, K2, K3, K4>(this.db, path, isCopy);
  }
}
