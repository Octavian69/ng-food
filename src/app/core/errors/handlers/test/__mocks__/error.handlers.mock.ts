
export class MockCustomError {

  public error: { message: string };

  constructor(message: string) {
    this.error = { message };
  }
}

