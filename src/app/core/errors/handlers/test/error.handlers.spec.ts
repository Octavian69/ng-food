import {
  apiErrorHandler,
  apiMessageException,
  baseErrorHandler,
  getErrorMessage,
} from '../error.handlers';
import { ErrorDB } from '../../db/errors.db';
import { MockCustomError } from './__mocks__/error.handlers.mock';
import { HttpException, HttpStatus } from '@nestjs/common';

describe('Test "error" handlers', () => {
  // "baseErrorHandler" function
  describe('Test "baseErrorHandler" function', () => {
    it('should be defined', () => {
      expect(baseErrorHandler).toBeDefined();
      expect(typeof baseErrorHandler).toBe('function');
    });

    it('should be instance of "Error"', () => {
      try {
        baseErrorHandler();
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });

    it('should throw correctly CUSTOM error message', () => {
      const message: string = 'Test error';
      try {
        baseErrorHandler(message);
      } catch (e) {
        expect(e.message).toBe(message);
      }
    });

    it('should throw correctly DEFAULT message', () => {
      try {
        baseErrorHandler();
      } catch (e) {
        expect(e.message).toBe('Ошибка');
      }
    });
  });

  // "getErrorMessage" function
  describe('Test "getErrorMessage" function', () => {
    it('should be defined', () => {
      expect(getErrorMessage).toBeDefined();
      expect(typeof getErrorMessage).toBe('function');
    });

    it('should return correctly message', () => {
      const customMsg: string = 'Custom message';
      const customErr: MockCustomError = new MockCustomError(customMsg);
      const defaultMsg: string = 'Default message';
      const defaultErr: Error = new Error(defaultMsg);
      const stringMsg: string = 'String message';

      expect(getErrorMessage(customErr)).toBe(customMsg);
      expect(getErrorMessage(defaultErr)).toBe(defaultMsg);
      expect(getErrorMessage(stringMsg)).toBe(stringMsg);
    });
  });

  // "apiMessageException" function
  describe('Test "apiMessageException" function', () => {
    it('should be defined', () => {
      expect(apiMessageException).toBeDefined();
      expect(typeof apiMessageException).toBe('function');
    });

    it('should be instance of "HttpException"', () => {
      try {
        apiMessageException('default');
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('should throw correct custom message', () => {
      const message: string = 'Custom message';
      try {
        apiMessageException(message);
      } catch (e) {
        expect(e.message).toBe(message);
      }
    });

    it('should throw correctly default status', () => {
      try {
        apiMessageException('message');
      } catch (e) {
        expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    });

    it('should throw correctly custom status', () => {
      try {
        apiMessageException('message', HttpStatus.NOT_FOUND);
      } catch (e) {
        expect(e.status).toBe(HttpStatus.NOT_FOUND);
      }
    });
  });

  // "apiErrorHandler" function
  describe('Test "apiErrorHandler" function', () => {
    it('should be defined', () => {
      expect(apiErrorHandler).toBeDefined();
      expect(typeof apiErrorHandler).toBe('function');
    });

    it('should throw correctly exception with default params', () => {
      try {
        apiErrorHandler();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.message).toBe(ErrorDB.main.default);
        expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    });

    it('should throw correctly exception with params', () => {
      try {
        apiErrorHandler(['main', 'nodePromisify'], HttpStatus.FORBIDDEN);
      } catch (e) {
        expect(e.message).toBe(ErrorDB.main.nodePromisify);
        expect(e.status).toBe(HttpStatus.FORBIDDEN);
      }
    });
  });
});
