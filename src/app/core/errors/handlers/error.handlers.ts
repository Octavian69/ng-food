import { getDataFromDB } from '@core/handlers/structural.handlers';
import { HttpStatus, HttpException } from '@nestjs/common';
import { TErrorDB, ErrorDB } from '../db/errors.db';

export function baseErrorHandler(message: string = 'Ошибка'): never {
  throw new Error(message);
}

export function getErrorMessage(err: any): string {
  return err.error?.message || err.message || String(err);
}

export function apiMessageException(
  message: string,
  status: number = HttpStatus.INTERNAL_SERVER_ERROR,
): never {
  throw new HttpException(message, status);
}

export function apiErrorHandler<
  K1 extends keyof TErrorDB & string = keyof TErrorDB & string,
  K2 extends keyof TErrorDB[K1] & string = keyof TErrorDB[K1] & string
>(
  path: [K1, K2?] = ['main', 'default'] as any,
  status: number = HttpStatus.INTERNAL_SERVER_ERROR,
): never {
  const message: string = getDataFromDB(ErrorDB, path);

  apiMessageException(message, status);
}
