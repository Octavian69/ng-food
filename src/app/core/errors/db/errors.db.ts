export const ErrorDB = {

  auth: {
    unauthorized: 'Некорректные данные. Проверьте правильность ввода.',
    deniedRefresh: 'Истекло время сессии. Используйте личные данные для входа в кабинет.',
    failedRegistration: 'Регистрация не завершилась. Попробуйте позже.'

  },
  main: {
    nodePromisify: 'Ошибка промисификации',
    default: 'Произошла ошибка на сервере'
  },

} as const;

export type TErrorDB = typeof ErrorDB;