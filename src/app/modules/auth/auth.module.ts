import { Module } from "@nestjs/common";
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { APP_GUARD } from '@nestjs/core';
import { PassportModule } from "@nestjs/passport";
import { UserModule } from "@module/user/user.module";
import { AuthService } from "./auth.service";
import { AuthController } from './auth.controller';
import { Tokens, TokensSchema } from './schemas/Tokens.schema';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtConfigService } from "./services/jwt-config.service";
import { JwtStrategy } from "./strategies/jwt.strategy";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Tokens.name, schema: TokensSchema }
    ]),
    PassportModule,
    JwtModule.registerAsync({
      useClass: JwtConfigService
    }),
    UserModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard
    }
  ],
  exports: [
    AuthService
  ]
})
export class AuthModule { }