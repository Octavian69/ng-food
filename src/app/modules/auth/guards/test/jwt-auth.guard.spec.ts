import {
  MockExecutionContext,
  MockReflector,
} from '@core/test/__mocks__/api.mock';
import { Reflector } from '@nestjs/core';
import { JwtAuthGuard } from '../jwt-auth.guard';

describe('Test "jwt-auth" guard', () => {
  let guard: JwtAuthGuard;
  let reflector: MockReflector;

  beforeEach(() => {
    reflector = new MockReflector();
    guard = new JwtAuthGuard(reflector as Reflector);
  });

  test('should eb defined', () => {
    expect(JwtAuthGuard).toBeTruthy();
  });

  // "canActivate" method
  describe('Test "canActivate" method', () => {
    test('should be defiend', () => {
      expect('canActivate' in guard).toBeTruthy();
    });

    it('should return true if public route', () => {
      const context = new MockExecutionContext();
      const ctxHandlerSpy = jest
        .spyOn(context, 'getHandler')
        .mockReturnValueOnce(true);
      const ctxClassSpy = jest
        .spyOn(context, 'getClass')
        .mockReturnValueOnce(true);
      reflector.getAllAndOverride = jest.fn(() => true);

      const result = guard.canActivate(context);

      expect(result).toBe(true);
      expect(ctxHandlerSpy).toHaveBeenCalledTimes(1);
      expect(ctxClassSpy).toHaveBeenCalledTimes(1);
      expect(reflector.getAllAndOverride).toHaveBeenCalledTimes(1);
    });

    it('should return false if NOT public route', () => {
      const context = new MockExecutionContext();
      const superProto = Object.getPrototypeOf(Object.getPrototypeOf(guard));
      const ctxHandlerSpy = jest
        .spyOn(context, 'getHandler')
        .mockReturnValueOnce(true);
      const ctxClassSpy = jest
        .spyOn(context, 'getClass')
        .mockReturnValueOnce(true);
      const protoSpy = jest
        .spyOn(superProto, 'canActivate')
        .mockReturnValueOnce(false);
      reflector.getAllAndOverride = jest.fn(() => false);

      const result = guard.canActivate(context);

      expect(result).toBe(false);
      expect(ctxHandlerSpy).toHaveBeenCalledTimes(1);
      expect(ctxClassSpy).toHaveBeenCalledTimes(1);
      expect(protoSpy).toHaveBeenCalledTimes(1);
      expect(reflector.getAllAndOverride).toHaveBeenCalledTimes(1);
    });
  });
});
