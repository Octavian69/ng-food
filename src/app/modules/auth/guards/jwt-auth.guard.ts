import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { config } from '@config/handlers/config.handlers';
import { EnAPI } from '@core/enums/api.enum';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  public canActivate(
    context: ExecutionContext,
  ): Promise<boolean> | Observable<boolean> | boolean {
    const isPublicRoute: boolean = this.reflector.getAllAndOverride<boolean>(
      EnAPI['PUBLIC_ROUTE_KEY'],
      [context.getHandler(), context.getClass()],
    );

    if (isPublicRoute) return true;

    return super.canActivate(context);
  }

  // handleRequest(err: any, user: any, info: any, context: any, status?: any) {
  //   console.log('err: ', err);
  //   console.log('user: ', user);
  //   console.log('status: ', status);
  //   console.log('context: ', context);
  //   console.log('arguments: ', arguments);

  //   return user;
  // }
}
