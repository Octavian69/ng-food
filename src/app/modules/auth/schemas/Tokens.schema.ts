import { MongoId } from '@module/mongo/types/mongo.types';
import { User } from '@module/user/schemas/User.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { ITokens } from '../interfaces/ITokens';

@Schema()
export class Tokens implements ITokens {
  @Prop({ default: null })
  access_token: string;

  @Prop({ default: null })
  refresh_token: string;

  @Prop({ required: true, ref: User.name, type: MongooseSchema.Types.ObjectId })
  User: MongoId;
}

export type TokensDocument = Tokens & Document;
export const TokensSchema: MongooseSchema<TokensDocument> = SchemaFactory.createForClass(
  Tokens,
);
