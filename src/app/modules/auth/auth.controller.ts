import {
  Body,
  Controller,
  Post,
  UseGuards,
  Patch,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { IAuthTokens } from './interfaces/IAuthTokens';
import { PublicRoute } from './metadata/auth.metadata';
import { RefreshTokenDto } from './dto/RefreshToken.dto';
import { UserCredentialsDto } from '@module/user/dto/UserCredentials.dto';
import { IUserInfo } from '@module/user/interfaces/IUserInfo';
import { UserDto } from '@module/user/dto/User.dto';
import { IMessage } from '@core/utils/messages/interfaces/IMessage';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  @PublicRoute()
  public async login(@Body() body: UserCredentialsDto): Promise<IAuthTokens> {
    const user: IUserInfo = await this.authService.login(body);

    return this.authService.updateTokens(user);
  }

  @Post('registration')
  @HttpCode(HttpStatus.CREATED)
  @PublicRoute()
  public async registration(@Body() user: UserDto): Promise<IMessage> {
    return this.authService.registration(user);
  }

  @Patch('refresh-tokens')
  @PublicRoute()
  public async refreshTokens(
    @Body() token: RefreshTokenDto,
  ): Promise<IAuthTokens> {
    return this.authService.refreshTokens(token.refresh_token);
  }
}
