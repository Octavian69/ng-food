import { MongoId } from '@module/mongo/types/mongo.types';
export interface ITokens {
  access_token: string;
  refresh_token: string;
  User: MongoId;
}
