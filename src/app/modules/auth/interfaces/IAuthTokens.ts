import { ITokens } from "./ITokens";

export interface IAuthTokens extends Omit<ITokens, 'User'> { }