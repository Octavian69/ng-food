import { SetMetadata } from '@nestjs/common';
import { EnAPI } from '@core/enums/api.enum';

export const PublicRoute = () => SetMetadata(EnAPI['PUBLIC_ROUTE_KEY'], true);
