import { fillNullishArgs } from '@core/test/utils/test.handlers';
import { AuthService } from '@module/auth/auth.service';
import { MockUser } from '@module/user/test/__mocks__/user.mock';
import { LocalStrategy } from '../local.strategy';

describe('Test "local" strategy', () => {
  let strategy: LocalStrategy;
  let authService: AuthService;

  beforeEach(() => {
    authService = new AuthService(...fillNullishArgs<3>(3));
    strategy = new LocalStrategy(authService);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  test('should be defined', () => {
    expect(LocalStrategy).toBeTruthy();
  });

  // "validate" method
  describe('Test "validate" method', () => {
    test('should be defined', () => {
      expect('validate' in strategy).toBeTruthy();
      expect(typeof strategy.validate).toBe('function');
    });

    it('should return info if user found', async () => {
      const mockUser = new MockUser();
      const spy = jest
        .spyOn(authService, 'login')
        .mockResolvedValueOnce(mockUser);

      const result = await strategy.validate(...fillNullishArgs<2>(2));

      expect(result).toBeTruthy();
      expect(result).toBe(mockUser);
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
