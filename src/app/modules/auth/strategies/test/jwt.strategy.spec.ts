import { AuthService } from '@module/auth/auth.service';
import { UserDocument } from '@module/user/schemas/User.schema';
import { UserInfoModel } from '@module/user/models/UserInfo.model';
import { MockUser, MockUserInfo } from '@module/user/test/__mocks__/user.mock';
import { fillNullishArgs } from '@core/test/utils/test.handlers';
import { JwtStrategy } from '../jwt.strategy';
import { HttpException } from '@nestjs/common';

describe('Test "jwt" strategy', () => {
  let strategy: JwtStrategy;
  let authService: AuthService;

  beforeEach(() => {
    authService = new AuthService(...fillNullishArgs<3>(3));
    strategy = new JwtStrategy(authService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should be defined', () => {
    expect(JwtStrategy).toBeTruthy();
  });

  // "validate" method
  describe('Test "validate" method', () => {
    test('should be defiend', () => {
      expect('validate' in strategy).toBeTruthy();
      expect(typeof strategy.validate).toBe('function');
    });

    it('should return info if user found', async () => {
      const user = Object.assign(new MockUserInfo());
      const mockDocument = {
        toJSON() {
          return user;
        },
      };
      const spy = jest
        .spyOn(authService, 'getValidateUser')
        .mockResolvedValueOnce(mockDocument as UserDocument);

      const result = (await strategy.validate(
        { _id: user._id },
        null,
      )) as UserInfoModel;

      expect(result).toBeTruthy();
      expect(result).toBeInstanceOf(UserInfoModel);
      expect(result.Name).toBe(user.Name);
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should return error if user not found', async () => {
      const spy = jest
        .spyOn(authService, 'getValidateUser')
        .mockResolvedValueOnce(null);

      const result: any = ((await strategy.validate({ _id: '123' }, (err) => {
        if (err) return err;
      })) as unknown) as HttpException;

      expect(result).toBeInstanceOf(HttpException);
      expect(result.status).toBe(401);
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
