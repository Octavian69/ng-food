import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifyCallback } from 'passport-jwt';
import { config } from '@config/handlers/config.handlers';
import { IUserInfo } from '@module/user/interfaces/IUserInfo';
import { UserInfoModel } from '@module/user/models/UserInfo.model';
import { UserDocument } from '@module/user/schemas/User.schema';
import { AuthService } from '../auth.service';
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get('SECRET_JWT'),
    });
  }

  public async validate(
    info: Partial<IUserInfo>,
    done: VerifyCallback,
  ): Promise<UserInfoModel | void> {
    const user: UserDocument = await this.authService.getValidateUser({
      _id: info._id,
    });

    if (!user) {
      return done(
        new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED),
        null,
      );
    }

    return new UserInfoModel(user.toJSON());
  }
}
