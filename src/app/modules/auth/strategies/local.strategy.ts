import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { IUserCredentials } from "@module/user/interfaces/IUserCredentials";
import { IUserInfo } from "@module/user/interfaces/IUserInfo";
import { UserCredentialsModel } from "@module/user/models/UserCredentials.model";
import { AuthService } from '../auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private authService: AuthService
  ) {
    super({
      usernameField: 'Login',
      passwordField: 'Password',
      session: false
    })
  }

  async validate(login: string, password: string): Promise<IUserInfo> {
    const credentials: IUserCredentials = new UserCredentialsModel(login, password);
    const userInfo: IUserInfo = await this.authService.login(credentials);

    return userInfo;
  }
}