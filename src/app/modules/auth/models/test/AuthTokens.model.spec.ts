import { AuthTokensModel } from '../AuthTokens.model';

describe('Test "AuthTokensModel" model', () => {
  test('should be defined', () => {
    expect(AuthTokensModel).toBeTruthy();
  });

  test('should correctly create instance', () => {
    expect(new AuthTokensModel('access', 'refresh')).toBeInstanceOf(
      AuthTokensModel,
    );
  });
});
