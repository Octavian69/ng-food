import { IAuthTokens } from "../interfaces/IAuthTokens";

export class AuthTokensModel implements IAuthTokens {
  constructor(
    public access_token: string,
    public refresh_token: string
  ) {
  }
} 
