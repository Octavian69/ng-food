import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { genSalt, hash, compare } from 'bcryptjs';
import { join } from 'path';

import { config } from '@config/handlers/config.handlers';
import { IMessage } from '@core/utils/messages/interfaces/IMessage';
import { createDirectories } from '@core/handlers/api.handlers';
import { Message } from '@core/utils/messages/Message';
import { equal } from '@core/handlers/condition.handlers';
import { ServiceManager } from '@core/managers/Service.manager';
import { EnFile } from '@core/file/enums/file.enum';
import {
  apiErrorHandler,
  baseErrorHandler,
} from '@core/errors/handlers/error.handlers';
import { IUserCredentials } from '@module/user/interfaces/IUserCredentials';
import { UserInfoModel } from '@module/user/models/UserInfo.model';
import { IUser } from '@module/user/interfaces/IUser';
import { UserDocument } from '@module/user/schemas/User.schema';
import { UserService } from '@module/user/user.service';
import { UserDto } from '@module/user/dto/User.dto';
import { IUserInfo } from '@module/user/interfaces/IUserInfo';
import { AuthDB, TAuthDB } from './db/auth.db';
import { EnJwt } from './enums/jwt.enum';
import { IAuthTokens } from './interfaces/IAuthTokens';
import { AuthTokensModel } from './models/AuthTokens.model';
import { Tokens, TokensDocument } from './schemas/Tokens.schema';
import { MongoId } from '../mongo/types/mongo.types';
import { toObjectId } from '@module/mongo/handlers/mongo.handlers';

@Injectable()
export class AuthService extends ServiceManager<TAuthDB> {
  constructor(
    @InjectModel(Tokens.name)
    private readonly TokensModel: Model<TokensDocument>,
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {
    super(AuthDB);
  }

  public async registration(candidate: UserDto): Promise<IMessage> {
    try {
      const salt: string = await genSalt(config.get('PASSWORD_SALT'));
      const Password: string = await hash(candidate.Password, salt);
      const user: IUser = Object.assign({}, candidate, { Password });
      const { _id }: UserDocument = await this.userService.create(user);
      const filesRoot = join(EnFile.FILES_ROOT, String(_id));

      await new this.TokensModel({ User: _id }).save();
      await this.createDirectories(filesRoot);

      return new Message('Анкета успешно зарегистрирована');
    } catch (e) {
      apiErrorHandler(['auth', 'failedRegistration']);
    }
  }

  public async login({
    Password,
    Login,
  }: IUserCredentials): Promise<IUserInfo> {
    try {
      const user: UserDocument = await this.userService.getUserWithCredentials({
        Login,
      });

      if (!user) baseErrorHandler();

      const isEqualPasswords: boolean = await compare(Password, user.Password);

      if (!isEqualPasswords) baseErrorHandler();

      const userInfo: IUserInfo = new UserInfoModel(user);

      return userInfo;
    } catch (e) {
      apiErrorHandler(['auth', 'unauthorized'], HttpStatus.NOT_FOUND);
    }
  }

  public async remove(userId: MongoId): Promise<void> {
    await this.TokensModel.findOneAndRemove({ User: userId });
  }

  private async createTokens<T extends object>(data: T): Promise<IAuthTokens> {
    const secret: string = config.get('SECRET_JWT');
    const payload: T = Object.assign({}, data);
    const access_token = await this.jwtService.signAsync(payload, {
      expiresIn: EnJwt.ACCESS_TIME,
      secret,
    });
    const refresh_token = await this.jwtService.signAsync(payload, {
      expiresIn: EnJwt.REFRESH_TIME,
      secret,
    });
    const tokens: IAuthTokens = new AuthTokensModel(
      access_token,
      refresh_token,
    );

    return tokens;
  }

  public async updateTokens(userInfo: IUserInfo): Promise<IAuthTokens> {
    const tokens: IAuthTokens = await this.createTokens(userInfo);
    const {
      access_token,
      refresh_token,
    } = await this.TokensModel.findOneAndUpdate(
      { User: userInfo._id },
      { $set: tokens },
      { new: true },
    );

    return new AuthTokensModel(access_token, refresh_token);
  }

  public async refreshTokens(refresh_token: string): Promise<IAuthTokens> {
    try {
      const {
        _id,
        Avatar,
        Name,
        Created,
        Gender,
        Position,
      } = this.jwtService.decode(refresh_token) as IUserInfo;
      const tokens: TokensDocument = await this.TokensModel.findOne({
        User: toObjectId(<string>(<unknown>_id)),
      });

      const isEqual: boolean = equal(refresh_token, tokens.refresh_token);

      if (!isEqual) baseErrorHandler();

      const secret: string = config.get('SECRET_JWT');

      await this.jwtService.verifyAsync(refresh_token, { secret });
      await this.jwtService.verifyAsync(tokens.refresh_token, { secret });

      return this.updateTokens({
        _id,
        Avatar,
        Name,
        Created,
        Gender,
        Position,
      });
    } catch (e) {
      apiErrorHandler(['auth', 'deniedRefresh'], HttpStatus.FORBIDDEN);
    }
  }

  public async getValidateUser<T extends Partial<IUserInfo>>(
    payload: T,
  ): Promise<UserDocument> {
    return this.userService.getUserWithoutCredentials(payload);
  }

  public async createDirectories(root: string): Promise<void> {
    await createDirectories([root]);

    const directories = this.getDataFromDB<string[]>([
      'directories',
    ]).map((dir: string) => join(root, dir));

    await createDirectories(directories, true);
  }
}
