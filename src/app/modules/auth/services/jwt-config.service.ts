import { config } from "@config/handlers/config.handlers";
import { JwtModuleOptions, JwtOptionsFactory } from "@nestjs/jwt";
import { EnJwt } from "../enums/jwt.enum";

export class JwtConfigService implements JwtOptionsFactory {
  createJwtOptions(): JwtModuleOptions {
    return {
      secret: config.get('SECRET_JWT'),
      signOptions: {
        expiresIn: EnJwt.ACCESS_TIME
      }
    }
  }
}