export const AuthDB = {
  socilaProviders: ['FACEBOOK', 'GOOGLE'],
  directories: ['img/avatars', 'img/categories', 'img/positions'],
} as const;

export type TAuthDB = typeof AuthDB;
