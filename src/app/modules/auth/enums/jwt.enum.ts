export enum EnJwt {
  ACCESS_TIME = '1h',
  REFRESH_TIME = '7d',
}
