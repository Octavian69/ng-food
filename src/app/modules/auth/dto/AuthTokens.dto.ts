import { IsDefined, IsJWT } from 'class-validator';
import { isInvalid } from '@core/validators/handlers/dto-validator.handlers';
import { IAuthTokens } from '../interfaces/IAuthTokens';

export class AuthTokensDto implements IAuthTokens {
  @IsDefined(isInvalid())
  @IsJWT(isInvalid())
  access_token: string;

  @IsDefined(isInvalid())
  @IsJWT(isInvalid())
  refresh_token: string;
}
