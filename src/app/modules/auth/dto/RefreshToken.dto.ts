import { PickType } from "@nestjs/mapped-types";
import { AuthTokensDto } from './AuthTokens.dto';

export class RefreshTokenDto extends PickType(AuthTokensDto, ['refresh_token'] as const) { }