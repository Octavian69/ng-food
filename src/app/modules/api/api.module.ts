import { AnalyticsModule } from '@module/analytics/analytics.module';
import { AuthSocialModule } from '@module/auth-social/auth-social.module';
import { AuthModule } from '@module/auth/auth.module';
import { CategoriesModule } from '@module/categories/categories.module';
import { EmailModule } from '@module/email/email.module';
import { HistoryModule } from '@module/history/history.module';
import { OrdersModule } from '@module/orders/orders.module';
import { PositionsModule } from '@module/positions/positions.module';
import { RemoveOfficeModule } from '@module/remove-office/remove-office.module';
import { UserModule } from '@module/user/user.module';
import { Module } from '@nestjs/common';

const modules = [
  UserModule,
  AuthModule,
  AuthSocialModule,
  RemoveOfficeModule,
  EmailModule,
  CategoriesModule,
  PositionsModule,
  OrdersModule,
  HistoryModule,
  AnalyticsModule,
];

@Module({
  imports: modules,
  exports: modules,
})
export class ApiModule {}
