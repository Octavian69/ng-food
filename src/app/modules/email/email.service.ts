import { Injectable } from '@nestjs/common';
import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { EmailOptionsDto } from './dto/EmailOptions.dto';
import { EmailDB, TEmailDB } from './db/email.db';
import { ServiceManager } from '@core/managers/Service.manager';
import {
  globalReplaceTags,
  TReplaceTags,
} from '@core/handlers/string.handlers';
import { validateDto } from '@core/validators/handlers/dto-validator.handlers';
import { IMessage } from '@core/utils/messages/interfaces/IMessage';
import { Message } from '@core/utils/messages/Message';
import { apiMessageException } from '@core/errors/handlers/error.handlers';

@Injectable()
export class EmailService extends ServiceManager<TEmailDB> {
  constructor(private mailerService: MailerService) {
    super(EmailDB);
  }

  public async completeEmailOptions(
    opts: ISendMailOptions,
    tags?: TReplaceTags,
  ): Promise<EmailOptionsDto> {
    const from: string = this.getDataFromDB([
      'configs',
      'default',
      'smtp_service_address',
    ]);
    const candidateOptions = Object.assign({}, { from }, opts);
    const replacedOptions = tags
      ? globalReplaceTags(candidateOptions, tags)
      : candidateOptions;
    const dtoOptions: EmailOptionsDto = Object.assign(
      new EmailOptionsDto(),
      replacedOptions,
    );
    const validateOptions: EmailOptionsDto = await validateDto(dtoOptions);

    return validateOptions;
  }

  public async sendEmail(options: ISendMailOptions): Promise<IMessage> {
    await this.mailerService.sendMail(options);
    const successMsg: string = this.getDataFromDB(['messages', 'success-send']);

    return new Message(successMsg);
  }

  public async completeAndSendEmail(
    opts: ISendMailOptions,
    tags?: TReplaceTags,
  ): Promise<IMessage> {
    try {
      const options: ISendMailOptions = await this.completeEmailOptions(
        opts,
        tags,
      );

      return await this.sendEmail(options);
    } catch (e) {
      apiMessageException(e);
    }
  }
}
