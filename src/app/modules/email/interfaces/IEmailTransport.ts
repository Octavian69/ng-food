export interface IEmailTransport {
  readonly host: string,
  readonly port: number,
  readonly auth: {
    user: string;
    pass: string;
  };
}