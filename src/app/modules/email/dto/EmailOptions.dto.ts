import {
  isEmailField,
  isRequired,
  isStringLength,
} from '@core/validators/handlers/dto-validator.handlers';
import { ISendMailOptions } from '@nestjs-modules/mailer';
import { IsDefined, IsEmail, IsString, Length } from 'class-validator';
import { NSEmailDtoValidation } from '../namespaces/email-dto-validation.namespace';
export class EmailOptionsDto implements ISendMailOptions {
  @IsDefined(isRequired('Отправитель'))
  @IsString()
  public from: string;

  @IsDefined(isRequired('Адресат'))
  @IsEmail(...isEmailField('Email Адресата'))
  public to: string;

  @IsDefined()
  @IsString()
  @Length(...isStringLength('Заголовок', 'subject', NSEmailDtoValidation))
  public subject: string;

  @IsDefined()
  @IsString()
  public html: string;
}
