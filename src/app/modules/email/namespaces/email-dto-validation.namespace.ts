import { ISendMailOptions } from '@nestjs-modules/mailer';
import { TValidationRange } from '@core/validators/types/validatior.types';

type EmailRange = TValidationRange<ISendMailOptions, 'subject'>;
export namespace NSEmailDtoValidation {
  export const min: EmailRange = { subject: 5 };
  export const max: EmailRange = { subject: 70 };
}
