import { config } from "@config/handlers/config.handlers";
import { getDataFromDB } from "@core/handlers/structural.handlers";
import { TMode } from "@core/types/api.types";
import { EmailDB } from "../db/email.db";
import { IEmailTransport } from "../interfaces/IEmailTransport";

export function getTransport(mode: TMode = config.get('mode')): IEmailTransport {
  switch (mode) {
    case 'testing': {
      return getDataFromDB(EmailDB, ['configs', 'default', 'transport']);
    }
    default: {
      return getDataFromDB(EmailDB, ['configs', 'default', 'transport']);
    }
  }
}