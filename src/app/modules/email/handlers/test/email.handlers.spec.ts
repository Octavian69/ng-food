import { EmailDB } from '@module/email/db/email.db';
import { IEmailTransport } from '@module/email/interfaces/IEmailTransport';
import { getTransport } from '../email.handlers';

describe('Test "email" handlers', () => {
  // "getTransport" function
  describe('Test "getTransport" function', () => {
    test('should be defined', () => {
      expect(getTransport).toBeTruthy();
      expect(typeof getTransport).toBe('function');
    });

    it('should return transport by environment mode', () => {
      const transport: IEmailTransport = getTransport();
      const currentTransport: IEmailTransport =
        EmailDB.configs.default.transport;

      expect(transport).toBeTruthy();
      expect(transport).toEqual(currentTransport);
    });

    it('should return default config if mode type wrong one', () => {
      const {
        default: { transport: defaultTransport },
      } = EmailDB.configs;
      const result: IEmailTransport = getTransport('wrong config' as any);

      expect(result).toBeTruthy();
      expect(result).toEqual(defaultTransport);
    });
  });
});
