import { Module } from "@nestjs/common";
import { MailerModule } from "@nestjs-modules/mailer";
import { EmailService } from "./email.service";
import { getTransport } from "./handlers/email.handlers";

const modules = [
  MailerModule,
  EmailService
];
@Module({
  imports: [
    MailerModule.forRoot({
      transport: getTransport()
    }),
  ],
  providers: [
    EmailService
  ],
  exports: modules,
})
export class EmailModule { }