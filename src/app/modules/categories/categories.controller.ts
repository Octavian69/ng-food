import { FileOptions } from '@core/file/classes/FileOptions';
import { getDataFromDB } from '@core/handlers/structural.handlers';
import { JSONParsePipe } from '@core/pipes/json-parse.pipe';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { CurrentUser } from '@module/user/decorators/user.decorators';
import { FileInterceptor } from '@nestjs/platform-express';
import { ObjectId } from 'mongoose';
import { CategoriesService } from './categories.service';
import { CategoryService } from './category.service';
import { CategoriesDB } from './db/categories.db';
import { CategoryDto } from './dto/Category.dto';
import { ICategory } from './interfaces/ICategory';
import { CategoryDocument } from './schemas/Category.schema';
import { CategoriesFetchQueryDto } from './types/categories.types';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';

@Controller('categories')
export class CategoriesController {
  constructor(
    private readonly categoriesService: CategoriesService,
    private readonly categoryService: CategoryService,
  ) {}

  @Post('fetch')
  @HttpCode(HttpStatus.OK)
  public async fetch(
    @CurrentUser('_id') userId: ObjectId,
    @Body() body: CategoriesFetchQueryDto,
  ): Promise<TPaginationValue<ICategory>> {
    return this.categoriesService.fetch(userId, body);
  }

  @Get('get-by-id/:id')
  public async getById(@Param('id') categoryId: string): Promise<ICategory> {
    return this.categoryService.getById(categoryId);
  }

  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(
    FileInterceptor(
      'preview',
      new FileOptions(getDataFromDB(CategoriesDB, ['files', 'create:edit'])),
    ),
  )
  public async create(
    @CurrentUser('_id') userId: ObjectId,
    @UploadedFile() file: Express.Multer.File,
    @Body('category', new JSONParsePipe(CategoryDto))
    candidate: ICategory,
  ): Promise<CategoryDocument> {
    return this.categoryService.create(userId, candidate, file);
  }

  @Patch('edit/:id')
  @UseInterceptors(
    FileInterceptor(
      'preview',
      new FileOptions(getDataFromDB(CategoriesDB, ['files', 'create:edit'])),
    ),
  )
  public async edit(
    @UploadedFile() file: Express.Multer.File,
    @Param('id') categoryId: string,
    @Body('category', new JSONParsePipe(CategoryDto)) candidate: ICategory,
  ): Promise<CategoryDocument> {
    return this.categoryService.edit(categoryId, candidate, file);
  }

  @Delete('remove/:id')
  public async remove(
    @Param('id') categoryId: string,
  ): Promise<CategoryDocument> {
    return this.categoryService.remove(categoryId);
  }
}
