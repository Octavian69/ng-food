import { User } from '@module/user/schemas/User.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as MongooseSchema } from 'mongoose';
import { ICategory } from '../interfaces/ICategory';

@Schema({ versionKey: false })
export class Category implements ICategory {
  @Prop({ required: true })
  public Title: string;

  @Prop({ default: null })
  public Description: string;

  @Prop({ default: null })
  public Preview: string;

  @Prop({ default: Date.now })
  public Created: Date;

  @Prop({ default: 0 })
  public TotalPositions: number;

  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: User.name })
  public User: ObjectId;
}

export type CategoryDocument = Category & Document;
export const CategorySchema: MongooseSchema<CategoryDocument> = SchemaFactory.createForClass(
  Category,
);
