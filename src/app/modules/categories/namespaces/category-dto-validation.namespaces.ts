import { TValidationRange } from '@core/validators/types/validatior.types';
import { ICategory } from '../interfaces/ICategory';

type MINRange = TValidationRange<ICategory, 'Title'>;
type MAXRange = TValidationRange<ICategory, 'Title' | 'Description'>;

export namespace NSCategroyValidation {
  export const min: MINRange = {
    Title: 3,
  };
  export const max: MAXRange = {
    Title: 50,
    Description: 300,
  };
}
