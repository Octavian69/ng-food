import { TDataAction } from '@core/types/data.types';
import { MongoId } from '@module/mongo/types/mongo.types';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { ICategory } from './interfaces/ICategory';
import { Category, CategoryDocument } from './schemas/Category.schema';
import {
  apiMessageException,
  baseErrorHandler,
  getErrorMessage,
} from '@core/errors/handlers/error.handlers';
import {
  removeFile,
  updateFileStorage,
} from '@core/file/handlers/file.handlers';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel(Category.name)
    private readonly CategoryModel: Model<CategoryDocument>,
  ) {}

  public async getById(categoryId: string): Promise<ICategory> {
    try {
      const category: ICategory = await this.CategoryModel.findById(
        categoryId,
        null,
        { lean: true },
      );

      if (!category) baseErrorHandler('Такой категории не существует.');

      return category;
    } catch (e) {
      apiMessageException(getErrorMessage(e), HttpStatus.BAD_REQUEST);
    }
  }

  public async create(
    userId: ObjectId,
    candidate: ICategory,
    file: Express.Multer.File,
  ): Promise<CategoryDocument> {
    try {
      const Preview: string = await updateFileStorage(file);
      const category: CategoryDocument = await new this.CategoryModel(
        Object.assign(candidate, { User: userId, Preview }),
      ).save();

      return category;
    } catch (e) {
      apiMessageException(getErrorMessage(e));
    }
  }

  public async edit(
    categoryId: string,
    candidate: ICategory,
    file: Express.Multer.File,
  ): Promise<CategoryDocument> {
    try {
      candidate.Preview = await updateFileStorage(file, candidate.Preview);

      const update: CategoryDocument = await this.CategoryModel.findByIdAndUpdate(
        categoryId,
        { $set: candidate },
        { new: true },
      );

      return update;
    } catch (e) {
      apiMessageException(getErrorMessage(e));
    }
  }

  public async updateTotalPositions(
    categoryId: MongoId,
    action: Exclude<TDataAction, 'edit'>,
  ): Promise<CategoryDocument> {
    const value: number = action === 'add' ? 1 : -1;
    const updatedCategory = await this.CategoryModel.findByIdAndUpdate(
      categoryId,
      {
        $inc: { TotalPositions: value },
      },
      { new: true },
    );

    return updatedCategory;
  }

  public async remove(categoryId: string): Promise<CategoryDocument> {
    try {
      const category: CategoryDocument = await this.CategoryModel.findByIdAndRemove(
        categoryId,
      );

      if (category.Preview) await removeFile(category.Preview);

      return category;
    } catch (e) {
      apiMessageException(getErrorMessage(e), HttpStatus.BAD_REQUEST);
    }
  }
}
