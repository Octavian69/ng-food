import { ISimple } from '@core/interfaces/shared/ISimple';
import { ServiceManager } from '@core/managers/Service.manager';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { MongoHelperService } from '@module/mongo/services/mongo-helper.service';
import { TMongoMatchParser, TMongoSort } from '@module/mongo/types/mongo.types';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { CategoriesDB, TCategoriesDB } from './db/categories.db';
import { ICategory } from './interfaces/ICategory';
import { Category, CategoryDocument } from './schemas/Category.schema';
import {
  CategoriesFetchQueryDto,
  TCategoriesSortFields,
} from './types/categories.types';

@Injectable()
export class CategoriesService extends ServiceManager<TCategoriesDB> {
  constructor(
    @InjectModel(Category.name)
    private readonly CategoryModel: Model<CategoryDocument>,
    private readonly mongoHelper: MongoHelperService,
  ) {
    super(CategoriesDB);
  }

  async fetch(
    userId: ObjectId,
    body: CategoriesFetchQueryDto,
  ): Promise<TPaginationValue<ICategory>> {
    const { paginate, filters, sort } = body;
    const matchParser: TMongoMatchParser = this.getDataFromDB(
      ['mongo', 'fetch'],
      false,
    );
    const matchQuery: ISimple<unknown> = Object.assign(
      { User: userId },
      filters,
    );
    const $match: object = this.mongoHelper.match(matchParser, matchQuery);
    const $sort: TMongoSort<TCategoriesSortFields> = this.mongoHelper.sort(
      sort,
    );

    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      $match,
      $sort,
    );

    const result: TPaginationValue<ICategory> = await this.mongoHelper.pagination(
      this.CategoryModel,
      aggregateQuery,
      paginate,
    );

    return result;
  }
}
