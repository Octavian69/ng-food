import { ObjectId } from 'mongoose';

export interface ICategory {
  Title: string;
  Description: string;
  Preview: string;
  Created: Date;
  TotalPositions: number;
  User: ObjectId;
  _id?: ObjectId;
}
