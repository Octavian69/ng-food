import { FetchQueryDto } from '@core/utils/fetch/dto/FetchQuery.dto';
import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { ICategory } from '../interfaces/ICategory';

export type CategoriesFetchFilter = {
  Title?: string;
  CreatedRange?: IRangeValue<Date>;
  TotalPositionsRange?: IRangeValue<number>;
};
export type TCategoriesSortFields = 'Created' | 'TotalPositions';
export type CategoriesFetchQueryDto = FetchQueryDto<
  ICategory,
  CategoriesFetchFilter,
  TCategoriesSortFields
>;
