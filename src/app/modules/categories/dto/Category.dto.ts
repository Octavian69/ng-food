import { IsStringDate } from '@core/validators/constraints/StringDate.constraint';
import { ObjectId } from 'mongoose';
import { ICategory } from '../interfaces/ICategory';
import { NSCategroyValidation } from '../namespaces/category-dto-validation.namespaces';
import {
  isInvalid,
  isMaxLength,
  isNumber,
  isRequired,
  isStringLength,
  isType,
} from '@core/validators/handlers/dto-validator.handlers';
import {
  IsDefined,
  IsMongoId,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  MaxLength,
  Validate,
} from 'class-validator';

export class CategoryDto implements ICategory {
  @IsString(isType('Title', 'string'))
  @IsDefined(isRequired('Title'))
  @Length(
    ...isStringLength('Название категории', 'Title', NSCategroyValidation),
  )
  public Title: string;

  @IsOptional()
  @IsString(isType('Description', 'string'))
  @MaxLength(
    ...isMaxLength(
      'Название категории',
      'Description',
      NSCategroyValidation.max,
    ),
  )
  public Description: string;

  @IsNumber(...isNumber('TotalPositions'))
  @IsDefined(isRequired('TotalPositions'))
  public TotalPositions: number;

  @IsOptional()
  @IsString(isType('Preview', 'string'))
  public Preview: string;

  @Validate(IsStringDate, ['Created'])
  public Created: Date;

  @IsOptional()
  @IsMongoId(isInvalid())
  public User: ObjectId;

  @IsOptional()
  @IsMongoId(isInvalid())
  public _id: ObjectId;
}
