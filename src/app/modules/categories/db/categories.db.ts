import { toDate } from '@core/handlers/transform.handlers';
import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { MongoRangeQuery } from '@module/mongo/classes/MongoRangeQuery';
import { toObjectId } from '@module/mongo/handlers/mongo.handlers';

export const CategoriesDB = {
  mongo: {
    fetch: {
      User: (value: string) => ({ $eq: toObjectId(value) }),
      Title: (value: string) => ({ $regex: new RegExp(value, 'gi') }),
      Created: (value: IRangeValue<Date>) => new MongoRangeQuery(value, toDate),
      TotalPositions: (value: IRangeValue<number>) =>
        new MongoRangeQuery(value),
    },
  },
  files: {
    'create:edit': {
      fileType: 'IMG',
      filePath: ['img', 'categories'],
      fileSize: 5,
    },
  },
} as const;

export type TCategoriesDB = typeof CategoriesDB;
