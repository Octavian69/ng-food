import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { CategoryService } from './category.service';
import { Category, CategorySchema } from './schemas/Category.schema';
import { MongoModule } from '../mongo/mongo.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Category.name, schema: CategorySchema },
    ]),
    MongoModule,
  ],
  controllers: [CategoriesController],
  providers: [CategoriesService, CategoryService],
  exports: [CategoryService],
})
export class CategoriesModule {}
