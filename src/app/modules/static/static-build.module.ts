import { DynamicModule, Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { resolve } from 'path';

@Module({})
export class StaticBuildModule {
  static forRoot(): DynamicModule {
    const exports = [];
    const clientBuildRoot: string = resolve(
      __dirname,
      '../../../../..',
      'client',
      'dist',
      'client',
    );
    const isProductionMode: boolean = process.env.NODE_ENV === 'production';

    if (isProductionMode) {
      exports.push(ServeStaticModule);
    }

    return {
      module: StaticBuildModule,
      imports: [
        ServeStaticModule.forRoot({
          rootPath: clientBuildRoot,
        }),
      ],
      exports,
    };
  }
}
