import { MongoId } from '@module/mongo/types/mongo.types';
import { CurrentUser } from '@module/user/decorators/user.decorators';
import {
  Body,
  Controller,
  Get,
  Post
  } from '@nestjs/common';
import { AnalyticsService } from './analytics.service';
import { AnalyticsOrdersDaysProfitDto } from './dto/AnalyticsOrdersDaysProfit.dto';
import { AnalyticsOrdersPeriodProfitDto } from './dto/AnalyticsOrdersPeriodProfit.dto';
import { AnalyticsPositionsDto } from './dto/AnanlyticsPositions.dto';
import { IAnalyticsDaysOrdersProfit } from './interfaces/IAnalyticsDaysOrdersProfit';
import { IAnalyticsFilterPositionItem } from './interfaces/IAnalyticsFilterPostionItem';
import { IAnalyticsOrders } from './interfaces/IAnalyticsOrders';
import { IAnalyticsPeriodOrdersProfit } from './interfaces/IAnalyticsPeriodOrdersProfit';
import { IAnalyticsPositionItem } from './interfaces/IAnalyticsPositionItem';

@Controller('analytics')
export class AnalyticsController {
  constructor(private analyticsService: AnalyticsService) {}

  // *Orders

  @Get('/fetch-orders-profit')
  public async fetchOrdersAnalytics(
    @CurrentUser('_id') userId: MongoId,
  ): Promise<IAnalyticsOrders> {
    return this.analyticsService.fetchOrdersPeriodProfit(userId);
  }

  @Post('/filter-orders-period-profit')
  public async filterOrdersAnanlytics(
    @CurrentUser('_id') userId: MongoId,
    @Body() body: AnalyticsOrdersPeriodProfitDto,
  ): Promise<IAnalyticsPeriodOrdersProfit> {
    return this.analyticsService.filterOrdersPeriodProfit(body, userId);
  }

  @Post('/filter-orders-days-profit')
  public async filterOrdersDaysProfit(
    @CurrentUser('_id') userId: MongoId,
    @Body() body: AnalyticsOrdersDaysProfitDto,
  ): Promise<IAnalyticsDaysOrdersProfit[]> {
    return this.analyticsService.filterOrdersDaysProfit(body, userId);
  }

  // *Positions

  @Get('/fetch-positions')
  public async fetchPositions(
    @CurrentUser('_id') userId: MongoId,
  ): Promise<IAnalyticsPositionItem[]> {
    return this.analyticsService.fetchPositionsPeriod(userId);
  }

  @Post('/filter-positions')
  public async filterPositions(
    @CurrentUser('_id') userId: MongoId,
    @Body() body: AnalyticsPositionsDto,
  ): Promise<IAnalyticsFilterPositionItem[]> {
    return this.analyticsService.filterPositions(body, userId);
  }
}
