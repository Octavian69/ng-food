import { MongoModule } from '@module/mongo/mongo.module';
import { Order, OrderSchema } from '@module/orders/schemas/Order.schema';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AnalyticsController } from './analytics.controller';
import { AnalyticsService } from './analytics.service';
import {
  Category,
  CategorySchema,
} from '@module/categories/schemas/Category.schema';
import {
  Position,
  PositionSchema,
} from '@module/positions/schemas/Position.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Category.name, schema: CategorySchema },
      { name: Order.name, schema: OrderSchema },
      { name: Position.name, schema: PositionSchema },
    ]),
    MongoModule,
  ],
  controllers: [AnalyticsController],
  providers: [AnalyticsService],
})
export class AnalyticsModule {}
