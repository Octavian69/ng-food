import { equal } from '@core/handlers/condition.handlers';
import { toDate } from '@core/handlers/transform.handlers';
import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { MongoRangeQuery } from '@module/mongo/classes/MongoRangeQuery';
import { toObjectId } from '@module/mongo/handlers/mongo.handlers';
import {
  PeriodsFormats,
  TAnalyticsOrderPeriod,
  TAnalyticsOrderPeriodItem,
} from '../types/analytics.types';

export const AnalyticsDB = {
  mongo: {
    positions: {
      parser: {
        range: (v: IRangeValue<Date>) => ({
          Created: new MongoRangeQuery(v, toDate),
        }),
        User: (v) => ({ User: { $eq: toObjectId(v) } }),
      },
      query: [
        {
          $unwind: '$Positions',
        },
        {
          $group: {
            _id: { $toObjectId: '$Positions.PositionId' },
            TotalAmount: { $sum: '$Positions.TotalCost' },
            Quantity: { $sum: '$Positions.Quantity' },
            PositionTitle: { $push: '$Positions.PositionTitle' },
          },
        },
        {
          $project: {
            _id: 0,
            TotalAmount: 1,
            Quantity: 1,
            Title: { $arrayElemAt: ['$PositionTitle', 0] },
          },
        },
        {
          $sort: { TotalAmount: -1 },
        },
      ],
    },
    orders: {
      days: {
        parser: {
          range: (v: IRangeValue<Date>) => ({
            Created: new MongoRangeQuery(v, toDate),
          }),
          User: (v) => ({ User: { $eq: toObjectId(v) } }),
        },
        query: [
          {
            $group: {
              _id: { $dateToString: { format: '%G-%m-%d', date: '$Created' } },
              TotalAmount: { $sum: '$TotalAmount' },
            },
          },
          {
            $project: {
              _id: 0,
              Date: { $toDate: '$_id' },
              TotalAmount: 1,
            },
          },
          {
            $sort: {
              Date: 1,
            },
          },
        ],
      },
      period: {
        parser: {
          range: (v: IRangeValue<Date>) => ({
            Created: new MongoRangeQuery(v, toDate),
          }),
          User: (v) => ({ User: { $eq: toObjectId(v) } }),
        },
        query: [
          {
            $group: {
              _id: null,
              TotalAmount: { $sum: '$TotalAmount' },
            },
          },
          {
            $project: {
              _id: 0,
            },
          },
        ],
      },
      'total-parser': function () {
        const DATE_FIELD = '$Created' as const;
        const AMOUNT_FIELD = '$TotalAmount' as const;
        const NOW = '$$NOW' as const;
        const amount: number = 1;
        const Periods: Readonly<TAnalyticsOrderPeriod[]> = [
          'Day',
          'Month',
          'Year',
        ] as const;
        const PeriodsItems: Readonly<TAnalyticsOrderPeriodItem[]> = [
          'Previous',
          'Current',
        ] as const;
        const PeriodsFormats: Readonly<PeriodsFormats> = {
          Day: '%G.%m.%d',
          Month: '%G.%m',
          Year: '%G',
        } as const;
        const subtract = (unit: Lowercase<TAnalyticsOrderPeriod>) => ({
          $dateSubtract: {
            startDate: NOW,
            unit,
            amount,
          },
        });
        const stage = (
          period: TAnalyticsOrderPeriod,
          item: TAnalyticsOrderPeriodItem,
        ) => ({
          $sum: {
            $cond: [
              {
                $eq: [
                  {
                    $dateToString: {
                      date: equal(item, 'Current')
                        ? NOW
                        : subtract(
                            period.toLocaleLowerCase() as Lowercase<TAnalyticsOrderPeriod>,
                          ),
                      format: PeriodsFormats[period],
                    },
                  },
                  {
                    $dateToString: {
                      date: DATE_FIELD,
                      format: PeriodsFormats[period],
                    },
                  },
                ],
              },
              AMOUNT_FIELD,
              0,
            ],
          },
        });
        const stages = Periods.reduce(
          (accum: object, period: TAnalyticsOrderPeriod) => {
            PeriodsItems.forEach((item: TAnalyticsOrderPeriodItem) => {
              const periodName: string = `${item}${period}`;
              const periodStage = stage(period, item);

              accum[periodName] = periodStage;
            });
            return accum;
          },
          {},
        );

        const requestQuery: object[] = [
          {
            $group: {
              _id: null,
              TotalAmount: { $sum: '$TotalAmount' },
              ...stages,
            },
          },
          {
            $project: {
              _id: 0,
            },
          },
        ];

        return requestQuery;
      },
    },
  },
} as const;

export type TAnalyticsDB = typeof AnalyticsDB;
