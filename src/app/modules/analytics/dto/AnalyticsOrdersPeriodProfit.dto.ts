import { RangeValueDateDto } from '@core/utils/ranges/dto/RangeValueDate.dto';
import { isType } from '@core/validators/handlers/dto-validator.handlers';
import { Equals, IsDefined, ValidateNested } from 'class-validator';
import { TAnalyticsOrders } from '../types/analytics.types';

export class AnalyticsOrdersPeriodProfitDto {
  @ValidateNested()
  public range: RangeValueDateDto;

  @IsDefined()
  @Equals('TotalAmount', isType('type', 'TotalAmount'))
  public type: TAnalyticsOrders;
}
