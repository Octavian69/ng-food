import { RangeValueDateDto } from '@core/utils/ranges/dto/RangeValueDate.dto';
import { isContain } from '@core/validators/handlers/dto-validator.handlers';
import { IsDefined, IsIn, ValidateNested } from 'class-validator';
import { TAnalyticsPositions } from '../types/analytics.types';

export class AnalyticsPositionsDto {
  @ValidateNested()
  public range: RangeValueDateDto;

  @IsDefined()
  @IsIn(
    ...isContain('type', ['TotalAmount', 'Quantity'] as TAnalyticsPositions[]),
  )
  public type: TAnalyticsPositions;
}
