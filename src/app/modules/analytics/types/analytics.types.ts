export type TAnalyticsOrderPeriod = 'Year' | 'Month' | 'Day';
export type TAnalyticsOrderPeriodItem = 'Previous' | 'Current';
export type PeriodsFormats = Record<TAnalyticsOrderPeriod, string>;
export type TAnalyticsOrders = 'TotalAmount';
export type TAnalyticsPositions = 'TotalCost' | 'Quantity';
