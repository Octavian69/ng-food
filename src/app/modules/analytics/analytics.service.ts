import { ServiceManager } from '@core/managers/Service.manager';
import { MongoHelperService } from '@module/mongo/services/mongo-helper.service';
import { MongoId, TMongoMatchParser } from '@module/mongo/types/mongo.types';
import { Order, OrderDocument } from '@module/orders/schemas/Order.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AnalyticsDB, TAnalyticsDB } from './db/analytics.db';
import { AnalyticsOrdersDaysProfitDto } from './dto/AnalyticsOrdersDaysProfit.dto';
import { AnalyticsOrdersPeriodProfitDto } from './dto/AnalyticsOrdersPeriodProfit.dto';
import { AnalyticsPositionsDto } from './dto/AnanlyticsPositions.dto';
import { EnAnalytics } from './enums/analytics.enum';
import { IAnalyticsDaysOrdersProfit } from './interfaces/IAnalyticsDaysOrdersProfit';
import { IAnalyticsFilterPositionItem } from './interfaces/IAnalyticsFilterPostionItem';
import { IAnalyticsOrders } from './interfaces/IAnalyticsOrders';
import { IAnalyticsPeriodOrdersProfit } from './interfaces/IAnalyticsPeriodOrdersProfit';
import { IAnalyticsPositionItem } from './interfaces/IAnalyticsPositionItem';

@Injectable()
export class AnalyticsService extends ServiceManager<TAnalyticsDB> {
  constructor(
    @InjectModel(Order.name) private readonly OrderModel: Model<OrderDocument>,
    private mongoHelper: MongoHelperService,
  ) {
    super(AnalyticsDB);
  }

  // *Positions

  private async fetchPositions(
    $match: object,
  ): Promise<IAnalyticsPositionItem[]> {
    const query: object[] = this.getDataFromDB(['mongo', 'positions', 'query']);
    const $limit: object = { $limit: EnAnalytics.DEFAULT_POSITIONS_TOP_LIMIT };

    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      $match,
      ...query,
      $limit,
    );
    const result = await this.mongoHelper.aggregate(
      this.OrderModel,
      aggregateQuery,
    );

    return result as IAnalyticsPositionItem[];
  }

  public async fetchPositionsPeriod(
    userId: MongoId,
  ): Promise<IAnalyticsPositionItem[]> {
    const $match: object = this.mongoHelper.stage('$match', { User: userId });

    return await this.fetchPositions($match);
  }

  public async filterPositions(
    { type, range }: AnalyticsPositionsDto,
    userId: MongoId,
  ): Promise<IAnalyticsFilterPositionItem[]> {
    const matchParser: TMongoMatchParser = this.getDataFromDB(
      ['mongo', 'positions', 'parser'],
      false,
    );
    const query: object = { User: userId, range };
    const $match: object = this.mongoHelper.complexMatch(matchParser, query);
    const result: IAnalyticsPositionItem[] = await this.fetchPositions($match);
    const response: IAnalyticsFilterPositionItem[] = result.map((position) => {
      const { Title } = position;
      const value: number = position[type];

      return { [type]: value, Title };
    });

    return response;
  }

  // *Orders

  private async fetchOrdersAnalytics<T>(aggregateQuery: object[]): Promise<T> {
    const [result] = await this.mongoHelper.aggregate(
      this.OrderModel,
      aggregateQuery,
    );

    return result as T;
  }

  public async fetchOrdersPeriodProfit(
    userId: MongoId,
  ): Promise<IAnalyticsOrders> {
    const queryParser: Function = this.getDataFromDB(
      ['mongo', 'orders', 'total-parser'],
      false,
    );
    const $match: object = this.mongoHelper.stage('$match', { User: userId });
    const query: object[] = queryParser();
    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      $match,
      ...query,
    );

    return await this.fetchOrdersAnalytics<IAnalyticsOrders>(aggregateQuery);
  }

  public async filterOrdersPeriodProfit(
    body: AnalyticsOrdersPeriodProfitDto,
    userId: MongoId,
  ): Promise<IAnalyticsPeriodOrdersProfit> {
    const queryParser: TMongoMatchParser = this.getDataFromDB(
      ['mongo', 'orders', 'period', 'parser'],
      false,
    );
    const query: object[] = this.getDataFromDB([
      'mongo',
      'orders',
      'period',
      'query',
    ]);
    const matchQuery: object = { User: userId, range: body.range };
    const match$: object = this.mongoHelper.complexMatch(
      queryParser,
      matchQuery,
    );
    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      match$,
      ...query,
    );

    return await this.fetchOrdersAnalytics(aggregateQuery);
  }

  public async filterOrdersDaysProfit(
    body: AnalyticsOrdersDaysProfitDto,
    userId: MongoId,
  ): Promise<IAnalyticsDaysOrdersProfit[]> {
    const queryParser: TMongoMatchParser = this.getDataFromDB(
      ['mongo', 'orders', 'days', 'parser'],
      false,
    );
    const matchQuery: object = { User: userId, range: body.range };
    const match$: object = this.mongoHelper.complexMatch(
      queryParser,
      matchQuery,
    );
    const query: object[] = this.getDataFromDB([
      'mongo',
      'orders',
      'days',
      'query',
    ]);
    const aggregationQuery: object[] = this.mongoHelper.completeQuery(
      match$,
      ...query,
    );

    const result = await this.mongoHelper.aggregate(
      this.OrderModel,
      aggregationQuery,
    );

    return result as IAnalyticsDaysOrdersProfit[];
  }
}
