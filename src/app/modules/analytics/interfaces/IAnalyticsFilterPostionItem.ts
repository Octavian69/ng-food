import { IAnalyticsPositionItem } from './IAnalyticsPositionItem';

export interface IAnalyticsFilterPositionItem
  extends Pick<IAnalyticsPositionItem, 'Title'>,
    Pick<Partial<IAnalyticsPositionItem>, 'Quantity' | 'TotalAmount'> {}
