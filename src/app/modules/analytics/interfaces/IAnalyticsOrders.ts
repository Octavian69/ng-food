export interface IAnalyticsOrders {
  TotalAmount: number;
  CurrentDay: number;
  PreviousDay: number;
  CurrentMonth: number;
  PreviousMonth: number;
  CurrentYear: number;
  PreviousYear: number;
}
