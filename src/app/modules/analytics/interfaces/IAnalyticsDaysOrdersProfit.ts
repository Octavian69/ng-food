export interface IAnalyticsDaysOrdersProfit {
  TotalAmount: number;
  Date: Date;
}
