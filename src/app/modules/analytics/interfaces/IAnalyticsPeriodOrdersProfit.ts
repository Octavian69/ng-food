import { IAnalyticsOrders } from './IAnalyticsOrders';

export interface IAnalyticsPeriodOrdersProfit
  extends Pick<IAnalyticsOrders, 'TotalAmount'> {}
