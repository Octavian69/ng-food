import { FetchQueryDto } from '@core/utils/fetch/dto/FetchQuery.dto';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { MongoId } from '@module/mongo/types/mongo.types';
import { CurrentUser } from '@module/user/decorators/user.decorators';
import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post
  } from '@nestjs/common';
import { HistoryService } from './history.service';
import { IHistoryOrder } from './interfaces/IHistoryOrder';

@Controller('history')
export class HistoryController {
  constructor(private historyService: HistoryService) {}

  @Post('fetch')
  @HttpCode(HttpStatus.OK)
  public async fetch(
    @Body() query: FetchQueryDto,
    @CurrentUser('_id') userId: MongoId,
  ): Promise<TPaginationValue<IHistoryOrder>> {
    return this.historyService.fetch(userId, query);
  }
}
