import { DatabaseManager } from '@core/managers/Database.manager';
import { FetchQueryDto } from '@core/utils/fetch/dto/FetchQuery.dto';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { MongoHelperService } from '@module/mongo/services/mongo-helper.service';
import { Order, OrderDocument } from '@module/orders/schemas/Order.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HistoryDB, THistoryDB } from './db/history.db';
import { IHistoryOrder } from './interfaces/IHistoryOrder';
import {
  MongoId,
  TMongoMatchParser,
  TMongoSortParser,
} from '@module/mongo/types/mongo.types';

@Injectable()
export class HistoryService extends DatabaseManager<THistoryDB> {
  constructor(
    private mongoHelper: MongoHelperService,
    @InjectModel(Order.name) private readonly OrderModel: Model<OrderDocument>,
  ) {
    super(HistoryDB);
  }

  public async fetch(
    userId: MongoId,
    query: FetchQueryDto,
  ): Promise<TPaginationValue<IHistoryOrder>> {
    const { paginate, filters, sort } = query;
    const matchParser: TMongoMatchParser = this.get(
      ['mongo', 'fetch', 'match-parser'],
      false,
    );
    const $lookup: object[] = this.get(['mongo', 'fetch', 'lookup'], false);
    const matchQuery: object = Object.assign({ User: userId }, filters);
    const sortParser: TMongoSortParser = this.get(
      ['mongo', 'fetch', 'sort-parser'],
      false,
    );
    const $sort = this.mongoHelper.complexSort(sortParser, sort);
    const $match = this.mongoHelper.complexMatch(matchParser, matchQuery);
    const $project: object = this.get(['mongo', 'fetch', 'project']);
    const aggregateQuery = this.mongoHelper.completeQuery(
      ...$lookup,
      $match,
      $sort,
      $project,
    );
    const result: TPaginationValue<IHistoryOrder> = await this.mongoHelper.pagination(
      this.OrderModel,
      aggregateQuery,
      paginate,
    );

    return result;
  }
}
