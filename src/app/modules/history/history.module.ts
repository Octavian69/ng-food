import { MongoModule } from '@module/mongo/mongo.module';
import { Order, OrderSchema } from '@module/orders/schemas/Order.schema';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HistoryController } from './history.cotroller';
import { HistoryService } from './history.service';

@Module({
  imports: [
    MongoModule,
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
  ],
  controllers: [HistoryController],
  providers: [HistoryService],
})
export class HistoryModule {}
