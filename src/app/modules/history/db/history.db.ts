import { toDate } from '@core/handlers/transform.handlers';
import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { MongoRangeQuery } from '@module/mongo/classes/MongoRangeQuery';
import { toObjectId } from '@module/mongo/handlers/mongo.handlers';
import { EnHistoryPositionStatus } from '../enums/history.enum';

export const HistoryDB = {
  mongo: {
    fetch: {
      lookup: [
        {
          $lookup: {
            from: 'positions',
            as: 'PositionsDetails',
            let: {
              userId: '$User',
              positions: '$Positions',
              positionsIds: {
                $reduce: {
                  input: '$Positions',
                  initialValue: [],
                  in: {
                    $concatArrays: [
                      '$$value',
                      [{ $toObjectId: '$$this.PositionId' }],
                    ],
                  },
                },
              },
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$$userId', '$User'] },
                      {
                        $in: ['$_id', '$$positionsIds'],
                      },
                    ],
                  },
                },
              },
            ],
          },
        },
        {
          $addFields: {
            PositionsQuantity: { $size: '$Positions' },
            ActivePositionsIds: {
              $map: {
                input: '$PositionsDetails',
                as: 'details',
                in: { $toString: '$$details._id' },
              },
            },
          },
        },
      ],
      project: {
        $project: {
          TotalAmount: 1,
          Created: 1,
          User: 1,
          Positions: {
            $map: {
              input: {
                $map: {
                  input: '$Positions',
                  as: 'position',
                  in: {
                    $mergeObjects: [
                      '$$position',
                      {
                        Status: {
                          $switch: {
                            branches: [
                              {
                                case: {
                                  $in: [
                                    '$$position.PositionId',
                                    '$ActivePositionsIds',
                                  ],
                                },
                                then: EnHistoryPositionStatus.ACTIVE,
                              },
                            ],
                            default: EnHistoryPositionStatus.REMOVE,
                          },
                        },
                      },
                      {
                        NewState: {
                          $cond: {
                            if: {
                              $in: [
                                '$$position.PositionId',
                                '$ActivePositionsIds',
                              ],
                            },
                            then: {
                              $arrayElemAt: [
                                {
                                  $filter: {
                                    input: '$PositionsDetails',
                                    as: 'current',
                                    cond: {
                                      $eq: [
                                        { $toString: '$$current._id' },
                                        '$$position.PositionId',
                                      ],
                                    },
                                  },
                                },
                                0,
                              ],
                            },
                            else: null,
                          },
                        },
                      },
                    ],
                  },
                },
              },
              as: 'pos',
              in: {
                $mergeObjects: [
                  '$$pos',
                  {
                    NewState: {
                      $switch: {
                        branches: [
                          {
                            case: {
                              $and: [
                                {
                                  $eq: [
                                    '$$pos.Status',
                                    EnHistoryPositionStatus.ACTIVE,
                                  ],
                                },
                                {
                                  $or: [
                                    {
                                      $ne: [
                                        '$$pos.PositionTitle',
                                        '$$pos.NewState.Title',
                                      ],
                                    },
                                    {
                                      $ne: [
                                        '$$pos.PositionCost',
                                        '$$pos.NewState.Cost',
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            then: {
                              PositionTitle: '$$pos.NewState.Title',
                              PositionCost: '$$pos.NewState.Cost',
                              PositionUpdated: '$$pos.NewState.Updated',
                            },
                          },
                        ],
                        default: null,
                      },
                    },
                  },
                ],
              },
            },
          },
        },
      },
      'match-parser': {
        User: (value: string) => ({ User: { $eq: toObjectId(value) } }),
        TotalAmount: (value: IRangeValue<number>) => ({
          TotalAmount: new MongoRangeQuery(value),
        }),
        Created: (value: IRangeValue<Date>) => ({
          Created: new MongoRangeQuery(value, toDate),
        }),
        Positions: (value: IRangeValue<number>) => ({
          PositionsQuantity: new MongoRangeQuery(value),
        }),
      },
      'sort-parser': {
        Positions: (value: number) => ({ PositionsQuantity: value }),
      },
    },
  },
} as const;

export type THistoryDB = typeof HistoryDB;
