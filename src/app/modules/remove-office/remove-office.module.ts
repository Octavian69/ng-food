import { Module } from "@nestjs/common";
import { RemoveOfficeService } from './remove-office.service';
import { RemoveOfficeController } from './remove-office.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthSocialModule } from '../auth-social/auth-social.module';
import { UserModule } from "../user/user.module";

@Module({
  imports: [
    AuthModule,
    AuthSocialModule,
    UserModule
  ],
  controllers: [
    RemoveOfficeController
  ],
  providers: [
    RemoveOfficeService
  ]
})
export class RemoveOfficeModule { }