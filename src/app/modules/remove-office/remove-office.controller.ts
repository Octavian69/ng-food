import { Controller, Delete } from '@nestjs/common';
import { CurrentUser } from '@module/user/decorators/user.decorators';
import { IMessage } from '@core/utils/messages/interfaces/IMessage';
import { RemoveOfficeService } from './remove-office.service';
import { MongoId } from '../mongo/types/mongo.types';

@Controller('remove-office')
export class RemoveOfficeController {
  constructor(private readonly removeOfficeService: RemoveOfficeService) {}

  @Delete('remove')
  public async remove(@CurrentUser('_id') userId: MongoId): Promise<IMessage> {
    return this.removeOfficeService.remove(userId);
  }
}
