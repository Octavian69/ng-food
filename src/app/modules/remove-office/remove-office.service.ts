import { Injectable } from '@nestjs/common';
import * as rimfraf from 'rimraf';
import { join } from 'path';
import { UserService } from '@module/user/user.service';
import { AuthService } from '@module/auth/auth.service';
import { AuthSocialService } from '@module/auth-social/auth-social.service';
import { IMessage } from '@core/utils/messages/interfaces/IMessage';
import { Message } from '@core/utils/messages/Message';
import { apiMessageException } from '@core/errors/handlers/error.handlers';
import { nodePromisify } from '@core/handlers/api.handlers';
import { EnFile } from '@core/file/enums/file.enum';
import { MongoId } from '../mongo/types/mongo.types';

@Injectable()
export class RemoveOfficeService {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly authSocialService: AuthSocialService,
  ) {}

  public async remove(userId: MongoId): Promise<IMessage> {
    try {
      const userFolder = join(EnFile.FILES_ROOT, String(userId));
      const message: string = 'Аккаунт успешно удален.';

      await this.userService.remove(userId);
      await this.authService.remove(userId);
      await this.authSocialService.remove(userId);
      await nodePromisify(rimfraf, userFolder);

      return new Message(message);
    } catch (e) {
      apiMessageException(e);
    }
  }
}
