import {
  Controller,
  Get,
  Delete,
  Param,
  Query,
  Redirect,
  Patch,
  UseInterceptors,
  UploadedFile,
  Body,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { url } from '@core/handlers/api.handlers';
import { PublicRoute } from '@module/auth/metadata/auth.metadata';
import { IUserInfo } from './interfaces/IUserInfo';
import { UserService } from './user.service';
import { FileOptions } from '@core/file/classes/FileOptions';
import { EditUserDto } from './dto/EditUser.dto';
import { CurrentUser } from './decorators/user.decorators';
import { JSONParsePipe } from '@core/pipes/json-parse.pipe';
import { getDataFromDB } from '@core/handlers/structural.handlers';
import { UserDB } from './db/user.db';
import { IUser } from './interfaces/IUser';
import { MongoId } from '@module/mongo/types/mongo.types';
import { toObjectId } from '@module/mongo/handlers/mongo.handlers';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('get-by-login')
  @PublicRoute()
  public async getByLogin(@Query('login') Login: string): Promise<IUserInfo> {
    return this.userService.getUserWithoutCredentials({ Login });
  }

  @Get('get-by-id/:id')
  public async getById(@Param('id') _id: string): Promise<IUserInfo> {
    return this.userService.getUserWithoutCredentials({ _id: toObjectId(_id) });
  }

  @Delete('remove')
  @Redirect(url('remove-office', 'remove'))
  public async remove() {}

  @Patch('edit')
  @UseInterceptors(
    FileInterceptor(
      'avatar',
      new FileOptions(getDataFromDB(UserDB, ['files', 'avatar'])),
    ),
  )
  public async edit(
    @CurrentUser('_id') userId: MongoId,
    @UploadedFile() file: Express.Multer.File,
    @Body('user', new JSONParsePipe(EditUserDto)) user: IUser,
  ): Promise<IUserInfo> {
    return this.userService.edit(userId, user, file);
  }
}
