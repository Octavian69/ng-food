import { Test } from '@nestjs/testing';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
import * as mockHttp from 'node-mocks-http';
import { getParamDecoratorFactory } from '@core/test/utils/test.handlers';
import { RemoveOfficeController } from '@module/remove-office/remove-office.controller';
import { MockUser } from '@module/user/test/__mocks__/user.mock';
import { AuthModule } from '@module/auth/auth.module';
import { AuthSocialModule } from '@module/auth-social/auth-social.module';
import { UserModule } from '@module/user/user.module';
import { RemoveOfficeService } from '@module/remove-office/remove-office.service';
import { CurrentUser } from '../user.decorators';
import { TestMongoConnection } from '@core/test/utils/TestMongoConnection';

describe('Test "user" decorators', () => {
  const mongo = new TestMongoConnection();

  // "CurrentUser" decorator
  describe('Test "CurrentUser" decorator', () => {
    let removeController: RemoveOfficeController;

    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        imports: [mongo.connect(), AuthModule, AuthSocialModule, UserModule],
        controllers: [RemoveOfficeController],
        providers: [RemoveOfficeService],
      }).compile();

      removeController = moduleRef.get<RemoveOfficeController>(
        RemoveOfficeController,
      );
    });

    afterEach(async () => {
      await mongo.disconnect();
      jest.restoreAllMocks();
    });

    test('should be defiend', () => {
      expect(CurrentUser).toBeTruthy();
    });

    it('should return request user', () => {
      const mockUser = new MockUser();
      const req = mockHttp.createRequest();
      const res = mockHttp.createResponse();
      req.user = mockUser;
      const mockDecoratorData = new ExecutionContextHost(
        [req, res],
        RemoveOfficeController,
        removeController.remove,
      );
      const factory = getParamDecoratorFactory(CurrentUser);
      const result = factory(null, mockDecoratorData);

      expect(result).toStrictEqual(req.user);
    });
  });
});
