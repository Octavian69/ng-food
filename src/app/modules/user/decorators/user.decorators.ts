import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IUser } from '../interfaces/IUser';
import { IUserInfo } from '../interfaces/IUserInfo';
import { IUserRequest } from '../interfaces/IUserRequest';

export const CurrentUser = createParamDecorator(
  (
    data: keyof IUser,
    ctx: ExecutionContext,
  ): IUserInfo | IUserInfo[keyof IUserInfo] => {
    const request: IUserRequest = ctx.switchToHttp().getRequest();
    const { user } = request;

    return data ? user[data] : user;
  },
);
