import {
  keys,
  makeMergeInstance,
  selectProps,
} from '@core/handlers/structural.handlers';
import { MongoId } from '@module/mongo/types/mongo.types';
import { IUser } from '../interfaces/IUser';
import { IUserInfo } from '../interfaces/IUserInfo';
import { TUserGender } from '../types/user.types';

export class UserInfoModel implements IUserInfo {
  public Name: string = null;
  public Created: Date = null;
  public Avatar: string = null;
  public Gender: TUserGender = null;
  public Position: string = null;
  public _id?: MongoId = null;

  constructor(user: IUser) {
    const thisKeys = keys(this) as Array<keyof UserInfoModel>;
    const selected = selectProps(user, thisKeys);

    return Object.assign(this, selected);
  }
}
