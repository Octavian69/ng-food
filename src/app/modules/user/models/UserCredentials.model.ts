import { IUserCredentials } from "../interfaces/IUserCredentials";

export class UserCredentialsModel implements IUserCredentials {
  constructor(
    public Login: string,
    public Password: string
  ) { }
}