import { UserCredentialsModel } from '../UserCredentials.model';

describe('Test "UserCredentials" model', () => {
  test('should be defined', () => {
    expect(UserCredentialsModel).toBeTruthy();
  });

  it('should correctly create instance', () => {
    const Login = 'test@login.com';
    const Password = 'Test password';
    const credentials = new UserCredentialsModel(Login, Password);

    expect(credentials).toBeInstanceOf(UserCredentialsModel);
    expect(credentials).toEqual({ Login, Password });
  });
});
