import { MockUserInfo } from '@module/user/test/__mocks__/user.mock';
import { UserInfoModel } from '../UserInfo.model';

describe('Test "UserInfo" model', () => {
  test('should be deifned', () => {
    expect(UserInfoModel).toBeTruthy();
  });

  it('should correctly create instance', () => {
    const mergeUser = new MockUserInfo();
    const user = new UserInfoModel(mergeUser);

    expect(user).toBeInstanceOf(UserInfoModel);
    expect(user.Name).toBe(mergeUser.Name);
    expect(user['Password']).toBeUndefined();
    expect(user['Login']).toBeUndefined();
  });
});
