import { IUser } from './IUser';

export interface IUserCredentials extends Pick<IUser, 'Login' | 'Password'> { }