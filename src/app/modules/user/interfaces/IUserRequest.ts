export { Request } from 'express';
import { IUserInfo } from './IUserInfo';
export interface IUserRequest extends Request {
  user: IUserInfo
}