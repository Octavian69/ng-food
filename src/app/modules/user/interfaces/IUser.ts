import { MongoId } from '@module/mongo/types/mongo.types';
import { TUserGender } from '../types/user.types';
export interface IUser {
  Login: string;
  Password: string;
  Name: string;
  Created: Date;
  Avatar: string;
  Gender: TUserGender;
  Position: string;
  _id?: MongoId;
}
