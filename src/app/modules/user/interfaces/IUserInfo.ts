import { IUser } from "./IUser";

export interface IUserInfo extends Omit<IUser, 'Login' | 'Password'> { }