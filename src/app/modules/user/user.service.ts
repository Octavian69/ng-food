import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  removeFile,
  updateFileStorage,
} from '@core/file/handlers/file.handlers';
import { User, UserDocument } from './schemas/User.schema';
import { UserDto } from './dto/User.dto';
import { IUser } from './interfaces/IUser';
import { IUserInfo } from './interfaces/IUserInfo';
import { MongoId } from '@module/mongo/types/mongo.types';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly UserModel: Model<UserDocument>,
  ) {}

  public async getUserWithCredentials(
    query: Partial<IUser>,
  ): Promise<UserDocument> {
    return this.UserModel.findOne(query);
  }

  public async getUserWithoutCredentials(
    query: Partial<IUser>,
  ): Promise<UserDocument> {
    return this.UserModel.findOne(query, { Password: 0, Login: 0 });
  }

  public async create(user: UserDto): Promise<UserDocument> {
    return new this.UserModel(user).save();
  }

  public async remove(userId: MongoId): Promise<void> {
    await this.UserModel.findByIdAndDelete(userId);
  }

  public async edit(
    userId: MongoId,
    candidate: IUser,
    file: Express.Multer.File,
  ): Promise<IUserInfo> {
    candidate.Avatar = await updateFileStorage(file, candidate.Avatar);

    const updateUser: IUserInfo = await this.UserModel.findByIdAndUpdate(
      userId,
      { $set: candidate },
      { new: true, projection: { Login: 0, Pasword: 0 } },
    );

    return updateUser;
  }
}
