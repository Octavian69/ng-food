import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { IUser } from '../interfaces/IUser';
import { TUserGender } from '../types/user.types';

@Schema()
export class User implements IUser {
  @Prop({ required: true, unique: true })
  Login: string;

  @Prop({ required: true })
  Name: string;

  @Prop({ required: true })
  Password: string;

  @Prop({ default: Date.now })
  Created: Date;

  @Prop({ default: null })
  Avatar: string;

  @Prop({ default: 'male' })
  Gender: TUserGender;

  @Prop({ default: null })
  Position: string;
}

export type UserDocument = User & Document;
export const UserSchema: MongooseSchema<UserDocument> = SchemaFactory.createForClass(
  User,
);
