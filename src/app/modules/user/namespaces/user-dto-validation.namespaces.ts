import { IValidationLength } from '@core/validators/interfaces/IValidationLength';
import { TValidationRange } from '@core/validators/types/validatior.types';
import { IUser } from '../interfaces/IUser';

type MINRange = TValidationRange<IUser, 'Name' | 'Password'>;
type MAXRange = TValidationRange<IUser, 'Login' | 'Name' | 'Password'>;

export namespace NSUserDtoValidation {
  export const min: MINRange = {
    Name: 3,
    Password: 7,
  };
  export const max: MAXRange = {
    Login: 50,
    Name: 40,
    Password: 25,
  };
}
