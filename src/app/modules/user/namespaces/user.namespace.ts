import { TUserGender } from "../types/user.types";

export namespace NSUser {
  export const Genders = Object.freeze(['male', 'female']) as TUserGender[];
}