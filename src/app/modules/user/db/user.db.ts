export const UserDB = {
  files: {
    avatar: {
      fileType: 'IMG',
      filePath: ['img', 'avatars'],
      fileSize: 5,
    },
  },
} as const;

export type TUserDB = typeof UserDB;
