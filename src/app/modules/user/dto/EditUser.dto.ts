import { PickType } from '@nestjs/mapped-types';
import { UserDto } from './User.dto';

export class EditUserDto extends PickType(UserDto, [
  'Name',
  'Position',
  'Gender',
  'Avatar',
] as const) {}
