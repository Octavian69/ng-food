import {
  IsEmail,
  Length,
  IsDefined,
  IsOptional,
  IsIn,
  IsString,
  Validate,
  MaxLength,
  IsMongoId,
} from 'class-validator';
import {
  isContain,
  isEmailField,
  isMaxLength,
  isRequired,
  isStringLength,
  isType,
} from '@core/validators/handlers/dto-validator.handlers';
import { IsStringDate } from '@core/validators/constraints/StringDate.constraint';
import { MongoId } from '@module/mongo/types/mongo.types';
import { IUser } from '../interfaces/IUser';
import { NSUserDtoValidation } from '../namespaces/user-dto-validation.namespaces';
import { TUserGender } from '../types/user.types';
import { NSUser } from '../namespaces/user.namespace';
export class UserDto implements IUser {
  @IsDefined(isRequired('Логин'))
  @IsEmail(...isEmailField('Логин'))
  @MaxLength(...isMaxLength('Логин', 'Login', NSUserDtoValidation.max))
  public Login: string;

  @IsDefined(isRequired('Пароль'))
  @Length(...isStringLength('Пароль', 'Password', NSUserDtoValidation))
  public Password: string;

  @IsDefined(isRequired('Имя'))
  @Length(...isStringLength('Имя', 'Name', NSUserDtoValidation))
  public Name: string;

  @Validate(IsStringDate, ['Created'])
  public Created: Date;

  @IsOptional()
  @IsIn(...isContain('Пол', NSUser.Genders))
  public Gender: TUserGender;

  @IsOptional()
  @IsString(isType('Аватар', 'string'))
  public Avatar: string;

  @IsOptional()
  @IsString(isType('Должность', 'string'))
  public Position: string;

  @IsOptional()
  @IsMongoId()
  public id?: MongoId;
}
