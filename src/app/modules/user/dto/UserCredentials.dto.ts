import { PickType } from '@nestjs/mapped-types'
import { IUserCredentials } from "../interfaces/IUserCredentials";
import { UserDto } from './User.dto';
export class UserCredentialsDto extends PickType(UserDto, ['Login', 'Password'] as const) implements IUserCredentials {
}