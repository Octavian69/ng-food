import { FetchQueryDto } from '@core/utils/fetch/dto/FetchQuery.dto';
import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { ICategory } from '@module/categories/interfaces/ICategory';

export type TOrdersCategoriesFilter = {
  Title?: string;
  TotalPositionsRange?: IRangeValue<number>;
};
export type TOrdersCategoriesSortFields = 'TotalPositions' | 'Created';
export type TOrdersFetchQueryDto = FetchQueryDto<
  ICategory,
  TOrdersCategoriesFilter,
  TOrdersCategoriesSortFields
>;
