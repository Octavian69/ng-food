import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { MongoId } from '@module/mongo/types/mongo.types';
import { CurrentUser } from '@module/user/decorators/user.decorators';
import { OrderDto } from './dto/Order.dto';
import { IOrderCategory } from './interfaces/IOrderCategory';
import { OrdersService } from './orders.service';
import { OrderDocument } from './schemas/Order.schema';
import { TOrdersFetchQueryDto } from './types/orders.types';
import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';

@Controller('orders')
export class OrdersController {
  constructor(private ordersService: OrdersService) {}

  @Post('/save-order')
  @HttpCode(HttpStatus.CREATED)
  public async saveOrder(
    @Body() candidate: OrderDto,
    @CurrentUser('_id') userId: MongoId,
  ): Promise<OrderDocument> {
    candidate.User = userId;

    return this.ordersService.saveOrder(candidate);
  }

  @Post('/fetch-order-categories')
  async fetchOrdersCategories(
    @CurrentUser('_id') userId: MongoId,
    @Body() query: TOrdersFetchQueryDto,
  ): Promise<TPaginationValue<IOrderCategory>> {
    return this.ordersService.fetchOrdersCategories(query, userId);
  }

  @Get('/total-stats')
  async getTotalStats(@CurrentUser('_id') userId: MongoId) {
    return this.ordersService.getTotalStats(userId);
  }
}
