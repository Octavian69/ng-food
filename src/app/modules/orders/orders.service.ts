import { ISimple } from '@core/interfaces/shared/ISimple';
import { ServiceManager } from '@core/managers/Service.manager';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { MongoHelperService } from '@module/mongo/services/mongo-helper.service';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { OrdersDB, TOrderDB } from './db/orders.db';
import { OrderDto } from './dto/Order.dto';
import { IOrderCategory } from './interfaces/IOrderCategory';
import { IOrdersTotalStats } from './interfaces/IOrdersTotalStats';
import { Order, OrderDocument } from './schemas/Order.schema';
import {
  Category,
  CategoryDocument,
} from '@module/categories/schemas/Category.schema';
import {
  MongoId,
  TMongoMatchParser,
  TMongoSort,
} from '@module/mongo/types/mongo.types';
import {
  Position,
  PositionDocument,
} from '@module/positions/schemas/Position.schema';
import {
  TOrdersCategoriesSortFields,
  TOrdersFetchQueryDto,
} from './types/orders.types';

@Injectable()
export class OrdersService extends ServiceManager<TOrderDB> {
  constructor(
    private readonly mongoHelper: MongoHelperService,
    @InjectModel(Order.name)
    private readonly OrderModel: Model<OrderDocument>,
    @InjectModel(Category.name)
    private readonly CategoryModel: Model<CategoryDocument>,
    @InjectModel(Position.name)
    private readonly PositionModel: Model<PositionDocument>,
  ) {
    super(OrdersDB);
  }

  async fetchOrdersCategories(
    query: TOrdersFetchQueryDto,
    userId: MongoId,
  ): Promise<TPaginationValue<IOrderCategory>> {
    const { paginate, filters, sort } = query;
    const matchParser: TMongoMatchParser = this.getDataFromDB(
      ['mongo', 'fetch-categories', '$match'],
      false,
    );
    const matchQuery: ISimple<unknown> = Object.assign(
      { User: userId },
      filters,
    );
    const lookupQuery: object = this.getDataFromDB(
      ['mongo', 'fetch-categories', '$lookup', 'position-cost'],
      false,
    );

    const $lookup = this.mongoHelper.stage('$lookup', lookupQuery);
    const $match: object = this.mongoHelper.match(matchParser, matchQuery);
    const $project: object[] = this.getDataFromDB([
      'mongo',
      'fetch-categories',
      '$project',
    ]);
    const $sort: TMongoSort<TOrdersCategoriesSortFields> = this.mongoHelper.sort(
      sort,
    );

    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      $lookup,
      $match,
      ...$project,
      $sort,
    );

    return await this.mongoHelper.pagination(
      this.CategoryModel,
      aggregateQuery,
      paginate,
    );
  }

  public async saveOrder(candidate: OrderDto): Promise<OrderDocument> {
    const order: OrderDocument = await new this.OrderModel(candidate).save();

    return order;
  }

  public async getTotalStats(userId: MongoId): Promise<IOrdersTotalStats> {
    const $match = this.mongoHelper.stage('$match', {
      User: userId,
    });
    const query: object[] = this.getDataFromDB(['mongo', 'total-stats']);
    const aggregationQuery: object[] = this.mongoHelper.completeQuery(
      $match,
      ...query,
    );
    const [result] = (await this.mongoHelper.aggregate<
      OrderDocument,
      IOrdersTotalStats
    >(this.OrderModel, aggregationQuery)) as [IOrdersTotalStats];

    return result;
  }
}
