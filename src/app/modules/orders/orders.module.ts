import { MongoModule } from '@module/mongo/mongo.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { Order, OrderSchema } from './schemas/Order.schema';
import {
  Category,
  CategorySchema,
} from '@module/categories/schemas/Category.schema';
import {
  Position,
  PositionSchema,
} from '@module/positions/schemas/Position.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Order.name, schema: OrderSchema },
      { name: Position.name, schema: PositionSchema },
      { name: Category.name, schema: CategorySchema },
    ]),
    MongoModule,
  ],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
