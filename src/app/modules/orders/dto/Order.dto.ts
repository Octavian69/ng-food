import { MongoId } from '@module/mongo/types/mongo.types';
import { Type } from 'class-transformer';
import { ObjectId } from 'mongoose';
import { IOrder, IOrderPositon } from '../interfaces/IOrder';
import {
  isInvalid,
  isNumber,
  isRequired,
  isType,
} from '@core/validators/handlers/dto-validator.handlers';
import {
  ArrayNotEmpty,
  IsArray,
  IsDateString,
  IsDefined,
  IsMongoId,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class OrderDto implements IOrder {
  @ValidateNested(isInvalid('Некорректное содержание позиций'))
  @IsArray(isType('Positions', 'array'))
  @ArrayNotEmpty(
    isInvalid('Поле "Positions" должно содержать хотя бы одну позицию'),
  )
  @Type(() => OrderPositionDto)
  public Positions: OrderPositionDto[];

  @IsDefined(isRequired('TotalAmount'))
  @IsNumber(...isNumber('TotalAmount'))
  public TotalAmount: number;

  @IsOptional()
  @IsDateString(isType('Создан', 'date'))
  public Created: Date;

  @IsOptional()
  @IsMongoId(isInvalid())
  public User: MongoId;

  @IsOptional()
  @IsMongoId(isInvalid())
  public _id: MongoId;
}

export class OrderPositionDto implements IOrderPositon {
  @IsDefined(isRequired('PositionId'))
  @IsMongoId(isInvalid())
  public PositionId: ObjectId;

  @IsDefined(isRequired('PositionCost'))
  @IsNumber(...isNumber('PositionCost'))
  public PositionCost: number;

  @IsDefined(isRequired('PositionTitle'))
  @IsString(isType('PositionTitle', 'string'))
  public PositionTitle: string;

  @IsDefined(isRequired('Quantity'))
  @IsNumber(...isNumber('Quantity'))
  public Quantity: number;

  @IsDefined(isRequired('TotalCost'))
  @IsNumber(...isNumber('TotalCost'))
  public TotalCost: number;
}
