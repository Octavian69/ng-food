import { toObjectId } from '@module/mongo/handlers/mongo.handlers';

const POSITION_COST_RANGE_FIELD = 'RangeCost' as const;

export const OrdersDB = {
  mongo: {
    'fetch-categories': {
      $match: {
        User: (value: string) => ({ $eq: toObjectId(value) }),
        Title: (value: string) => ({ $regex: new RegExp(value, 'gi') }),
      },

      $project: [
        {
          $addFields: {
            MinPositionCost: {
              $arrayElemAt: [
                `$${POSITION_COST_RANGE_FIELD}.MinPositionCost`,
                0,
              ],
            },
            MaxPositionCost: {
              $arrayElemAt: [
                `$${POSITION_COST_RANGE_FIELD}.MaxPositionCost`,
                0,
              ],
            },
          },
        },
        {
          $project: {
            [POSITION_COST_RANGE_FIELD]: 0,
          },
        },
      ],

      $lookup: {
        'position-cost': {
          from: 'positions',
          let: { rootCategoryId: '$_id' },
          as: POSITION_COST_RANGE_FIELD,
          pipeline: [
            {
              $addFields: {
                positionCategoryId: { $toObjectId: '$Category' },
              },
            },
            {
              $match: {
                $expr: {
                  $eq: ['$positionCategoryId', '$$rootCategoryId'],
                },
              },
            },
            {
              $sort: { Cost: 1 },
            },
            { $project: { Cost: 1 } },
            {
              $group: {
                _id: '$$rootCategoryId',
                first: { $first: '$$ROOT' },
                last: { $last: '$$ROOT' },
              },
            },
            {
              $addFields: {
                MinPositionCost: '$first.Cost',
                MaxPositionCost: '$last.Cost',
              },
            },
            {
              $project: {
                _id: 0,
                MinPositionCost: 1,
                MaxPositionCost: 1,
              },
            },
          ],
        },
      },
    },
    'total-stats': [
      {
        $group: {
          _id: null,
          MinTotalPositions: { $min: { $size: '$Positions' } },
          MaxTotalPositions: { $max: { $size: '$Positions' } },
          MinTotalAmount: { $min: '$TotalAmount' },
          MaxTotalAmount: { $max: '$TotalAmount' },
          MinCreated: { $min: '$Created' },
          MaxCreated: { $max: '$Created' },
        },
      },
      {
        $project: {
          _id: 0,
        },
      },
    ],
  },
} as const;

export type TOrderDB = typeof OrdersDB;
