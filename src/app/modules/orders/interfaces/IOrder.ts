import { MongoId } from '@module/mongo/types/mongo.types';

export interface IOrder {
  Positions: IOrderPositon[];
  TotalAmount: number;
  Created: Date;
  User?: MongoId;
  _id?: MongoId;
}

export interface IOrderPositon {
  PositionId: MongoId;
  PositionCost: number;
  PositionTitle: string;
  Quantity: number;
  TotalCost: number;
}
