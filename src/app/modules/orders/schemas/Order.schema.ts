import { MongoId } from '@module/mongo/types/mongo.types';
import { User } from '@module/user/schemas/User.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { IOrder, IOrderPositon } from '../interfaces/IOrder';

@Schema({ versionKey: false })
export class Order implements IOrder {
  @Prop({ required: true, type: MongooseSchema.Types.Mixed })
  public Positions: IOrderPositon[];

  @Prop({ required: true, type: Number })
  public TotalAmount: number;

  @Prop({ required: true, default: Date.now, type: Date })
  public Created: Date;

  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: User.name })
  public User: MongoId;
}

export type OrderDocument = Document<Order>;
export const OrderSchema: MongooseSchema<OrderDocument> = SchemaFactory.createForClass(
  Order,
);
