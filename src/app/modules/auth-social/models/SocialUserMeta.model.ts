import { MongoId } from '@module/mongo/types/mongo.types';
import { ISocialUserMeta } from '../interfaces/ISocialUserMeta';
import { ISocialWebMeta } from '../interfaces/ISocialWebMeta';
import { TSocialWebProvider } from '../types/auth-social';

export class SocialUserMetaModel implements ISocialUserMeta {
  public Providers: TSocialWebProvider[] = [];
  public SocialWebsMeta: ISocialWebMeta[] = [];

  constructor(public User: MongoId, meta: ISocialWebMeta) {
    const { Provider } = meta;
    this.Providers.push(Provider);
    this.SocialWebsMeta.push(meta);
  }
}
