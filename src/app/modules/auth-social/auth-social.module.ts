import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '@module/auth/auth.module';
import { EmailModule } from '@module/email/email.module';
import { UserModule } from '@module/user/user.module';
import { AuthSocialController } from './auth-social.controller';
import { AuthSocialService } from './auth-social.service';
import { SocialUserMeta, SocialUserMetaSchema } from './schemas/SocialUserMeta.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SocialUserMeta.name, schema: SocialUserMetaSchema }
    ]),
    UserModule,
    AuthModule,
    EmailModule
  ],
  providers: [
    AuthSocialService
  ],
  controllers: [
    AuthSocialController
  ],
  exports: [
    AuthSocialService
  ]
})
export class AuthSocialModule { }

