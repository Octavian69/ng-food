import { MongoId } from '@module/mongo/types/mongo.types';
import { User } from '@module/user/schemas/User.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { ISocialUserMeta } from '../interfaces/ISocialUserMeta';
import { ISocialWebMeta } from '../interfaces/ISocialWebMeta';
import { TSocialWebProvider } from '../types/auth-social';

@Schema()
export class SocialUserMeta implements ISocialUserMeta {
  @Prop({ required: true, ref: User.name, type: MongooseSchema.Types.ObjectId })
  User: MongoId;

  @Prop({ required: true })
  Providers: TSocialWebProvider[];

  @Prop({ required: true })
  SocialWebsMeta: ISocialWebMeta[];
}

export type SocialUserMetaDocument = SocialUserMeta & Document;
export const SocialUserMetaSchema: MongooseSchema<SocialUserMetaDocument> = SchemaFactory.createForClass(
  SocialUserMeta,
);
