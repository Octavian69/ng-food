import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ISendMailOptions } from '@nestjs-modules/mailer';
import { Model } from 'mongoose';
import {
  SocialUserMetaDocument,
  SocialUserMeta,
} from './schemas/SocialUserMeta.schema';
import { SocialWebMetaDto } from './dto/SocialWebMetaDto.dto';
import { SocialUserMetaModel } from './models/SocialUserMeta.model';
import { ISocialUserMeta } from './interfaces/ISocialUserMeta';
import { AuthSocialDB, TAuthSocialDB } from './db/auth-social.db';
import { UserService } from '@module/user/user.service';
import { UserDocument } from '@module/user/schemas/User.schema';
import { IUserInfo } from '@module/user/interfaces/IUserInfo';
import { UserDto } from '@module/user/dto/User.dto';
import { UserInfoModel } from '@module/user/models/UserInfo.model';
import { AuthService } from '@module/auth/auth.service';
import { IAuthTokens } from '@module/auth/interfaces/IAuthTokens';
import { EmailService } from '@module/email/email.service';
import {
  apiMessageException,
  getErrorMessage,
} from '@core/errors/handlers/error.handlers';
import { getRandomPassword } from '@core/handlers/api.handlers';
import { validateDto } from '@core/validators/handlers/dto-validator.handlers';
import { ServiceManager } from '@core/managers/Service.manager';
import { MongoId } from '@module/mongo/types/mongo.types';

@Injectable()
export class AuthSocialService extends ServiceManager<TAuthSocialDB> {
  constructor(
    @InjectModel(SocialUserMeta.name)
    private readonly SocialUserMetaModel: Model<SocialUserMetaDocument>,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly emailService: EmailService,
  ) {
    super(AuthSocialDB);
  }

  public async login(socialCandidate: SocialWebMetaDto): Promise<IAuthTokens> {
    try {
      const { Login } = socialCandidate;
      const user: UserDocument = await this.userService.getUserWithCredentials({
        Login,
      });

      if (user) {
        await this.checkAndUpdateMeta(user, socialCandidate);
        const tokens: IAuthTokens = await this.authenticate(user);

        return tokens;
      }

      return null;
    } catch (e) {
      apiMessageException(getErrorMessage(e), HttpStatus.BAD_REQUEST);
    }
  }

  public async registration(candidate: SocialWebMetaDto): Promise<IAuthTokens> {
    try {
      const newUser: UserDocument = await this.createSocialUser(candidate);
      const tokens: IAuthTokens = await this.authenticate(newUser);

      return tokens;
    } catch (e) {
      apiMessageException(getErrorMessage(e), HttpStatus.BAD_REQUEST);
    }
  }

  private async createSocialUser(
    socialCandidate: SocialWebMetaDto,
  ): Promise<UserDocument> {
    const candidate: UserDto = await this.createCandidateUser(socialCandidate);
    await this.authService.registration(candidate);
    const user: UserDocument = await this.userService.getUserWithoutCredentials(
      { Login: candidate.Login },
    );
    await this.sendUserСredentialsToEmail(candidate);
    await this.createSocialUserMeta(user._id, socialCandidate);

    return user;
  }

  private async createSocialUserMeta(
    userId: MongoId,
    meta: SocialWebMetaDto,
  ): Promise<SocialUserMetaDocument> {
    const candidateUserMeta: ISocialUserMeta = new SocialUserMetaModel(
      userId,
      meta,
    );
    const newMeta: SocialUserMetaDocument = await new this.SocialUserMetaModel(
      candidateUserMeta,
    ).save();

    return newMeta;
  }

  private async createCandidateUser(
    socialCandidate: SocialWebMetaDto,
  ): Promise<UserDto> {
    const Password: string = getRandomPassword();
    const Created: Date = new Date();
    const { Name, Login, Avatar } = socialCandidate;
    const validateCandidate: UserDto = Object.assign(new UserDto(), {
      Login,
      Password,
      Name,
      Avatar,
      Created,
    });
    const candidate: UserDto = await validateDto<UserDto>(validateCandidate);

    return candidate;
  }

  private async sendUserСredentialsToEmail({
    Login,
    Password,
  }: UserDto): Promise<void> {
    const emailOptions: ISendMailOptions = this.getDataFromDB([
      'email',
      'registration-by-social',
    ]);
    await this.emailService.completeAndSendEmail(emailOptions, {
      Login,
      Password,
    });
  }

  private async checkAndUpdateMeta(
    user: UserDocument,
    meta: SocialWebMetaDto,
  ): Promise<SocialUserMetaDocument> {
    const userMeta: SocialUserMetaDocument = await this.SocialUserMetaModel.findOne(
      { User: user._id },
    );
    const isExistSocialWeb: boolean = userMeta.Providers.includes(
      meta.Provider,
    );

    if (!isExistSocialWeb) {
      userMeta.Providers.push(meta.Provider);
      userMeta.SocialWebsMeta.push(meta);

      await userMeta.save();
    }

    return userMeta;
  }

  private async authenticate(user: UserDocument): Promise<IAuthTokens> {
    const userInfo: IUserInfo = new UserInfoModel(user.toJSON());
    const tokens: IAuthTokens = await this.authService.updateTokens(userInfo);

    return tokens;
  }

  public async remove(userId: MongoId): Promise<void> {
    await this.SocialUserMetaModel.findOneAndDelete({ User: String(userId) });
  }
}
