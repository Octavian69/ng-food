import { tag } from '@core/handlers/string.handlers';

export const AuthSocialDB = {
  email: {
    'registration-by-social': {
      to: `${tag('Login')}`,
      subject: 'Создание личного кабинта',
      html: `
        <pre>
          <h4>Учетные данные вашего кабинета:</h4>
          <strong>Логин<strong>: <mark>${tag('Login')}</mark> 
          <strong>Пароль</strong>: <mark>${tag('Password')}</mark>
        </pre>
      `,
    },
  },
} as const;

export type TAuthSocialDB = typeof AuthSocialDB;
