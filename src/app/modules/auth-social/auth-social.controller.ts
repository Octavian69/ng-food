import { Body, Controller, HttpCode, Post, HttpStatus } from '@nestjs/common';
import { IAuthTokens } from '@module/auth/interfaces/IAuthTokens';
import { PublicRoute } from '@module/auth/metadata/auth.metadata';
import { AuthSocialService } from './auth-social.service';
import { SocialWebMetaDto } from './dto/SocialWebMetaDto.dto';

@Controller('auth-social')
export class AuthSocialController {
  constructor(private readonly authSocialService: AuthSocialService) {}

  @Post('login-by-social-web')
  @PublicRoute()
  public login(@Body() candidate: SocialWebMetaDto): Promise<IAuthTokens> {
    return this.authSocialService.login(candidate);
  }

  @Post('registration-by-social-web')
  @HttpCode(HttpStatus.CREATED)
  @PublicRoute()
  public registration(
    @Body() candidate: SocialWebMetaDto,
  ): Promise<IAuthTokens> {
    return this.authSocialService.registration(candidate);
  }
}
