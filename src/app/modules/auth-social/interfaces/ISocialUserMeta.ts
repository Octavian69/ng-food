import { MongoId } from '@module/mongo/types/mongo.types';
import { TSocialWebProvider } from '../types/auth-social';
import { ISocialWebMeta } from './ISocialWebMeta';

export interface ISocialUserMeta {
  User: MongoId;
  Providers: TSocialWebProvider[];
  SocialWebsMeta: ISocialWebMeta[];
  _id?: MongoId;
}
