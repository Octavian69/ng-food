import { TSocialWebProvider } from "../types/auth-social";

export interface ISocialWebMeta {
  AccountID: string;
  Provider: TSocialWebProvider
  Name: string;
  Login: string;
  Avatar: string;
}