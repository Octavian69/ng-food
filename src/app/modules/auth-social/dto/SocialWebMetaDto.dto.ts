import { IsEmail, IsIn, IsString, IsDefined, IsOptional } from 'class-validator';
import { getDataFromDB } from '@core/handlers/structural.handlers';
import { isEmailField, isRequired, isType } from '@core/validators/handlers/dto-validator.handlers';
import { AuthDB } from '@module/auth/db/auth.db';
import { ISocialWebMeta } from '../interfaces/ISocialWebMeta';
import { TSocialWebProvider } from '../types/auth-social';

export class SocialWebMetaDto implements ISocialWebMeta {

  @IsDefined(isRequired('ID'))
  @IsString(isType('AccounID', 'string'))
  public AccountID: string;

  @IsIn(getDataFromDB(AuthDB, ['socilaProviders']), { message: 'Некорректный формат социальной сети.' })
  public Provider: TSocialWebProvider;

  @IsDefined(isRequired('Имя'))
  @IsString(isType('Имя', 'string'))
  public Name: string;

  @IsEmail(...isEmailField('Логин'))
  public Login: string;

  @IsOptional()
  @IsString(isType('Аватар', 'string'))
  public Avatar: string;
}