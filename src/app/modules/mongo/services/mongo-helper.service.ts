import { isNotNullish } from '@core/handlers/condition.handlers';
import { IPagination } from '@core/utils/fetch/pagination/interfaces/IPagination';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { TSortQuery } from '@core/utils/fetch/types/fetch.types';
import { Injectable } from '@nestjs/common';
import { Document, Model } from 'mongoose';
import {
  TMongoMatchParser,
  TMongoPaginationResult,
  TMongoSort,
  TMongoSortParser,
} from '../types/mongo.types';

@Injectable()
export class MongoHelperService {
  public async aggregate<T extends Document, V>(
    Model: Model<T>,
    query: Array<object>,
  ): Promise<Array<V>> {
    return await Model.aggregate(query);
  }

  public async pagination<T extends Document, V>(
    Model: Model<T>,
    query: Array<object>,
    paginate: IPagination,
  ): Promise<TPaginationValue<V>> {
    const paginationQuery: Array<object> = this.paginationOptions(
      query,
      paginate,
    );
    const result = (await this.aggregate(
      Model,
      paginationQuery,
    )) as TMongoPaginationResult<V>;

    return this.getPaginationValue(result);
  }

  private getPaginationValue<V>(
    result: TMongoPaginationResult<V>,
  ): TPaginationValue<V> {
    const [value] = result;
    const { items } = value;
    const totalCount = value.count[0]?.totalCount || 0;

    return { items, totalCount };
  }

  private paginationOptions(
    stages: Array<object>,
    pagination: IPagination,
  ): Array<object> {
    const { skip, limit: $limit } = pagination;
    const $skip: number = +skip * +$limit;

    return [
      ...(stages || []),
      {
        $facet: {
          items: [{ $skip }, { $limit }],
          count: [
            { $group: { _id: null, totalCount: { $sum: 1 } } },
            { $project: { _id: 0 } },
          ],
        },
      },
    ];
  }

  public stage<T extends object>(
    stage: string,
    query: T,
  ): Record<typeof stage, T> {
    return { [stage]: query };
  }

  public completeQuery(...queries: Array<object>): Array<object> {
    return queries.filter(Boolean);
  }

  public match(parser: TMongoMatchParser, query: object): object {
    const options: object = Object.entries(query).reduce(
      (accum: object, [key, value]: [string, unknown]) => {
        if (isNotNullish(value)) {
          const handler: Function = parser[key];
          accum[key] = handler ? handler(value) : value;
        }

        return accum;
      },
      {},
    );

    return this.stage('$match', options);
  }

  public complexMatch<Q extends object>(
    parser: TMongoMatchParser,
    query: Q,
  ): object {
    const $match = Object.entries(query).reduce(
      (accum: object, [key, value]: [string, any]) => {
        const parseMethod: TMongoMatchParser[string] = parser[key];
        if (parseMethod) {
          const result = parseMethod(value);

          Object.assign(accum, result);
        } else {
          accum[key] = value;
        }

        return accum;
      },
      {},
    );

    return this.stage('$match', $match);
  }

  public sort<T extends string = string>(
    sortQuery: TSortQuery<T>,
  ): TMongoSort<T> {
    if (sortQuery) {
      return this.stage('$sort', sortQuery) as TMongoSort<T>;
    }

    return null;
  }

  public complexSort(
    parser: TMongoSortParser,
    sortQuery: object,
  ): Record<'$sort', TSortQuery> {
    if (!sortQuery) return null;

    const sortItems = Object.entries(sortQuery).map(([key, value]) => {
      const parseMethod: TMongoSortParser[string] = parser[key];

      return parseMethod ? parseMethod(value) : { [key]: value };
    });
    const $sort: TSortQuery = Object.assign({}, ...sortItems);

    return this.stage('$sort', $sort);
  }
}
