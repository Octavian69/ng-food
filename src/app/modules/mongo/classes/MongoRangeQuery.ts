import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';

export class MongoRangeQuery<T = unknown> {
  public $gte: T;
  public $lte: T;

  constructor(range: IRangeValue<T>, transformer?: Function) {
    if (range?.start)
      this.$gte = transformer ? transformer(range.start) : range.start;
    if (range?.end)
      this.$lte = transformer ? transformer(range.end) : range.end;
  }
}
