import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { Types } from 'mongoose';
import { MongoRangeQuery } from '../classes/MongoRangeQuery';

export function toObjectId(id: any): Types.ObjectId {
  return (Types as any).ObjectId(id);
}

export function toSizeArrayRange<T extends IRangeValue>(
  query: T,
  field: string,
) {
  const mogoRange = new MongoRangeQuery(query);
  const range = Object.entries(mogoRange).map(
    ([key, value]: [string & keyof T, T[keyof T]]) => {
      return { [key]: [{ $size: `$${field}` }, value] };
    },
  );

  return { $and: range };
}
