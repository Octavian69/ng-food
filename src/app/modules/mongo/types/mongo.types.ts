import { TSortQuery } from '@core/utils/fetch/types/fetch.types';
import { Schema, Types } from 'mongoose';

export type MongoId = Types.ObjectId | Schema.Types.ObjectId;
export type TMongoPaginationResult<T> = [
  {
    items: T[];
    count: [{ totalCount: number }];
  },
];

export type TMongoMatchParser = {
  [prop: string]: (v: any) => object;
};

export type TMongoSortParser = {
  [prop: string]: (value: any) => TSortQuery<typeof prop>;
};

export type TMongoSort<T extends string = string> = {
  $sort: TSortQuery<T>;
};
