import { Module } from '@nestjs/common';
import { MongoHelperService } from './services/mongo-helper.service';

const providers = [MongoHelperService];

@Module({
  providers,
  exports: providers,
})
export class MongoModule {}
