import { config } from '@config/handlers/config.handlers';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot(config.get('MONGO_URI'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
  ],
  exports: [MongooseModule],
})
export class MongoConnectionModule {}
