import { Category } from '@module/categories/schemas/Category.schema';
import { MongoId } from '@module/mongo/types/mongo.types';
import { User } from '@module/user/schemas/User.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { IPosition } from '../interfaces/IPosition';

@Schema()
export class Position implements IPosition {
  @Prop({ reuqired: true })
  public Title: string;

  @Prop({ required: true })
  public Cost: number;

  @Prop({ default: null })
  public Description: string;

  @Prop({ default: Date.now })
  public Created: Date;

  @Prop({ default: Date.now })
  public Updated: Date;

  @Prop({ default: null })
  public Preview: string;

  @Prop({
    required: true,
    type: MongooseSchema.Types.ObjectId,
    ref: Category.name,
  })
  public Category: MongoId;

  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: User.name })
  public User: MongoId;
}

export type PositionDocument = Position & Document;
export const PositionSchema: MongooseSchema<PositionDocument> = SchemaFactory.createForClass(
  Position,
);
