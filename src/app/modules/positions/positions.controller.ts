import { FileOptions } from '@core/file/classes/FileOptions';
import { getDataFromDB } from '@core/handlers/structural.handlers';
import { JSONParsePipe } from '@core/pipes/json-parse.pipe';
import { FetchQueryDto } from '@core/utils/fetch/dto/FetchQuery.dto';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { MongoId } from '@module/mongo/types/mongo.types';
import { CurrentUser } from '@module/user/decorators/user.decorators';
import { FileInterceptor } from '@nestjs/platform-express';
import { PositionsDB } from './db/positions.db';
import { PositionDto } from './dto/Position.dto';
import { IPosition } from './interfaces/IPosition';
import { PositionsService } from './positions.service';
import { PositionDocument } from './schemas/Position.schema';
import { TPositionsCostStats } from './types/positions.types';
import { toObjectId } from '../mongo/handlers/mongo.handlers';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';

@Controller('positions')
export class PositionsController {
  constructor(private positionsService: PositionsService) {}

  @Post('fetch/:id')
  @HttpCode(HttpStatus.OK)
  public async fetch(
    @CurrentUser('_id') userId: MongoId,
    @Param('id') categoryId: string,
    @Body() body: FetchQueryDto,
  ): Promise<TPaginationValue<IPosition>> {
    return this.positionsService.fetch(userId, toObjectId(categoryId), body);
  }

  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(
    FileInterceptor(
      'preview',
      new FileOptions(getDataFromDB(PositionsDB, ['files', 'create:edit'])),
    ),
  )
  public async create(
    @UploadedFile() file: Express.Multer.File,
    @Body('position', new JSONParsePipe(PositionDto)) candidate: IPosition,
  ): Promise<PositionDocument> {
    return this.positionsService.create(candidate, file);
  }

  @Patch('edit/:id')
  @UseInterceptors(
    FileInterceptor(
      'preview',
      new FileOptions(getDataFromDB(PositionsDB, ['files', 'create:edit'])),
    ),
  )
  public async edit(
    @UploadedFile() file: Express.Multer.File,
    @Param('id')
    positionId: string,
    @Body('position', new JSONParsePipe(PositionDto)) candidate: IPosition,
  ): Promise<PositionDocument> {
    return this.positionsService.edit(positionId, candidate, file);
  }

  @Delete('/remove/:id')
  async remove(@Param('id') positionId: string): Promise<IPosition> {
    return this.positionsService.remove(positionId);
  }

  @Get('/get-cost-stats/:categoryId')
  async getCostStats(
    @CurrentUser('_id') userId: MongoId,
    @Param('categoryId') categoryId: string,
  ): Promise<TPositionsCostStats> {
    return this.positionsService.getCostStats(userId, toObjectId(categoryId));
  }
}
