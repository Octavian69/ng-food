import { TValidationRange } from '@core/validators/types/validatior.types';
import { IPosition } from '../interfaces/IPosition';

type MINRange = TValidationRange<IPosition, 'Title' | 'Cost'>;
type MAXRange = TValidationRange<IPosition, 'Title' | 'Description'>;

export namespace PositionDtoValidation {
  export const min: MINRange = {
    Title: 3,
    Cost: 1,
  };

  export const max: MAXRange = {
    Title: 40,
    Description: 70,
  };
}
