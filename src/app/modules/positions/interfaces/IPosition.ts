import { MongoId } from '@module/mongo/types/mongo.types';
export interface IPosition {
  Title: string;
  Cost: number;
  Description: string;
  Created: Date;
  Updated: Date;
  Preview: string;
  Category: MongoId;
  User: MongoId;
  _id?: MongoId;
}
