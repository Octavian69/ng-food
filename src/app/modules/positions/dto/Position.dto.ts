import { IsStringDate } from '@core/validators/constraints/StringDate.constraint';
import { MongoId } from '@module/mongo/types/mongo.types';
import { IPosition } from '../interfaces/IPosition';
import { PositionDtoValidation } from '../namespaces/position-dto-validation.namespaces';
import {
  isInvalid,
  isMaxLength,
  isMin,
  isNumber,
  isRequired,
  isStringLength,
  isType,
} from '@core/validators/handlers/dto-validator.handlers';
import {
  IsDefined,
  IsMongoId,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  MaxLength,
  Min,
  Validate,
} from 'class-validator';

export class PositionDto implements IPosition {
  @IsString(isType('Title', 'string'))
  @IsDefined(isRequired('Название'))
  @Length(...isStringLength('Название', 'Title', PositionDtoValidation))
  public Title: string;

  @IsOptional()
  @IsString(isType('Description', 'string'))
  @MaxLength(
    ...isMaxLength('Описание', 'Description', PositionDtoValidation.max),
  )
  public Description: string;

  @IsDefined(isRequired('Стоимость'))
  @IsNumber(...isNumber('Cost'))
  @Min(...isMin(1, 'Cost'))
  public Cost: number;

  @IsOptional()
  @IsString(isType('Preview', 'string'))
  public Preview: string;

  @IsOptional()
  @Validate(IsStringDate, ['Created'])
  public Created: Date;

  @IsOptional()
  @Validate(IsStringDate, ['Created'])
  public Updated: Date;

  @IsOptional()
  @IsMongoId(isInvalid())
  public Category: MongoId;

  @IsOptional()
  @IsMongoId(isInvalid())
  public User: MongoId;

  @IsOptional()
  @IsMongoId(isInvalid())
  public _id: MongoId;
}
