import { CategoriesModule } from '@module/categories/categories.module';
import { MongoModule } from '@module/mongo/mongo.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PositionsController } from './positions.controller';
import { PositionsService } from './positions.service';
import { Position, PositionSchema } from './schemas/Position.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Position.name, schema: PositionSchema },
    ]),
    CategoriesModule,
    MongoModule,
  ],
  controllers: [PositionsController],
  providers: [PositionsService],
})
export class PositionsModule {}
