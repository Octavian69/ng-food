export type TPositionsCostStats = {
  MinCost: number;
  MaxCost: number;
};
