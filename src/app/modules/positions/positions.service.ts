import { ServiceManager } from '@core/managers/Service.manager';
import { FetchQueryDto } from '@core/utils/fetch/dto/FetchQuery.dto';
import { TPaginationValue } from '@core/utils/fetch/pagination/types/pagination.types';
import { CategoryService } from '@module/categories/category.service';
import { MongoHelperService } from '@module/mongo/services/mongo-helper.service';
import { MongoId, TMongoMatchParser } from '@module/mongo/types/mongo.types';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PositionsDB, TPositionsDB } from './db/positions.db';
import { IPosition } from './interfaces/IPosition';
import { Position, PositionDocument } from './schemas/Position.schema';
import { TPositionsCostStats } from './types/positions.types';
import {
  removeFile,
  updateFileStorage,
} from '@core/file/handlers/file.handlers';

@Injectable()
export class PositionsService extends ServiceManager<TPositionsDB> {
  constructor(
    @InjectModel(Position.name)
    private PositionModel: Model<PositionDocument>,
    private mongoHelper: MongoHelperService,
    private categoryService: CategoryService,
  ) {
    super(PositionsDB);
  }

  public async fetch(
    userId: MongoId,
    categoryId: MongoId,
    query: FetchQueryDto,
  ): Promise<TPaginationValue<IPosition>> {
    const parser: TMongoMatchParser = this.getDataFromDB(
      ['mongo', 'fetch'],
      false,
    );
    const matchQuery = Object.assign(
      {
        Category: categoryId,
        User: userId,
      },
      query.filters,
    );

    const $match = this.mongoHelper.match(parser, matchQuery);
    const $sort = this.mongoHelper.sort(query.sort);
    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      $match,
      $sort,
    );

    const result: TPaginationValue<IPosition> = await this.mongoHelper.pagination(
      this.PositionModel,
      aggregateQuery,
      query.paginate,
    );

    return result;
  }

  public async create(
    candidate: IPosition,
    file: Express.Multer.File,
  ): Promise<PositionDocument> {
    candidate.Preview = await updateFileStorage(file);
    const position: PositionDocument = await new this.PositionModel(
      candidate,
    ).save();

    await this.categoryService.updateTotalPositions(position.Category, 'add');

    return position;
  }

  public async edit(
    positionId: string,
    candidate: IPosition,
    file: Express.Multer.File,
  ): Promise<PositionDocument> {
    candidate.Preview = await updateFileStorage(file, candidate.Preview);
    candidate.Updated = new Date();

    const position: PositionDocument = await this.PositionModel.findByIdAndUpdate(
      positionId,
      { $set: candidate },
      { new: true },
    );

    return position;
  }

  public async remove(positionId: string): Promise<IPosition> {
    const position: PositionDocument = await this.PositionModel.findByIdAndRemove(
      positionId,
    );
    await this.categoryService.updateTotalPositions(
      position.Category,
      'remove',
    );

    if (position.Preview) await removeFile(position.Preview);
    return position;
  }

  public async getCostStats(
    userId: MongoId,
    categoryId: MongoId,
  ): Promise<TPositionsCostStats> {
    const $match = this.mongoHelper.stage('$match', {
      User: userId,
      Category: categoryId,
    });
    const query: object[] = this.getDataFromDB(['mongo', 'cost-stats']);
    const aggregateQuery: object[] = this.mongoHelper.completeQuery(
      $match,
      ...query,
    );
    const [result] = (await this.mongoHelper.aggregate(
      this.PositionModel,
      aggregateQuery,
    )) as [TPositionsCostStats];

    return result;
  }
}
