import { IRangeValue } from '@core/utils/ranges/interfaces/IRangeValue';
import { MongoRangeQuery } from '@module/mongo/classes/MongoRangeQuery';
import { toObjectId } from '@module/mongo/handlers/mongo.handlers';

export const PositionsDB = {
  mongo: {
    fetch: {
      User: (value: string) => ({ $eq: toObjectId(value) }),
      Category: (value: string) => ({ $eq: toObjectId(value) }),
      Title: (value: string) => ({ $regex: new RegExp(value, 'gi') }),
      Cost: (value: IRangeValue<number>) => new MongoRangeQuery(value),
    },
    'cost-stats': [
      {
        $group: {
          _id: null,
          MinCost: { $min: '$Cost' },
          MaxCost: { $max: '$Cost' },
        },
      },
      {
        $project: {
          _id: 0,
        },
      },
    ],
  },
  files: {
    'create:edit': {
      fileType: 'IMG',
      filePath: ['img', 'positions'],
    },
  },
} as const;

export type TPositionsDB = typeof PositionsDB;
