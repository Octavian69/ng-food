import { config } from '@config/handlers/config.handlers';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import * as morgan from 'morgan';
import { resolve } from 'path';
import { AppModule } from './app.module';

function applyDevelopment(app: INestApplication): void {
  app.use(morgan('dev'));
}

async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule);
  const prefix: string = config.get('API_PREFIX');
  const isDevProcess: boolean = process.env.NODE_ENV === 'development';

  if (isDevProcess) {
    applyDevelopment(app);
  }

  app.enableCors();
  app.setGlobalPrefix(prefix);
  app.use(`/${prefix}/uploads/`, express.static(resolve('uploads')));
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
