import { ApiModule } from '@module/api/api.module';
import { MongoConnectionModule } from '@module/mongo/mongo-connection.module';
import { StaticBuildModule } from '@module/static/static-build.module';
import { Module } from '@nestjs/common';
@Module({
  imports: [StaticBuildModule.forRoot(), MongoConnectionModule, ApiModule],
})
export class AppModule {}
